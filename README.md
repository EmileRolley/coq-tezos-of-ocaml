# ![](doc/static/img/rooster-tezos-64.png) Coq Tezos of OCaml 💫
> A translation & formal verification of the protocol of [Tezos](https://tezos.com/) in [Coq](https://coq.inria.fr/)

**Website 📢 https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/**

This is a translation & formal verification of the OCaml code of the [protocol of Tezos](https://gitlab.com/tezos/tezos/-/tree/master/src/proto_alpha/lib_protocol) in the proof system Coq 🐓. We use the translator [coq-of-ocaml](https://github.com/foobar-land/coq-of-ocaml) to keep our Coq translation up to date with the constantly evolving OCaml code 🐫. We verify arbitrarily complex properties thanks to the expressivity of Coq. For example, we verify [code optimizations](https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/blog/2021/08/23/optimization-by-continuation), [serialization functions](https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/blog/2021/10/20/data-encoding-usage) or parts of the [Michelson interpreter](https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/blog/2021/11/01/verify-michelson-types-mi-cho-coq). This translation is a shallow embedding in Coq. It is as readable as the source OCaml code 👓.

## Install 📦
To compile this translation as a Coq library, the simplest is to use [opam](https://github.com/coq/opam-coq-archive). Run from the command line:
```
opam repo add coq-released https://coq.inria.fr/opam/released
opam install coq-of-ocaml coq-menhirlib.20211012 coq-moment coq-hammer coq.8.13.2
./configure.sh
make -j
```
We are using Coq version 8.13.

## Content 🐓
* `src/Proto_alpha` A translation & verification of the development version of the protocol of Tezos. We keep track of the `proto_alpha` version of the protocol from the `master` branch of Tezos. To re-generate the Coq code, run the `import.rb` script from `scripts/alpha` using a version of the protocol compatible with `coq-of-ocaml`.
* `src/Proto_2021_01` A translation & verification of the development version of the protocol from January 2021. To re-generate the Coq code, run the `import.rb` script from `scripts/2021_01`. We generate the Coq code using [this branch](https://gitlab.com/clarus1/tezos/-/tree/guillaume-claret@compiling-in-coq-rebased-with-mli-arm64-no-annots) of the Tezos code, which includes some changes to help the translation to Coq.
* `src/Proto_demo` The first protocol version for which we generate a translation for all the files 🏛️. Look at the page [coq-of-ocaml examples](https://clarus.github.io/coq-of-ocaml/examples/tezos/) for more information. This corresponds to a protocol version in-between Babylon and Carthage. You can look at the changes made on the protocol to get this demo to compile on this [merge request](https://gitlab.com/clarus1/tezos/-/merge_requests/1). The scripts to translate this protocol version are on [https://github.com/foobar-land/coq-of-ocaml/tree/gh-pages/build-tezos](https://github.com/foobar-land/coq-of-ocaml/tree/gh-pages/build-tezos), using the branch `custom-tezos` of `coq-of-ocaml`.

## Website 📢
To build the website of the project:
```
COQDOCEXTRAFLAGS="--no-lib-name --parse-comments --plain-comments -s --body-only --no-externals" make html
ruby build-doc.rb
cd doc
yarn install # install the JavaScript's dependencies
yarn start # development mode, to get a live preview
yarn build # generation of the static html files, in order to next deploy
```
We use a combination of [coqdoc](https://coq.inria.fr/refman/using/tools/coqdoc.html) and [Docusaurus](https://docusaurus.io/) 🦖 bound together by the `build-doc.rb` script. We translate all the `.v` files and `.md` files in the `src/` folder to a web page. Thus we can mix documentations and proofs. This system is also useful for any Coq projects.

We deploy and host the website using the GitLab CI on [https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml](https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/). The configuration file is `gitlab-ci.yml`.

## Coding style 🎨
Here are the coding style rules we use to write proofs:
* 80 characters as maximum line width.
* space before and after colons `:` as in OCaml:
```coq
(* right ✅ *)
Lemma force_decode_cost_is_valid {a : Set} (lexpr : Data_encoding.lazy_t a)
  : Gas_limit_repr.Cost.Valid.t (Script_repr.force_decode_cost lexpr).

(* wrong ❌ *)
Lemma force_decode_cost_is_valid {a: Set} (lexpr:Data_encoding.lazy_t a)
  : Gas_limit_repr.Cost.Valid.t (Script_repr.force_decode_cost lexpr).
```
* no `Proof` keyword to reduce verbosity:
```coq
(* right ✅ *)
Lemma refl ctxt : t ctxt ctxt [].
  reflexivity.
Qed.

(* wrong ❌ *)
Lemma refl ctxt : t ctxt ctxt [].
Proof.
  reflexivity.
Qed.
```
* no multiple empty lines:
```coq
(* right ✅ *)
Definition n := 12.

Definition m := 23.

(* wrong ❌ *)
Definition n := 12.


Definition m := 23.
```

We limit our use of generated names in the proofs.

## Contact 📝
If you have remarks, want training, or contribute, you can either open an issue in this repository or send an email at [contact@nomadic-labs.com](mailto:contact@nomadic-labs.com) 😃.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Sapling_services.
Require TezosOfOCaml.Proto_2021_01.Script_expr_hash.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Services_registration.

Definition custom_root : RPC_path.context RPC_context.t :=
  RPC_path.op_div (RPC_path.op_div RPC_path.open_root "context") "contracts".

Definition big_map_root : RPC_path.context RPC_context.t :=
  RPC_path.op_div (RPC_path.op_div RPC_path.open_root "context") "big_maps".

Module info.
  Record record : Set := Build {
    balance : Alpha_context.Tez.t;
    delegate : option Alpha_context.public_key_hash;
    counter : option Alpha_context.counter;
    script : option Alpha_context.Script.t }.
  Definition with_balance balance (r : record) :=
    Build balance r.(delegate) r.(counter) r.(script).
  Definition with_delegate delegate (r : record) :=
    Build r.(balance) delegate r.(counter) r.(script).
  Definition with_counter counter (r : record) :=
    Build r.(balance) r.(delegate) counter r.(script).
  Definition with_script script (r : record) :=
    Build r.(balance) r.(delegate) r.(counter) script.
End info.
Definition info := info.record.

Definition info_encoding : Data_encoding.encoding info :=
  (let arg :=
    Data_encoding.conv
      (fun (function_parameter : info) =>
        let '{|
          info.balance := balance;
            info.delegate := delegate;
            info.counter := counter;
            info.script := script
            |} := function_parameter in
        (balance, delegate, script, counter))
      (fun (function_parameter :
        Alpha_context.Tez.t * option Alpha_context.public_key_hash *
          option Alpha_context.Script.t * option Alpha_context.counter) =>
        let '(balance, delegate, script, counter) := function_parameter in
        {| info.balance := balance; info.delegate := delegate;
          info.counter := counter; info.script := script |}) in
  fun (eta :
    Data_encoding.encoding
      (Alpha_context.Tez.t * option Alpha_context.public_key_hash *
        option Alpha_context.Script.t * option Alpha_context.counter)) =>
    arg None eta)
    (Data_encoding.obj4
      (Data_encoding.req None None "balance" Alpha_context.Tez.encoding)
      (Data_encoding.opt None None "delegate"
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
      (Data_encoding.opt None None "script" Alpha_context.Script.encoding)
      (Data_encoding.opt None None "counter" Data_encoding.n)).

Module S.
  Definition balance
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service (Some "Access the balance of a contract.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "balance").
  
  Definition manager_key
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit
      (option Signature.public_key) :=
    RPC_service.get_service (Some "Access the manager of a contract.")
      RPC_query.empty
      (Data_encoding.option_value
        Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding))
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "manager_key").
  
  Definition delegate
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit
      Signature.public_key_hash :=
    RPC_service.get_service (Some "Access the delegate of a contract, if any.")
      RPC_query.empty
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "delegate").
  
  Definition counter
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit Z.t :=
    RPC_service.get_service (Some "Access the counter of a contract, if any.")
      RPC_query.empty Data_encoding.z
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "counter").
  
  Definition script
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit
      Alpha_context.Script.t :=
    RPC_service.get_service (Some "Access the code and data of the contract.")
      RPC_query.empty Alpha_context.Script.encoding
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "script").
  
  Definition storage_value
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit
      Alpha_context.Script.expr :=
    RPC_service.get_service (Some "Access the data of the contract.")
      RPC_query.empty Alpha_context.Script.expr_encoding
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "storage").
  
  Definition entrypoint_type
    : RPC_service.service RPC_context.t
      ((RPC_context.t * Alpha_context.Contract.contract) * string) unit unit
      Alpha_context.Script.expr :=
    RPC_service.get_service
      (Some "Return the type of the given entrypoint of the contract")
      RPC_query.empty Alpha_context.Script.expr_encoding
      (RPC_path.op_divcolon
        (RPC_path.op_div
          (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
          "entrypoints") RPC_arg.string_value).
  
  Definition list_entrypoints
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit
      (list (list Michelson_v1_primitives.prim) *
        list (string * Alpha_context.Script.expr)) :=
    RPC_service.get_service
      (Some "Return the list of entrypoints of the contract") RPC_query.empty
      (Data_encoding.obj2
        (Data_encoding.dft None None "unreachable"
          (Data_encoding.list_value None
            (Data_encoding.obj1
              (Data_encoding.req None None "path"
                (Data_encoding.list_value None
                  Michelson_v1_primitives.prim_encoding)))) nil)
        (Data_encoding.req None None "entrypoints"
          (Data_encoding.assoc Alpha_context.Script.expr_encoding)))
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "entrypoints").
  
  Definition contract_big_map_get_opt
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit
      (Alpha_context.Script.expr * Alpha_context.Script.expr)
      (option Alpha_context.Script.expr) :=
    RPC_service.post_service
      (Some
        "Access the value associated with a key in a big map of the contract (deprecated).")
      RPC_query.empty
      (Data_encoding.obj2
        (Data_encoding.req None None "key" Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "type" Alpha_context.Script.expr_encoding))
      (Data_encoding.option_value Alpha_context.Script.expr_encoding)
      (RPC_path.op_div
        (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg)
        "big_map_get").
  
  Definition big_map_get
    : RPC_service.service RPC_context.t
      ((RPC_context.t *
        Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) *
        Script_expr_hash.t) unit unit Alpha_context.Script.expr :=
    RPC_service.get_service
      (Some "Access the value associated with a key in a big map.")
      RPC_query.empty Alpha_context.Script.expr_encoding
      (RPC_path.op_divcolon
        (RPC_path.op_divcolon big_map_root
          Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.rpc_arg))
        Script_expr_hash.rpc_arg).
  
  Definition info_value
    : RPC_service.service RPC_context.t
      (RPC_context.t * Alpha_context.Contract.contract) unit unit info :=
    RPC_service.get_service (Some "Access the complete status of a contract.")
      RPC_query.empty info_encoding
      (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg).
  
  Definition list_value
    : RPC_service.service RPC_context.t RPC_context.t unit unit
      (list Alpha_context.Contract.t) :=
    RPC_service.get_service
      (Some "All existing contracts (including non-empty default contracts).")
      RPC_query.empty
      (Data_encoding.list_value None Alpha_context.Contract.encoding)
      custom_root.
  
  Module Sapling.
    Definition single_sapling_get_id
      (ctxt : Alpha_context.context)
      (contract_id : Alpha_context.Contract.contract)
      : M=?
        (Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t) *
          Alpha_context.context) :=
      let=? '(ctxt, script) :=
        Alpha_context.Contract.get_script ctxt contract_id in
      match script with
      | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
      | Some script =>
        let ctxt := Alpha_context.Gas.set_unlimited ctxt in
        let= tzresult :=
          Script_ir_translator.parse_script None ctxt true true script in
        return=
          (let? '(Script_ir_translator.Ex_script script, ctxt) := tzresult in
          let 'existT _ __Ex_script_'b [ctxt, script] :=
            existT (A := Set)
              (fun __Ex_script_'b =>
                [Alpha_context.context ** Script_typed_ir.script __Ex_script_'b])
              _ [ctxt, script] in
          Script_ir_translator.get_single_sapling_state ctxt
            script.(Script_typed_ir.script.storage_type)
            script.(Script_typed_ir.script.storage))
      end.
    
    Definition make_service {A B : Set}
      (function_parameter : Sapling_services.S.Args.t A B)
      : RPC_service.service RPC_context.t
        (RPC_context.t * Alpha_context.Contract.contract) A unit B *
        (Alpha_context.context -> Alpha_context.Contract.contract -> A ->
        unit -> M=? B) :=
      let '{|
        Sapling_services.S.Args.t.name := name;
          Sapling_services.S.Args.t.description := description;
          Sapling_services.S.Args.t.query := query_value;
          Sapling_services.S.Args.t.output := output;
          Sapling_services.S.Args.t.f := f
          |} := function_parameter in
      let name := Pervasives.op_caret "single_sapling_" name in
      let path :=
        RPC_path.op_div
          (RPC_path.op_divcolon custom_root Alpha_context.Contract.rpc_arg) name
        in
      let service :=
        RPC_service.get_service (Some description) query_value output path in
      (service,
        (fun (ctxt : Alpha_context.context) =>
          fun (contract_id : Alpha_context.Contract.contract) =>
            fun (q : A) =>
              fun (function_parameter : unit) =>
                let '_ := function_parameter in
                let=? '(sapling_id, ctxt) :=
                  single_sapling_get_id ctxt contract_id in
                f ctxt sapling_id q)).
    
    Definition get_diff
      : RPC_service.service RPC_context.t
        (RPC_context.t * Alpha_context.Contract.contract)
        Sapling_services.diff_query unit
        (Alpha_context.Sapling.root * Alpha_context.Sapling.diff) *
        (Alpha_context.context -> Alpha_context.Contract.contract ->
        Sapling_services.diff_query -> unit ->
        M=? (Alpha_context.Sapling.root * Alpha_context.Sapling.diff)) :=
      make_service Sapling_services.S.Args.get_diff.
    
    Definition register (function_parameter : unit) : unit :=
      let '_ := function_parameter in
      let reg {A B C D : Set}
        (function_parameter :
          RPC_service.t Updater.rpc_context (Updater.rpc_context * A) B C D *
            (Alpha_context.t -> A -> B -> C -> M=? D)) : unit :=
        let '(service, f) := function_parameter in
        Services_registration.register1 service f in
      reg get_diff.
    
    Definition mk_call1 {A B C D E : Set}
      (function_parameter :
        RPC_service.t RPC_context.t (RPC_context.t * A) B unit C * D)
      : RPC_context.simple E -> E -> A -> B -> M= (Error_monad.shell_tzresult C) :=
      let '(service, _f) := function_parameter in
      fun (ctxt : RPC_context.simple E) =>
        fun (block : E) =>
          fun (id : A) =>
            fun (q : B) => RPC_context.make_call1 service ctxt block id q tt.
  End Sapling.
End S.

Definition register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ :=
    Services_registration.register0 S.list_value
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Error_monad.op_gtpipeeq (Alpha_context.Contract.list_value ctxt)
              Error_monad.ok) in
  let register_field {A : Set}
    (s :
      RPC_service.t Updater.rpc_context
        (Updater.rpc_context * Alpha_context.Contract.contract) unit unit A)
    (f : Alpha_context.t -> Alpha_context.Contract.contract -> M=? A) : unit :=
    Services_registration.register1 s
      (fun (ctxt : Alpha_context.t) =>
        fun (contract : Alpha_context.Contract.contract) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? function_parameter :=
                Alpha_context.Contract._exists ctxt contract in
              match function_parameter with
              | true => f ctxt contract
              | false => Pervasives.raise (Build_extensible "Not_found" unit tt)
              end) in
  let register_opt_field {A : Set}
    (s :
      RPC_service.t Updater.rpc_context
        (Updater.rpc_context * Alpha_context.Contract.contract) unit unit A)
    (f : Alpha_context.t -> Alpha_context.Contract.contract -> M=? (option A))
    : unit :=
    register_field s
      (fun (ctxt : Alpha_context.t) =>
        fun (a1 : Alpha_context.Contract.contract) =>
          let=? function_parameter := f ctxt a1 in
          match function_parameter with
          | None =>
            return=? (Pervasives.raise (Build_extensible "Not_found" unit tt))
          | Some v => return=? v
          end) in
  let do_big_map_get
    (ctxt : Alpha_context.context)
    (id : Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    (key_value : Script_expr_hash.t)
    : M=? (Micheline.canonical Alpha_context.Script.prim) :=
    let ctxt := Alpha_context.Gas.set_unlimited ctxt in
    let=? '(ctxt, types) := Alpha_context.Big_map._exists ctxt id in
    match types with
    | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
    | Some (_, value_type) =>
      let=? '(Script_ir_translator.Ex_ty value_type, ctxt) :=
        return=
          (Script_ir_translator.parse_big_map_value_ty ctxt true
            (Micheline.root value_type)) in
      let 'existT _ __Ex_ty_'a [ctxt, value_type] :=
        cast_exists (Es := Set)
          (fun __Ex_ty_'a => [Alpha_context.context ** Script_typed_ir.ty])
          [ctxt, value_type] in
      let=? '(_ctxt, value) := Alpha_context.Big_map.get_opt ctxt id key_value
        in
      match value with
      | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
      | Some value =>
        let=? '(value, ctxt) :=
          Script_ir_translator.parse_data None ctxt true true value_type
            (Micheline.root value) in
        let=? '(value, _ctxt) :=
          Script_ir_translator.unparse_data ctxt Script_ir_translator.Readable
            value_type (value : __Ex_ty_'a) in
        return=? (Micheline.strip_locations value)
      end
    end in
  let '_ := register_field S.balance Alpha_context.Contract.get_balance in
  let '_ :=
    Services_registration.register1 S.manager_key
      (fun (ctxt : Alpha_context.t) =>
        fun (contract : Alpha_context.Contract.contract) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              match Alpha_context.Contract.is_implicit contract with
              | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
              | Some mgr =>
                let=? function_parameter :=
                  Alpha_context.Contract.is_manager_key_revealed ctxt mgr in
                match function_parameter with
                | false => Error_monad.return_none
                | true =>
                  Error_monad.op_gtgteqquestion
                    (Alpha_context.Contract.get_manager_key ctxt mgr)
                    Error_monad.return_some
                end
              end) in
  let '_ := register_opt_field S.delegate Alpha_context.Delegate.get in
  let '_ :=
    Services_registration.register1 S.counter
      (fun (ctxt : Alpha_context.t) =>
        fun (contract : Alpha_context.Contract.contract) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              match Alpha_context.Contract.is_implicit contract with
              | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
              | Some mgr => Alpha_context.Contract.get_counter ctxt mgr
              end) in
  let '_ :=
    register_opt_field S.script
      (fun (c : Alpha_context.t) =>
        fun (v : Alpha_context.Contract.contract) =>
          let=? '(_, v) := Alpha_context.Contract.get_script c v in
          return=? v) in
  let '_ :=
    register_opt_field S.storage_value
      (fun (ctxt : Alpha_context.t) =>
        fun (contract : Alpha_context.Contract.contract) =>
          let=? '(ctxt, script) :=
            Alpha_context.Contract.get_script ctxt contract in
          match script with
          | None => Error_monad.return_none
          | Some script =>
            let ctxt := Alpha_context.Gas.set_unlimited ctxt in
            let=? '(Script_ir_translator.Ex_script script, ctxt) :=
              Script_ir_translator.parse_script None ctxt true true script in
            let 'existT _ __Ex_script_'b [ctxt, script] :=
              existT (A := Set)
                (fun __Ex_script_'b =>
                  [Alpha_context.context **
                    Script_typed_ir.script __Ex_script_'b]) _ [ctxt, script] in
            let=? '(script, ctxt) :=
              Script_ir_translator.unparse_script ctxt
                Script_ir_translator.Readable script in
            let=? '(storage_value, _ctxt) :=
              return=
                (Alpha_context.Script.force_decode_in_context ctxt
                  script.(Alpha_context.Script.t.storage)) in
            Error_monad.return_some storage_value
          end) in
  let '_ :=
    Services_registration.register2 S.entrypoint_type
      (fun (ctxt : Alpha_context.t) =>
        fun (v : Alpha_context.Contract.contract) =>
          fun (entrypoint : string) =>
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              fun (function_parameter : unit) =>
                let '_ := function_parameter in
                let=? '(_, expr) :=
                  Alpha_context.Contract.get_script_code ctxt v in
                match expr with
                | None =>
                  Pervasives.raise (Build_extensible "Not_found" unit tt)
                | Some expr =>
                  let ctxt := Alpha_context.Gas.set_unlimited ctxt in
                  let legacy := true in
                  Lwt._return
                    (let? '(expr, _) :=
                      Alpha_context.Script.force_decode_in_context ctxt expr in
                    (fun (function_parameter :
                      M?
                        ((Alpha_context.Script.node -> Alpha_context.Script.node)
                          * Script_ir_translator.ex_ty)) =>
                      match function_parameter with
                      | Pervasives.Ok (_f, Script_ir_translator.Ex_ty ty) =>
                        let? '(ty_node, _) :=
                          Script_ir_translator.unparse_ty ctxt ty in
                        return? (Micheline.strip_locations ty_node)
                      | Pervasives.Error _ =>
                        Pervasives.raise (Build_extensible "Not_found" unit tt)
                      end)
                      (let? '{|
                        Script_ir_translator.toplevel.arg_type := arg_type;
                          Script_ir_translator.toplevel.root_name := root_name
                          |} := Script_ir_translator.parse_toplevel legacy expr
                        in
                      let?
                        '(Script_ir_translator.Ex_parameter_ty_entrypoints
                          arg_type entrypoints, _) :=
                        Script_ir_translator.parse_parameter_ty_and_entrypoints
                          ctxt legacy root_name arg_type in
                      Script_ir_translator.find_entrypoint arg_type entrypoints
                        entrypoint))
                end) in
  let '_ :=
    Services_registration.register1 S.list_entrypoints
      (fun (ctxt : Alpha_context.t) =>
        fun (v : Alpha_context.Contract.contract) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '(_, expr) := Alpha_context.Contract.get_script_code ctxt v
                in
              match expr with
              | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
              | Some expr =>
                let ctxt := Alpha_context.Gas.set_unlimited ctxt in
                let legacy := true in
                Lwt._return
                  (let? '(expr, _) :=
                    Alpha_context.Script.force_decode_in_context ctxt expr in
                  let? '(unreachable_entrypoint, map) :=
                    let? '{|
                      Script_ir_translator.toplevel.arg_type := arg_type;
                        Script_ir_translator.toplevel.root_name := root_name
                        |} := Script_ir_translator.parse_toplevel legacy expr in
                    let?
                      '(Script_ir_translator.Ex_parameter_ty_entrypoints
                        arg_type entrypoints, _) :=
                      Script_ir_translator.parse_parameter_ty_and_entrypoints
                        ctxt legacy root_name arg_type in
                    Script_ir_translator.list_entrypoints ctxt arg_type
                      entrypoints in
                  return?
                    (unreachable_entrypoint,
                      (Script_ir_translator.Entrypoints_map.(S.MAP.fold)
                        (fun (entry : string) =>
                          fun (function_parameter :
                            list Michelson_v1_primitives.prim *
                              Micheline.node Alpha_context.Script.location
                                Alpha_context.Script.prim) =>
                            let '(_, ty) := function_parameter in
                            fun (acc_value :
                              list (string * Alpha_context.Script.expr)) =>
                              cons (entry, (Micheline.strip_locations ty))
                                acc_value) map nil)))
              end) in
  let '_ :=
    Services_registration.register1 S.contract_big_map_get_opt
      (fun (ctxt : Alpha_context.t) =>
        fun (contract : Alpha_context.Contract.contract) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter :
              Alpha_context.Script.expr * Alpha_context.Script.expr) =>
              let '(key_value, key_type) := function_parameter in
              let=? '(ctxt, script) :=
                Alpha_context.Contract.get_script ctxt contract in
              let=? '(Script_ir_translator.Ex_comparable_ty key_type, ctxt) :=
                return=
                  (Script_ir_translator.parse_comparable_ty ctxt
                    (Micheline.root key_type)) in
              let 'existT _ __Ex_comparable_ty_'a [ctxt, key_type] :=
                cast_exists (Es := Set)
                  (fun __Ex_comparable_ty_'a =>
                    [Alpha_context.context ** Script_typed_ir.comparable_ty])
                  [ctxt, key_type] in
              let=? '(key_value, ctxt) :=
                Script_ir_translator.parse_comparable_data None ctxt key_type
                  (Micheline.root key_value)
                  (fun (function_parameter : __Ex_comparable_ty_'a) =>
                    let '_ := function_parameter in
                    tt) in
              let=? '(key_value, ctxt) :=
                Script_ir_translator.hash_comparable_data ctxt key_type
                  key_value in
              match script with
              | None => Pervasives.raise (Build_extensible "Not_found" unit tt)
              | Some script =>
                let ctxt := Alpha_context.Gas.set_unlimited ctxt in
                let=? '(Script_ir_translator.Ex_script script, ctxt) :=
                  Script_ir_translator.parse_script None ctxt true true script
                  in
                let 'existT _ __Ex_script_'b1 [ctxt, script] :=
                  existT (A := Set)
                    (fun __Ex_script_'b1 =>
                      [Alpha_context.context **
                        Script_typed_ir.script __Ex_script_'b1]) _
                    [ctxt, script] in
                let=? '(ids, _ctxt) :=
                  return=
                    (Script_ir_translator.collect_lazy_storage ctxt
                      script.(Script_typed_ir.script.storage_type)
                      script.(Script_typed_ir.script.storage)) in
                match Script_ir_translator.list_of_big_map_ids ids with
                | ([] | cons _ (cons _ _)) => Error_monad.return_none
                | cons id [] =>
                  (* ❌ Try-with are not handled *)
                  try_with
                    (fun _ =>
                      Error_monad.op_gtgteqquestion
                        (do_big_map_get ctxt id key_value)
                        Error_monad.return_some)
                    (fun exn_value =>
                      match exn_value with
                      | Build_extensible tag _ payload =>
                        if String.eqb tag "Not_found" then
                          Error_monad.return_none
                        else Pervasives.raise exn_value
                      end)
                end
              end) in
  let '_ :=
    Services_registration.register2 S.big_map_get
      (fun (ctxt : Alpha_context.t) =>
        fun (id : Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) =>
          fun (key_value : Script_expr_hash.t) =>
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              fun (function_parameter : unit) =>
                let '_ := function_parameter in
                do_big_map_get ctxt id key_value) in
  let '_ :=
    register_field S.info_value
      (fun (ctxt : Alpha_context.t) =>
        fun (contract : Alpha_context.Contract.contract) =>
          let=? balance := Alpha_context.Contract.get_balance ctxt contract in
          let=? delegate := Alpha_context.Delegate.get ctxt contract in
          let=? counter :=
            match Alpha_context.Contract.is_implicit contract with
            | Some manager =>
              let=? counter := Alpha_context.Contract.get_counter ctxt manager
                in
              Error_monad.return_some counter
            | None => Error_monad.return_none
            end in
          let=? '(ctxt, script) :=
            Alpha_context.Contract.get_script ctxt contract in
          let=? '(script, _ctxt) :=
            match script with
            | None => Error_monad._return (None, ctxt)
            | Some script =>
              let ctxt := Alpha_context.Gas.set_unlimited ctxt in
              let=? '(Script_ir_translator.Ex_script script, ctxt) :=
                Script_ir_translator.parse_script None ctxt true true script in
              let 'existT _ __Ex_script_'b2 [ctxt, script] :=
                existT (A := Set)
                  (fun __Ex_script_'b2 =>
                    [Alpha_context.context **
                      Script_typed_ir.script __Ex_script_'b2]) _ [ctxt, script]
                in
              let=? '(script, ctxt) :=
                Script_ir_translator.unparse_script ctxt
                  Script_ir_translator.Readable script in
              return=? ((Some script), ctxt)
            end in
          return=?
            {| info.balance := balance; info.delegate := delegate;
              info.counter := counter; info.script := script |}) in
  S.Sapling.register tt.

Definition list_value {A : Set} (ctxt : RPC_context.simple A) (block : A)
  : M= (Error_monad.shell_tzresult (list Alpha_context.Contract.t)) :=
  RPC_context.make_call0 S.list_value ctxt block tt tt.

Definition info_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult info) :=
  RPC_context.make_call1 S.info_value ctxt block contract tt tt.

Definition balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.balance ctxt block contract tt tt.

Definition manager_key {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (mgr : Alpha_context.public_key_hash)
  : M= (Error_monad.shell_tzresult (option Signature.public_key)) :=
  RPC_context.make_call1 S.manager_key ctxt block
    (Alpha_context.Contract.implicit_contract mgr) tt tt.

Definition delegate {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult Signature.public_key_hash) :=
  RPC_context.make_call1 S.delegate ctxt block contract tt tt.

Definition delegate_opt {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult (option Signature.public_key_hash)) :=
  RPC_context.make_opt_call1 S.delegate ctxt block contract tt tt.

Definition counter {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (mgr : Alpha_context.public_key_hash) : M= (Error_monad.shell_tzresult Z.t) :=
  RPC_context.make_call1 S.counter ctxt block
    (Alpha_context.Contract.implicit_contract mgr) tt tt.

Definition script {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult Alpha_context.Script.t) :=
  RPC_context.make_call1 S.script ctxt block contract tt tt.

Definition script_opt {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult (option Alpha_context.Script.t)) :=
  RPC_context.make_opt_call1 S.script ctxt block contract tt tt.

Definition storage_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult Alpha_context.Script.expr) :=
  RPC_context.make_call1 S.storage_value ctxt block contract tt tt.

Definition entrypoint_type {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract) (entrypoint : string)
  : M= (Error_monad.shell_tzresult Alpha_context.Script.expr) :=
  RPC_context.make_call2 S.entrypoint_type ctxt block contract entrypoint tt tt.

Definition list_entrypoints {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M=
    (Error_monad.shell_tzresult
      (list (list Michelson_v1_primitives.prim) *
        list (string * Alpha_context.Script.expr))) :=
  RPC_context.make_call1 S.list_entrypoints ctxt block contract tt tt.

Definition storage_opt {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  : M= (Error_monad.shell_tzresult (option Alpha_context.Script.expr)) :=
  RPC_context.make_opt_call1 S.storage_value ctxt block contract tt tt.

Definition big_map_get {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (id : Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
  (key_value : Script_expr_hash.t)
  : M= (Error_monad.shell_tzresult Alpha_context.Script.expr) :=
  RPC_context.make_call2 S.big_map_get ctxt block id key_value tt tt.

Definition contract_big_map_get_opt {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (contract : Alpha_context.Contract.contract)
  (key_value : Alpha_context.Script.expr * Alpha_context.Script.expr)
  : M= (Error_monad.shell_tzresult (option Alpha_context.Script.expr)) :=
  RPC_context.make_call1 S.contract_big_map_get_opt ctxt block contract tt
    key_value.

Definition single_sapling_get_diff {A : Set}
  (ctxt : RPC_context.simple A) (block : A)
  (id : Alpha_context.Contract.contract) (offset_commitment : option Int64.t)
  (offset_nullifier : option Int64.t) (function_parameter : unit)
  : M=
    (Error_monad.shell_tzresult
      (Alpha_context.Sapling.root * Alpha_context.Sapling.diff)) :=
  let '_ := function_parameter in
  S.Sapling.mk_call1 S.Sapling.get_diff ctxt block id
    {| Sapling_services.diff_query.offset_commitment := offset_commitment;
      Sapling_services.diff_query.offset_nullifier := offset_nullifier |}.

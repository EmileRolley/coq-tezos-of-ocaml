Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Storage_description.

Definition H :=
  Blake2B.Make
    {|
      Blake2B.Register.register_encoding _ := Base58.register_encoding
    |}
    (let name := "Blinded public key hash" in
    let title := "A blinded public key hash" in
    let b58check_prefix := "\001\0021\223" in
    let size := Some Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size)
      in
    {|
      Blake2B.PrefixedName.name := name;
      Blake2B.PrefixedName.title := title;
      Blake2B.PrefixedName.size := size;
      Blake2B.PrefixedName.b58check_prefix := b58check_prefix
    |}).

(** Inclusion of the module [H] *)
Definition t := H.(S.HASH.t).

Definition name := H.(S.HASH.name).

Definition title := H.(S.HASH.title).

Definition pp := H.(S.HASH.pp).

Definition pp_short := H.(S.HASH.pp_short).

Definition op_eq := H.(S.HASH.op_eq).

Definition op_ltgt := H.(S.HASH.op_ltgt).

Definition op_lt := H.(S.HASH.op_lt).

Definition op_lteq := H.(S.HASH.op_lteq).

Definition op_gteq := H.(S.HASH.op_gteq).

Definition op_gt := H.(S.HASH.op_gt).

Definition compare := H.(S.HASH.compare).

Definition equal := H.(S.HASH.equal).

Definition max := H.(S.HASH.max).

Definition min := H.(S.HASH.min).

Definition hash_bytes := H.(S.HASH.hash_bytes).

Definition hash_string := H.(S.HASH.hash_string).

Definition zero := H.(S.HASH.zero).

Definition size := H.(S.HASH.size).

Definition to_bytes := H.(S.HASH.to_bytes).

Definition of_bytes_opt := H.(S.HASH.of_bytes_opt).

Definition of_bytes_exn := H.(S.HASH.of_bytes_exn).

Definition to_b58check := H.(S.HASH.to_b58check).

Definition to_short_b58check := H.(S.HASH.to_short_b58check).

Definition of_b58check_exn := H.(S.HASH.of_b58check_exn).

Definition of_b58check_opt := H.(S.HASH.of_b58check_opt).

Definition b58check_encoding := H.(S.HASH.b58check_encoding).

Definition encoding := H.(S.HASH.encoding).

Definition rpc_arg := H.(S.HASH.rpc_arg).

Definition to_path := H.(S.HASH.to_path).

Definition of_path := H.(S.HASH.of_path).

Definition of_path_exn := H.(S.HASH.of_path_exn).

Definition prefix_path := H.(S.HASH.prefix_path).

Definition path_length := H.(S.HASH.path_length).

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  Base58.check_encoded_prefix b58check_encoding "btz1" 37.

Definition of_ed25519_pkh
  (activation_code : bytes)
  (pkh : Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t)) : t :=
  hash_bytes (Some activation_code)
    [ Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_bytes) pkh ].

Definition activation_code : Set := bytes.

Definition activation_code_size : int :=
  Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size).

Definition activation_code_encoding : Data_encoding.encoding bytes :=
  Data_encoding.Fixed.bytes_value activation_code_size.

Definition activation_code_of_hex (h : string) : bytes :=
  let '_ :=
    if (String.length h) <>i (activation_code_size *i 2) then
      Pervasives.invalid_arg "Blinded_public_key_hash.activation_code_of_hex"
    else
      tt in
  Hex.to_bytes (Hex.Hex h).

Definition Index : Storage_description.INDEX (t := t) :=
  {|
    Storage_description.INDEX.path_length := H.(S.HASH.path_length);
    Storage_description.INDEX.to_path := H.(S.HASH.to_path);
    Storage_description.INDEX.of_path := H.(S.HASH.of_path);
    Storage_description.INDEX.rpc_arg := H.(S.HASH.rpc_arg);
    Storage_description.INDEX.encoding := H.(S.HASH.encoding);
    Storage_description.INDEX.compare := H.(S.HASH.compare)
  |}.

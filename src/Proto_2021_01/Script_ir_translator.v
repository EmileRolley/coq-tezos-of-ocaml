Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_gas.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Script_expr_hash.
Require TezosOfOCaml.Proto_2021_01.Script_ir_annot.
Require TezosOfOCaml.Proto_2021_01.Script_repr.
Require TezosOfOCaml.Proto_2021_01.Script_tc_errors.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Module Typecheck_costs := Michelson_v1_gas.Cost_of.Typechecking.

Module Unparse_costs := Michelson_v1_gas.Cost_of.Unparsing.

Inductive ex_comparable_ty : Set :=
| Ex_comparable_ty : Script_typed_ir.comparable_ty -> ex_comparable_ty.

Inductive ex_ty : Set :=
| Ex_ty : Script_typed_ir.ty -> ex_ty.

Inductive ex_parameter_ty_and_entrypoints : Set :=
| Ex_parameter_ty_entrypoints :
  Script_typed_ir.ty -> Script_typed_ir.entrypoints ->
  ex_parameter_ty_and_entrypoints.

Inductive ex_stack_ty : Set :=
| Ex_stack_ty : Script_typed_ir.stack_ty -> ex_stack_ty.

(** Records for the constructor parameters *)
Module ConstructorRecords_tc_context.
  Module tc_context.
    Module Toplevel.
      Record record {storage_type param_type entrypoints : Set} : Set := Build {
        storage_type : storage_type;
        param_type : param_type;
        entrypoints : entrypoints }.
      Arguments record : clear implicits.
      Definition with_storage_type {t_storage_type t_param_type t_entrypoints}
        storage_type (r : record t_storage_type t_param_type t_entrypoints) :=
        Build t_storage_type t_param_type t_entrypoints storage_type
          r.(param_type) r.(entrypoints).
      Definition with_param_type {t_storage_type t_param_type t_entrypoints}
        param_type (r : record t_storage_type t_param_type t_entrypoints) :=
        Build t_storage_type t_param_type t_entrypoints r.(storage_type)
          param_type r.(entrypoints).
      Definition with_entrypoints {t_storage_type t_param_type t_entrypoints}
        entrypoints (r : record t_storage_type t_param_type t_entrypoints) :=
        Build t_storage_type t_param_type t_entrypoints r.(storage_type)
          r.(param_type) entrypoints.
    End Toplevel.
    Definition Toplevel_skeleton := Toplevel.record.
  End tc_context.
End ConstructorRecords_tc_context.
Import ConstructorRecords_tc_context.

Reserved Notation "'tc_context.Toplevel".

Inductive tc_context : Set :=
| Lambda : tc_context
| Dip : Script_typed_ir.stack_ty -> tc_context -> tc_context
| Toplevel : 'tc_context.Toplevel -> tc_context

where "'tc_context.Toplevel" :=
  (tc_context.Toplevel_skeleton Script_typed_ir.ty Script_typed_ir.ty
    Script_typed_ir.entrypoints).

Module tc_context.
  Include ConstructorRecords_tc_context.tc_context.
  Definition Toplevel := 'tc_context.Toplevel.
End tc_context.

Inductive unparsing_mode : Set :=
| Optimized : unparsing_mode
| Readable : unparsing_mode
| Optimized_legacy : unparsing_mode.

Definition type_logger : Set :=
  int -> list (Alpha_context.Script.expr * Alpha_context.Script.annot) ->
  list (Alpha_context.Script.expr * Alpha_context.Script.annot) -> unit.

Definition add_dip (ty : Script_typed_ir.ty) (prev : tc_context) : tc_context :=
  match prev with
  | (Lambda | Toplevel _) =>
    Dip (Script_typed_ir.Item_t ty Script_typed_ir.Empty_t) prev
  | Dip stack_value _ => Dip (Script_typed_ir.Item_t ty stack_value) prev
  end.

Fixpoint comparable_type_size (ty : Script_typed_ir.comparable_ty) : int :=
  match ty with
  | Script_typed_ir.Unit_key => 1
  | Script_typed_ir.Never_key => 1
  | Script_typed_ir.Int_key => 1
  | Script_typed_ir.Nat_key => 1
  | Script_typed_ir.Signature_key => 1
  | Script_typed_ir.String_key => 1
  | Script_typed_ir.Bytes_key => 1
  | Script_typed_ir.Mutez_key => 1
  | Script_typed_ir.Bool_key => 1
  | Script_typed_ir.Key_hash_key => 1
  | Script_typed_ir.Key_key => 1
  | Script_typed_ir.Timestamp_key => 1
  | Script_typed_ir.Chain_id_key => 1
  | Script_typed_ir.Address_key => 1
  | Script_typed_ir.Pair_key t1 t2 =>
    (1 +i (comparable_type_size t1)) +i (comparable_type_size t2)
  | Script_typed_ir.Union_key t1 t2 =>
    (1 +i (comparable_type_size t1)) +i (comparable_type_size t2)
  | Script_typed_ir.Option_key t_value => 1 +i (comparable_type_size t_value)
  end.

Fixpoint type_size (ty : Script_typed_ir.ty) : int :=
  match ty with
  | Script_typed_ir.Unit_t => 1
  | Script_typed_ir.Int_t => 1
  | Script_typed_ir.Nat_t => 1
  | Script_typed_ir.Signature_t => 1
  | Script_typed_ir.Bytes_t => 1
  | Script_typed_ir.String_t => 1
  | Script_typed_ir.Mutez_t => 1
  | Script_typed_ir.Key_hash_t => 1
  | Script_typed_ir.Key_t => 1
  | Script_typed_ir.Timestamp_t => 1
  | Script_typed_ir.Address_t => 1
  | Script_typed_ir.Bool_t => 1
  | Script_typed_ir.Operation_t => 1
  | Script_typed_ir.Chain_id_t => 1
  | Script_typed_ir.Never_t => 1
  | Script_typed_ir.Bls12_381_g1_t => 1
  | Script_typed_ir.Bls12_381_g2_t => 1
  | Script_typed_ir.Bls12_381_fr_t => 1
  | Script_typed_ir.Sapling_transaction_t _ => 1
  | Script_typed_ir.Sapling_state_t _ => 1
  | Script_typed_ir.Pair_t l_value r_value =>
    (1 +i (type_size l_value)) +i (type_size r_value)
  | Script_typed_ir.Union_t l_value r_value =>
    (1 +i (type_size l_value)) +i (type_size r_value)
  | Script_typed_ir.Lambda_t arg ret =>
    (1 +i (type_size arg)) +i (type_size ret)
  | Script_typed_ir.Option_t t_value => 1 +i (type_size t_value)
  | Script_typed_ir.List_t t_value => 1 +i (type_size t_value)
  | Script_typed_ir.Ticket_t t_value => 1 +i (comparable_type_size t_value)
  | Script_typed_ir.Set_t k => 1 +i (comparable_type_size k)
  | Script_typed_ir.Map_t k v =>
    (1 +i (comparable_type_size k)) +i (type_size v)
  | Script_typed_ir.Big_map_t k v =>
    (1 +i (comparable_type_size k)) +i (type_size v)
  | Script_typed_ir.Contract_t arg => 1 +i (type_size arg)
  end.

Fixpoint type_size_of_stack_head
  (stack_value : Script_typed_ir.stack_ty) (up_to : int) : int :=
  match stack_value with
  | Script_typed_ir.Empty_t => 0
  | Script_typed_ir.Item_t head tail =>
    if up_to >i 0 then
      Compare.Int.(Compare.S.max) (type_size head)
        (type_size_of_stack_head tail (up_to -i 1))
    else
      0
  end.

Definition number_of_generated_growing_types
  (function_parameter : Script_typed_ir.instr) : int :=
  match function_parameter with
  | Script_typed_ir.Const _ => 1
  | Script_typed_ir.Cons_pair => 1
  | Script_typed_ir.Cons_some => 1
  | Script_typed_ir.Cons_none _ => 1
  | Script_typed_ir.Cons_left => 1
  | Script_typed_ir.Cons_right => 1
  | Script_typed_ir.Nil => 1
  | Script_typed_ir.Empty_set _ => 1
  | Script_typed_ir.Empty_map _ _ => 1
  | Script_typed_ir.Empty_big_map _ _ => 1
  | Script_typed_ir.Lambda _ => 1
  | Script_typed_ir.Self _ _ => 1
  | Script_typed_ir.Contract _ _ => 1
  | Script_typed_ir.Ticket => 1
  | Script_typed_ir.Read_ticket => 1
  | Script_typed_ir.Split_ticket => 1
  | Script_typed_ir.Unpack _ => 1
  | Script_typed_ir.List_map _ => 1
  | Script_typed_ir.Map_map _ => 1
  | Script_typed_ir.Drop => 0
  | Script_typed_ir.Dup => 0
  | Script_typed_ir.Swap => 0
  | Script_typed_ir.Unpair => 0
  | Script_typed_ir.Car => 0
  | Script_typed_ir.Cdr => 0
  | Script_typed_ir.If_none _ _ => 0
  | Script_typed_ir.If_left _ _ => 0
  | Script_typed_ir.Cons_list => 0
  | Script_typed_ir.If_cons _ _ => 0
  | Script_typed_ir.List_size => 0
  | Script_typed_ir.List_iter _ => 0
  | Script_typed_ir.Set_iter _ => 0
  | Script_typed_ir.Set_mem => 0
  | Script_typed_ir.Set_update => 0
  | Script_typed_ir.Set_size => 0
  | Script_typed_ir.Map_iter _ => 0
  | Script_typed_ir.Map_mem => 0
  | Script_typed_ir.Map_get => 0
  | Script_typed_ir.Map_update => 0
  | Script_typed_ir.Map_get_and_update => 0
  | Script_typed_ir.Map_size => 0
  | Script_typed_ir.Big_map_get => 0
  | Script_typed_ir.Big_map_update => 0
  | Script_typed_ir.Big_map_get_and_update => 0
  | Script_typed_ir.Big_map_mem => 0
  | Script_typed_ir.Concat_string => 0
  | Script_typed_ir.Concat_string_pair => 0
  | Script_typed_ir.Slice_string => 0
  | Script_typed_ir.String_size => 0
  | Script_typed_ir.Concat_bytes => 0
  | Script_typed_ir.Concat_bytes_pair => 0
  | Script_typed_ir.Slice_bytes => 0
  | Script_typed_ir.Bytes_size => 0
  | Script_typed_ir.Add_seconds_to_timestamp => 0
  | Script_typed_ir.Add_timestamp_to_seconds => 0
  | Script_typed_ir.Sub_timestamp_seconds => 0
  | Script_typed_ir.Diff_timestamps => 0
  | Script_typed_ir.Add_tez => 0
  | Script_typed_ir.Sub_tez => 0
  | Script_typed_ir.Mul_teznat => 0
  | Script_typed_ir.Mul_nattez => 0
  | Script_typed_ir.Ediv_teznat => 0
  | Script_typed_ir.Ediv_tez => 0
  | Script_typed_ir.Or => 0
  | Script_typed_ir.And => 0
  | Script_typed_ir.Xor => 0
  | Script_typed_ir.Not => 0
  | Script_typed_ir.Is_nat => 0
  | Script_typed_ir.Neg_nat => 0
  | Script_typed_ir.Neg_int => 0
  | Script_typed_ir.Abs_int => 0
  | Script_typed_ir.Int_nat => 0
  | Script_typed_ir.Add_intint => 0
  | Script_typed_ir.Add_intnat => 0
  | Script_typed_ir.Add_natint => 0
  | Script_typed_ir.Add_natnat => 0
  | Script_typed_ir.Sub_int => 0
  | Script_typed_ir.Mul_intint => 0
  | Script_typed_ir.Mul_intnat => 0
  | Script_typed_ir.Mul_natint => 0
  | Script_typed_ir.Mul_natnat => 0
  | Script_typed_ir.Ediv_intint => 0
  | Script_typed_ir.Ediv_intnat => 0
  | Script_typed_ir.Ediv_natint => 0
  | Script_typed_ir.Ediv_natnat => 0
  | Script_typed_ir.Lsl_nat => 0
  | Script_typed_ir.Lsr_nat => 0
  | Script_typed_ir.Or_nat => 0
  | Script_typed_ir.And_nat => 0
  | Script_typed_ir.And_int_nat => 0
  | Script_typed_ir.Xor_nat => 0
  | Script_typed_ir.Not_nat => 0
  | Script_typed_ir.Not_int => 0
  | Script_typed_ir.Seq _ _ => 0
  | Script_typed_ir.If _ _ => 0
  | Script_typed_ir.Loop _ => 0
  | Script_typed_ir.Loop_left _ => 0
  | Script_typed_ir.Dip _ => 0
  | Script_typed_ir.Exec => 0
  | Script_typed_ir.Apply _ => 0
  | Script_typed_ir.Failwith _ => 0
  | Script_typed_ir.Nop => 0
  | Script_typed_ir.Compare _ => 0
  | Script_typed_ir.Eq => 0
  | Script_typed_ir.Neq => 0
  | Script_typed_ir.Lt => 0
  | Script_typed_ir.Gt => 0
  | Script_typed_ir.Le => 0
  | Script_typed_ir.Ge => 0
  | Script_typed_ir.Address => 0
  | Script_typed_ir.Transfer_tokens => 0
  | Script_typed_ir.Implicit_account => 0
  | Script_typed_ir.Create_contract _ _ _ _ => 0
  | Script_typed_ir.Now => 0
  | Script_typed_ir.Level => 0
  | Script_typed_ir.Balance => 0
  | Script_typed_ir.Check_signature => 0
  | Script_typed_ir.Hash_key => 0
  | Script_typed_ir.Blake2b => 0
  | Script_typed_ir.Sha256 => 0
  | Script_typed_ir.Sha512 => 0
  | Script_typed_ir.Source => 0
  | Script_typed_ir.Sender => 0
  | Script_typed_ir.Amount => 0
  | Script_typed_ir.Self_address => 0
  | Script_typed_ir.Sapling_empty_state _ => 0
  | Script_typed_ir.Sapling_verify_update => 0
  | Script_typed_ir.Set_delegate => 0
  | Script_typed_ir.Pack _ => 0
  | Script_typed_ir.Dig _ _ => 0
  | Script_typed_ir.Dug _ _ => 0
  | Script_typed_ir.Dipn _ _ _ => 0
  | Script_typed_ir.Dropn _ _ => 0
  | Script_typed_ir.ChainId => 0
  | Script_typed_ir.Never => 0
  | Script_typed_ir.Voting_power => 0
  | Script_typed_ir.Total_voting_power => 0
  | Script_typed_ir.Keccak => 0
  | Script_typed_ir.Sha3 => 0
  | Script_typed_ir.Add_bls12_381_g1 => 0
  | Script_typed_ir.Add_bls12_381_g2 => 0
  | Script_typed_ir.Add_bls12_381_fr => 0
  | Script_typed_ir.Mul_bls12_381_g1 => 0
  | Script_typed_ir.Mul_bls12_381_g2 => 0
  | Script_typed_ir.Mul_bls12_381_fr => 0
  | Script_typed_ir.Mul_bls12_381_fr_z => 0
  | Script_typed_ir.Mul_bls12_381_z_fr => 0
  | Script_typed_ir.Int_bls12_381_fr => 0
  | Script_typed_ir.Neg_bls12_381_g1 => 0
  | Script_typed_ir.Neg_bls12_381_g2 => 0
  | Script_typed_ir.Neg_bls12_381_fr => 0
  | Script_typed_ir.Pairing_check_bls12_381 => 0
  | Script_typed_ir.Uncomb _ _ => 0
  | Script_typed_ir.Comb_get _ _ => 0
  | Script_typed_ir.Comb _ _ => 1
  | Script_typed_ir.Comb_set _ _ => 1
  | Script_typed_ir.Dup_n _ _ => 0
  | Script_typed_ir.Join_tickets _ => 0
  end.

Definition location {A B : Set} (function_parameter : Micheline.node A B) : A :=
  match function_parameter with
  |
    (Micheline.Prim loc _ _ _ | Micheline.Int loc _ | Micheline.String loc _ |
    Micheline.Bytes loc _ | Micheline.Seq loc _) => loc
  end.

Definition kind_equal
  (a_value : Script_tc_errors.kind) (b_value : Script_tc_errors.kind) : bool :=
  match (a_value, b_value) with
  |
    ((Script_tc_errors.Int_kind, Script_tc_errors.Int_kind) |
    (Script_tc_errors.String_kind, Script_tc_errors.String_kind) |
    (Script_tc_errors.Bytes_kind, Script_tc_errors.Bytes_kind) |
    (Script_tc_errors.Prim_kind, Script_tc_errors.Prim_kind) |
    (Script_tc_errors.Seq_kind, Script_tc_errors.Seq_kind)) => true
  | _ => false
  end.

Definition kind_value {A B : Set} (function_parameter : Micheline.node A B)
  : Script_tc_errors.kind :=
  match function_parameter with
  | Micheline.Int _ _ => Script_tc_errors.Int_kind
  | Micheline.String _ _ => Script_tc_errors.String_kind
  | Micheline.Bytes _ _ => Script_tc_errors.Bytes_kind
  | Micheline.Prim _ _ _ _ => Script_tc_errors.Prim_kind
  | Micheline.Seq _ _ => Script_tc_errors.Seq_kind
  end.

Definition unexpected
  (expr :
    Micheline.node Alpha_context.Script.location Michelson_v1_primitives.prim)
  (exp_kinds : list Script_tc_errors.kind)
  (exp_ns : Michelson_v1_primitives.namespace)
  (exp_prims : list Alpha_context.Script.prim) : Error_monad._error :=
  match expr with
  | Micheline.Int loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.Int_kind)
  | Micheline.String loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.String_kind)
  | Micheline.Bytes loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.Bytes_kind)
  | Micheline.Seq loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.Seq_kind)
  | Micheline.Prim loc name _ _ =>
    match ((Michelson_v1_primitives.namespace_value name), exp_ns) with
    |
      ((Michelson_v1_primitives.Type_namespace,
        Michelson_v1_primitives.Type_namespace) |
      (Michelson_v1_primitives.Instr_namespace,
        Michelson_v1_primitives.Instr_namespace) |
      (Michelson_v1_primitives.Constant_namespace,
        Michelson_v1_primitives.Constant_namespace)) =>
      Build_extensible "Invalid_primitive"
        (Alpha_context.Script.location * list Alpha_context.Script.prim *
          Michelson_v1_primitives.prim) (loc, exp_prims, name)
    | (ns, _) =>
      Build_extensible "Invalid_namespace"
        (Alpha_context.Script.location * Michelson_v1_primitives.prim *
          Michelson_v1_primitives.namespace * Michelson_v1_primitives.namespace)
        (loc, name, exp_ns, ns)
    end
  end.

Definition check_kind {A : Set}
  (kinds : list Script_tc_errors.kind)
  (expr : Micheline.node Alpha_context.Script.location A) : M? unit :=
  let kind_value := kind_value expr in
  if List._exists (kind_equal kind_value) kinds then
    Error_monad.ok_unit
  else
    let loc := location expr in
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind) (loc, kinds, kind_value)).

Definition list_empty {a : Set} : Script_typed_ir.boxed_list a :=
  {| Script_typed_ir.boxed_list.elements := nil;
    Script_typed_ir.boxed_list.length := 0 |}.

Definition list_cons {a : Set}
  (elt_value : a) (l_value : Script_typed_ir.boxed_list a)
  : Script_typed_ir.boxed_list a :=
  {|
    Script_typed_ir.boxed_list.elements :=
      cons elt_value l_value.(Script_typed_ir.boxed_list.elements);
    Script_typed_ir.boxed_list.length :=
      1 +i l_value.(Script_typed_ir.boxed_list.length) |}.

Definition wrap_compare {A B : Set}
  (compare : A -> B -> int) (a_value : A) (b_value : B) : int :=
  let res := compare a_value b_value in
  if res =i 0 then
    0
  else
    if res >i 0 then
      1
    else
      (-1).

Definition compare_address
  (function_parameter : Alpha_context.Contract.t * string)
  : Alpha_context.Contract.t * string -> int :=
  let '(x, ex) := function_parameter in
  fun (function_parameter : Alpha_context.Contract.t * string) =>
    let '(y, ey) := function_parameter in
    let lres := Alpha_context.Contract.compare x y in
    if lres =i 0 then
      Compare.String.(Compare.S.compare) ex ey
    else
      lres.

Fixpoint compare_comparable {a : Set}
  (kind_value : Script_typed_ir.comparable_ty) (x : a) (y : a)
  {struct kind_value} : int :=
  match (kind_value, x, y) with
  | (Script_typed_ir.Unit_key, _, _) => 0
  
  | (Script_typed_ir.Signature_key, x, y) =>
    let '[y, x] :=
      cast [Alpha_context.signature ** Alpha_context.signature] [y, x] in
    wrap_compare Signature.compare x y
  
  | (Script_typed_ir.String_key, x, y) =>
    let '[y, x] := cast [string ** string] [y, x] in
    wrap_compare Compare.String.(Compare.S.compare) x y
  
  | (Script_typed_ir.Bool_key, x, y) =>
    let '[y, x] := cast [bool ** bool] [y, x] in
    wrap_compare Compare.Bool.(Compare.S.compare) x y
  
  | (Script_typed_ir.Mutez_key, x, y) =>
    let '[y, x] := cast [Tez_repr.t ** Tez_repr.t] [y, x] in
    wrap_compare Alpha_context.Tez.compare x y
  
  | (Script_typed_ir.Key_hash_key, x, y) =>
    let '[y, x] :=
      cast [Alpha_context.public_key_hash ** Alpha_context.public_key_hash]
        [y, x] in
    wrap_compare Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
      x y
  
  | (Script_typed_ir.Key_key, x, y) =>
    let '[y, x] :=
      cast [Alpha_context.public_key ** Alpha_context.public_key] [y, x] in
    wrap_compare Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.compare) x y
  
  | (Script_typed_ir.Int_key, x, y) =>
    let '[y, x] :=
      cast [Alpha_context.Script_int.num ** Alpha_context.Script_int.num] [y, x]
      in
    wrap_compare Alpha_context.Script_int.compare x y
  
  | (Script_typed_ir.Nat_key, x, y) =>
    let '[y, x] :=
      cast [Alpha_context.Script_int.num ** Alpha_context.Script_int.num] [y, x]
      in
    wrap_compare Alpha_context.Script_int.compare x y
  
  | (Script_typed_ir.Timestamp_key, x, y) =>
    let '[y, x] :=
      cast
        [Alpha_context.Script_timestamp.t ** Alpha_context.Script_timestamp.t]
        [y, x] in
    wrap_compare Alpha_context.Script_timestamp.compare x y
  
  | (Script_typed_ir.Address_key, x, y) =>
    let '[y, x] :=
      cast [Script_typed_ir.address ** Script_typed_ir.address] [y, x] in
    wrap_compare compare_address x y
  
  | (Script_typed_ir.Bytes_key, x, y) =>
    let '[y, x] := cast [bytes ** bytes] [y, x] in
    wrap_compare Compare.Bytes.(Compare.S.compare) x y
  
  | (Script_typed_ir.Chain_id_key, x, y) =>
    let '[y, x] := cast [Chain_id.t ** Chain_id.t] [y, x] in
    wrap_compare Chain_id.compare x y
  
  | (Script_typed_ir.Pair_key tl tr, x, y) =>
    let 'existT _ [__0, __1] [y, x, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__0, __1] =>
          [__0 * __1 ** __0 * __1 ** Script_typed_ir.comparable_ty **
            Script_typed_ir.comparable_ty]) [y, x, tr, tl] in
    let '(lx, rx) := x in
    let '(ly, ry) := y in
    let lres := compare_comparable tl lx ly in
    if lres =i 0 then
      compare_comparable tr rx ry
    else
      lres
  
  | (Script_typed_ir.Union_key tl tr, x, y) =>
    let 'existT _ [__2, __3] [y, x, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__2, __3] =>
          [Script_typed_ir.union __2 __3 ** Script_typed_ir.union __2 __3 **
            Script_typed_ir.comparable_ty ** Script_typed_ir.comparable_ty])
        [y, x, tr, tl] in
    match (x, y) with
    | (Script_typed_ir.L x, Script_typed_ir.L y) => compare_comparable tl x y
    | (Script_typed_ir.L _, Script_typed_ir.R _) => (-1)
    | (Script_typed_ir.R _, Script_typed_ir.L _) => 1
    | (Script_typed_ir.R x, Script_typed_ir.R y) => compare_comparable tr x y
    end
  
  | (Script_typed_ir.Option_key t_value, x, y) =>
    let 'existT _ __4 [y, x, t_value] :=
      cast_exists (Es := Set)
        (fun __4 => [option __4 ** option __4 ** Script_typed_ir.comparable_ty])
        [y, x, t_value] in
    match (x, y) with
    | (None, None) => 0
    | (None, Some _) => (-1)
    | (Some _, None) => 1
    | (Some x, Some y) => compare_comparable t_value x y
    end
  | _ => unreachable_gadt_branch
  end.

Definition empty_set {a : Set} (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.set a :=
  let OPS :=
    _Set.Make
      (let t : Set := a in
      let compare := compare_comparable ty in
      {|
        Compare.COMPARABLE.compare := compare
      |}) in
  existS (A := Set) _ _
    (let elt : Set := a in
    let elt_ty := ty in
    let OPS := OPS in
    let boxed := OPS.(S.SET.empty) in
    let size := 0 in
    {|
      Script_typed_ir.Boxed_set.elt_ty := elt_ty;
      Script_typed_ir.Boxed_set.OPS := OPS;
      Script_typed_ir.Boxed_set.boxed := boxed;
      Script_typed_ir.Boxed_set.size := size
    |}).

Definition set_update {a : Set}
  (v : a) (b_value : bool) (Box : Script_typed_ir.set a)
  : Script_typed_ir.set a :=
  let 'existS _ _ Box := Box in
  existS (A := Set) _ _
    (let elt : Set := a in
    let elt_ty := Box.(Script_typed_ir.Boxed_set.elt_ty) in
    let OPS := Box.(Script_typed_ir.Boxed_set.OPS) in
    let boxed :=
      if b_value then
        Box.(Script_typed_ir.Boxed_set.OPS).(S.SET.add) v
          Box.(Script_typed_ir.Boxed_set.boxed)
      else
        Box.(Script_typed_ir.Boxed_set.OPS).(S.SET.remove) v
          Box.(Script_typed_ir.Boxed_set.boxed) in
    let size :=
      let mem :=
        Box.(Script_typed_ir.Boxed_set.OPS).(S.SET.mem) v
          Box.(Script_typed_ir.Boxed_set.boxed) in
      if mem then
        if b_value then
          Box.(Script_typed_ir.Boxed_set.size)
        else
          Box.(Script_typed_ir.Boxed_set.size) -i 1
      else
        if b_value then
          Box.(Script_typed_ir.Boxed_set.size) +i 1
        else
          Box.(Script_typed_ir.Boxed_set.size) in
    {|
      Script_typed_ir.Boxed_set.elt_ty := elt_ty;
      Script_typed_ir.Boxed_set.OPS := OPS;
      Script_typed_ir.Boxed_set.boxed := boxed;
      Script_typed_ir.Boxed_set.size := size
    |}).

Definition set_mem {elt : Set} (v : elt) (Box : Script_typed_ir.set elt)
  : bool :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_set.OPS).(S.SET.mem) v
    Box.(Script_typed_ir.Boxed_set.boxed).

Definition set_fold {elt acc : Set}
  (f : elt -> acc -> acc) (Box : Script_typed_ir.set elt) : acc -> acc :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_set.OPS).(S.SET.fold) f
    Box.(Script_typed_ir.Boxed_set.boxed).

Definition set_size {elt : Set} (Box : Script_typed_ir.set elt)
  : Alpha_context.Script_int.num :=
  let 'existS _ _ Box := Box in
  Alpha_context.Script_int.abs
    (Alpha_context.Script_int.of_int Box.(Script_typed_ir.Boxed_set.size)).

Definition map_key_ty {a b : Set} (Box : Script_typed_ir.map a b)
  : Script_typed_ir.comparable_ty :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.key_ty).

Definition empty_map {a b : Set} (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.map a b :=
  let OPS :=
    Map.Make
      (let t : Set := a in
      let compare := compare_comparable ty in
      {|
        Compare.COMPARABLE.compare := compare
      |}) in
  existS (A := Set -> Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := ty in
    let OPS := OPS in
    let boxed {C : Set} : OPS.(S.MAP.t) C * int :=
      (OPS.(S.MAP.empty), 0) in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed
    |}).

Definition map_get {key value : Set}
  (k : key) (Box : Script_typed_ir.map key value) : option value :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.find_opt) k
    (Pervasives.fst Box.(Script_typed_ir.Boxed_map.boxed)).

Definition map_update {a b : Set}
  (k : a) (v : option b) (Box : Script_typed_ir.map a b)
  : Script_typed_ir.map a b :=
  let 'existS _ _ Box := Box in
  existS (A := Set -> Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := Box.(Script_typed_ir.Boxed_map.key_ty) in
    let OPS := Box.(Script_typed_ir.Boxed_map.OPS) in
    let boxed :=
      let '(map, size) := Box.(Script_typed_ir.Boxed_map.boxed) in
      let contains := Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.mem) k map in
      match v with
      | Some v =>
        ((Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.add) k v map),
          (size +i
          (if contains then
            0
          else
            1)))
      | None =>
        ((Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.remove) k map),
          (size -i
          (if contains then
            1
          else
            0)))
      end in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed
    |}).

Definition map_set {a b : Set} (k : a) (v : b) (Box : Script_typed_ir.map a b)
  : Script_typed_ir.map a b :=
  let 'existS _ _ Box := Box in
  existS (A := Set -> Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := Box.(Script_typed_ir.Boxed_map.key_ty) in
    let OPS := Box.(Script_typed_ir.Boxed_map.OPS) in
    let boxed :=
      let '(map, size) := Box.(Script_typed_ir.Boxed_map.boxed) in
      ((Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.add) k v map),
        (if Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.mem) k map then
          size
        else
          size +i 1)) in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed
    |}).

Definition map_mem {key value : Set}
  (k : key) (Box : Script_typed_ir.map key value) : bool :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.mem) k
    (Pervasives.fst Box.(Script_typed_ir.Boxed_map.boxed)).

Definition map_fold {key value acc : Set}
  (f : key -> value -> acc -> acc) (Box : Script_typed_ir.map key value)
  : acc -> acc :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(S.MAP.fold) f
    (Pervasives.fst Box.(Script_typed_ir.Boxed_map.boxed)).

Definition map_size {key value : Set} (Box : Script_typed_ir.map key value)
  : Alpha_context.Script_int.num :=
  let 'existS _ _ Box := Box in
  Alpha_context.Script_int.abs
    (Alpha_context.Script_int.of_int
      (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).

Fixpoint ty_of_comparable_ty
  (function_parameter : Script_typed_ir.comparable_ty) : Script_typed_ir.ty :=
  match function_parameter with
  | Script_typed_ir.Unit_key => Script_typed_ir.Unit_t
  | Script_typed_ir.Never_key => Script_typed_ir.Never_t
  | Script_typed_ir.Int_key => Script_typed_ir.Int_t
  | Script_typed_ir.Nat_key => Script_typed_ir.Nat_t
  | Script_typed_ir.Signature_key => Script_typed_ir.Signature_t
  | Script_typed_ir.String_key => Script_typed_ir.String_t
  | Script_typed_ir.Bytes_key => Script_typed_ir.Bytes_t
  | Script_typed_ir.Mutez_key => Script_typed_ir.Mutez_t
  | Script_typed_ir.Bool_key => Script_typed_ir.Bool_t
  | Script_typed_ir.Key_hash_key => Script_typed_ir.Key_hash_t
  | Script_typed_ir.Key_key => Script_typed_ir.Key_t
  | Script_typed_ir.Timestamp_key => Script_typed_ir.Timestamp_t
  | Script_typed_ir.Address_key => Script_typed_ir.Address_t
  | Script_typed_ir.Chain_id_key => Script_typed_ir.Chain_id_t
  | Script_typed_ir.Pair_key l_value r_value =>
    Script_typed_ir.Pair_t (ty_of_comparable_ty l_value)
      (ty_of_comparable_ty r_value)
  | Script_typed_ir.Union_key l_value r_value =>
    Script_typed_ir.Union_t (ty_of_comparable_ty l_value)
      (ty_of_comparable_ty r_value)
  | Script_typed_ir.Option_key t_value =>
    Script_typed_ir.Option_t (ty_of_comparable_ty t_value)
  end.

Fixpoint unparse_comparable_ty
  (function_parameter : Script_typed_ir.comparable_ty)
  : Alpha_context.Script.node :=
  match function_parameter with
  | Script_typed_ir.Unit_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_unit nil nil
  | Script_typed_ir.Never_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_never nil nil
  | Script_typed_ir.Int_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_int nil nil
  | Script_typed_ir.Nat_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_nat nil nil
  | Script_typed_ir.Signature_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_signature nil nil
  | Script_typed_ir.String_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_string nil nil
  | Script_typed_ir.Bytes_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_bytes nil nil
  | Script_typed_ir.Mutez_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_mutez nil nil
  | Script_typed_ir.Bool_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_bool nil nil
  | Script_typed_ir.Key_hash_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_key_hash nil nil
  | Script_typed_ir.Key_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_key nil nil
  | Script_typed_ir.Timestamp_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_timestamp nil nil
  | Script_typed_ir.Address_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_address nil nil
  | Script_typed_ir.Chain_id_key =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_chain_id nil nil
  | Script_typed_ir.Pair_key l_value r_value =>
    let tl := unparse_comparable_ty l_value in
    let tr := unparse_comparable_ty r_value in
    match tr with
    | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
      Micheline.Prim (-1) Michelson_v1_primitives.T_pair (cons tl ts) nil
    | _ => Micheline.Prim (-1) Michelson_v1_primitives.T_pair [ tl; tr ] nil
    end
  | Script_typed_ir.Union_key l_value r_value =>
    let tl := unparse_comparable_ty l_value in
    let tr := unparse_comparable_ty r_value in
    Micheline.Prim (-1) Michelson_v1_primitives.T_or [ tl; tr ] nil
  | Script_typed_ir.Option_key t_value =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_option
      [ unparse_comparable_ty t_value ] nil
  end.

Definition unparse_memo_size {A : Set}
  (memo_size : Alpha_context.Sapling.Memo_size.t) : Micheline.node int A :=
  let z := Alpha_context.Sapling.Memo_size.unparse_to_z memo_size in
  Micheline.Int (-1) z.

Fixpoint unparse_ty (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.unparse_type_cycle
    in
  let _return {A B C : Set}
    (ctxt : A) (function_parameter : B * list (Micheline.node int B))
    : Pervasives.result (Micheline.node int B * A) C :=
    let '(name, args) := function_parameter in
    return? ((Micheline.Prim (-1) name args nil), ctxt) in
  match ty with
  | Script_typed_ir.Unit_t => _return ctxt (Michelson_v1_primitives.T_unit, nil)
  | Script_typed_ir.Int_t => _return ctxt (Michelson_v1_primitives.T_int, nil)
  | Script_typed_ir.Nat_t => _return ctxt (Michelson_v1_primitives.T_nat, nil)
  | Script_typed_ir.Signature_t =>
    _return ctxt (Michelson_v1_primitives.T_signature, nil)
  | Script_typed_ir.String_t =>
    _return ctxt (Michelson_v1_primitives.T_string, nil)
  | Script_typed_ir.Bytes_t =>
    _return ctxt (Michelson_v1_primitives.T_bytes, nil)
  | Script_typed_ir.Mutez_t =>
    _return ctxt (Michelson_v1_primitives.T_mutez, nil)
  | Script_typed_ir.Bool_t => _return ctxt (Michelson_v1_primitives.T_bool, nil)
  | Script_typed_ir.Key_hash_t =>
    _return ctxt (Michelson_v1_primitives.T_key_hash, nil)
  | Script_typed_ir.Key_t => _return ctxt (Michelson_v1_primitives.T_key, nil)
  | Script_typed_ir.Timestamp_t =>
    _return ctxt (Michelson_v1_primitives.T_timestamp, nil)
  | Script_typed_ir.Address_t =>
    _return ctxt (Michelson_v1_primitives.T_address, nil)
  | Script_typed_ir.Operation_t =>
    _return ctxt (Michelson_v1_primitives.T_operation, nil)
  | Script_typed_ir.Chain_id_t =>
    _return ctxt (Michelson_v1_primitives.T_chain_id, nil)
  | Script_typed_ir.Never_t =>
    _return ctxt (Michelson_v1_primitives.T_never, nil)
  | Script_typed_ir.Bls12_381_g1_t =>
    _return ctxt (Michelson_v1_primitives.T_bls12_381_g1, nil)
  | Script_typed_ir.Bls12_381_g2_t =>
    _return ctxt (Michelson_v1_primitives.T_bls12_381_g2, nil)
  | Script_typed_ir.Bls12_381_fr_t =>
    _return ctxt (Michelson_v1_primitives.T_bls12_381_fr, nil)
  | Script_typed_ir.Contract_t ut =>
    let? '(t_value, ctxt) := unparse_ty ctxt ut in
    _return ctxt (Michelson_v1_primitives.T_contract, [ t_value ])
  | Script_typed_ir.Pair_t utl utr =>
    let? '(tl, ctxt) := unparse_ty ctxt utl in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt
      (Michelson_v1_primitives.T_pair,
        match tr with
        | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] => cons tl ts
        | _ => [ tl; tr ]
        end)
  | Script_typed_ir.Union_t utl utr =>
    let? '(tl, ctxt) := unparse_ty ctxt utl in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt (Michelson_v1_primitives.T_or, [ tl; tr ])
  | Script_typed_ir.Lambda_t uta utr =>
    let? '(ta, ctxt) := unparse_ty ctxt uta in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt (Michelson_v1_primitives.T_lambda, [ ta; tr ])
  | Script_typed_ir.Option_t ut =>
    let? '(ut, ctxt) := unparse_ty ctxt ut in
    _return ctxt (Michelson_v1_primitives.T_option, [ ut ])
  | Script_typed_ir.List_t ut =>
    let? '(t_value, ctxt) := unparse_ty ctxt ut in
    _return ctxt (Michelson_v1_primitives.T_list, [ t_value ])
  | Script_typed_ir.Ticket_t ut =>
    let t_value := unparse_comparable_ty ut in
    _return ctxt (Michelson_v1_primitives.T_ticket, [ t_value ])
  | Script_typed_ir.Set_t ut =>
    let t_value := unparse_comparable_ty ut in
    _return ctxt (Michelson_v1_primitives.T_set, [ t_value ])
  | Script_typed_ir.Map_t uta utr =>
    let ta := unparse_comparable_ty uta in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt (Michelson_v1_primitives.T_map, [ ta; tr ])
  | Script_typed_ir.Big_map_t uta utr =>
    let ta := unparse_comparable_ty uta in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt (Michelson_v1_primitives.T_big_map, [ ta; tr ])
  | Script_typed_ir.Sapling_transaction_t memo_size =>
    _return ctxt
      (Michelson_v1_primitives.T_sapling_transaction,
        [ unparse_memo_size memo_size ])
  | Script_typed_ir.Sapling_state_t memo_size =>
    _return ctxt
      (Michelson_v1_primitives.T_sapling_state, [ unparse_memo_size memo_size ])
  end.

Fixpoint add_entrypoints
  (entrypoints : Script_typed_ir.entrypoints) (node : Alpha_context.Script.node)
  : Alpha_context.Script.node :=
  match (entrypoints, node) with
  |
    ({|
      Script_typed_ir.entrypoints.name := name;
        Script_typed_ir.entrypoints.nested := Script_typed_ir.Entrypoints_None
        |}, Micheline.Prim loc prim args annots) =>
    Micheline.Prim loc prim args
      (Pervasives.op_at annots (Script_ir_annot.unparse_field_annot name))
  |
    ({|
      Script_typed_ir.entrypoints.nested := Script_typed_ir.Entrypoints_None
        |}, node) => node
  |
    ({|
      Script_typed_ir.entrypoints.name := name;
        Script_typed_ir.entrypoints.nested :=
          Script_typed_ir.Entrypoints_Union {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                := _right
              |}
        |},
      Micheline.Prim loc Michelson_v1_primitives.T_or (cons utl (cons utr []))
        annots) =>
    let utl := add_entrypoints _left utl in
    let utr := add_entrypoints _right utr in
    Micheline.Prim loc Michelson_v1_primitives.T_or [ utl; utr ]
      (Pervasives.op_at annots (Script_ir_annot.unparse_field_annot name))
  |
    ({|
      Script_typed_ir.entrypoints.nested := Script_typed_ir.Entrypoints_Union _
        |}, _) =>
    (* ❌ Assert instruction is not handled. *)
    assert Alpha_context.Script.node false
  end.

Definition unparse_parameter_ty
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty)
  (entrypoints : Script_typed_ir.entrypoints)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? '(unparsed, ctxt) := unparse_ty ctxt ty in
  return? ((add_entrypoints entrypoints unparsed), ctxt).

Definition serialize_ty_for_error
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty)
  : M? (Micheline.canonical Alpha_context.Script.prim * Alpha_context.context) :=
  Error_monad.record_trace (Build_extensible "Cannot_serialize_error" unit tt)
    (let? '(ty, ctxt) := unparse_ty ctxt ty in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.strip_locations_cost ty) in
    return? ((Micheline.strip_locations ty), ctxt)).

Fixpoint comparable_ty_of_ty
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (ty : Script_typed_ir.ty)
  : M? (Script_typed_ir.comparable_ty * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.comparable_ty_of_ty_cycle in
  match ty with
  | Script_typed_ir.Unit_t => return? (Script_typed_ir.Unit_key, ctxt)
  | Script_typed_ir.Never_t => return? (Script_typed_ir.Never_key, ctxt)
  | Script_typed_ir.Int_t => return? (Script_typed_ir.Int_key, ctxt)
  | Script_typed_ir.Nat_t => return? (Script_typed_ir.Nat_key, ctxt)
  | Script_typed_ir.Signature_t => return? (Script_typed_ir.Signature_key, ctxt)
  | Script_typed_ir.String_t => return? (Script_typed_ir.String_key, ctxt)
  | Script_typed_ir.Bytes_t => return? (Script_typed_ir.Bytes_key, ctxt)
  | Script_typed_ir.Mutez_t => return? (Script_typed_ir.Mutez_key, ctxt)
  | Script_typed_ir.Bool_t => return? (Script_typed_ir.Bool_key, ctxt)
  | Script_typed_ir.Key_hash_t => return? (Script_typed_ir.Key_hash_key, ctxt)
  | Script_typed_ir.Key_t => return? (Script_typed_ir.Key_key, ctxt)
  | Script_typed_ir.Timestamp_t => return? (Script_typed_ir.Timestamp_key, ctxt)
  | Script_typed_ir.Address_t => return? (Script_typed_ir.Address_key, ctxt)
  | Script_typed_ir.Chain_id_t => return? (Script_typed_ir.Chain_id_key, ctxt)
  | Script_typed_ir.Pair_t l_value r_value =>
    let? '(lty, ctxt) := comparable_ty_of_ty ctxt loc l_value in
    let? '(rty, ctxt) := comparable_ty_of_ty ctxt loc r_value in
    return? ((Script_typed_ir.Pair_key lty rty), ctxt)
  | Script_typed_ir.Union_t l_value r_value =>
    let? '(lty, ctxt) := comparable_ty_of_ty ctxt loc l_value in
    let? '(rty, ctxt) := comparable_ty_of_ty ctxt loc r_value in
    return? ((Script_typed_ir.Union_key lty rty), ctxt)
  | Script_typed_ir.Option_t ty =>
    let? '(ty, ctxt) := comparable_ty_of_ty ctxt loc ty in
    return? ((Script_typed_ir.Option_key ty), ctxt)
  |
    (Script_typed_ir.Lambda_t _ _ | Script_typed_ir.List_t _ |
    Script_typed_ir.Ticket_t _ | Script_typed_ir.Set_t _ |
    Script_typed_ir.Map_t _ _ | Script_typed_ir.Big_map_t _ _ |
    Script_typed_ir.Contract_t _ | Script_typed_ir.Operation_t |
    Script_typed_ir.Bls12_381_fr_t | Script_typed_ir.Bls12_381_g1_t |
    Script_typed_ir.Bls12_381_g2_t | Script_typed_ir.Sapling_state_t _ |
    Script_typed_ir.Sapling_transaction_t _) =>
    let? '(t_value, _ctxt) := serialize_ty_for_error ctxt ty in
    Error_monad.error_value
      (Build_extensible "Comparable_type_expected"
        (Alpha_context.Script.location *
          Micheline.canonical Alpha_context.Script.prim) (loc, t_value))
  end.

Fixpoint unparse_stack
  (ctxt : Alpha_context.context) (function_parameter : Script_typed_ir.stack_ty)
  {struct function_parameter}
  : M?
    (list (Alpha_context.Script.expr * Alpha_context.Script.annot) *
      Alpha_context.context) :=
  match function_parameter with
  | Script_typed_ir.Empty_t => return? (nil, ctxt)
  | Script_typed_ir.Item_t ty rest =>
    let? '(uty, ctxt) := unparse_ty ctxt ty in
    let? '(urest, ctxt) := unparse_stack ctxt rest in
    return? ((cons ((Micheline.strip_locations uty), nil) urest), ctxt)
  end.

Definition serialize_stack_for_error
  (ctxt : Alpha_context.context) (stack_ty : Script_typed_ir.stack_ty)
  : M?
    (list (Alpha_context.Script.expr * Alpha_context.Script.annot) *
      Alpha_context.context) :=
  Error_monad.record_trace (Build_extensible "Cannot_serialize_error" unit tt)
    (unparse_stack ctxt stack_ty).

Definition check_dupable_comparable_ty
  (function_parameter : Script_typed_ir.comparable_ty) : unit :=
  match function_parameter with
  |
    (Script_typed_ir.Unit_key | Script_typed_ir.Never_key |
    Script_typed_ir.Int_key | Script_typed_ir.Nat_key |
    Script_typed_ir.Signature_key | Script_typed_ir.String_key |
    Script_typed_ir.Bytes_key | Script_typed_ir.Mutez_key |
    Script_typed_ir.Bool_key | Script_typed_ir.Key_hash_key |
    Script_typed_ir.Key_key | Script_typed_ir.Timestamp_key |
    Script_typed_ir.Chain_id_key | Script_typed_ir.Address_key |
    Script_typed_ir.Pair_key _ _ | Script_typed_ir.Union_key _ _ |
    Script_typed_ir.Option_key _) => tt
  end.

Fixpoint check_dupable_ty
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (ty : Script_typed_ir.ty) : M? Alpha_context.context :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.check_dupable_cycle in
  match ty with
  | Script_typed_ir.Unit_t => return? ctxt
  | Script_typed_ir.Int_t => return? ctxt
  | Script_typed_ir.Nat_t => return? ctxt
  | Script_typed_ir.Signature_t => return? ctxt
  | Script_typed_ir.String_t => return? ctxt
  | Script_typed_ir.Bytes_t => return? ctxt
  | Script_typed_ir.Mutez_t => return? ctxt
  | Script_typed_ir.Key_hash_t => return? ctxt
  | Script_typed_ir.Key_t => return? ctxt
  | Script_typed_ir.Timestamp_t => return? ctxt
  | Script_typed_ir.Address_t => return? ctxt
  | Script_typed_ir.Bool_t => return? ctxt
  | Script_typed_ir.Contract_t _ => return? ctxt
  | Script_typed_ir.Operation_t => return? ctxt
  | Script_typed_ir.Chain_id_t => return? ctxt
  | Script_typed_ir.Never_t => return? ctxt
  | Script_typed_ir.Bls12_381_g1_t => return? ctxt
  | Script_typed_ir.Bls12_381_g2_t => return? ctxt
  | Script_typed_ir.Bls12_381_fr_t => return? ctxt
  | Script_typed_ir.Sapling_state_t _ => return? ctxt
  | Script_typed_ir.Sapling_transaction_t _ => return? ctxt
  | Script_typed_ir.Ticket_t _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_ticket" Alpha_context.Script.location loc)
  | Script_typed_ir.Pair_t ty_a ty_b =>
    let? ctxt := check_dupable_ty ctxt loc ty_a in
    check_dupable_ty ctxt loc ty_b
  | Script_typed_ir.Union_t ty_a ty_b =>
    let? ctxt := check_dupable_ty ctxt loc ty_a in
    check_dupable_ty ctxt loc ty_b
  | Script_typed_ir.Lambda_t _ _ => return? ctxt
  | Script_typed_ir.Option_t ty => check_dupable_ty ctxt loc ty
  | Script_typed_ir.List_t ty => check_dupable_ty ctxt loc ty
  | Script_typed_ir.Set_t key_ty =>
    let '_ := check_dupable_comparable_ty key_ty in
    return? ctxt
  | Script_typed_ir.Map_t key_ty val_ty =>
    let '_ := check_dupable_comparable_ty key_ty in
    check_dupable_ty ctxt loc val_ty
  | Script_typed_ir.Big_map_t key_ty val_ty =>
    let '_ := check_dupable_comparable_ty key_ty in
    check_dupable_ty ctxt loc val_ty
  end.

Inductive eq : Set :=
| Eq : eq.

Definition record_inconsistent {A : Set}
  (ctxt : Alpha_context.context) (ta : Script_typed_ir.ty)
  (tb : Script_typed_ir.ty) : M? A -> M? A :=
  Error_monad.record_trace_eval
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let? '(ta, ctxt) := serialize_ty_for_error ctxt ta in
      let? '(tb, _ctxt) := serialize_ty_for_error ctxt tb in
      return?
        (Build_extensible "Inconsistent_types"
          (Micheline.canonical Alpha_context.Script.prim *
            Micheline.canonical Alpha_context.Script.prim) (ta, tb))).

Fixpoint comparable_ty_eq
  (ctxt : Alpha_context.context) (ta : Script_typed_ir.comparable_ty)
  (tb : Script_typed_ir.comparable_ty) : M? (eq * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.merge_cycle in
  match (ta, tb) with
  | (Script_typed_ir.Unit_key, Script_typed_ir.Unit_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Never_key, Script_typed_ir.Never_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Int_key, Script_typed_ir.Int_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Nat_key, Script_typed_ir.Nat_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Signature_key, Script_typed_ir.Signature_key) =>
    return? (Eq, ctxt)
  | (Script_typed_ir.String_key, Script_typed_ir.String_key) =>
    return? (Eq, ctxt)
  | (Script_typed_ir.Bytes_key, Script_typed_ir.Bytes_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Mutez_key, Script_typed_ir.Mutez_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Bool_key, Script_typed_ir.Bool_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Key_hash_key, Script_typed_ir.Key_hash_key) =>
    return? (Eq, ctxt)
  | (Script_typed_ir.Key_key, Script_typed_ir.Key_key) => return? (Eq, ctxt)
  | (Script_typed_ir.Timestamp_key, Script_typed_ir.Timestamp_key) =>
    return? (Eq, ctxt)
  | (Script_typed_ir.Chain_id_key, Script_typed_ir.Chain_id_key) =>
    return? (Eq, ctxt)
  | (Script_typed_ir.Address_key, Script_typed_ir.Address_key) =>
    return? (Eq, ctxt)
  |
    (Script_typed_ir.Pair_key left_a right_a,
      Script_typed_ir.Pair_key left_b right_b) =>
    let? '(Eq, ctxt) := comparable_ty_eq ctxt left_a left_b in
    let? '(Eq, ctxt) := comparable_ty_eq ctxt right_a right_b in
    return? (Eq, ctxt)
  |
    (Script_typed_ir.Union_key left_a right_a,
      Script_typed_ir.Union_key left_b right_b) =>
    let? '(Eq, ctxt) := comparable_ty_eq ctxt left_a left_b in
    let? '(Eq, ctxt) := comparable_ty_eq ctxt right_a right_b in
    return? (Eq, ctxt)
  | (Script_typed_ir.Option_key ta, Script_typed_ir.Option_key tb) =>
    let? '(Eq, ctxt) := comparable_ty_eq ctxt ta tb in
    return? (Eq, ctxt)
  | (_, _) =>
    let? '(ta, ctxt) := serialize_ty_for_error ctxt (ty_of_comparable_ty ta) in
    let? '(tb, _ctxt) := serialize_ty_for_error ctxt (ty_of_comparable_ty tb) in
    Error_monad.error_value
      (Build_extensible "Inconsistent_types"
        (Micheline.canonical Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim) (ta, tb))
  end.

Definition memo_size_eq
  (ms1 : Alpha_context.Sapling.Memo_size.t)
  (ms2 : Alpha_context.Sapling.Memo_size.t) : M? unit :=
  if Alpha_context.Sapling.Memo_size.equal ms1 ms2 then
    Error_monad.ok_unit
  else
    Error_monad.error_value
      (Build_extensible "Inconsistent_memo_sizes"
        (Alpha_context.Sapling.Memo_size.t * Alpha_context.Sapling.Memo_size.t)
        (ms1, ms2)).

Fixpoint ty_eq
  (ctxt : Alpha_context.context) (ty1 : Script_typed_ir.ty)
  (ty2 : Script_typed_ir.ty) : M? (eq * Alpha_context.context) :=
  let help
    (ctxt : Alpha_context.context) (ty1 : Script_typed_ir.ty)
    (ty2 : Script_typed_ir.ty) : M? (eq * Alpha_context.context) :=
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.merge_cycle in
    match (ty1, ty2) with
    | (Script_typed_ir.Unit_t, Script_typed_ir.Unit_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Int_t, Script_typed_ir.Int_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Nat_t, Script_typed_ir.Nat_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Key_t, Script_typed_ir.Key_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Key_hash_t, Script_typed_ir.Key_hash_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.String_t, Script_typed_ir.String_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Bytes_t, Script_typed_ir.Bytes_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Signature_t, Script_typed_ir.Signature_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Mutez_t, Script_typed_ir.Mutez_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Timestamp_t, Script_typed_ir.Timestamp_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Address_t, Script_typed_ir.Address_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Bool_t, Script_typed_ir.Bool_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Chain_id_t, Script_typed_ir.Chain_id_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Never_t, Script_typed_ir.Never_t) => return? (Eq, ctxt)
    | (Script_typed_ir.Operation_t, Script_typed_ir.Operation_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Bls12_381_g1_t, Script_typed_ir.Bls12_381_g1_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Bls12_381_g2_t, Script_typed_ir.Bls12_381_g2_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Bls12_381_fr_t, Script_typed_ir.Bls12_381_fr_t) =>
      return? (Eq, ctxt)
    | (Script_typed_ir.Map_t tal tar, Script_typed_ir.Map_t tbl tbr) =>
      let? '(Eq, ctxt) := ty_eq ctxt tar tbr in
      let? '(Eq, ctxt) := comparable_ty_eq ctxt tal tbl in
      return? (Eq, ctxt)
    | (Script_typed_ir.Big_map_t tal tar, Script_typed_ir.Big_map_t tbl tbr) =>
      let? '(Eq, ctxt) := ty_eq ctxt tar tbr in
      let? '(Eq, ctxt) := comparable_ty_eq ctxt tal tbl in
      return? (Eq, ctxt)
    | (Script_typed_ir.Set_t ea, Script_typed_ir.Set_t eb) =>
      let? '(Eq, ctxt) := comparable_ty_eq ctxt ea eb in
      return? (Eq, ctxt)
    | (Script_typed_ir.Ticket_t ea, Script_typed_ir.Ticket_t eb) =>
      let? '(Eq, ctxt) := comparable_ty_eq ctxt ea eb in
      return? (Eq, ctxt)
    | (Script_typed_ir.Pair_t tal tar, Script_typed_ir.Pair_t tbl tbr) =>
      let? '(Eq, ctxt) := ty_eq ctxt tal tbl in
      let? '(Eq, ctxt) := ty_eq ctxt tar tbr in
      return? (Eq, ctxt)
    | (Script_typed_ir.Union_t tal tar, Script_typed_ir.Union_t tbl tbr) =>
      let? '(Eq, ctxt) := ty_eq ctxt tal tbl in
      let? '(Eq, ctxt) := ty_eq ctxt tar tbr in
      return? (Eq, ctxt)
    | (Script_typed_ir.Lambda_t tal tar, Script_typed_ir.Lambda_t tbl tbr) =>
      let? '(Eq, ctxt) := ty_eq ctxt tal tbl in
      let? '(Eq, ctxt) := ty_eq ctxt tar tbr in
      return? (Eq, ctxt)
    | (Script_typed_ir.Contract_t tal, Script_typed_ir.Contract_t tbl) =>
      let? '(Eq, ctxt) := ty_eq ctxt tal tbl in
      return? (Eq, ctxt)
    | (Script_typed_ir.Option_t tva, Script_typed_ir.Option_t tvb) =>
      let? '(Eq, ctxt) := ty_eq ctxt tva tvb in
      return? (Eq, ctxt)
    | (Script_typed_ir.List_t tva, Script_typed_ir.List_t tvb) =>
      let? '(Eq, ctxt) := ty_eq ctxt tva tvb in
      return? (Eq, ctxt)
    | (Script_typed_ir.Sapling_state_t ms1, Script_typed_ir.Sapling_state_t ms2)
      =>
      let? '_ := memo_size_eq ms1 ms2 in
      return? (Eq, ctxt)
    |
      (Script_typed_ir.Sapling_transaction_t ms1,
        Script_typed_ir.Sapling_transaction_t ms2) =>
      let? '_ := memo_size_eq ms1 ms2 in
      return? (Eq, ctxt)
    | (_, _) =>
      let? '(ty1, ctxt) := serialize_ty_for_error ctxt ty1 in
      let? '(ty2, _ctxt) := serialize_ty_for_error ctxt ty2 in
      Error_monad.error_value
        (Build_extensible "Inconsistent_types"
          (Micheline.canonical Alpha_context.Script.prim *
            Micheline.canonical Alpha_context.Script.prim) (ty1, ty2))
    end in
  record_inconsistent ctxt ty1 ty2 (help ctxt ty1 ty2).

Fixpoint stacks_eq
  (ctxt : Alpha_context.context) (lvl : int) (stack1 : Script_typed_ir.stack_ty)
  (stack2 : Script_typed_ir.stack_ty) : M? (eq * Alpha_context.context) :=
  match (stack1, stack2) with
  | (Script_typed_ir.Empty_t, Script_typed_ir.Empty_t) => return? (Eq, ctxt)
  | (Script_typed_ir.Item_t ty1 rest1, Script_typed_ir.Item_t ty2 rest2) =>
    let? '(Eq, ctxt) :=
      Error_monad.record_trace (Build_extensible "Bad_stack_item" int lvl)
        (ty_eq ctxt ty1 ty2) in
    let? '(Eq, ctxt) := stacks_eq ctxt (lvl +i 1) rest1 rest2 in
    return? (Eq, ctxt)
  | (_, _) =>
    Error_monad.error_value (Build_extensible "Bad_stack_length" unit tt)
  end.

(** Records for the constructor parameters *)
Module ConstructorRecords_judgement.
  Module judgement.
    Module Failed.
      Record record {descr : Set} : Set := Build {
        descr : descr }.
      Arguments record : clear implicits.
      Definition with_descr {t_descr} descr (r : record t_descr) :=
        Build t_descr descr.
    End Failed.
    Definition Failed_skeleton := Failed.record.
  End judgement.
End ConstructorRecords_judgement.
Import ConstructorRecords_judgement.

Reserved Notation "'judgement.Failed".

Inductive judgement : Set :=
| Typed : Script_typed_ir.descr -> judgement
| Failed : 'judgement.Failed -> judgement

where "'judgement.Failed" :=
  (judgement.Failed_skeleton (Script_typed_ir.stack_ty -> Script_typed_ir.descr)).

Module judgement.
  Include ConstructorRecords_judgement.judgement.
  Definition Failed := 'judgement.Failed.
End judgement.

Module branch.
  Record record : Set := Build {
    branch :
      Script_typed_ir.descr -> Script_typed_ir.descr -> Script_typed_ir.descr }.
  Definition with_branch branch (r : record) :=
    Build branch.
End branch.
Definition branch := branch.record.

Definition merge_branches
  (ctxt : Alpha_context.context) (loc : int) (btr : judgement) (bfr : judgement)
  (function_parameter : branch) : M? (judgement * Alpha_context.context) :=
  let '{| branch.branch := branch |} := function_parameter in
  match (btr, bfr) with
  |
    (Typed ({| Script_typed_ir.descr.aft := aftbt |} as dbt),
      Typed ({| Script_typed_ir.descr.aft := aftbf |} as dbf)) =>
    let unmatched_branches (function_parameter : unit)
      : M? Error_monad._error :=
      let '_ := function_parameter in
      let? '(aftbt, ctxt) := serialize_stack_for_error ctxt aftbt in
      let? '(aftbf, _ctxt) := serialize_stack_for_error ctxt aftbf in
      return?
        (Build_extensible "Unmatched_branches"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc, aftbt, aftbf)) in
    Error_monad.record_trace_eval unmatched_branches
      (let? '(Eq, ctxt) := stacks_eq ctxt 1 aftbt aftbf in
      return? ((Typed (branch dbt dbf)), ctxt))
  |
    (Failed {| judgement.Failed.descr := descrt |},
      Failed {| judgement.Failed.descr := descrf |}) =>
    let descr_value (ret : Script_typed_ir.stack_ty) : Script_typed_ir.descr :=
      branch (descrt ret) (descrf ret) in
    return? ((Failed {| judgement.Failed.descr := descr_value |}), ctxt)
  | (Typed dbt, Failed {| judgement.Failed.descr := descrf |}) =>
    return?
      ((Typed (branch dbt (descrf dbt.(Script_typed_ir.descr.aft)))), ctxt)
  | (Failed {| judgement.Failed.descr := descrt |}, Typed dbf) =>
    return?
      ((Typed (branch (descrt dbf.(Script_typed_ir.descr.aft)) dbf)), ctxt)
  end.

Definition parse_memo_size
  (n : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? Alpha_context.Sapling.Memo_size.t :=
  match n with
  | Micheline.Int _ z =>
    match Alpha_context.Sapling.Memo_size.parse_z z with
    | Pervasives.Ok memo_size => return? memo_size
    | Pervasives.Error msg =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          ((location n), (Micheline.strip_locations n), msg))
    end
  | _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location n), [ Script_tc_errors.Int_kind ], (kind_value n)))
  end.

Fixpoint parse_comparable_ty
  (ctxt : Alpha_context.context) (ty : Alpha_context.Script.node) {struct ty}
  : M? (ex_comparable_ty * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
    in
  match ty with
  | Micheline.Prim _loc Michelson_v1_primitives.T_unit [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Unit_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_never [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Never_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_int [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Int_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_nat [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Nat_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_signature [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Signature_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_string [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.String_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_bytes [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Bytes_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_mutez [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Mutez_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_bool [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Bool_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_key_hash [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Key_hash_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_key [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Key_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_timestamp [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Timestamp_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_chain_id [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Chain_id_key), ctxt)
  | Micheline.Prim _loc Michelson_v1_primitives.T_address [] _annot =>
    return? ((Ex_comparable_ty Script_typed_ir.Address_key), ctxt)
  |
    Micheline.Prim loc
      ((Michelson_v1_primitives.T_unit | Michelson_v1_primitives.T_never |
      Michelson_v1_primitives.T_int | Michelson_v1_primitives.T_nat |
      Michelson_v1_primitives.T_string | Michelson_v1_primitives.T_bytes |
      Michelson_v1_primitives.T_mutez | Michelson_v1_primitives.T_bool |
      Michelson_v1_primitives.T_key_hash | Michelson_v1_primitives.T_timestamp |
      Michelson_v1_primitives.T_address | Michelson_v1_primitives.T_chain_id |
      Michelson_v1_primitives.T_signature | Michelson_v1_primitives.T_key) as
        prim) l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, prim, 0, (List.length l_value)))
  | Micheline.Prim loc Michelson_v1_primitives.T_pair (cons _left _right) _annot
    =>
    let _right :=
      match _right with
      | cons _right [] => _right
      | _right => Micheline.Prim loc Michelson_v1_primitives.T_pair _right nil
      end in
    let? '(Ex_comparable_ty _right, ctxt) := parse_comparable_ty ctxt _right in
    let? '(Ex_comparable_ty _left, ctxt) := parse_comparable_ty ctxt _left in
    return? ((Ex_comparable_ty (Script_typed_ir.Pair_key _left _right)), ctxt)
  |
    Micheline.Prim _loc Michelson_v1_primitives.T_or
      (cons _left (cons _right [])) _annot =>
    let? '(Ex_comparable_ty _right, ctxt) := parse_comparable_ty ctxt _right in
    let? '(Ex_comparable_ty _left, ctxt) := parse_comparable_ty ctxt _left in
    return? ((Ex_comparable_ty (Script_typed_ir.Union_key _left _right)), ctxt)
  |
    Micheline.Prim loc
      ((Michelson_v1_primitives.T_pair | Michelson_v1_primitives.T_or) as prim)
      l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, prim, 2, (List.length l_value)))
  |
    Micheline.Prim _loc Michelson_v1_primitives.T_option (cons t_value [])
      _annot =>
    let? '(Ex_comparable_ty t_value, ctxt) := parse_comparable_ty ctxt t_value
      in
    return? ((Ex_comparable_ty (Script_typed_ir.Option_key t_value)), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.T_option l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.T_option, 1, (List.length l_value)))
  |
    Micheline.Prim loc
      (Michelson_v1_primitives.T_set | Michelson_v1_primitives.T_map |
      Michelson_v1_primitives.T_list | Michelson_v1_primitives.T_lambda |
      Michelson_v1_primitives.T_contract | Michelson_v1_primitives.T_operation)
      _ _ =>
    Error_monad.error_value
      (Build_extensible "Comparable_type_expected"
        (Alpha_context.Script.location *
          Micheline.canonical Alpha_context.Script.prim)
        (loc, (Micheline.strip_locations ty)))
  | expr =>
    Error_monad.error_value
      (unexpected expr nil Michelson_v1_primitives.Type_namespace
        [
          Michelson_v1_primitives.T_unit;
          Michelson_v1_primitives.T_never;
          Michelson_v1_primitives.T_int;
          Michelson_v1_primitives.T_nat;
          Michelson_v1_primitives.T_string;
          Michelson_v1_primitives.T_bytes;
          Michelson_v1_primitives.T_mutez;
          Michelson_v1_primitives.T_bool;
          Michelson_v1_primitives.T_key_hash;
          Michelson_v1_primitives.T_timestamp;
          Michelson_v1_primitives.T_address;
          Michelson_v1_primitives.T_pair;
          Michelson_v1_primitives.T_or;
          Michelson_v1_primitives.T_option;
          Michelson_v1_primitives.T_chain_id;
          Michelson_v1_primitives.T_signature;
          Michelson_v1_primitives.T_key
        ])
  end.

Reserved Notation "'parse_parameter_ty".
Reserved Notation "'parse_any_ty".
Reserved Notation "'parse_big_map_value_ty".
Reserved Notation "'parse_big_map_ty".

Fixpoint parse_ty
  (ctxt : Alpha_context.context) (legacy : bool) (allow_lazy_storage : bool)
  (allow_operation : bool) (allow_contract : bool) (allow_ticket : bool)
  (node : Alpha_context.Script.node) {struct node}
  : M? (ex_ty * Alpha_context.context) :=
  let parse_parameter_ty := 'parse_parameter_ty in
  let parse_any_ty := 'parse_any_ty in
  let parse_big_map_value_ty := 'parse_big_map_value_ty in
  let parse_big_map_ty := 'parse_big_map_ty in
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
    in
  match
    (node,
      match node with
      | Micheline.Prim loc Michelson_v1_primitives.T_big_map args _annot =>
        allow_lazy_storage
      | _ => false
      end,
      match node with
      |
        Micheline.Prim _loc Michelson_v1_primitives.T_sapling_state
          (cons memo_size []) _annot => allow_lazy_storage
      | _ => false
      end) with
  | (Micheline.Prim _loc Michelson_v1_primitives.T_unit [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Unit_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_int [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Int_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_nat [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Nat_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_string [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.String_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_bytes [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Bytes_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_mutez [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Mutez_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_bool [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Bool_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_key [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Key_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_key_hash [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Key_hash_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_timestamp [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Timestamp_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_address [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Address_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_signature [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Signature_t), ctxt)
  | (Micheline.Prim loc Michelson_v1_primitives.T_operation [] _annot, _, _) =>
    if allow_operation then
      return? ((Ex_ty Script_typed_ir.Operation_t), ctxt)
    else
      Error_monad.error_value
        (Build_extensible "Unexpected_operation" Alpha_context.Script.location
          loc)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_chain_id [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Chain_id_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_never [] _annot, _, _) =>
    return? ((Ex_ty Script_typed_ir.Never_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_bls12_381_g1 [] _annot, _, _)
    => return? ((Ex_ty Script_typed_ir.Bls12_381_g1_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_bls12_381_g2 [] _annot, _, _)
    => return? ((Ex_ty Script_typed_ir.Bls12_381_g2_t), ctxt)
  | (Micheline.Prim _loc Michelson_v1_primitives.T_bls12_381_fr [] _annot, _, _)
    => return? ((Ex_ty Script_typed_ir.Bls12_381_fr_t), ctxt)
  |
    (Micheline.Prim loc Michelson_v1_primitives.T_contract (cons utl []) _annot,
      _, _) =>
    if allow_contract then
      let? '(Ex_ty tl, ctxt) := parse_parameter_ty ctxt legacy utl in
      return? ((Ex_ty (Script_typed_ir.Contract_t tl)), ctxt)
    else
      Error_monad.error_value
        (Build_extensible "Unexpected_contract" Alpha_context.Script.location
          loc)
  |
    (Micheline.Prim loc Michelson_v1_primitives.T_pair (cons utl utr) _annot, _,
      _) =>
    let? '(Ex_ty tl, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket utl in
    let utr :=
      match utr with
      | cons utr [] => utr
      | utr => Micheline.Prim loc Michelson_v1_primitives.T_pair utr nil
      end in
    let? '(Ex_ty tr, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket utr in
    return? ((Ex_ty (Script_typed_ir.Pair_t tl tr)), ctxt)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_or (cons utl (cons utr []))
      _annot, _, _) =>
    let? '(Ex_ty tl, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket utl in
    let? '(Ex_ty tr, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket utr in
    return? ((Ex_ty (Script_typed_ir.Union_t tl tr)), ctxt)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_lambda
      (cons uta (cons utr [])) _annot, _, _) =>
    let? '(Ex_ty ta, ctxt) := parse_any_ty ctxt legacy uta in
    let? '(Ex_ty tr, ctxt) := parse_any_ty ctxt legacy utr in
    return? ((Ex_ty (Script_typed_ir.Lambda_t ta tr)), ctxt)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_option (cons ut []) _annot,
      _, _) =>
    let? '(Ex_ty t_value, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket ut in
    return? ((Ex_ty (Script_typed_ir.Option_t t_value)), ctxt)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_list (cons ut []) _annot, _,
      _) =>
    let? '(Ex_ty t_value, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket ut in
    return? ((Ex_ty (Script_typed_ir.List_t t_value)), ctxt)
  |
    (Micheline.Prim loc Michelson_v1_primitives.T_ticket (cons ut []) _annot, _,
      _) =>
    if allow_ticket then
      let? '(Ex_comparable_ty t_value, ctxt) := parse_comparable_ty ctxt ut in
      return? ((Ex_ty (Script_typed_ir.Ticket_t t_value)), ctxt)
    else
      Error_monad.error_value
        (Build_extensible "Unexpected_ticket" Alpha_context.Script.location loc)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_set (cons ut []) _annot, _, _)
    =>
    let? '(Ex_comparable_ty t_value, ctxt) := parse_comparable_ty ctxt ut in
    return? ((Ex_ty (Script_typed_ir.Set_t t_value)), ctxt)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_map (cons uta (cons utr []))
      _annot, _, _) =>
    let? '(Ex_comparable_ty ta, ctxt) := parse_comparable_ty ctxt uta in
    let? '(Ex_ty tr, ctxt) :=
      parse_ty ctxt legacy allow_lazy_storage allow_operation allow_contract
        allow_ticket utr in
    return? ((Ex_ty (Script_typed_ir.Map_t ta tr)), ctxt)
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_sapling_transaction
      (cons memo_size []) _annot, _, _) =>
    let? memo_size := parse_memo_size memo_size in
    return? ((Ex_ty (Script_typed_ir.Sapling_transaction_t memo_size)), ctxt)
  | (Micheline.Prim loc Michelson_v1_primitives.T_big_map args _annot, true, _)
    => parse_big_map_ty ctxt legacy loc args
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_sapling_state
      (cons memo_size []) _annot, _, true) =>
    let? memo_size := parse_memo_size memo_size in
    return? ((Ex_ty (Script_typed_ir.Sapling_state_t memo_size)), ctxt)
  |
    (Micheline.Prim loc
      (Michelson_v1_primitives.T_big_map |
      Michelson_v1_primitives.T_sapling_state) _ _, _, _) =>
    Error_monad.error_value
      (Build_extensible "Unexpected_lazy_storage" Alpha_context.Script.location
        loc)
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.T_unit | Michelson_v1_primitives.T_signature |
      Michelson_v1_primitives.T_int | Michelson_v1_primitives.T_nat |
      Michelson_v1_primitives.T_string | Michelson_v1_primitives.T_bytes |
      Michelson_v1_primitives.T_mutez | Michelson_v1_primitives.T_bool |
      Michelson_v1_primitives.T_key | Michelson_v1_primitives.T_key_hash |
      Michelson_v1_primitives.T_timestamp | Michelson_v1_primitives.T_address |
      Michelson_v1_primitives.T_chain_id | Michelson_v1_primitives.T_operation |
      Michelson_v1_primitives.T_never) as prim) l_value _, _, _) =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, prim, 0, (List.length l_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.T_set | Michelson_v1_primitives.T_list |
      Michelson_v1_primitives.T_option | Michelson_v1_primitives.T_contract |
      Michelson_v1_primitives.T_ticket) as prim) l_value _, _, _) =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, prim, 1, (List.length l_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.T_pair | Michelson_v1_primitives.T_or |
      Michelson_v1_primitives.T_map | Michelson_v1_primitives.T_lambda) as prim)
      l_value _, _, _) =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, prim, 2, (List.length l_value)))
  | (expr, _, _) =>
    Error_monad.error_value
      (unexpected expr nil Michelson_v1_primitives.Type_namespace
        [
          Michelson_v1_primitives.T_pair;
          Michelson_v1_primitives.T_or;
          Michelson_v1_primitives.T_set;
          Michelson_v1_primitives.T_map;
          Michelson_v1_primitives.T_list;
          Michelson_v1_primitives.T_option;
          Michelson_v1_primitives.T_lambda;
          Michelson_v1_primitives.T_unit;
          Michelson_v1_primitives.T_signature;
          Michelson_v1_primitives.T_contract;
          Michelson_v1_primitives.T_int;
          Michelson_v1_primitives.T_nat;
          Michelson_v1_primitives.T_operation;
          Michelson_v1_primitives.T_string;
          Michelson_v1_primitives.T_bytes;
          Michelson_v1_primitives.T_mutez;
          Michelson_v1_primitives.T_bool;
          Michelson_v1_primitives.T_key;
          Michelson_v1_primitives.T_key_hash;
          Michelson_v1_primitives.T_timestamp;
          Michelson_v1_primitives.T_chain_id;
          Michelson_v1_primitives.T_never;
          Michelson_v1_primitives.T_bls12_381_g1;
          Michelson_v1_primitives.T_bls12_381_g2;
          Michelson_v1_primitives.T_bls12_381_fr;
          Michelson_v1_primitives.T_ticket
        ])
  end

where "'parse_parameter_ty" :=
  (fun (ctxt : Alpha_context.context) (legacy : bool) =>
    parse_ty ctxt legacy true false true true)

and "'parse_any_ty" :=
  (fun (ctxt : Alpha_context.context) (legacy : bool) =>
    let parse_parameter_ty := 'parse_parameter_ty in
    parse_ty ctxt legacy true true true true)

and "'parse_big_map_value_ty" :=
  (fun
    (ctxt : Alpha_context.context) (legacy : bool)
    (value_ty : Alpha_context.Script.node) =>
    let parse_parameter_ty := 'parse_parameter_ty in
    let parse_any_ty := 'parse_any_ty in
    parse_ty ctxt legacy false false legacy true value_ty)

and "'parse_big_map_ty" :=
  (fun
    (ctxt : Alpha_context.context) (legacy : bool)
    (big_map_loc : Alpha_context.Script.location)
    (args :
      list
        (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim))
    =>
    let parse_parameter_ty := 'parse_parameter_ty in
    let parse_any_ty := 'parse_any_ty in
    let parse_big_map_value_ty := 'parse_big_map_value_ty in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
      in
    match args with
    | cons key_ty (cons value_ty []) =>
      let? '(Ex_comparable_ty key_ty, ctxt) := parse_comparable_ty ctxt key_ty
        in
      let? '(Ex_ty value_ty, ctxt) :=
        parse_big_map_value_ty ctxt legacy value_ty in
      let big_map_ty := Script_typed_ir.Big_map_t key_ty value_ty in
      return? ((Ex_ty big_map_ty), ctxt)
    | args =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (big_map_loc, Michelson_v1_primitives.T_big_map, 2, (List.length args)))
    end).

Definition parse_parameter_ty := 'parse_parameter_ty.
Definition parse_any_ty := 'parse_any_ty.
Definition parse_big_map_value_ty := 'parse_big_map_value_ty.
Definition parse_big_map_ty := 'parse_big_map_ty.

Definition parse_normal_storage_ty
  (ctxt : Alpha_context.context) (legacy : bool)
  : Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context) :=
  parse_ty ctxt legacy true false legacy true.

Definition parse_storage_ty
  (ctxt : Alpha_context.context) (legacy : bool)
  (node : Alpha_context.Script.node) : M? (ex_ty * Alpha_context.context) :=
  match
    (node,
      match node with
      |
        Micheline.Prim _loc Michelson_v1_primitives.T_pair
          (cons
            (Micheline.Prim big_map_loc Michelson_v1_primitives.T_big_map args
              _map_annot) (cons remaining_storage [])) storage_annot => legacy
      | _ => false
      end) with
  |
    (Micheline.Prim _loc Michelson_v1_primitives.T_pair
      (cons
        (Micheline.Prim big_map_loc Michelson_v1_primitives.T_big_map args
          _map_annot) (cons remaining_storage [])) storage_annot, true) =>
    match
      (storage_annot,
        match storage_annot with
        | cons single [] =>
          ((String.length single) >i 0) &&
          (Compare.Char.(Compare.S.op_eq) (String.get single 0) "%" % char)
        | _ => false
        end) with
    | ([], _) => parse_normal_storage_ty ctxt legacy node
    | (cons single [], true) => parse_normal_storage_ty ctxt legacy node
    | (_, _) =>
      let? ctxt :=
        Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
      let? '(Ex_ty big_map_ty, ctxt) :=
        parse_big_map_ty ctxt legacy big_map_loc args in
      let? '(Ex_ty remaining_storage, ctxt) :=
        parse_normal_storage_ty ctxt legacy remaining_storage in
      return?
        ((Ex_ty (Script_typed_ir.Pair_t big_map_ty remaining_storage)), ctxt)
    end
  | (_, _) => parse_normal_storage_ty ctxt legacy node
  end.

Definition parse_packable_ty (ctxt : Alpha_context.context) (legacy : bool)
  : Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context) :=
  parse_ty ctxt legacy false false legacy false.

Fixpoint parse_entrypoints
  (root_name : option Script_typed_ir.field_annot)
  (node : Alpha_context.Script.node) (arg_ty : Script_typed_ir.ty)
  : M? Script_typed_ir.entrypoints :=
  let? '(node, name) :=
    match root_name with
    | Some _ => return? (node, root_name)
    | None => Script_ir_annot.extract_field_annot node
    end in
  let? nested :=
    match (arg_ty, node) with
    |
      (Script_typed_ir.Union_t tl tr,
        Micheline.Prim _loc Michelson_v1_primitives.T_or
          (cons utl (cons utr [])) _annot) =>
      let? _left := parse_entrypoints None utl tl in
      let? _right := parse_entrypoints None utr tr in
      return?
        (Script_typed_ir.Entrypoints_Union
          {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._right :=
              _right |})
    | (Script_typed_ir.Union_t _ _, _) =>
      (* ❌ Assert instruction is not handled. *)
      assert (M? Script_typed_ir.nested_entrypoints) false
    | (_, _) => return? Script_typed_ir.Entrypoints_None
    end in
  return?
    {| Script_typed_ir.entrypoints.name := name;
      Script_typed_ir.entrypoints.nested := nested |}.

Definition check_packable
  (legacy : bool) (loc : Alpha_context.Script.location)
  (root : Script_typed_ir.ty) : M? unit :=
  let fix check (function_parameter : Script_typed_ir.ty) : M? unit :=
    match
      (function_parameter,
        match function_parameter with
        | Script_typed_ir.Contract_t _ => legacy
        | _ => false
        end) with
    | (Script_typed_ir.Big_map_t _ _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_lazy_storage"
          Alpha_context.Script.location loc)
    | (Script_typed_ir.Sapling_state_t _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_lazy_storage"
          Alpha_context.Script.location loc)
    | (Script_typed_ir.Operation_t, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_operation" Alpha_context.Script.location
          loc)
    | (Script_typed_ir.Unit_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Int_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Nat_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Signature_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.String_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bytes_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Mutez_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Key_hash_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Key_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Timestamp_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Address_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bool_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Chain_id_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Never_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Set_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Ticket_t _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_ticket" Alpha_context.Script.location loc)
    | (Script_typed_ir.Lambda_t _ _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bls12_381_g1_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bls12_381_g2_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bls12_381_fr_t, _) => Error_monad.ok_unit
    | (Script_typed_ir.Pair_t l_ty r_ty, _) =>
      let? '_ := check l_ty in
      check r_ty
    | (Script_typed_ir.Union_t l_ty r_ty, _) =>
      let? '_ := check l_ty in
      check r_ty
    | (Script_typed_ir.Option_t v_ty, _) => check v_ty
    | (Script_typed_ir.List_t elt_ty, _) => check elt_ty
    | (Script_typed_ir.Map_t _ elt_ty, _) => check elt_ty
    | (Script_typed_ir.Contract_t _, true) => Error_monad.ok_unit
    | (Script_typed_ir.Contract_t _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_contract" Alpha_context.Script.location
          loc)
    | (Script_typed_ir.Sapling_transaction_t _, _) => return? tt
    end in
  check root.

Module toplevel.
  Record record : Set := Build {
    code_field : Alpha_context.Script.node;
    arg_type : Alpha_context.Script.node;
    storage_type : Alpha_context.Script.node;
    root_name : option Script_typed_ir.field_annot }.
  Definition with_code_field code_field (r : record) :=
    Build code_field r.(arg_type) r.(storage_type) r.(root_name).
  Definition with_arg_type arg_type (r : record) :=
    Build r.(code_field) arg_type r.(storage_type) r.(root_name).
  Definition with_storage_type storage_type (r : record) :=
    Build r.(code_field) r.(arg_type) storage_type r.(root_name).
  Definition with_root_name root_name (r : record) :=
    Build r.(code_field) r.(arg_type) r.(storage_type) root_name.
End toplevel.
Definition toplevel := toplevel.record.

Module code.
  Record record : Set := Build {
    code : Script_typed_ir.lambda;
    arg_type : Script_typed_ir.ty;
    storage_type : Script_typed_ir.ty;
    entrypoints : Script_typed_ir.entrypoints }.
  Definition with_code code (r : record) :=
    Build code r.(arg_type) r.(storage_type) r.(entrypoints).
  Definition with_arg_type arg_type (r : record) :=
    Build r.(code) arg_type r.(storage_type) r.(entrypoints).
  Definition with_storage_type storage_type (r : record) :=
    Build r.(code) r.(arg_type) storage_type r.(entrypoints).
  Definition with_entrypoints entrypoints (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) entrypoints.
End code.
Definition code := code.record.

Inductive ex_script : Set :=
| Ex_script : forall {c : Set}, Script_typed_ir.script c -> ex_script.

Inductive ex_code : Set :=
| Ex_code : code -> ex_code.

Inductive dig_proof_argument : Set :=
| Dig_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness * Script_typed_ir.ty *
    Script_typed_ir.stack_ty -> dig_proof_argument.

Inductive dug_proof_argument : Set :=
| Dug_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness * unit *
    Script_typed_ir.stack_ty -> dug_proof_argument.

Inductive dipn_proof_argument : Set :=
| Dipn_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness *
    (Alpha_context.context * Script_typed_ir.descr) * Script_typed_ir.stack_ty
  -> dipn_proof_argument.

Inductive dropn_proof_argument : Set :=
| Dropn_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness * Script_typed_ir.stack_ty *
    Script_typed_ir.stack_ty -> dropn_proof_argument.

Inductive comb_proof_argument : Set :=
| Comb_proof_argument :
  Script_typed_ir.comb_gadt_witness -> Script_typed_ir.stack_ty ->
  comb_proof_argument.

Inductive uncomb_proof_argument : Set :=
| Uncomb_proof_argument :
  Script_typed_ir.uncomb_gadt_witness -> Script_typed_ir.stack_ty ->
  uncomb_proof_argument.

Inductive comb_get_proof_argument : Set :=
| Comb_get_proof_argument :
  Script_typed_ir.comb_get_gadt_witness -> Script_typed_ir.ty ->
  comb_get_proof_argument.

Inductive comb_set_proof_argument : Set :=
| Comb_set_proof_argument :
  Script_typed_ir.comb_set_gadt_witness -> Script_typed_ir.ty ->
  comb_set_proof_argument.

Inductive dup_n_proof_argument : Set :=
| Dup_n_proof_argument :
  Script_typed_ir.dup_n_gadt_witness -> Script_typed_ir.ty ->
  dup_n_proof_argument.

Definition find_entrypoint
  (full : Script_typed_ir.ty) (entrypoints : Script_typed_ir.entrypoints)
  (entrypoint : string)
  : M? ((Alpha_context.Script.node -> Alpha_context.Script.node) * ex_ty) :=
  let fix find_entrypoint
    (ty : Script_typed_ir.ty) (entrypoints : Script_typed_ir.entrypoints)
    (entrypoint : string)
    : (Alpha_context.Script.node -> Alpha_context.Script.node) * ex_ty :=
    match
      ((ty, entrypoints),
        match (ty, entrypoints) with
        |
          (_, {|
            Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot name)
              |}) => Compare.String.(Compare.S.op_eq) entrypoint name
        | _ => false
        end) with
    |
      ((_, {|
        Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot name)
          |}), true) => ((fun (e : Alpha_context.Script.node) => e), (Ex_ty ty))
    |
      ((Script_typed_ir.Union_t tl tr, {|
        Script_typed_ir.entrypoints.nested :=
          Script_typed_ir.Entrypoints_Union {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                := _right
              |}
          |}), _) =>
      match
        Misc.result_of_exception
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            find_entrypoint tl _left entrypoint) with
      | Pervasives.Ok (f, t_value) =>
        ((fun (e : Alpha_context.Script.node) =>
          Micheline.Prim 0 Michelson_v1_primitives.D_Left [ f e ] nil), t_value)
      | Pervasives.Error exn_value =>
        match exn_value with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Not_found" then
            let '(f, t_value) := find_entrypoint tr _right entrypoint in
            ((fun (e : Alpha_context.Script.node) =>
              Micheline.Prim 0 Michelson_v1_primitives.D_Right [ f e ] nil),
              t_value)
          else Pervasives.raise exn_value
        end
      end
    |
      ((_, {|
        Script_typed_ir.entrypoints.nested := Script_typed_ir.Entrypoints_None
          |}), _) => Pervasives.raise (Build_extensible "Not_found" unit tt)
    |
      ((_, {|
        Script_typed_ir.entrypoints.nested := Script_typed_ir.Entrypoints_Union _
          |}), _) =>
      (* ❌ Assert instruction is not handled. *)
      assert ((Alpha_context.Script.node -> Alpha_context.Script.node) * ex_ty)
        false
    end in
  let entrypoint :=
    if Compare.String.(Compare.S.op_eq) entrypoint "" then
      "default"
    else
      entrypoint in
  if (String.length entrypoint) >i 31 then
    Error_monad.error_value
      (Build_extensible "Entrypoint_name_too_long" string entrypoint)
  else
    match
      Misc.result_of_exception
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          find_entrypoint full entrypoints entrypoint) with
    | Pervasives.Ok f_t => return? f_t
    | Pervasives.Error exn_value =>
      match exn_value with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Not_found" then
          match entrypoint with
          | "default" =>
            return? ((fun (e : Alpha_context.Script.node) => e), (Ex_ty full))
          | _ =>
            Error_monad.error_value
              (Build_extensible "No_such_entrypoint" string entrypoint)
          end
        else Pervasives.raise exn_value
      end
    end.

Definition find_entrypoint_for_type
  (full : Script_typed_ir.ty) (expected : Script_typed_ir.ty)
  (entrypoints : Script_typed_ir.entrypoints) (entrypoint : string)
  (ctxt : Alpha_context.context)
  : M? (Alpha_context.context * string * Script_typed_ir.ty) :=
  let? '(_, Ex_ty ty) := find_entrypoint full entrypoints entrypoint in
  match ((ty_eq ctxt ty expected), entrypoint, entrypoints) with
  | (Pervasives.Ok (Eq, ctxt), _, _) => return? (ctxt, entrypoint, ty)
  |
    (Pervasives.Error _, "default", {|
      Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot "root")
        |}) =>
    let? '(Eq, ctxt) := ty_eq ctxt full expected in
    return? (ctxt, "root", full)
  | (Pervasives.Error err, _, _) => Pervasives.Error err
  end.

Definition Entrypoints :=
  _Set.Make
    {|
      Compare.COMPARABLE.compare := String.compare
    |}.

Definition well_formed_entrypoints
  (full : Script_typed_ir.ty) (entrypoints : Script_typed_ir.entrypoints)
  : M? unit :=
  let merge {A : Set}
    (path : list A) (ty : Script_typed_ir.ty)
    (entrypoints : Script_typed_ir.entrypoints) (reachable : bool)
    (function_parameter : option (list A) * Entrypoints.(S.SET.t))
    : (option (list A) * Entrypoints.(S.SET.t)) * bool :=
    let '(first_unreachable, all) as acc_value := function_parameter in
    match entrypoints with
    | {|
      Script_typed_ir.entrypoints.name :=
        (None | Some (Script_typed_ir.Field_annot ""))
        |} =>
      ((if reachable then
        acc_value
      else
        match ty with
        | Script_typed_ir.Union_t _ _ => acc_value
        | _ =>
          match first_unreachable with
          | None => ((Some (List.rev path)), all)
          | Some _ => acc_value
          end
        end), reachable)
    | {|
      Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot name)
        |} =>
      if (String.length name) >i 31 then
        Pervasives.raise (Build_extensible "Entrypoints_too_long" string name)
      else
        if Entrypoints.(S.SET.mem) name all then
          Pervasives.raise
            (Build_extensible "Entrypoints_duplicate" string name)
        else
          ((first_unreachable, (Entrypoints.(S.SET.add) name all)), true)
    end in
  let fix check
    (t_value : Script_typed_ir.ty) (entrypoints : Script_typed_ir.entrypoints)
    (path : list Alpha_context.Script.prim) (reachable : bool)
    (acc_value : option (list Alpha_context.Script.prim) * Entrypoints.(S.SET.t))
    : option (list Alpha_context.Script.prim) * Entrypoints.(S.SET.t) :=
    match (t_value, entrypoints) with
    |
      (Script_typed_ir.Union_t tl tr, {|
        Script_typed_ir.entrypoints.nested :=
          Script_typed_ir.Entrypoints_Union {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                := _right
              |}
          |}) =>
      let '(acc_value, l_reachable) :=
        merge (cons Michelson_v1_primitives.D_Left path) tl _left reachable
          acc_value in
      let '(acc_value, r_reachable) :=
        merge (cons Michelson_v1_primitives.D_Right path) tr _right reachable
          acc_value in
      let acc_value :=
        check tl _left (cons Michelson_v1_primitives.D_Left path) l_reachable
          acc_value in
      check tr _right (cons Michelson_v1_primitives.D_Right path) r_reachable
        acc_value
    | _ => acc_value
    end in
  let '(init_value, reachable) :=
    match entrypoints with
    | {|
      Script_typed_ir.entrypoints.name :=
        (None | Some (Script_typed_ir.Field_annot ""))
        |} => (Entrypoints.(S.SET.empty), false)
    | {|
      Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot name)
        |} => ((Entrypoints.(S.SET.singleton) name), true)
    end in
  match
    Misc.result_of_exception
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        check full entrypoints nil reachable (None, init_value)) with
  | Pervasives.Ok (first_unreachable, all) =>
    if Pervasives.not (Entrypoints.(S.SET.mem) "default" all) then
      Error_monad.ok_unit
    else
      match first_unreachable with
      | None => Error_monad.ok_unit
      | Some path =>
        Error_monad.error_value
          (Build_extensible "Unreachable_entrypoint"
            (list Alpha_context.Script.prim) path)
      end
  | Pervasives.Error exn_value =>
    match exn_value with
    | Build_extensible tag _ payload =>
      if String.eqb tag "Entrypoints_duplicate" then
        let 'name := cast string payload in
        Error_monad.error_value
          (Build_extensible "Duplicate_entrypoint" string name)
      else if String.eqb tag "Entrypoints_too_long" then
        let 'name := cast string payload in
        Error_monad.error_value
          (Build_extensible "Entrypoint_name_too_long" string name)
      else Pervasives.raise exn_value
    end
  end.

Definition parse_parameter_ty_and_entrypoints
  (ctxt : Alpha_context.context) (legacy : bool)
  (root_name : option Script_typed_ir.field_annot)
  (node : Alpha_context.Script.node)
  : M? (ex_parameter_ty_and_entrypoints * Alpha_context.context) :=
  let? '(Ex_ty arg_ty, ctxt) := parse_parameter_ty ctxt legacy node in
  let? entrypoints := parse_entrypoints root_name node arg_ty in
  let? '_ :=
    if legacy then
      Error_monad.ok_unit
    else
      well_formed_entrypoints arg_ty entrypoints in
  return? ((Ex_parameter_ty_entrypoints arg_ty entrypoints), ctxt).

Definition parse_uint (nb_bits : int)
  : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? int :=
  let '_ :=
    (* ❌ Assert instruction is not handled. *)
    assert unit ((nb_bits >=i 0) && (nb_bits <=i 30)) in
  let max_int := (Pervasives.lsl 1 nb_bits) -i 1 in
  let max_z := Z.of_int max_int in
  fun (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim) =>
    match
      (function_parameter,
        match function_parameter with
        | Micheline.Int _ n => (Z.zero <=Z n) && (n <=Z max_z)
        | _ => false
        end) with
    | (Micheline.Int _ n, true) => return? (Z.to_int n)
    | (node, _) =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          ((location node), (Micheline.strip_locations node),
            (Pervasives.op_caret "a positive "
              (Pervasives.op_caret (Pervasives.string_of_int nb_bits)
                (Pervasives.op_caret "-bit integer (between 0 and "
                  (Pervasives.op_caret (Pervasives.string_of_int max_int) ")"))))))
    end.

Definition parse_uint10
  : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? int := parse_uint 10.

Definition parse_uint11
  : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? int := parse_uint 11.

Definition opened_ticket_type (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.comparable_ty :=
  Script_typed_ir.Pair_key Script_typed_ir.Address_key
    (Script_typed_ir.Pair_key ty Script_typed_ir.Nat_key).

Definition parse_unit
  (ctxt : Alpha_context.context) (legacy : bool)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? (unit * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_Unit [] annot =>
    let? '_ :=
      if legacy then
        Error_monad.ok_unit
      else
        Script_ir_annot.error_unexpected_annot loc annot in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.unit_value in
    return? (tt, ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Unit l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Unit, 0, (List.length l_value)))
  | expr =>
    Error_monad.error_value
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Unit ])
  end.

Definition parse_bool
  (ctxt : Alpha_context.context) (legacy : bool)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? (bool * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_True [] annot =>
    let? '_ :=
      if legacy then
        Error_monad.ok_unit
      else
        Script_ir_annot.error_unexpected_annot loc annot in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bool_value in
    return? (true, ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_False [] annot =>
    let? '_ :=
      if legacy then
        Error_monad.ok_unit
      else
        Script_ir_annot.error_unexpected_annot loc annot in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bool_value in
    return? (false, ctxt)
  |
    Micheline.Prim loc
      ((Michelson_v1_primitives.D_True | Michelson_v1_primitives.D_False) as c)
      l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, c, 0, (List.length l_value)))
  | expr =>
    Error_monad.error_value
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_True; Michelson_v1_primitives.D_False ])
  end.

Fixpoint check_printable_ascii_aux (i : int) (s : string) {struct i} : bool :=
  if i <i 0 then
    true
  else
    match String.get s i with
    |
      ("010" % char | " " % char | "!" % char | """" % char | "#" % char |
      "$" % char | "%" % char | "&" % char | "'" % char | "(" % char |
      ")" % char | "*" % char | "+" % char | "," % char | "-" % char |
      "." % char | "/" % char | "0" % char | "1" % char | "2" % char |
      "3" % char | "4" % char | "5" % char | "6" % char | "7" % char |
      "8" % char | "9" % char | ":" % char | ";" % char | "<" % char |
      "=" % char | ">" % char | "?" % char | "@" % char | "A" % char |
      "B" % char | "C" % char | "D" % char | "E" % char | "F" % char |
      "G" % char | "H" % char | "I" % char | "J" % char | "K" % char |
      "L" % char | "M" % char | "N" % char | "O" % char | "P" % char |
      "Q" % char | "R" % char | "S" % char | "T" % char | "U" % char |
      "V" % char | "W" % char | "X" % char | "Y" % char | "Z" % char |
      "[" % char | "\" % char | "]" % char | "^" % char | "_" % char |
      "`" % char | "a" % char | "b" % char | "c" % char | "d" % char |
      "e" % char | "f" % char | "g" % char | "h" % char | "i" % char |
      "j" % char | "k" % char | "l" % char | "m" % char | "n" % char |
      "o" % char | "p" % char | "q" % char | "r" % char | "s" % char |
      "t" % char | "u" % char | "v" % char | "w" % char | "x" % char |
      "y" % char | "z" % char | "{" % char | "|" % char | "}" % char |
      "~" % char) => check_printable_ascii_aux (i -i 1) s
    | _ => false
    end.

Definition check_printable_ascii (s : string) : bool :=
  check_printable_ascii_aux ((String.length s) -i 1) s.

Definition parse_string
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (string * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.String loc v) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.check_printable v) in
    if check_printable_ascii v then
      return? (v, ctxt)
    else
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a printable ascii string"))
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.String_kind ], (kind_value expr)))
  end.

Definition parse_bytes {A B : Set}
  (ctxt : A)
  (function_parameter : Micheline.node Alpha_context.Script.location B)
  : M? (bytes * A) :=
  match function_parameter with
  | Micheline.Bytes _ v => return? (v, ctxt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Bytes_kind ], (kind_value expr)))
  end.

Definition parse_int {A B : Set}
  (ctxt : A)
  (function_parameter : Micheline.node Alpha_context.Script.location B)
  : M? (Alpha_context.Script_int.num * A) :=
  match function_parameter with
  | Micheline.Int _ v => return? ((Alpha_context.Script_int.of_zint v), ctxt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
  end.

Definition parse_nat {A : Set}
  (ctxt : A) (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script_int.num * A) :=
  match function_parameter with
  | (Micheline.Int loc v) as expr =>
    let v := Alpha_context.Script_int.of_zint v in
    match Alpha_context.Script_int.is_nat v with
    | Some nat => return? (nat, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a non-negative integer"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
  end.

Definition parse_mutez {A : Set}
  (ctxt : A) (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Tez.tez * A) :=
  match function_parameter with
  | (Micheline.Int loc v) as expr =>
    let mk_err {B : Set} (function_parameter : unit) : M? B :=
      let '_ := function_parameter in
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid mutez amount")) in
    (* ❌ Try-with are not handled *)
    try_with
      (fun _ =>
        match Alpha_context.Tez.of_mutez (Z.to_int64 v) with
        | None => mk_err tt
        | Some tez => return? (tez, ctxt)
        end) (fun _ => mk_err tt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
  end.

Definition parse_timestamp
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script_timestamp.t * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Int _ v =>
    return? ((Alpha_context.Script_timestamp.of_zint v), ctxt)
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.timestamp_readable in
    match Alpha_context.Script_timestamp.of_string s with
    | Some v => return? (v, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid timestamp"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Int_kind ],
          (kind_value expr)))
  end.

Definition parse_key
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Signature.public_key * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.public_key_optimized in
    match
      Data_encoding.Binary.of_bytes
        Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding) bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid public key"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.public_key_readable in
    match Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.of_b58check_opt) s with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid public key"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_key_hash
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Signature.public_key_hash * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.key_hash_optimized in
    match
      Data_encoding.Binary.of_bytes
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
        bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid key hash"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.key_hash_readable in
    match
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_b58check_opt) s
      with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid key hash"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_signature
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Signature.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.signature_optimized in
    match Data_encoding.Binary.of_bytes Signature.encoding bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid signature"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.signature_readable in
    match Signature.of_b58check_opt s with
    | Some s => return? (s, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid signature"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_chain_id
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Chain_id.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.chain_id_optimized in
    match Data_encoding.Binary.of_bytes Chain_id.encoding bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid chain id"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.chain_id_readable in
    match Chain_id.of_b58check_opt s with
    | Some s => return? (s, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid chain id"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_address
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? ((Alpha_context.Contract.t * string) * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.contract in
    match
      Data_encoding.Binary.of_bytes
        (Data_encoding.tup2 Alpha_context.Contract.encoding
          Data_encoding._Variable.string_value) bytes_value with
    | Some (c, entrypoint) =>
      if (String.length entrypoint) >i 31 then
        Error_monad.error_value
          (Build_extensible "Entrypoint_name_too_long" string entrypoint)
      else
        match entrypoint with
        | "" => return? ((c, "default"), ctxt)
        | "default" =>
          Error_monad.error_value
            (Build_extensible "Unexpected_annotation"
              Alpha_context.Script.location loc)
        | name => return? ((c, name), ctxt)
        end
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid address"))
    end
  | Micheline.String loc s =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.contract in
    let? '(addr, entrypoint) :=
      match String.index_opt s "%" % char with
      | None => return? (s, "default")
      | Some pos =>
        let len := ((String.length s) -i pos) -i 1 in
        let name := String.sub s (pos +i 1) len in
        if len >i 31 then
          Error_monad.error_value
            (Build_extensible "Entrypoint_name_too_long" string name)
        else
          match ((String.sub s 0 pos), name) with
          | (addr, "") => return? (addr, "default")
          | (_, "default") =>
            Error_monad.error_value
              (Build_extensible "Unexpected_annotation"
                Alpha_context.Script.location loc)
          | addr_and_name => return? addr_and_name
          end
      end in
    let? c := Alpha_context.Contract.of_b58check addr in
    return? ((c, entrypoint), ctxt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_never {A : Set}
  (expr : Micheline.node Alpha_context.Script.location A)
  : M? (Script_typed_ir.never * Alpha_context.context) :=
  Error_monad.error_value
    (Build_extensible "Invalid_never_expr" Alpha_context.Script.location
      (location expr)).

Inductive comb_witness : Set :=
| Comb_Pair : comb_witness -> comb_witness
| Comb_Any : comb_witness.

Definition parse_pair {l r : Set}
  (parse_l :
    Alpha_context.context -> Alpha_context.Script.node ->
    M=? (l * Alpha_context.context))
  (parse_r :
    Alpha_context.context -> Alpha_context.Script.node ->
    M=? (r * Alpha_context.context)) (ctxt : Alpha_context.context)
  (legacy : bool) (r_comb_witness : comb_witness)
  (expr : Alpha_context.Script.node) : M=? ((l * r) * Alpha_context.context) :=
  let parse_comb
    (loc : Alpha_context.Script.location) (l_value : Alpha_context.Script.node)
    (rs : list Alpha_context.Script.node)
    : M=? ((l * r) * Alpha_context.context) :=
    let=? '(l_value, ctxt) := parse_l ctxt l_value in
    let=? r_value :=
      match (rs, r_comb_witness) with
      | (cons r_value [], _) => return=? r_value
      | ([], _) =>
        return=
          (Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int) (loc, Michelson_v1_primitives.D_Pair, 2, 1)))
      | (cons _ _, Comb_Pair _) =>
        return=? (Micheline.Prim loc Michelson_v1_primitives.D_Pair rs nil)
      | _ =>
        return=
          (Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int)
              (loc, Michelson_v1_primitives.D_Pair, 2, (1 +i (List.length rs)))))
      end in
    let=? '(r_value, ctxt) := parse_r ctxt r_value in
    return=? ((l_value, r_value), ctxt) in
  match expr with
  | Micheline.Prim loc Michelson_v1_primitives.D_Pair (cons l_value rs) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    parse_comb loc l_value rs
  | Micheline.Prim loc Michelson_v1_primitives.D_Pair l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Pair, 2, (List.length l_value)))
  | Micheline.Seq loc (cons l_value ((cons _ _) as rs)) =>
    parse_comb loc l_value rs
  | Micheline.Seq loc l_value =>
    Error_monad.fail
      (Build_extensible "Invalid_seq_arity"
        (Alpha_context.Script.location * int * int)
        (loc, 2, (List.length l_value)))
  | expr =>
    Error_monad.fail
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Pair ])
  end.

Definition parse_union {l r : Set}
  (parse_l :
    Alpha_context.context -> Alpha_context.Script.node ->
    M=? (l * Alpha_context.context))
  (parse_r :
    Alpha_context.context -> Alpha_context.Script.node ->
    M=? (r * Alpha_context.context)) (ctxt : Alpha_context.context)
  (legacy : bool) (function_parameter : Alpha_context.Script.node)
  : M=? (Script_typed_ir.union l r * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_Left (cons v []) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(v, ctxt) := parse_l ctxt v in
    return=? ((Script_typed_ir.L v), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Left l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Left, 1, (List.length l_value)))
  | Micheline.Prim loc Michelson_v1_primitives.D_Right (cons v []) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(v, ctxt) := parse_r ctxt v in
    return=? ((Script_typed_ir.R v), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Right l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Right, 1, (List.length l_value)))
  | expr =>
    Error_monad.fail
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Left; Michelson_v1_primitives.D_Right ])
  end.

Definition parse_option {t : Set}
  (parse_v :
    Alpha_context.context -> Alpha_context.Script.node ->
    M=? (t * Alpha_context.context)) (ctxt : Alpha_context.context)
  (legacy : bool) (function_parameter : Alpha_context.Script.node)
  : M=? (option t * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_Some (cons v []) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(v, ctxt) := parse_v ctxt v in
    return=? ((Some v), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Some l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Some, 1, (List.length l_value)))
  | Micheline.Prim loc Michelson_v1_primitives.D_None [] annot =>
    Lwt._return
      (let? '_ :=
        if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot in
      return? (None, ctxt))
  | Micheline.Prim loc Michelson_v1_primitives.D_None l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_None, 0, (List.length l_value)))
  | expr =>
    Error_monad.fail
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Some; Michelson_v1_primitives.D_None ])
  end.

Definition comparable_comb_witness1
  (function_parameter : Script_typed_ir.comparable_ty) : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_key _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Fixpoint parse_comparable_data {a : Set}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (ty : Script_typed_ir.comparable_ty) (script_data : Alpha_context.Script.node)
  (ignore : a -> unit) {struct ty} : M=? (a * Alpha_context.context) :=
  let parse_data_error (function_parameter : unit) : M? Error_monad._error :=
    let '_ := function_parameter in
    let? '(ty, _ctxt) := serialize_ty_for_error ctxt (ty_of_comparable_ty ty) in
    return?
      (Build_extensible "Invalid_constant"
        (Alpha_context.Script.location *
          Micheline.canonical Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim)
        ((location script_data), (Micheline.strip_locations script_data), ty))
    in
  let traced_no_lwt {B : Set} (body : M? B) : M? B :=
    Error_monad.record_trace_eval parse_data_error body in
  let traced {B : Set} (body : M=? B) : M=? B :=
    Error_monad.trace_eval
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Lwt._return (parse_data_error tt)) body in
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Typecheck_costs.parse_data_cycle) in
  let legacy := false in
  match (ty, script_data, ignore) with
  | (Script_typed_ir.Unit_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_unit ctxt legacy expr)))
  
  | (Script_typed_ir.Bool_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_bool ctxt legacy expr)))
  
  | (Script_typed_ir.String_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_string ctxt expr)))
  
  | (Script_typed_ir.Bytes_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_bytes ctxt expr)))
  
  | (Script_typed_ir.Int_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_int ctxt expr)))
  
  | (Script_typed_ir.Nat_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_nat ctxt expr)))
  
  | (Script_typed_ir.Mutez_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_mutez ctxt expr)))
  
  | (Script_typed_ir.Timestamp_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_timestamp ctxt expr)))
  
  | (Script_typed_ir.Key_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_key ctxt expr)))
  
  | (Script_typed_ir.Key_hash_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_key_hash ctxt expr)))
  
  | (Script_typed_ir.Signature_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_signature ctxt expr)))
  
  | (Script_typed_ir.Chain_id_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_chain_id ctxt expr)))
  
  | (Script_typed_ir.Address_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_address ctxt expr)))
  
  | (Script_typed_ir.Pair_key tl tr, expr, _ignore) =>
    let 'existT _ [__0, __1] [_ignore, expr, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__0, __1] =>
          [__0 * __1 -> unit ** Alpha_context.Script.node **
            Script_typed_ir.comparable_ty ** Script_typed_ir.comparable_ty])
        [_ignore, expr, tr, tl] in
    cast (M=? (a * Alpha_context.context))
    (let r_witness := comparable_comb_witness1 tr in
    let parse_l (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__0 * Alpha_context.context) :=
      parse_comparable_data type_logger_value ctxt tl v
        (fun (function_parameter : __0) =>
          let '_ := function_parameter in
          tt) in
    let parse_r (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__1 * Alpha_context.context) :=
      parse_comparable_data type_logger_value ctxt tr v
        (fun (function_parameter : __1) =>
          let '_ := function_parameter in
          tt) in
    traced (parse_pair parse_l parse_r ctxt legacy r_witness expr))
  
  | (Script_typed_ir.Union_key tl tr, expr, _ignore) =>
    let 'existT _ [__2, __3] [_ignore, expr, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__2, __3] =>
          [Script_typed_ir.union __2 __3 -> unit ** Alpha_context.Script.node **
            Script_typed_ir.comparable_ty ** Script_typed_ir.comparable_ty])
        [_ignore, expr, tr, tl] in
    cast (M=? (a * Alpha_context.context))
    (let parse_l (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__2 * Alpha_context.context) :=
      parse_comparable_data type_logger_value ctxt tl v
        (fun (function_parameter : __2) =>
          let '_ := function_parameter in
          tt) in
    let parse_r (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__3 * Alpha_context.context) :=
      parse_comparable_data type_logger_value ctxt tr v
        (fun (function_parameter : __3) =>
          let '_ := function_parameter in
          tt) in
    traced (parse_union parse_l parse_r ctxt legacy expr))
  
  | (Script_typed_ir.Option_key t_value, expr, _ignore) =>
    let 'existT _ __4 [_ignore, expr, t_value] :=
      cast_exists (Es := Set)
        (fun __4 =>
          [option __4 -> unit ** Alpha_context.Script.node **
            Script_typed_ir.comparable_ty]) [_ignore, expr, t_value] in
    cast (M=? (a * Alpha_context.context))
    (let parse_v (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__4 * Alpha_context.context) :=
      parse_comparable_data type_logger_value ctxt t_value v
        (fun (function_parameter : __4) =>
          let '_ := function_parameter in
          tt) in
    traced (parse_option parse_v ctxt legacy expr))
  
  | (Script_typed_ir.Never_key, expr, _) =>
    let expr := cast Alpha_context.Script.node expr in
    cast (M=? (a * Alpha_context.context))
    (Lwt._return (traced_no_lwt (parse_never expr)))
  end.

Definition comb_witness1 (function_parameter : Script_typed_ir.ty)
  : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_t _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Definition parse_toplevel
  (legacy : bool) (toplevel_value : Alpha_context.Script.expr) : M? toplevel :=
  Error_monad.record_trace
    (Build_extensible "Ill_typed_contract"
      (Alpha_context.Script.expr * Script_tc_errors.type_map)
      (toplevel_value, nil))
    match Micheline.root toplevel_value with
    | Micheline.Int loc _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Int_kind))
    | Micheline.String loc _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.String_kind))
    | Micheline.Bytes loc _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Bytes_kind))
    | Micheline.Prim loc _ _ _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Prim_kind))
    | Micheline.Seq _ fields =>
      let fix find_fields
        (p_value :
          option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot))
        (s :
          option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot))
        (c :
          option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot))
        (fields :
          list
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim))
        : M?
          (option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot) *
            option
              (Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim * Alpha_context.Script.location *
                Micheline.annot) *
            option
              (Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim * Alpha_context.Script.location *
                Micheline.annot)) :=
        match fields with
        | [] => return? (p_value, s, c)
        | cons (Micheline.Int loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.Int_kind))
        | cons (Micheline.String loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.String_kind))
        | cons (Micheline.Bytes loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.Bytes_kind))
        | cons (Micheline.Seq loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.Seq_kind))
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_parameter
              (cons arg []) annot) rest =>
          match p_value with
          | None => find_fields (Some (arg, loc, annot)) s c rest
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Duplicate_field"
                (Alpha_context.Script.location * Alpha_context.Script.prim)
                (loc, Michelson_v1_primitives.K_parameter))
          end
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_storage (cons arg [])
              annot) rest =>
          match s with
          | None => find_fields p_value (Some (arg, loc, annot)) c rest
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Duplicate_field"
                (Alpha_context.Script.location * Alpha_context.Script.prim)
                (loc, Michelson_v1_primitives.K_storage))
          end
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_code (cons arg [])
              annot) rest =>
          match c with
          | None => find_fields p_value s (Some (arg, loc, annot)) rest
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Duplicate_field"
                (Alpha_context.Script.location * Alpha_context.Script.prim)
                (loc, Michelson_v1_primitives.K_code))
          end
        |
          cons
            (Micheline.Prim loc
              ((Michelson_v1_primitives.K_parameter |
              Michelson_v1_primitives.K_storage | Michelson_v1_primitives.K_code)
                as name) args _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int) (loc, name, 1, (List.length args)))
        | cons (Micheline.Prim loc name _ _) _ =>
          let allowed :=
            [
              Michelson_v1_primitives.K_parameter;
              Michelson_v1_primitives.K_storage;
              Michelson_v1_primitives.K_code
            ] in
          Error_monad.error_value
            (Build_extensible "Invalid_primitive"
              (Alpha_context.Script.location * list Alpha_context.Script.prim *
                Alpha_context.Script.prim) (loc, allowed, name))
        end in
      let? function_parameter := find_fields None None None fields in
      match function_parameter with
      | (None, _, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_parameter)
      | (Some _, None, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_storage)
      | (Some _, Some _, None) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_code)
      |
        (Some (p_value, ploc, pannot), Some (s, sloc, sannot),
          Some (c, cloc, carrot)) =>
        let maybe_root_name :=
          let? '(p_value, root_name) :=
            Script_ir_annot.extract_field_annot p_value in
          match root_name with
          | Some _ => return? (p_value, pannot, root_name)
          | None =>
            match
              (pannot,
                match pannot with
                | cons single [] =>
                  ((String.length single) >i 0) &&
                  (Compare.Char.(Compare.S.op_eq) (String.get single 0)
                    "%" % char)
                | _ => false
                end) with
            | (cons single [], true) =>
              let? pannot := Script_ir_annot.parse_field_annot ploc single in
              return? (p_value, nil, pannot)
            | (_, _) => return? (p_value, pannot, None)
            end
          end in
        let? '(arg_type, root_name) :=
          if legacy then
            match maybe_root_name with
            | Pervasives.Ok (p_value, _, root_name) =>
              return? (p_value, root_name)
            | Pervasives.Error _ => return? (p_value, None)
            end
          else
            let? '(p_value, pannot, root_name) := maybe_root_name in
            let? '_ := Script_ir_annot.error_unexpected_annot ploc pannot in
            let? '_ := Script_ir_annot.error_unexpected_annot cloc carrot in
            let? '_ := Script_ir_annot.error_unexpected_annot sloc sannot in
            return? (p_value, root_name) in
        return?
          {| toplevel.code_field := c; toplevel.arg_type := arg_type;
            toplevel.storage_type := s; toplevel.root_name := root_name |}
      end
    end.

Definition parse_contract
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (arg : Script_typed_ir.ty) (contract : Alpha_context.Contract.t)
  (entrypoint : string)
  : M=? (Alpha_context.context * Script_typed_ir.typed_contract) :=
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Typecheck_costs.contract_exists) in
  let=? function_parameter := Alpha_context.Contract._exists ctxt contract in
  match function_parameter with
  | false =>
    Error_monad.fail
      (Build_extensible "Invalid_contract"
        (Alpha_context.Script.location * Alpha_context.Contract.t)
        (loc, contract))
  | true =>
    let=? '(ctxt, code) :=
      Error_monad.trace_value
        (Build_extensible "Invalid_contract"
          (Alpha_context.Script.location * Alpha_context.Contract.t)
          (loc, contract))
        (Alpha_context.Contract.get_script_code ctxt contract) in
    Lwt._return
      match code with
      | None =>
        let? '(Eq, ctxt) := ty_eq ctxt arg Script_typed_ir.Unit_t in
        match entrypoint with
        | "default" =>
          let contract := (arg, (contract, entrypoint)) in
          return? (ctxt, contract)
        | entrypoint =>
          Error_monad.error_value
            (Build_extensible "No_such_entrypoint" string entrypoint)
        end
      | Some code =>
        let? '(code, ctxt) :=
          Alpha_context.Script.force_decode_in_context ctxt code in
        let? '{|
          toplevel.arg_type := arg_type; toplevel.root_name := root_name |} :=
          parse_toplevel true code in
        let? '(Ex_parameter_ty_entrypoints targ entrypoints, ctxt) :=
          parse_parameter_ty_and_entrypoints ctxt true root_name arg_type in
        let? '(ctxt, entrypoint, arg) :=
          find_entrypoint_for_type targ arg entrypoints entrypoint ctxt in
        let contract := (arg, (contract, entrypoint)) in
        return? (ctxt, contract)
      end
  end.

Definition parse_contract_for_script
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (arg : Script_typed_ir.ty) (contract : Alpha_context.Contract.t)
  (entrypoint : string)
  : M=? (Alpha_context.context * option Script_typed_ir.typed_contract) :=
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Typecheck_costs.contract_exists) in
  match ((Alpha_context.Contract.is_implicit contract), entrypoint) with
  | (Some _, "default") =>
    Lwt._return
      match ty_eq ctxt arg Script_typed_ir.Unit_t with
      | Pervasives.Ok (Eq, ctxt) =>
        let contract := (arg, (contract, entrypoint)) in
        return? (ctxt, (Some contract))
      | Pervasives.Error _ =>
        let? ctxt :=
          Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
        return? (ctxt, None)
      end
  | (Some _, _) =>
    Lwt._return
      (let? ctxt :=
        Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
      return? (ctxt, None))
  | (None, _) =>
    let=? function_parameter := Alpha_context.Contract._exists ctxt contract in
    match function_parameter with
    | false => Error_monad._return (ctxt, None)
    | true =>
      let=? '(ctxt, code) :=
        Error_monad.trace_value
          (Build_extensible "Invalid_contract"
            (Alpha_context.Script.location * Alpha_context.Contract.t)
            (loc, contract))
          (Alpha_context.Contract.get_script_code ctxt contract) in
      match code with
      | None =>
        (* ❌ Assert instruction is not handled. *)
        assert
          (M=? (Alpha_context.context * option Script_typed_ir.typed_contract))
          false
      | Some code =>
        Lwt._return
          (let? '(code, ctxt) :=
            Alpha_context.Script.force_decode_in_context ctxt code in
          match parse_toplevel true code with
          | Pervasives.Error _ =>
            Error_monad.error_value
              (Build_extensible "Invalid_contract"
                (Alpha_context.Script.location * Alpha_context.Contract.t)
                (loc, contract))
          |
            Pervasives.Ok {|
              toplevel.arg_type := arg_type;
                toplevel.root_name := root_name
                |} =>
            match
              parse_parameter_ty_and_entrypoints ctxt true root_name arg_type
              with
            | Pervasives.Error _ =>
              Error_monad.error_value
                (Build_extensible "Invalid_contract"
                  (Alpha_context.Script.location * Alpha_context.Contract.t)
                  (loc, contract))
            | Pervasives.Ok (Ex_parameter_ty_entrypoints targ entrypoints, ctxt)
              =>
              let res :=
                let? '(ctxt, entrypoint, arg) :=
                  find_entrypoint_for_type targ arg entrypoints entrypoint ctxt
                  in
                let contract := (arg, (contract, entrypoint)) in
                return? (ctxt, (Some contract)) in
              match res with
              | Pervasives.Ok res => return? res
              | Pervasives.Error _ =>
                let? '(Eq, ctxt) := ty_eq ctxt targ targ in
                return? (ctxt, None)
              end
            end
          end)
      end
    end
  end.

Reserved Notation "'parse_returning".

Fixpoint parse_data_with_stack {a : Set}
  (type_logger_value : option type_logger) (stack_depth : int)
  (ctxt : Alpha_context.context) (legacy : bool) (allow_forged : bool)
  (ty : Script_typed_ir.ty) (script_data : Alpha_context.Script.node)
  {struct ty} : M=? (a * Alpha_context.context) :=
  let parse_returning := 'parse_returning in
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Typecheck_costs.parse_data_cycle) in
  let non_terminal_recursion {B : Set}
    (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
    (legacy : bool) (ty : Script_typed_ir.ty)
    (script_data : Alpha_context.Script.node)
    : M=? (B * Alpha_context.context) :=
    if stack_depth >i 10000 then
      Error_monad.fail
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    else
      parse_data_with_stack type_logger_value (stack_depth +i 1) ctxt legacy
        allow_forged ty script_data in
  let parse_data_error (function_parameter : unit) : M? Error_monad._error :=
    let '_ := function_parameter in
    let? '(ty, _ctxt) := serialize_ty_for_error ctxt ty in
    return?
      (Build_extensible "Invalid_constant"
        (Alpha_context.Script.location *
          Micheline.canonical Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim)
        ((location script_data), (Micheline.strip_locations script_data), ty))
    in
  let fail_parse_data {B : Set} (function_parameter : unit) : M=? B :=
    let '_ := function_parameter in
    Error_monad.op_gtgtquestioneq (parse_data_error tt) Error_monad.fail in
  let traced_no_lwt {B : Set} (body : M? B) : M? B :=
    Error_monad.record_trace_eval parse_data_error body in
  let traced {B : Set} (body : M=? B) : M=? B :=
    Error_monad.trace_eval
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Lwt._return (parse_data_error tt)) body in
  let traced_fail {B : Set} (err : Error_monad._error) : M=? B :=
    Lwt._return (traced_no_lwt (Error_monad.error_value err)) in
  let parse_items {B C D : Set}
    (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
    (expr : Alpha_context.Script.node)
    (key_type : Script_typed_ir.comparable_ty) (value_type : Script_typed_ir.ty)
    (items : list Alpha_context.Script.node) (item_wrapper : B -> C)
    : M=? (Script_typed_ir.map D C * Alpha_context.context) :=
    let=? '(_, items, ctxt) :=
      traced
        (Error_monad.fold_left_s
          (fun (function_parameter :
            option D * Script_typed_ir.map D C * Alpha_context.context) =>
            let '(last_value, map, ctxt) := function_parameter in
            fun (item :
              Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim) =>
              match item with
              |
                Micheline.Prim loc Michelson_v1_primitives.D_Elt
                  (cons k (cons v [])) annot =>
                let=? '_ :=
                  return=
                    (if legacy then
                      Error_monad.ok_unit
                    else
                      Script_ir_annot.error_unexpected_annot loc annot) in
                let=? '(k, ctxt) :=
                  parse_comparable_data type_logger_value ctxt key_type k
                    (fun (function_parameter : D) =>
                      let '_ := function_parameter in
                      tt) in
                let=? '(v, ctxt) :=
                  non_terminal_recursion type_logger_value ctxt legacy
                    value_type v in
                Lwt._return
                  (let? ctxt :=
                    match last_value with
                    | Some value =>
                      let? ctxt :=
                        Alpha_context.Gas.consume ctxt
                          (Michelson_v1_gas.Cost_of.Interpreter.compare key_type
                            value k) in
                      let c := compare_comparable key_type value k in
                      if 0 <=i c then
                        if 0 =i c then
                          Error_monad.error_value
                            (Build_extensible "Duplicate_map_keys"
                              (Alpha_context.Script.location *
                                Micheline.canonical Alpha_context.Script.prim)
                              (loc, (Micheline.strip_locations expr)))
                        else
                          Error_monad.error_value
                            (Build_extensible "Unordered_map_keys"
                              (Alpha_context.Script.location *
                                Micheline.canonical Alpha_context.Script.prim)
                              (loc, (Micheline.strip_locations expr)))
                      else
                        return? ctxt
                    | None => return? ctxt
                    end in
                  let? ctxt :=
                    Alpha_context.Gas.consume ctxt
                      (Michelson_v1_gas.Cost_of.Interpreter.map_update k map) in
                  return?
                    ((Some k), (map_update k (Some (item_wrapper v)) map), ctxt))
              | Micheline.Prim loc Michelson_v1_primitives.D_Elt l_value _ =>
                Error_monad.fail
                  (Build_extensible "Invalid_arity"
                    (Alpha_context.Script.location * Alpha_context.Script.prim *
                      int * int)
                    (loc, Michelson_v1_primitives.D_Elt, 2,
                      (List.length l_value)))
              | Micheline.Prim loc name _ _ =>
                Error_monad.fail
                  (Build_extensible "Invalid_primitive"
                    (Alpha_context.Script.location *
                      list Alpha_context.Script.prim * Alpha_context.Script.prim)
                    (loc, [ Michelson_v1_primitives.D_Elt ], name))
              |
                (Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _
                | Micheline.Seq _ _) => fail_parse_data tt
              end) (None, (empty_map key_type), ctxt) items) in
    return=? (items, ctxt) in
  match (ty, script_data) with
  | (Script_typed_ir.Unit_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_unit ctxt legacy expr)))
  
  | (Script_typed_ir.Bool_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_bool ctxt legacy expr)))
  
  | (Script_typed_ir.String_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_string ctxt expr)))
  
  | (Script_typed_ir.Bytes_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_bytes ctxt expr)))
  
  | (Script_typed_ir.Int_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_int ctxt expr)))
  
  | (Script_typed_ir.Nat_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_nat ctxt expr)))
  
  | (Script_typed_ir.Mutez_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_mutez ctxt expr)))
  
  | (Script_typed_ir.Timestamp_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_timestamp ctxt expr)))
  
  | (Script_typed_ir.Key_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_key ctxt expr)))
  
  | (Script_typed_ir.Key_hash_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_key_hash ctxt expr)))
  
  | (Script_typed_ir.Signature_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_signature ctxt expr)))
  
  | (Script_typed_ir.Operation_t, _) =>
    (* ❌ Assert instruction is not handled. *)
    assert (M=? (a * Alpha_context.context)) false
  
  | (Script_typed_ir.Chain_id_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_chain_id ctxt expr)))
  
  | (Script_typed_ir.Address_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt
        (cast (M? (a * Alpha_context.context)) (parse_address ctxt expr)))
  
  | (Script_typed_ir.Contract_t ty, expr) =>
    let 'existT _ __0 [expr, ty] :=
      cast_exists (Es := Set)
        (fun __0 => [Alpha_context.Script.node ** Script_typed_ir.ty])
        [expr, ty] in
    traced
      (let=? '((c, entrypoint), ctxt) := return= (parse_address ctxt expr) in
      let loc := location expr in
      let=? '(ctxt, _) := parse_contract ctxt loc ty c entrypoint in
      return=? ((cast a (ty, (c, entrypoint))), ctxt))
  
  | (Script_typed_ir.Pair_t tl tr, expr) =>
    let 'existT _ [__1, __2] [expr, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__1, __2] =>
          [Alpha_context.Script.node ** Script_typed_ir.ty **
            Script_typed_ir.ty]) [expr, tr, tl] in
    let r_witness := comb_witness1 tr in
    let parse_l (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__1 * Alpha_context.context) :=
      non_terminal_recursion type_logger_value ctxt legacy tl v in
    let parse_r (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__2 * Alpha_context.context) :=
      non_terminal_recursion type_logger_value ctxt legacy tr v in
    traced
      (cast (M=? (a * Alpha_context.context))
        (parse_pair parse_l parse_r ctxt legacy r_witness expr))
  
  | (Script_typed_ir.Union_t tl tr, expr) =>
    let 'existT _ [__3, __4] [expr, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__3, __4] =>
          [Alpha_context.Script.node ** Script_typed_ir.ty **
            Script_typed_ir.ty]) [expr, tr, tl] in
    let parse_l (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__3 * Alpha_context.context) :=
      non_terminal_recursion type_logger_value ctxt legacy tl v in
    let parse_r (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__4 * Alpha_context.context) :=
      non_terminal_recursion type_logger_value ctxt legacy tr v in
    traced
      (cast (M=? (a * Alpha_context.context))
        (parse_union parse_l parse_r ctxt legacy expr))
  
  | (Script_typed_ir.Lambda_t ta tr, (Micheline.Seq _loc _) as script_instr) =>
    let 'existT _ [__5, __6] [script_instr, _loc, tr, ta] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__5, __6] =>
          [Micheline.node Alpha_context.Script.location
            Alpha_context.Script.prim ** Alpha_context.Script.location **
            Script_typed_ir.ty ** Script_typed_ir.ty])
        [script_instr, _loc, tr, ta] in
    traced
      (cast (M=? (a * Alpha_context.context))
        (parse_returning type_logger_value stack_depth Lambda ctxt legacy ta tr
          script_instr))
  
  | (Script_typed_ir.Lambda_t _ _, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Seq_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Option_t t_value, expr) =>
    let 'existT _ __9 [expr, t_value] :=
      cast_exists (Es := Set)
        (fun __9 => [Alpha_context.Script.node ** Script_typed_ir.ty])
        [expr, t_value] in
    let parse_v (ctxt : Alpha_context.context) (v : Alpha_context.Script.node)
      : M=? (__9 * Alpha_context.context) :=
      non_terminal_recursion type_logger_value ctxt legacy t_value v in
    traced
      (cast (M=? (a * Alpha_context.context))
        (parse_option parse_v ctxt legacy expr))
  
  | (Script_typed_ir.List_t t_value, Micheline.Seq _loc items) =>
    let 'existT _ __10 [items, _loc, t_value] :=
      cast_exists (Es := Set)
        (fun __10 =>
          [list
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim) ** Alpha_context.Script.location **
            Script_typed_ir.ty]) [items, _loc, t_value] in
    traced
      (cast (M=? (a * Alpha_context.context))
        (Error_monad.fold_right_s
          (fun (v : Alpha_context.Script.node) =>
            fun (function_parameter :
              Script_typed_ir.boxed_list __10 * Alpha_context.context) =>
              let '(rest, ctxt) := function_parameter in
              let=? '(v, ctxt) :=
                non_terminal_recursion type_logger_value ctxt legacy t_value v
                in
              return=? ((list_cons v rest), ctxt)) items (list_empty, ctxt)))
  
  | (Script_typed_ir.List_t _, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Seq_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Ticket_t t_value, expr) =>
    let 'existT _ __12 [expr, t_value] :=
      cast_exists (Es := Set)
        (fun __12 =>
          [Alpha_context.Script.node ** Script_typed_ir.comparable_ty])
        [expr, t_value] in
    if allow_forged then
      let=? '((ticketer, (contents, amount)), ctxt) :=
        parse_comparable_data type_logger_value ctxt
          (opened_ticket_type t_value) expr
          (fun (function_parameter :
            Script_typed_ir.pair Script_typed_ir.address
              (Script_typed_ir.pair __12 Alpha_context.Script_int.num)) =>
            let '_ := function_parameter in
            tt) in
      return=?
        ((cast a
          {| Script_typed_ir.ticket.ticketer := ticketer;
            Script_typed_ir.ticket.contents := contents;
            Script_typed_ir.ticket.amount := amount |}), ctxt)
    else
      traced_fail
        (Build_extensible "Unexpected_forged_value"
          Alpha_context.Script.location (location expr))
  
  | (Script_typed_ir.Set_t t_value, (Micheline.Seq loc vs) as expr) =>
    let 'existT _ __13 [expr, vs, loc, t_value] :=
      cast_exists (Es := Set)
        (fun __13 =>
          [Micheline.node Alpha_context.Script.location
            Alpha_context.Script.prim **
            list
              (Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim) ** Alpha_context.Script.location **
            Script_typed_ir.comparable_ty]) [expr, vs, loc, t_value] in
    let=? '(_, set, ctxt) :=
      traced
        (Error_monad.fold_left_s
          (fun (function_parameter :
            option __13 * Script_typed_ir.set __13 * Alpha_context.context) =>
            let '(last_value, set, ctxt) := function_parameter in
            fun (v : Alpha_context.Script.node) =>
              let=? '(v, ctxt) :=
                parse_comparable_data type_logger_value ctxt t_value v
                  (fun (function_parameter : __13) =>
                    let '_ := function_parameter in
                    tt) in
              Lwt._return
                (let? ctxt :=
                  match last_value with
                  | Some value =>
                    let? ctxt :=
                      Alpha_context.Gas.consume ctxt
                        (Michelson_v1_gas.Cost_of.Interpreter.compare t_value
                          value v) in
                    let c := compare_comparable t_value value v in
                    if 0 <=i c then
                      if 0 =i c then
                        Error_monad.error_value
                          (Build_extensible "Duplicate_set_values"
                            (Alpha_context.Script.location *
                              Micheline.canonical Alpha_context.Script.prim)
                            (loc, (Micheline.strip_locations expr)))
                      else
                        Error_monad.error_value
                          (Build_extensible "Unordered_set_values"
                            (Alpha_context.Script.location *
                              Micheline.canonical Alpha_context.Script.prim)
                            (loc, (Micheline.strip_locations expr)))
                    else
                      return? ctxt
                  | None => return? ctxt
                  end in
                let? ctxt :=
                  Alpha_context.Gas.consume ctxt
                    (Michelson_v1_gas.Cost_of.Interpreter.set_update v set) in
                return? ((Some v), (set_update v true set), ctxt)))
          (None, (empty_set t_value), ctxt) vs) in
    return=? ((cast a set), ctxt)
  
  | (Script_typed_ir.Set_t _, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Seq_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Map_t tk tv, (Micheline.Seq _ vs) as expr) =>
    let 'existT _ [__15, __16] [expr, vs, tv, tk] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__15, __16] =>
          [Micheline.node Alpha_context.Script.location
            Alpha_context.Script.prim **
            list
              (Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim) ** Script_typed_ir.ty **
            Script_typed_ir.comparable_ty]) [expr, vs, tv, tk] in
    let x :=
      (fun (x : M=? (Script_typed_ir.map __15 __16 * Alpha_context.context)) =>
        x)
        (parse_items type_logger_value ctxt expr tk tv vs (fun (x : __16) => x))
      in
    cast (M=? (a * Alpha_context.context)) x
  
  | (Script_typed_ir.Map_t _ _, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Seq_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Big_map_t tk tv, expr) =>
    let 'existT _ [__19, __20] [expr, tv, tk] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__19, __20] =>
          [Alpha_context.Script.node ** Script_typed_ir.ty **
            Script_typed_ir.comparable_ty]) [expr, tv, tk] in
    let=? '(id_opt, diff_value, ctxt) :=
      match expr with
      | Micheline.Int loc id =>
        Error_monad._return ((Some (id, loc)), (empty_map tk), ctxt)
      | Micheline.Seq _ vs =>
        let=? '(diff_value, ctxt) :=
          parse_items type_logger_value ctxt expr tk tv vs
            (fun (x : __20) => Some x) in
        return=? (None, diff_value, ctxt)
      |
        Micheline.Prim loc Michelson_v1_primitives.D_Pair
          (cons (Micheline.Int loc_id id) (cons (Micheline.Seq _ vs) [])) annot
        =>
        let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot)
          in
        let tv_opt := Script_typed_ir.Option_t tv in
        let=? '(diff_value, ctxt) :=
          parse_items type_logger_value ctxt expr tk tv_opt vs
            (fun (x : option __20) => x) in
        return=? ((Some (id, loc_id)), diff_value, ctxt)
      |
        Micheline.Prim _ Michelson_v1_primitives.D_Pair
          (cons (Micheline.Int _ _) (cons expr [])) _ =>
        traced_fail
          (Build_extensible "Invalid_kind"
            (Alpha_context.Script.location * list Script_tc_errors.kind *
              Script_tc_errors.kind)
            ((location expr), [ Script_tc_errors.Seq_kind ], (kind_value expr)))
      |
        Micheline.Prim _ Michelson_v1_primitives.D_Pair (cons expr (cons _ []))
          _ =>
        traced_fail
          (Build_extensible "Invalid_kind"
            (Alpha_context.Script.location * list Script_tc_errors.kind *
              Script_tc_errors.kind)
            ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
      | Micheline.Prim loc Michelson_v1_primitives.D_Pair l_value _ =>
        traced_fail
          (Build_extensible "Invalid_arity"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              int)
            (loc, Michelson_v1_primitives.D_Pair, 2, (List.length l_value)))
      | _ =>
        traced_fail
          (unexpected expr
            [ Script_tc_errors.Seq_kind; Script_tc_errors.Int_kind ]
            Michelson_v1_primitives.Constant_namespace
            [ Michelson_v1_primitives.D_Pair ])
      end in
    let diff_value :=
      (fun (x : Script_typed_ir.map __19 (option __20)) => x) diff_value in
    let=? '(id, ctxt) :=
      match id_opt with
      | None => Error_monad._return (None, ctxt)
      | Some (id, loc) =>
        if allow_forged then
          let id :=
            Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.parse_z) id
            in
          let=? function_parameter := Alpha_context.Big_map._exists ctxt id in
          match function_parameter with
          | (_, None) =>
            traced_fail
              (Build_extensible "Invalid_big_map"
                (Alpha_context.Script.location *
                  Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
                (loc, id))
          | (ctxt, Some (btk, btv)) =>
            Lwt._return
              (let? '(Ex_comparable_ty btk, ctxt) :=
                parse_comparable_ty ctxt (Micheline.root btk) in
              let? '(Ex_ty btv, ctxt) :=
                parse_big_map_value_ty ctxt legacy (Micheline.root btv) in
              let? '(Eq, ctxt) := comparable_ty_eq ctxt tk btk in
              let? '(Eq, ctxt) := ty_eq ctxt tv btv in
              return? ((Some id), ctxt))
          end
        else
          traced_fail
            (Build_extensible "Unexpected_forged_value"
              Alpha_context.Script.location loc)
      end in
    return=?
      ((cast a
        {| Script_typed_ir.big_map.id := id;
          Script_typed_ir.big_map.diff := diff_value;
          Script_typed_ir.big_map.key_type := tk;
          Script_typed_ir.big_map.value_type := tv |}), ctxt)
  
  | (Script_typed_ir.Never_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    Lwt._return
      (traced_no_lwt (cast (M? (a * Alpha_context.context)) (parse_never expr)))
  
  | (Script_typed_ir.Bls12_381_g1_t, Micheline.Bytes _ bs) =>
    let bs := cast bytes bs in
    let=? ctxt :=
      return= (Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_g1) in
    match Bls12_381.G1.(S.CURVE.of_bytes_opt) bs with
    | Some pt => Error_monad._return ((cast a pt), ctxt)
    | None => fail_parse_data tt
    end
  
  | (Script_typed_ir.Bls12_381_g1_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Bytes_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Bls12_381_g2_t, Micheline.Bytes _ bs) =>
    let bs := cast bytes bs in
    let=? ctxt :=
      return= (Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_g2) in
    match Bls12_381.G2.(S.CURVE.of_bytes_opt) bs with
    | Some pt => Error_monad._return ((cast a pt), ctxt)
    | None => fail_parse_data tt
    end
  
  | (Script_typed_ir.Bls12_381_g2_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Bytes_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Bls12_381_fr_t, Micheline.Bytes _ bs) =>
    let bs := cast bytes bs in
    let=? ctxt :=
      return= (Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_fr) in
    match Bls12_381.Fr.(S.PRIME_FIELD.of_bytes_opt) bs with
    | Some pt => Error_monad._return ((cast a pt), ctxt)
    | None => fail_parse_data tt
    end
  
  | (Script_typed_ir.Bls12_381_fr_t, Micheline.Int _ v) =>
    let v := cast Z.t v in
    let=? ctxt :=
      return= (Alpha_context.Gas.consume ctxt Typecheck_costs.bls12_381_fr) in
    Error_monad._return ((cast a (Bls12_381.Fr.(S.PRIME_FIELD.of_z) v)), ctxt)
  
  | (Script_typed_ir.Bls12_381_fr_t, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Bytes_kind ], (kind_value expr)))
  
  |
    (Script_typed_ir.Sapling_transaction_t memo_size,
      Micheline.Bytes _ bytes_value) =>
    let '[bytes_value, memo_size] :=
      cast [bytes ** Alpha_context.Sapling.Memo_size.t] [bytes_value, memo_size]
      in
    match
      Data_encoding.Binary.of_bytes Alpha_context.Sapling.transaction_encoding
        bytes_value with
    | Some transaction =>
      match Alpha_context.Sapling.transaction_get_memo_size transaction with
      | None => Error_monad._return ((cast a transaction), ctxt)
      | Some transac_memo_size =>
        Lwt._return
          (let? '_ := memo_size_eq memo_size transac_memo_size in
          return? ((cast a transaction), ctxt))
      end
    | None => fail_parse_data tt
    end
  
  | (Script_typed_ir.Sapling_transaction_t _, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Bytes_kind ], (kind_value expr)))
  
  | (Script_typed_ir.Sapling_state_t memo_size, Micheline.Int loc id) =>
    let '[id, loc, memo_size] :=
      cast
        [Z.t ** Alpha_context.Script.location **
          Alpha_context.Sapling.Memo_size.t] [id, loc, memo_size] in
    if allow_forged then
      let id :=
        Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.parse_z) id in
      let=? '(state, ctxt) := Alpha_context.Sapling.state_from_id ctxt id in
      Lwt._return
        (let? '_ :=
          traced_no_lwt
            (memo_size_eq memo_size
              state.(Alpha_context.Sapling.state.memo_size)) in
        return? ((cast a state), ctxt))
    else
      traced_fail
        (Build_extensible "Unexpected_forged_value"
          Alpha_context.Script.location loc)
  
  | (Script_typed_ir.Sapling_state_t memo_size, Micheline.Seq _ []) =>
    let memo_size := cast Alpha_context.Sapling.Memo_size.t memo_size in
    Error_monad._return
      ((cast a (Alpha_context.Sapling.empty_state None memo_size tt)), ctxt)
  
  | (Script_typed_ir.Sapling_state_t _, expr) =>
    let expr := cast Alpha_context.Script.node expr in
    traced_fail
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.Int_kind; Script_tc_errors.Seq_kind ],
          (kind_value expr)))
  end

with parse_instr_with_stack
  (type_logger_value : option type_logger) (stack_depth : int)
  (tc_context_value : tc_context) (ctxt : Alpha_context.context) (legacy : bool)
  (script_instr : Alpha_context.Script.node)
  (stack_ty : Script_typed_ir.stack_ty) {struct stack_ty}
  : M=? (judgement * Alpha_context.context) :=
  let parse_returning := 'parse_returning in
  let check_item_ty
    (ctxt : Alpha_context.context) (exp : Script_typed_ir.ty)
    (got : Script_typed_ir.ty) (loc : Alpha_context.Script.location)
    (name : Alpha_context.Script.prim) (n : int) (m : int)
    : M? (eq * Alpha_context.context) :=
    Error_monad.record_trace_eval
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        let? '(stack_ty, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        return?
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty) (loc, name, m, stack_ty)))
      (Error_monad.record_trace (Build_extensible "Bad_stack_item" int n)
        (ty_eq ctxt exp got)) in
  let log_stack
    (ctxt : Alpha_context.context) (loc : int)
    (stack_ty : Script_typed_ir.stack_ty) (aft : Script_typed_ir.stack_ty)
    : M? unit :=
    match (type_logger_value, script_instr) with
    |
      ((None, _) |
      (Some _,
        (Micheline.Seq (-1) _ | Micheline.Int _ _ | Micheline.String _ _ |
        Micheline.Bytes _ _))) => Error_monad.ok_unit
    | (Some log, (Micheline.Prim _ _ _ _ | Micheline.Seq _ _)) =>
      let ctxt := Alpha_context.Gas.set_unlimited ctxt in
      let? '(stack_ty, _) := unparse_stack ctxt stack_ty in
      let? '(aft, _) := unparse_stack ctxt aft in
      let '_ := log loc stack_ty aft in
      return? tt
    end in
  let return_no_lwt (ctxt : Alpha_context.context) (judgement_value : judgement)
    : M? (judgement * Alpha_context.context) :=
    match judgement_value with
    |
      Typed {|
        Script_typed_ir.descr.loc := loc;
          Script_typed_ir.descr.aft := aft;
          Script_typed_ir.descr.instr := instr
          |} =>
      let maximum_type_size :=
        Alpha_context.Constants.michelson_maximum_type_size ctxt in
      let type_size :=
        type_size_of_stack_head aft (number_of_generated_growing_types instr) in
      if type_size >i maximum_type_size then
        Error_monad.error_value
          (Build_extensible "Type_too_large"
            (Alpha_context.Script.location * int * int)
            (loc, type_size, maximum_type_size))
      else
        return? (judgement_value, ctxt)
    | Failed _ => return? (judgement_value, ctxt)
    end in
  let _return (ctxt : Alpha_context.context) (judgement_value : judgement)
    : M=? (judgement * Alpha_context.context) :=
    Lwt._return (return_no_lwt ctxt judgement_value) in
  let typed_no_lwt
    (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
    (instr : Script_typed_ir.instr) (aft : Script_typed_ir.stack_ty)
    : M? (judgement * Alpha_context.context) :=
    let? '_ := log_stack ctxt loc stack_ty aft in
    return_no_lwt ctxt
      (Typed
        {| Script_typed_ir.descr.loc := loc;
          Script_typed_ir.descr.bef := stack_ty;
          Script_typed_ir.descr.aft := aft; Script_typed_ir.descr.instr := instr
          |}) in
  let typed
    (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
    (instr : Script_typed_ir.instr) (aft : Script_typed_ir.stack_ty)
    : M=? (judgement * Alpha_context.context) :=
    Lwt._return (typed_no_lwt ctxt loc instr aft) in
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle)
    in
  let non_terminal_recursion
    (type_logger_value : option type_logger) (tc_context_value : tc_context)
    (ctxt : Alpha_context.context) (legacy : bool)
    (script_instr : Alpha_context.Script.node)
    (stack_ty : Script_typed_ir.stack_ty)
    : M=? (judgement * Alpha_context.context) :=
    if stack_depth >i 10000 then
      Error_monad.fail
        (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
    else
      parse_instr_with_stack type_logger_value (stack_depth +i 1)
        tc_context_value ctxt legacy script_instr stack_ty in
  match (script_instr, stack_ty) with
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DROP [] annot,
      Script_typed_ir.Item_t _ rest) =>
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    typed ctxt loc Script_typed_ir.Drop rest
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DROP (cons n []) result_annot,
      whole_stack) =>
    let=? whole_n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument whole_n))
      in
    let fix make_proof_argument (n : int) (stk : Script_typed_ir.stack_ty)
      : M? dropn_proof_argument :=
      match ((n =i 0), stk) with
      | (true, rest) =>
        return? (Dropn_proof_argument (Script_typed_ir.Rest, rest, rest))
      | (false, Script_typed_ir.Item_t v rest) =>
        let? 'Dropn_proof_argument (n', stack_after_drops, aft') :=
          make_proof_argument (n -i 1) rest in
        return?
          (Dropn_proof_argument
            ((Script_typed_ir.Prefix n'), stack_after_drops,
              (Script_typed_ir.Item_t v aft')))
      | (_, _) =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt whole_stack
          in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_DROP, whole_n, whole_stack))
      end in
    let=? '_ :=
      return= (Script_ir_annot.error_unexpected_annot loc result_annot) in
    let=? 'Dropn_proof_argument (n', stack_after_drops, _aft) :=
      return= (make_proof_argument whole_n whole_stack) in
    typed ctxt loc (Script_typed_ir.Dropn whole_n n') stack_after_drops
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DROP
      ((cons _ (cons _ _)) as l_value) _, _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.I_DROP, 1, (List.length l_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DUP [] _annot,
      Script_typed_ir.Item_t v rest) =>
    let=? ctxt :=
      return=
        (Error_monad.record_trace_eval
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let? '(t_value, _ctxt) := serialize_ty_for_error ctxt v in
            return?
              (Build_extensible "Non_dupable_type"
                (Alpha_context.Script.location *
                  Micheline.canonical Alpha_context.Script.prim) (loc, t_value)))
          (check_dupable_ty ctxt loc v)) in
    typed ctxt loc Script_typed_ir.Dup
      (Script_typed_ir.Item_t v (Script_typed_ir.Item_t v rest))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DUP (cons n []) _v_annot,
      stack_ty) =>
    let fix make_proof_argument (n : int) (stack_ty : Script_typed_ir.stack_ty)
      : M? dup_n_proof_argument :=
      match (n, stack_ty) with
      | (1, Script_typed_ir.Item_t hd_ty _) =>
        return? (Dup_n_proof_argument Script_typed_ir.Dup_n_zero hd_ty)
      | (n, Script_typed_ir.Item_t _ tl_ty) =>
        let? 'Dup_n_proof_argument dup_n_witness b_ty :=
          make_proof_argument (n -i 1) tl_ty in
        return?
          (Dup_n_proof_argument (Script_typed_ir.Dup_n_succ dup_n_witness) b_ty)
      | _ =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_DUP, 1, whole_stack))
      end in
    let=? n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let=? '_ :=
      return=
        (Error_monad.error_unless (n >i 0)
          (Build_extensible "Dup_n_bad_argument" Alpha_context.Script.location
            loc)) in
    let=? 'Dup_n_proof_argument witness after_ty :=
      return=
        (Error_monad.record_trace
          (Build_extensible "Dup_n_bad_stack" Alpha_context.Script.location loc)
          (make_proof_argument n stack_ty)) in
    let=? ctxt :=
      return=
        (Error_monad.record_trace_eval
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let? '(t_value, _ctxt) := serialize_ty_for_error ctxt after_ty in
            return?
              (Build_extensible "Non_dupable_type"
                (Alpha_context.Script.location *
                  Micheline.canonical Alpha_context.Script.prim) (loc, t_value)))
          (check_dupable_ty ctxt loc after_ty)) in
    typed ctxt loc (Script_typed_ir.Dup_n n witness)
      (Script_typed_ir.Item_t after_ty stack_ty)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DIG (cons n []) result_annot,
      stack_value) =>
    let fix make_proof_argument (n : int) (stk : Script_typed_ir.stack_ty)
      : M? dig_proof_argument :=
      match ((n =i 0), stk) with
      | (true, Script_typed_ir.Item_t v rest) =>
        return? (Dig_proof_argument (Script_typed_ir.Rest, v, rest))
      | (false, Script_typed_ir.Item_t v rest) =>
        let? 'Dig_proof_argument (n', x, aft') :=
          make_proof_argument (n -i 1) rest in
        return?
          (Dig_proof_argument
            ((Script_typed_ir.Prefix n'), x, (Script_typed_ir.Item_t v aft')))
      | (_, _) =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt stack_value
          in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_DIG, 1, whole_stack))
      end in
    let=? n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let=? '_ :=
      return= (Script_ir_annot.error_unexpected_annot loc result_annot) in
    let=? 'Dig_proof_argument (n', x, aft) :=
      return= (make_proof_argument n stack_value) in
    typed ctxt loc (Script_typed_ir.Dig n n') (Script_typed_ir.Item_t x aft)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DIG
      (([] | cons _ (cons _ _)) as l_value) _, _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.I_DIG, 1, (List.length l_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DUG (cons n []) result_annot,
      Script_typed_ir.Item_t x whole_stack) =>
    let=? whole_n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument whole_n))
      in
    let fix make_proof_argument
      (n : int) (x : Script_typed_ir.ty) (stk : Script_typed_ir.stack_ty)
      : M? dug_proof_argument :=
      match ((n =i 0), stk) with
      | (true, rest) =>
        return?
          (Dug_proof_argument
            (Script_typed_ir.Rest, tt, (Script_typed_ir.Item_t x rest)))
      | (false, Script_typed_ir.Item_t v rest) =>
        let? 'Dug_proof_argument (n', _, aft') :=
          make_proof_argument (n -i 1) x rest in
        return?
          (Dug_proof_argument
            ((Script_typed_ir.Prefix n'), tt, (Script_typed_ir.Item_t v aft')))
      | (_, _) =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt whole_stack
          in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_DUG, whole_n, whole_stack))
      end in
    let=? '_ :=
      return= (Script_ir_annot.error_unexpected_annot loc result_annot) in
    let=? 'Dug_proof_argument (n', _, aft) :=
      return= (make_proof_argument whole_n x whole_stack) in
    typed ctxt loc (Script_typed_ir.Dug whole_n n') aft
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DUG (cons _ []) result_annot,
      Script_typed_ir.Empty_t as stack_value) =>
    Lwt._return
      (let? '_ := Script_ir_annot.error_unexpected_annot loc result_annot in
      let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
        in
      Error_monad.error_value
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty)
          (loc, Michelson_v1_primitives.I_DUG, 1, stack_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DUG
      (([] | cons _ (cons _ _)) as l_value) _, _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.I_DUG, 1, (List.length l_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SWAP [] annot,
      Script_typed_ir.Item_t v (Script_typed_ir.Item_t w rest)) =>
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    typed ctxt loc Script_typed_ir.Swap
      (Script_typed_ir.Item_t w (Script_typed_ir.Item_t v rest))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_PUSH
      (cons t_value (cons d [])) _annot, stack_value) =>
    let=? '(Ex_ty t_value, ctxt) :=
      return= (parse_packable_ty ctxt legacy t_value) in
    let 'existT _ __Ex_ty_'a1 [ctxt, t_value] :=
      cast_exists (Es := Set)
        (fun __Ex_ty_'a1 => [Alpha_context.context ** Script_typed_ir.ty])
        [ctxt, t_value] in
    let=? '(v, ctxt) :=
      parse_data_with_stack type_logger_value (stack_depth +i 1) ctxt legacy
        false t_value d in
    let v := (fun (x : __Ex_ty_'a1) => x) v in
    typed ctxt loc (Script_typed_ir.Const v)
      (Script_typed_ir.Item_t t_value stack_value)
  | (Micheline.Prim loc Michelson_v1_primitives.I_UNIT [] _annot, stack_value)
    =>
    typed ctxt loc (Script_typed_ir.Const tt)
      (Script_typed_ir.Item_t Script_typed_ir.Unit_t stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SOME [] _annot,
      Script_typed_ir.Item_t t_value rest) =>
    typed ctxt loc Script_typed_ir.Cons_some
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t t_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NONE (cons t_value []) _annot,
      stack_value) =>
    let=? '(Ex_ty t_value, ctxt) := return= (parse_any_ty ctxt legacy t_value)
      in
    typed ctxt loc (Script_typed_ir.Cons_none t_value)
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t t_value) stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_IF_NONE (cons bt (cons bf []))
      annot,
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t t_value) rest) as bef)
    =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bt) in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bf) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(btr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bt
        rest in
    let=? '(bfr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bf
        (Script_typed_ir.Item_t t_value rest) in
    let branch (ibt : Script_typed_ir.descr) (ibf : Script_typed_ir.descr)
      : Script_typed_ir.descr :=
      {| Script_typed_ir.descr.loc := loc; Script_typed_ir.descr.bef := bef;
        Script_typed_ir.descr.aft := ibt.(Script_typed_ir.descr.aft);
        Script_typed_ir.descr.instr := Script_typed_ir.If_none ibt ibf |} in
    let=? '(judgement_value, ctxt) :=
      return= (merge_branches ctxt loc btr bfr {| branch.branch := branch |}) in
    _return ctxt judgement_value
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_PAIR [] _annot,
      Script_typed_ir.Item_t a_value (Script_typed_ir.Item_t b_value rest)) =>
    typed ctxt loc Script_typed_ir.Cons_pair
      (Script_typed_ir.Item_t (Script_typed_ir.Pair_t a_value b_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_PAIR (cons n []) _annot,
      stack_ty) =>
    let fix make_proof_argument (n : int) (stack_ty : Script_typed_ir.stack_ty)
      {struct n} : M? comb_proof_argument :=
      match (n, stack_ty) with
      | (1, Script_typed_ir.Item_t a_ty tl_ty) =>
        return?
          (Comb_proof_argument Script_typed_ir.Comb_one
            (Script_typed_ir.Item_t a_ty tl_ty))
      | (n, Script_typed_ir.Item_t a_ty tl_ty) =>
        let? function_parameter := make_proof_argument (n -i 1) tl_ty in
        match function_parameter with
        | Comb_proof_argument comb_witness (Script_typed_ir.Item_t b_ty tl_ty')
          =>
          let pair_t := Script_typed_ir.Pair_t a_ty b_ty in
          return?
            (Comb_proof_argument (Script_typed_ir.Comb_succ comb_witness)
              (Script_typed_ir.Item_t pair_t tl_ty'))
        | _ => unreachable_gadt_branch
        end
      | _ =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_PAIR, 1, whole_stack))
      end in
    let=? n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let=? '_ :=
      return=
        (Error_monad.error_unless (n >i 1)
          (Build_extensible "Pair_bad_argument" Alpha_context.Script.location
            loc)) in
    let=? 'Comb_proof_argument witness after_ty :=
      return= (make_proof_argument n stack_ty) in
    typed ctxt loc (Script_typed_ir.Comb n witness) after_ty
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UNPAIR (cons n []) annot,
      stack_ty) =>
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let fix make_proof_argument (n : int) (stack_ty : Script_typed_ir.stack_ty)
      {struct n} : M? uncomb_proof_argument :=
      match (n, stack_ty) with
      | (1, Script_typed_ir.Item_t a_ty tl_ty) =>
        return?
          (Uncomb_proof_argument Script_typed_ir.Uncomb_one
            (Script_typed_ir.Item_t a_ty tl_ty))
      | (n, Script_typed_ir.Item_t (Script_typed_ir.Pair_t a_ty b_ty) tl_ty) =>
        let? 'Uncomb_proof_argument uncomb_witness after_ty :=
          make_proof_argument (n -i 1) (Script_typed_ir.Item_t b_ty tl_ty) in
        return?
          (Uncomb_proof_argument (Script_typed_ir.Uncomb_succ uncomb_witness)
            (Script_typed_ir.Item_t a_ty after_ty))
      | _ =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_UNPAIR, 1, whole_stack))
      end in
    let=? n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let=? '_ :=
      return=
        (Error_monad.error_unless (n >i 1)
          (Build_extensible "Unpair_bad_argument" Alpha_context.Script.location
            loc)) in
    let=? 'Uncomb_proof_argument witness after_ty :=
      return= (make_proof_argument n stack_ty) in
    typed ctxt loc (Script_typed_ir.Uncomb n witness) after_ty
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GET (cons n []) _annot,
      Script_typed_ir.Item_t comb_ty rest_ty) =>
    let fix make_proof_argument (n : int) (ty : Script_typed_ir.ty)
      : M? comb_get_proof_argument :=
      match (n, ty) with
      | (0, value_ty) =>
        return? (Comb_get_proof_argument Script_typed_ir.Comb_get_zero value_ty)
      | (1, Script_typed_ir.Pair_t hd_ty _) =>
        return? (Comb_get_proof_argument Script_typed_ir.Comb_get_one hd_ty)
      | (n, Script_typed_ir.Pair_t _ tl_ty) =>
        let? 'Comb_get_proof_argument comb_get_left_witness ty' :=
          make_proof_argument (n -i 2) tl_ty in
        return?
          (Comb_get_proof_argument
            (Script_typed_ir.Comb_get_plus_two comb_get_left_witness) ty')
      | _ =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_GET, 1, whole_stack))
      end in
    let=? n := return= (parse_uint11 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let=? 'Comb_get_proof_argument witness ty' :=
      return= (make_proof_argument n comb_ty) in
    let after_stack_ty := Script_typed_ir.Item_t ty' rest_ty in
    typed ctxt loc (Script_typed_ir.Comb_get n witness) after_stack_ty
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UPDATE (cons n []) _annot,
      Script_typed_ir.Item_t value_ty (Script_typed_ir.Item_t comb_ty rest_ty))
    =>
    let fix make_proof_argument
      (n : int) (value_ty : Script_typed_ir.ty) (ty : Script_typed_ir.ty)
      : M? comb_set_proof_argument :=
      match (n, ty) with
      | (0, _) =>
        return? (Comb_set_proof_argument Script_typed_ir.Comb_set_zero value_ty)
      | (1, Script_typed_ir.Pair_t _hd_ty tl_ty) =>
        let after_ty := Script_typed_ir.Pair_t value_ty tl_ty in
        return? (Comb_set_proof_argument Script_typed_ir.Comb_set_one after_ty)
      | (n, Script_typed_ir.Pair_t hd_ty tl_ty) =>
        let? 'Comb_set_proof_argument comb_set_left_witness tl_ty' :=
          make_proof_argument (n -i 2) value_ty tl_ty in
        let after_ty := Script_typed_ir.Pair_t hd_ty tl_ty' in
        return?
          (Comb_set_proof_argument
            (Script_typed_ir.Comb_set_plus_two comb_set_left_witness) after_ty)
      | _ =>
        let? '(whole_stack, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_stack"
            (Alpha_context.Script.location * Alpha_context.Script.prim * int *
              Script_tc_errors.unparsed_stack_ty)
            (loc, Michelson_v1_primitives.I_UPDATE, 2, whole_stack))
      end in
    let=? n := return= (parse_uint11 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let=? 'Comb_set_proof_argument witness after_ty :=
      return= (make_proof_argument n value_ty comb_ty) in
    let after_stack_ty := Script_typed_ir.Item_t after_ty rest_ty in
    typed ctxt loc (Script_typed_ir.Comb_set n witness) after_stack_ty
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UNPAIR [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Pair_t a_value b_value) rest) =>
    typed ctxt loc Script_typed_ir.Unpair
      (Script_typed_ir.Item_t a_value (Script_typed_ir.Item_t b_value rest))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CAR [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Pair_t a_value _) rest) =>
    typed ctxt loc Script_typed_ir.Car (Script_typed_ir.Item_t a_value rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CDR [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Pair_t _ b_value) rest) =>
    typed ctxt loc Script_typed_ir.Cdr (Script_typed_ir.Item_t b_value rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LEFT (cons tr []) _annot,
      Script_typed_ir.Item_t tl rest) =>
    let=? '(Ex_ty tr, ctxt) := return= (parse_any_ty ctxt legacy tr) in
    typed ctxt loc Script_typed_ir.Cons_left
      (Script_typed_ir.Item_t (Script_typed_ir.Union_t tl tr) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_RIGHT (cons tl []) _annot,
      Script_typed_ir.Item_t tr rest) =>
    let=? '(Ex_ty tl, ctxt) := return= (parse_any_ty ctxt legacy tl) in
    typed ctxt loc Script_typed_ir.Cons_right
      (Script_typed_ir.Item_t (Script_typed_ir.Union_t tl tr) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_IF_LEFT (cons bt (cons bf []))
      annot,
      (Script_typed_ir.Item_t (Script_typed_ir.Union_t tl tr) rest) as bef) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bt) in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bf) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(btr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bt
        (Script_typed_ir.Item_t tl rest) in
    let=? '(bfr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bf
        (Script_typed_ir.Item_t tr rest) in
    let branch (ibt : Script_typed_ir.descr) (ibf : Script_typed_ir.descr)
      : Script_typed_ir.descr :=
      {| Script_typed_ir.descr.loc := loc; Script_typed_ir.descr.bef := bef;
        Script_typed_ir.descr.aft := ibt.(Script_typed_ir.descr.aft);
        Script_typed_ir.descr.instr := Script_typed_ir.If_left ibt ibf |} in
    let=? '(judgement_value, ctxt) :=
      return= (merge_branches ctxt loc btr bfr {| branch.branch := branch |}) in
    _return ctxt judgement_value
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NIL (cons t_value []) _annot,
      stack_value) =>
    let=? '(Ex_ty t_value, ctxt) := return= (parse_any_ty ctxt legacy t_value)
      in
    typed ctxt loc Script_typed_ir.Nil
      (Script_typed_ir.Item_t (Script_typed_ir.List_t t_value) stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CONS [] _annot,
      Script_typed_ir.Item_t tv
        (Script_typed_ir.Item_t (Script_typed_ir.List_t t_value) rest)) =>
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt tv t_value loc Michelson_v1_primitives.I_CONS 1 2)
      in
    typed ctxt loc Script_typed_ir.Cons_list
      (Script_typed_ir.Item_t (Script_typed_ir.List_t t_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_IF_CONS (cons bt (cons bf []))
      annot,
      (Script_typed_ir.Item_t (Script_typed_ir.List_t t_value) rest) as bef) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bt) in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bf) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(btr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bt
        (Script_typed_ir.Item_t t_value
          (Script_typed_ir.Item_t (Script_typed_ir.List_t t_value) rest)) in
    let=? '(bfr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bf
        rest in
    let branch (ibt : Script_typed_ir.descr) (ibf : Script_typed_ir.descr)
      : Script_typed_ir.descr :=
      {| Script_typed_ir.descr.loc := loc; Script_typed_ir.descr.bef := bef;
        Script_typed_ir.descr.aft := ibt.(Script_typed_ir.descr.aft);
        Script_typed_ir.descr.instr := Script_typed_ir.If_cons ibt ibf |} in
    let=? '(judgement_value, ctxt) :=
      return= (merge_branches ctxt loc btr bfr {| branch.branch := branch |}) in
    _return ctxt judgement_value
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SIZE [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.List_t _) rest) =>
    typed ctxt loc Script_typed_ir.List_size
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MAP (cons body []) _annot,
      Script_typed_ir.Item_t (Script_typed_ir.List_t elt_value) starting_rest)
    =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (Script_typed_ir.Item_t elt_value starting_rest) in
    match judgement_value with
    |
      Typed
        ({| Script_typed_ir.descr.aft := Script_typed_ir.Item_t ret rest |} as
          ibody) =>
      let invalid_map_body (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, _ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        return?
          (Build_extensible "Invalid_map_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty)
            (loc, aft)) in
      Lwt._return
        (Error_monad.record_trace_eval invalid_map_body
          (let? '(Eq, ctxt) := stacks_eq ctxt 1 rest starting_rest in
          typed_no_lwt ctxt loc (Script_typed_ir.List_map ibody)
            (Script_typed_ir.Item_t (Script_typed_ir.List_t ret) rest)))
    | Typed {| Script_typed_ir.descr.aft := aft |} =>
      Lwt._return
        (let? '(aft, _ctxt) := serialize_stack_for_error ctxt aft in
        Error_monad.error_value
          (Build_extensible "Invalid_map_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty)
            (loc, aft)))
    | Failed _ =>
      Error_monad.fail
        (Build_extensible "Invalid_map_block_fail" Alpha_context.Script.location
          loc)
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ITER (cons body []) annot,
      Script_typed_ir.Item_t (Script_typed_ir.List_t elt_value) rest) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (Script_typed_ir.Item_t elt_value rest) in
    match judgement_value with
    | Typed ({| Script_typed_ir.descr.aft := aft |} as ibody) =>
      let invalid_iter_body (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        let? '(rest, _ctxt) := serialize_stack_for_error ctxt rest in
        return?
          (Build_extensible "Invalid_iter_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty
              * Script_tc_errors.unparsed_stack_ty) (loc, rest, aft)) in
      Lwt._return
        (Error_monad.record_trace_eval invalid_iter_body
          (let? '(Eq, ctxt) := stacks_eq ctxt 1 aft rest in
          typed_no_lwt ctxt loc (Script_typed_ir.List_iter ibody) rest))
    | Failed {| judgement.Failed.descr := descr_value |} =>
      typed ctxt loc (Script_typed_ir.List_iter (descr_value rest)) rest
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EMPTY_SET (cons t_value [])
      _annot, rest) =>
    let=? '(Ex_comparable_ty t_value, ctxt) :=
      return= (parse_comparable_ty ctxt t_value) in
    typed ctxt loc (Script_typed_ir.Empty_set t_value)
      (Script_typed_ir.Item_t (Script_typed_ir.Set_t t_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ITER (cons body []) annot,
      Script_typed_ir.Item_t (Script_typed_ir.Set_t comp_elt) rest) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let elt_value := ty_of_comparable_ty comp_elt in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (Script_typed_ir.Item_t elt_value rest) in
    match judgement_value with
    | Typed ({| Script_typed_ir.descr.aft := aft |} as ibody) =>
      let invalid_iter_body (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        let? '(rest, _ctxt) := serialize_stack_for_error ctxt rest in
        return?
          (Build_extensible "Invalid_iter_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty
              * Script_tc_errors.unparsed_stack_ty) (loc, rest, aft)) in
      Lwt._return
        (Error_monad.record_trace_eval invalid_iter_body
          (let? '(Eq, ctxt) := stacks_eq ctxt 1 aft rest in
          typed_no_lwt ctxt loc (Script_typed_ir.Set_iter ibody) rest))
    | Failed {| judgement.Failed.descr := descr_value |} =>
      typed ctxt loc (Script_typed_ir.Set_iter (descr_value rest)) rest
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MEM [] _annot,
      Script_typed_ir.Item_t v
        (Script_typed_ir.Item_t (Script_typed_ir.Set_t elt_value) rest)) =>
    let elt_value := ty_of_comparable_ty elt_value in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt elt_value v loc Michelson_v1_primitives.I_MEM 1 2)
      in
    typed ctxt loc Script_typed_ir.Set_mem
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UPDATE [] _annot,
      Script_typed_ir.Item_t v
        (Script_typed_ir.Item_t Script_typed_ir.Bool_t
          (Script_typed_ir.Item_t (Script_typed_ir.Set_t elt_value) rest))) =>
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt (ty_of_comparable_ty elt_value) v loc
          Michelson_v1_primitives.I_UPDATE 1 3) in
    typed ctxt loc Script_typed_ir.Set_update
      (Script_typed_ir.Item_t (Script_typed_ir.Set_t elt_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SIZE [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Set_t _) rest) =>
    typed ctxt loc Script_typed_ir.Set_size
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EMPTY_MAP
      (cons tk (cons tv [])) _annot, stack_value) =>
    let=? '(Ex_comparable_ty tk, ctxt) := return= (parse_comparable_ty ctxt tk)
      in
    let=? '(Ex_ty tv, ctxt) := return= (parse_any_ty ctxt legacy tv) in
    typed ctxt loc (Script_typed_ir.Empty_map tk tv)
      (Script_typed_ir.Item_t (Script_typed_ir.Map_t tk tv) stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MAP (cons body []) _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Map_t ck elt_value) starting_rest)
    =>
    let k := ty_of_comparable_ty ck in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (Script_typed_ir.Item_t (Script_typed_ir.Pair_t k elt_value)
          starting_rest) in
    match judgement_value with
    |
      Typed
        ({| Script_typed_ir.descr.aft := Script_typed_ir.Item_t ret rest |} as
          ibody) =>
      let invalid_map_body (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, _ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        return?
          (Build_extensible "Invalid_map_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty)
            (loc, aft)) in
      Lwt._return
        (Error_monad.record_trace_eval invalid_map_body
          (let? '(Eq, ctxt) := stacks_eq ctxt 1 rest starting_rest in
          typed_no_lwt ctxt loc (Script_typed_ir.Map_map ibody)
            (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck ret) rest)))
    | Typed {| Script_typed_ir.descr.aft := aft |} =>
      Lwt._return
        (let? '(aft, _ctxt) := serialize_stack_for_error ctxt aft in
        Error_monad.error_value
          (Build_extensible "Invalid_map_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty)
            (loc, aft)))
    | Failed _ =>
      Error_monad.fail
        (Build_extensible "Invalid_map_block_fail" Alpha_context.Script.location
          loc)
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ITER (cons body []) annot,
      Script_typed_ir.Item_t (Script_typed_ir.Map_t comp_elt element_ty) rest)
    =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let key_value := ty_of_comparable_ty comp_elt in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (Script_typed_ir.Item_t (Script_typed_ir.Pair_t key_value element_ty)
          rest) in
    match judgement_value with
    | Typed ({| Script_typed_ir.descr.aft := aft |} as ibody) =>
      let invalid_iter_body (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        let? '(rest, _ctxt) := serialize_stack_for_error ctxt rest in
        return?
          (Build_extensible "Invalid_iter_body"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty
              * Script_tc_errors.unparsed_stack_ty) (loc, rest, aft)) in
      Lwt._return
        (Error_monad.record_trace_eval invalid_iter_body
          (let? '(Eq, ctxt) := stacks_eq ctxt 1 aft rest in
          typed_no_lwt ctxt loc (Script_typed_ir.Map_iter ibody) rest))
    | Failed {| judgement.Failed.descr := descr_value |} =>
      typed ctxt loc (Script_typed_ir.Map_iter (descr_value rest)) rest
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MEM [] _annot,
      Script_typed_ir.Item_t vk
        (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck _) rest)) =>
    let k := ty_of_comparable_ty ck in
    let=? '(Eq, ctxt) :=
      return= (check_item_ty ctxt vk k loc Michelson_v1_primitives.I_MEM 1 2) in
    typed ctxt loc Script_typed_ir.Map_mem
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GET [] _annot,
      Script_typed_ir.Item_t vk
        (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck elt_value) rest)) =>
    let k := ty_of_comparable_ty ck in
    let=? '(Eq, ctxt) :=
      return= (check_item_ty ctxt vk k loc Michelson_v1_primitives.I_GET 1 2) in
    typed ctxt loc Script_typed_ir.Map_get
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t elt_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UPDATE [] _annot,
      Script_typed_ir.Item_t vk
        (Script_typed_ir.Item_t (Script_typed_ir.Option_t vv)
          (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck v) rest))) =>
    let k := ty_of_comparable_ty ck in
    let=? '(Eq, ctxt) :=
      return= (check_item_ty ctxt vk k loc Michelson_v1_primitives.I_UPDATE 1 3)
      in
    let=? '(Eq, ctxt) :=
      return= (check_item_ty ctxt vv v loc Michelson_v1_primitives.I_UPDATE 2 3)
      in
    typed ctxt loc Script_typed_ir.Map_update
      (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck v) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GET_AND_UPDATE [] _annot,
      Script_typed_ir.Item_t vk
        (Script_typed_ir.Item_t (Script_typed_ir.Option_t vv)
          (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck v) rest))) =>
    let k := ty_of_comparable_ty ck in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt vk k loc Michelson_v1_primitives.I_GET_AND_UPDATE 1
          3) in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt vv v loc Michelson_v1_primitives.I_GET_AND_UPDATE 2
          3) in
    typed ctxt loc Script_typed_ir.Map_get_and_update
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t vv)
        (Script_typed_ir.Item_t (Script_typed_ir.Map_t ck v) rest))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SIZE [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Map_t _ _) rest) =>
    typed ctxt loc Script_typed_ir.Map_size
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EMPTY_BIG_MAP
      (cons tk (cons tv [])) _annot, stack_value) =>
    let=? '(Ex_comparable_ty tk, ctxt) := return= (parse_comparable_ty ctxt tk)
      in
    let=? '(Ex_ty tv, ctxt) := return= (parse_big_map_value_ty ctxt legacy tv)
      in
    typed ctxt loc (Script_typed_ir.Empty_big_map tk tv)
      (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t tk tv) stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MEM [] _annot,
      Script_typed_ir.Item_t set_key
        (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t map_key _) rest)) =>
    let k := ty_of_comparable_ty map_key in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt set_key k loc Michelson_v1_primitives.I_MEM 1 2) in
    typed ctxt loc Script_typed_ir.Big_map_mem
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GET [] _annot,
      Script_typed_ir.Item_t vk
        (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t ck elt_value) rest))
    =>
    let k := ty_of_comparable_ty ck in
    let=? '(Eq, ctxt) :=
      return= (check_item_ty ctxt vk k loc Michelson_v1_primitives.I_GET 1 2) in
    typed ctxt loc Script_typed_ir.Big_map_get
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t elt_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UPDATE [] _annot,
      Script_typed_ir.Item_t set_key
        (Script_typed_ir.Item_t (Script_typed_ir.Option_t set_value)
          (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t map_key map_value)
            rest))) =>
    let k := ty_of_comparable_ty map_key in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt set_key k loc Michelson_v1_primitives.I_UPDATE 1 3)
      in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt set_value map_value loc
          Michelson_v1_primitives.I_UPDATE 2 3) in
    typed ctxt loc Script_typed_ir.Big_map_update
      (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t map_key map_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GET_AND_UPDATE [] _annot,
      Script_typed_ir.Item_t vk
        (Script_typed_ir.Item_t (Script_typed_ir.Option_t vv)
          (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t ck v) rest))) =>
    let k := ty_of_comparable_ty ck in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt vk k loc Michelson_v1_primitives.I_GET_AND_UPDATE 1
          3) in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt vv v loc Michelson_v1_primitives.I_GET_AND_UPDATE 2
          3) in
    typed ctxt loc Script_typed_ir.Big_map_get_and_update
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t vv)
        (Script_typed_ir.Item_t (Script_typed_ir.Big_map_t ck v) rest))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SAPLING_EMPTY_STATE
      (cons memo_size []) _annot, rest) =>
    let=? memo_size := return= (parse_memo_size memo_size) in
    typed ctxt loc
      (Script_typed_ir.Sapling_empty_state
        {| Script_typed_ir.instr.Sapling_empty_state.memo_size := memo_size |})
      (Script_typed_ir.Item_t (Script_typed_ir.Sapling_state_t memo_size) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SAPLING_VERIFY_UPDATE [] _,
      Script_typed_ir.Item_t
        (Script_typed_ir.Sapling_transaction_t transaction_memo_size)
        (Script_typed_ir.Item_t
          ((Script_typed_ir.Sapling_state_t state_memo_size) as state_ty) rest))
    =>
    let=? '_ := return= (memo_size_eq state_memo_size transaction_memo_size) in
    typed ctxt loc Script_typed_ir.Sapling_verify_update
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Int_t state_ty)) rest)
  | (Micheline.Seq loc [], stack_value) =>
    typed ctxt loc Script_typed_ir.Nop stack_value
  | (Micheline.Seq loc (cons single []), stack_value) =>
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy
        single stack_value in
    match judgement_value with
    | Typed ({| Script_typed_ir.descr.aft := aft |} as instr) =>
      let nop :=
        {| Script_typed_ir.descr.loc := loc; Script_typed_ir.descr.bef := aft;
          Script_typed_ir.descr.aft := aft;
          Script_typed_ir.descr.instr := Script_typed_ir.Nop |} in
      typed ctxt loc (Script_typed_ir.Seq instr nop) aft
    | Failed {| judgement.Failed.descr := descr_value |} =>
      let descr_value (aft : Script_typed_ir.stack_ty)
        : Script_typed_ir.descr :=
        let nop :=
          {| Script_typed_ir.descr.loc := loc; Script_typed_ir.descr.bef := aft;
            Script_typed_ir.descr.aft := aft;
            Script_typed_ir.descr.instr := Script_typed_ir.Nop |} in
        let descr_value := descr_value aft in
        Script_typed_ir.descr.with_instr (Script_typed_ir.Seq descr_value nop)
          descr_value in
      _return ctxt (Failed {| judgement.Failed.descr := descr_value |})
    end
  | (Micheline.Seq loc (cons hd tl), stack_value) =>
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy hd
        stack_value in
    match judgement_value with
    | Failed _ =>
      Error_monad.fail
        (Build_extensible "Fail_not_in_tail_position"
          Alpha_context.Script.location (Micheline.location hd))
    | Typed ({| Script_typed_ir.descr.aft := middle |} as ihd) =>
      let=? '(judgement_value, ctxt) :=
        non_terminal_recursion type_logger_value tc_context_value ctxt legacy
          (Micheline.Seq (-1) tl) middle in
      match judgement_value with
      | Failed {| judgement.Failed.descr := descr_value |} =>
        let descr_value (ret : Script_typed_ir.stack_ty)
          : Script_typed_ir.descr :=
          {| Script_typed_ir.descr.loc := loc;
            Script_typed_ir.descr.bef := stack_value;
            Script_typed_ir.descr.aft := ret;
            Script_typed_ir.descr.instr :=
              Script_typed_ir.Seq ihd (descr_value ret) |} in
        _return ctxt (Failed {| judgement.Failed.descr := descr_value |})
      | Typed itl =>
        typed ctxt loc (Script_typed_ir.Seq ihd itl)
          itl.(Script_typed_ir.descr.aft)
      end
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_IF (cons bt (cons bf []))
      annot, (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest) as bef) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bt) in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] bf) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(btr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bt
        rest in
    let=? '(bfr, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy bf
        rest in
    let branch (ibt : Script_typed_ir.descr) (ibf : Script_typed_ir.descr)
      : Script_typed_ir.descr :=
      {| Script_typed_ir.descr.loc := loc; Script_typed_ir.descr.bef := bef;
        Script_typed_ir.descr.aft := ibt.(Script_typed_ir.descr.aft);
        Script_typed_ir.descr.instr := Script_typed_ir.If ibt ibf |} in
    let=? '(judgement_value, ctxt) :=
      return= (merge_branches ctxt loc btr bfr {| branch.branch := branch |}) in
    _return ctxt judgement_value
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LOOP (cons body []) annot,
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest) as stack_value) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        rest in
    match judgement_value with
    | Typed ibody =>
      let unmatched_branches (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
          in
        return?
          (Build_extensible "Unmatched_branches"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty
              * Script_tc_errors.unparsed_stack_ty) (loc, aft, stack_value)) in
      Lwt._return
        (Error_monad.record_trace_eval unmatched_branches
          (let? '(Eq, ctxt) :=
            stacks_eq ctxt 1 ibody.(Script_typed_ir.descr.aft) stack_value in
          typed_no_lwt ctxt loc (Script_typed_ir.Loop ibody) rest))
    | Failed {| judgement.Failed.descr := descr_value |} =>
      let ibody := descr_value stack_value in
      typed ctxt loc (Script_typed_ir.Loop ibody) rest
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LOOP_LEFT (cons body [])
      _annot,
      (Script_typed_ir.Item_t (Script_typed_ir.Union_t tl tr) rest) as
        stack_value) =>
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] body) in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value tc_context_value ctxt legacy body
        (Script_typed_ir.Item_t tl rest) in
    match judgement_value with
    | Typed ibody =>
      let unmatched_branches (function_parameter : unit)
        : M? Error_monad._error :=
        let '_ := function_parameter in
        let? '(aft, ctxt) :=
          serialize_stack_for_error ctxt ibody.(Script_typed_ir.descr.aft) in
        let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
          in
        return?
          (Build_extensible "Unmatched_branches"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty
              * Script_tc_errors.unparsed_stack_ty) (loc, aft, stack_value)) in
      Lwt._return
        (Error_monad.record_trace_eval unmatched_branches
          (let? '(Eq, ctxt) :=
            stacks_eq ctxt 1 ibody.(Script_typed_ir.descr.aft) stack_value in
          typed_no_lwt ctxt loc (Script_typed_ir.Loop_left ibody)
            (Script_typed_ir.Item_t tr rest)))
    | Failed {| judgement.Failed.descr := descr_value |} =>
      let ibody := descr_value stack_value in
      typed ctxt loc (Script_typed_ir.Loop_left ibody)
        (Script_typed_ir.Item_t tr rest)
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LAMBDA
      (cons arg (cons ret (cons code []))) _annot, stack_value) =>
    let=? '(Ex_ty arg, ctxt) := return= (parse_any_ty ctxt legacy arg) in
    let=? '(Ex_ty ret, ctxt) := return= (parse_any_ty ctxt legacy ret) in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] code) in
    let=? '(lambda, ctxt) :=
      parse_returning type_logger_value stack_depth Lambda ctxt legacy arg ret
        code in
    typed ctxt loc (Script_typed_ir.Lambda lambda)
      (Script_typed_ir.Item_t (Script_typed_ir.Lambda_t arg ret) stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EXEC [] _annot,
      Script_typed_ir.Item_t arg
        (Script_typed_ir.Item_t (Script_typed_ir.Lambda_t param ret) rest)) =>
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt arg param loc Michelson_v1_primitives.I_EXEC 1 2) in
    typed ctxt loc Script_typed_ir.Exec (Script_typed_ir.Item_t ret rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_APPLY [] _annot,
      Script_typed_ir.Item_t capture
        (Script_typed_ir.Item_t
          (Script_typed_ir.Lambda_t (Script_typed_ir.Pair_t capture_ty arg_ty)
            ret) rest)) =>
    let=? '_ := return= (check_packable false loc capture_ty) in
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt capture capture_ty loc
          Michelson_v1_primitives.I_APPLY 1 2) in
    typed ctxt loc (Script_typed_ir.Apply capture_ty)
      (Script_typed_ir.Item_t (Script_typed_ir.Lambda_t arg_ty ret) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DIP (cons code []) annot,
      Script_typed_ir.Item_t v rest) =>
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '_ := return= (check_kind [ Script_tc_errors.Seq_kind ] code) in
    let=? '(judgement_value, ctxt) :=
      non_terminal_recursion type_logger_value (add_dip v tc_context_value) ctxt
        legacy code rest in
    match judgement_value with
    | Typed descr_value =>
      typed ctxt loc (Script_typed_ir.Dip descr_value)
        (Script_typed_ir.Item_t v descr_value.(Script_typed_ir.descr.aft))
    | Failed _ =>
      Error_monad.fail
        (Build_extensible "Fail_not_in_tail_position"
          Alpha_context.Script.location loc)
    end
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DIP (cons n (cons code []))
      result_annot, stack_value) =>
    let=? n := return= (parse_uint10 n) in
    let=? ctxt :=
      return=
        (Alpha_context.Gas.consume ctxt (Typecheck_costs.proof_argument n)) in
    let fix make_proof_argument
      (n : int) (inner_tc_context : tc_context) (stk : Script_typed_ir.stack_ty)
      : M=? dipn_proof_argument :=
      match ((n =i 0), stk) with
      | (true, rest) =>
        let=? '(judgement_value, ctxt) :=
          non_terminal_recursion type_logger_value inner_tc_context ctxt legacy
            code rest in
        Lwt._return
          match judgement_value with
          | Typed descr_value =>
            return?
              (Dipn_proof_argument
                (Script_typed_ir.Rest, (ctxt, descr_value),
                  descr_value.(Script_typed_ir.descr.aft)))
          | Failed _ =>
            Error_monad.error_value
              (Build_extensible "Fail_not_in_tail_position"
                Alpha_context.Script.location loc)
          end
      | (false, Script_typed_ir.Item_t v rest) =>
        let=? 'Dipn_proof_argument (n', descr_value, aft') :=
          make_proof_argument (n -i 1) (add_dip v tc_context_value) rest in
        return=?
          (Dipn_proof_argument
            ((Script_typed_ir.Prefix n'), descr_value,
              (Script_typed_ir.Item_t v aft')))
      | (_, _) =>
        Lwt._return
          (let? '(whole_stack, _ctxt) :=
            serialize_stack_for_error ctxt stack_value in
          Error_monad.error_value
            (Build_extensible "Bad_stack"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                Script_tc_errors.unparsed_stack_ty)
              (loc, Michelson_v1_primitives.I_DIP, 1, whole_stack)))
      end in
    let=? '_ :=
      return= (Script_ir_annot.error_unexpected_annot loc result_annot) in
    let=? 'Dipn_proof_argument (n', (new_ctxt, descr_value), aft) :=
      make_proof_argument n tc_context_value stack_value in
    typed new_ctxt loc (Script_typed_ir.Dipn n n' descr_value) aft
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_DIP
      (([] | cons _ (cons _ (cons _ _))) as l_value) _, _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.I_DIP, 2, (List.length l_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_FAILWITH [] annot,
      Script_typed_ir.Item_t v _rest) =>
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          check_packable false loc v) in
    let descr_value (aft : Script_typed_ir.stack_ty) : Script_typed_ir.descr :=
      {| Script_typed_ir.descr.loc := loc;
        Script_typed_ir.descr.bef := stack_ty; Script_typed_ir.descr.aft := aft;
        Script_typed_ir.descr.instr := Script_typed_ir.Failwith v |} in
    let=? '_ := return= (log_stack ctxt loc stack_ty Script_typed_ir.Empty_t) in
    _return ctxt (Failed {| judgement.Failed.descr := descr_value |})
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEVER [] annot,
      Script_typed_ir.Item_t Script_typed_ir.Never_t _rest) =>
    let=? '_ := return= (Script_ir_annot.error_unexpected_annot loc annot) in
    let descr_value (aft : Script_typed_ir.stack_ty) : Script_typed_ir.descr :=
      {| Script_typed_ir.descr.loc := loc;
        Script_typed_ir.descr.bef := stack_ty; Script_typed_ir.descr.aft := aft;
        Script_typed_ir.descr.instr := Script_typed_ir.Never |} in
    let=? '_ := return= (log_stack ctxt loc stack_ty Script_typed_ir.Empty_t) in
    _return ctxt (Failed {| judgement.Failed.descr := descr_value |})
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Timestamp_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_timestamp_to_seconds
      (Script_typed_ir.Item_t Script_typed_ir.Timestamp_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Timestamp_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_seconds_to_timestamp
      (Script_typed_ir.Item_t Script_typed_ir.Timestamp_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Timestamp_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Sub_timestamp_seconds
      (Script_typed_ir.Item_t Script_typed_ir.Timestamp_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Timestamp_t
        (Script_typed_ir.Item_t Script_typed_ir.Timestamp_t rest)) =>
    typed ctxt loc Script_typed_ir.Diff_timestamps
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CONCAT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.String_t
        (Script_typed_ir.Item_t Script_typed_ir.String_t rest)) =>
    typed ctxt loc Script_typed_ir.Concat_string_pair
      (Script_typed_ir.Item_t Script_typed_ir.String_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CONCAT [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.List_t Script_typed_ir.String_t)
        rest) =>
    typed ctxt loc Script_typed_ir.Concat_string
      (Script_typed_ir.Item_t Script_typed_ir.String_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SLICE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t
          (Script_typed_ir.Item_t Script_typed_ir.String_t rest))) =>
    typed ctxt loc Script_typed_ir.Slice_string
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t Script_typed_ir.String_t) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SIZE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.String_t rest) =>
    typed ctxt loc Script_typed_ir.String_size
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CONCAT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t
        (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)) =>
    typed ctxt loc Script_typed_ir.Concat_bytes_pair
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CONCAT [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.List_t Script_typed_ir.Bytes_t)
        rest) =>
    typed ctxt loc Script_typed_ir.Concat_bytes
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SLICE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t
          (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest))) =>
    typed ctxt loc Script_typed_ir.Slice_bytes
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t Script_typed_ir.Bytes_t)
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SIZE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    typed ctxt loc Script_typed_ir.Bytes_size
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Mutez_t
        (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_tez
      (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Mutez_t
        (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)) =>
    typed ctxt loc Script_typed_ir.Sub_tez
      (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Mutez_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_teznat
      (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_nattez
      (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_OR [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bool_t
        (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)) =>
    typed ctxt loc Script_typed_ir.Or
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_AND [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bool_t
        (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)) =>
    typed ctxt loc Script_typed_ir.And
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_XOR [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bool_t
        (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)) =>
    typed ctxt loc Script_typed_ir.Xor
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NOT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bool_t rest) =>
    typed ctxt loc Script_typed_ir.Not
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ABS [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Abs_int
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ISNAT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Is_nat
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t Script_typed_ir.Nat_t)
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_INT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t rest) =>
    typed ctxt loc Script_typed_ir.Int_nat
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEG [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Neg_int
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEG [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t rest) =>
    typed ctxt loc Script_typed_ir.Neg_nat
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_intint
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_intnat
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_natint
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_natnat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Sub_int
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Sub_int
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Sub_int
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SUB [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Sub_int
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_intint
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_intnat
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_natint
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_natnat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EDIV [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Mutez_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Ediv_teznat
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Mutez_t
            Script_typed_ir.Mutez_t)) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EDIV [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Mutez_t
        (Script_typed_ir.Item_t Script_typed_ir.Mutez_t rest)) =>
    typed ctxt loc Script_typed_ir.Ediv_tez
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Nat_t Script_typed_ir.Mutez_t))
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EDIV [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Ediv_intint
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Int_t Script_typed_ir.Nat_t))
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EDIV [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Ediv_intnat
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Int_t Script_typed_ir.Nat_t))
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EDIV [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Ediv_natint
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Int_t Script_typed_ir.Nat_t))
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EDIV [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Ediv_natnat
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t
          (Script_typed_ir.Pair_t Script_typed_ir.Nat_t Script_typed_ir.Nat_t))
        rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LSL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Lsl_nat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LSR [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Lsr_nat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_OR [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Or_nat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_AND [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.And_nat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_AND [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.And_int_nat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_XOR [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Xor_nat
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NOT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Not_int
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NOT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t rest) =>
    typed ctxt loc Script_typed_ir.Not_nat
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_COMPARE [] _annot,
      Script_typed_ir.Item_t t1 (Script_typed_ir.Item_t t2 rest)) =>
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt t1 t2 loc Michelson_v1_primitives.I_COMPARE 1 2) in
    let=? '(key_value, ctxt) := return= (comparable_ty_of_ty ctxt loc t1) in
    typed ctxt loc (Script_typed_ir.Compare key_value)
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_EQ [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Eq
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEQ [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Neq
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Lt
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Gt
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Le
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_GE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t rest) =>
    typed ctxt loc Script_typed_ir.Ge
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CAST (cons cast_t []) _annot,
      Script_typed_ir.Item_t t_value stack_value) =>
    let=? '(Ex_ty cast_t, ctxt) := return= (parse_any_ty ctxt legacy cast_t) in
    let=? '(Eq, ctxt) := return= (ty_eq ctxt cast_t t_value) in
    typed ctxt loc Script_typed_ir.Nop
      (Script_typed_ir.Item_t cast_t stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_RENAME [] _annot,
      Script_typed_ir.Item_t t_value stack_value) =>
    typed ctxt loc Script_typed_ir.Nop
      (Script_typed_ir.Item_t t_value stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_PACK [] _annot,
      Script_typed_ir.Item_t t_value rest) =>
    let=? '_ := return= (check_packable true loc t_value) in
    typed ctxt loc (Script_typed_ir.Pack t_value)
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_UNPACK (cons ty []) _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    let=? '(Ex_ty t_value, ctxt) := return= (parse_packable_ty ctxt legacy ty)
      in
    typed ctxt loc (Script_typed_ir.Unpack t_value)
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t t_value) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADDRESS [] _annot,
      Script_typed_ir.Item_t (Script_typed_ir.Contract_t _) rest) =>
    typed ctxt loc Script_typed_ir.Address
      (Script_typed_ir.Item_t Script_typed_ir.Address_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CONTRACT (cons ty []) annot,
      Script_typed_ir.Item_t Script_typed_ir.Address_t rest) =>
    let=? '(Ex_ty t_value, ctxt) := return= (parse_parameter_ty ctxt legacy ty)
      in
    let=? entrypoint :=
      return= (Script_ir_annot.parse_entrypoint_annot loc annot) in
    let=? entrypoint :=
      match entrypoint with
      | None => return= (Pervasives.Ok "default")
      | Some (Script_typed_ir.Field_annot "default") =>
        return=
          (Error_monad.error_value
            (Build_extensible "Unexpected_annotation"
              Alpha_context.Script.location loc))
      | Some (Script_typed_ir.Field_annot entrypoint) =>
        return=
          (if (String.length entrypoint) >i 31 then
            Error_monad.error_value
              (Build_extensible "Entrypoint_name_too_long" string entrypoint)
          else
            Pervasives.Ok entrypoint)
      end in
    typed ctxt loc (Script_typed_ir.Contract t_value entrypoint)
      (Script_typed_ir.Item_t
        (Script_typed_ir.Option_t (Script_typed_ir.Contract_t t_value)) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_TRANSFER_TOKENS [] _annot,
      Script_typed_ir.Item_t p_value
        (Script_typed_ir.Item_t Script_typed_ir.Mutez_t
          (Script_typed_ir.Item_t (Script_typed_ir.Contract_t cp) rest))) =>
    let=? '(Eq, ctxt) :=
      return=
        (check_item_ty ctxt p_value cp loc
          Michelson_v1_primitives.I_TRANSFER_TOKENS 1 4) in
    typed ctxt loc Script_typed_ir.Transfer_tokens
      (Script_typed_ir.Item_t Script_typed_ir.Operation_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SET_DELEGATE [] _annot,
      Script_typed_ir.Item_t
        (Script_typed_ir.Option_t Script_typed_ir.Key_hash_t) rest) =>
    typed ctxt loc Script_typed_ir.Set_delegate
      (Script_typed_ir.Item_t Script_typed_ir.Operation_t rest)
  | (Micheline.Prim _ Michelson_v1_primitives.I_CREATE_ACCOUNT _ _, _) =>
    Error_monad.fail
      (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim
        Michelson_v1_primitives.I_CREATE_ACCOUNT)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_IMPLICIT_ACCOUNT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Key_hash_t rest) =>
    typed ctxt loc Script_typed_ir.Implicit_account
      (Script_typed_ir.Item_t
        (Script_typed_ir.Contract_t Script_typed_ir.Unit_t) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CREATE_CONTRACT
      (cons ((Micheline.Seq _ _) as code) []) _annot,
      Script_typed_ir.Item_t
        (Script_typed_ir.Option_t Script_typed_ir.Key_hash_t)
        (Script_typed_ir.Item_t Script_typed_ir.Mutez_t
          (Script_typed_ir.Item_t ginit rest))) =>
    let canonical_code := Pervasives.fst (Micheline.extract_locations code) in
    let=? '{|
      toplevel.code_field := code_field;
        toplevel.arg_type := arg_type;
        toplevel.storage_type := storage_type;
        toplevel.root_name := root_name
        |} := return= (parse_toplevel legacy canonical_code) in
    let=? '(Ex_parameter_ty_entrypoints arg_type entrypoints, ctxt) :=
      return=
        (Error_monad.record_trace
          (Build_extensible "Ill_formed_type"
            (option string * Micheline.canonical Alpha_context.Script.prim *
              Alpha_context.Script.location)
            ((Some "parameter"), canonical_code, (location arg_type)))
          (parse_parameter_ty_and_entrypoints ctxt legacy root_name arg_type))
      in
    let=? '(Ex_ty storage_type, ctxt) :=
      return=
        (Error_monad.record_trace
          (Build_extensible "Ill_formed_type"
            (option string * Micheline.canonical Alpha_context.Script.prim *
              Alpha_context.Script.location)
            ((Some "storage"), canonical_code, (location storage_type)))
          (parse_storage_ty ctxt legacy storage_type)) in
    let arg_type_full := Script_typed_ir.Pair_t arg_type storage_type in
    let ret_type_full :=
      Script_typed_ir.Pair_t
        (Script_typed_ir.List_t Script_typed_ir.Operation_t) storage_type in
    let=? function_parameter :=
      Error_monad.trace_value
        (Build_extensible "Ill_typed_contract"
          (Micheline.canonical Alpha_context.Script.prim *
            Script_tc_errors.type_map) (canonical_code, nil))
        (parse_returning type_logger_value stack_depth
          (Toplevel
            {| tc_context.Toplevel.storage_type := storage_type;
              tc_context.Toplevel.param_type := arg_type;
              tc_context.Toplevel.entrypoints := entrypoints |}) ctxt legacy
          arg_type_full ret_type_full code_field) in
    match function_parameter with
    |
      ((Script_typed_ir.Lam {|
        Script_typed_ir.descr.bef :=
          Script_typed_ir.Item_t arg Script_typed_ir.Empty_t;
          Script_typed_ir.descr.aft :=
            Script_typed_ir.Item_t ret Script_typed_ir.Empty_t
          |} _) as lambda, ctxt) =>
      let=? '(Eq, ctxt) := return= (ty_eq ctxt arg arg_type_full) in
      let=? '(Eq, ctxt) := return= (ty_eq ctxt ret ret_type_full) in
      let=? '(Eq, ctxt) := return= (ty_eq ctxt storage_type ginit) in
      typed ctxt loc
        (Script_typed_ir.Create_contract storage_type arg_type lambda
          entrypoints)
        (Script_typed_ir.Item_t Script_typed_ir.Operation_t
          (Script_typed_ir.Item_t Script_typed_ir.Address_t rest))
    | _ => unreachable_gadt_branch
    end
  | (Micheline.Prim loc Michelson_v1_primitives.I_NOW [] _annot, stack_value) =>
    typed ctxt loc Script_typed_ir.Now
      (Script_typed_ir.Item_t Script_typed_ir.Timestamp_t stack_value)
  | (Micheline.Prim loc Michelson_v1_primitives.I_AMOUNT [] _annot, stack_value)
    =>
    typed ctxt loc Script_typed_ir.Amount
      (Script_typed_ir.Item_t Script_typed_ir.Mutez_t stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CHAIN_ID [] _annot,
      stack_value) =>
    typed ctxt loc Script_typed_ir.ChainId
      (Script_typed_ir.Item_t Script_typed_ir.Chain_id_t stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_BALANCE [] _annot, stack_value)
    =>
    typed ctxt loc Script_typed_ir.Balance
      (Script_typed_ir.Item_t Script_typed_ir.Mutez_t stack_value)
  | (Micheline.Prim loc Michelson_v1_primitives.I_LEVEL [] _annot, stack_value)
    =>
    typed ctxt loc Script_typed_ir.Level
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_VOTING_POWER [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Key_hash_t rest) =>
    typed ctxt loc Script_typed_ir.Voting_power
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_TOTAL_VOTING_POWER [] _annot,
      stack_value) =>
    typed ctxt loc Script_typed_ir.Total_voting_power
      (Script_typed_ir.Item_t Script_typed_ir.Nat_t stack_value)
  | (Micheline.Prim _ Michelson_v1_primitives.I_STEPS_TO_QUOTA _ _, _) =>
    Error_monad.fail
      (Build_extensible "Deprecated_instruction" Alpha_context.Script.prim
        Michelson_v1_primitives.I_STEPS_TO_QUOTA)
  | (Micheline.Prim loc Michelson_v1_primitives.I_SOURCE [] _annot, stack_value)
    =>
    typed ctxt loc Script_typed_ir.Source
      (Script_typed_ir.Item_t Script_typed_ir.Address_t stack_value)
  | (Micheline.Prim loc Michelson_v1_primitives.I_SENDER [] _annot, stack_value)
    =>
    typed ctxt loc Script_typed_ir.Sender
      (Script_typed_ir.Item_t Script_typed_ir.Address_t stack_value)
  | (Micheline.Prim loc Michelson_v1_primitives.I_SELF [] annot, stack_value) =>
    Lwt._return
      (let? entrypoint := Script_ir_annot.parse_entrypoint_annot loc annot in
      let entrypoint :=
        Option.fold "default"
          (fun (function_parameter : Script_typed_ir.field_annot) =>
            let 'Script_typed_ir.Field_annot annot := function_parameter in
            annot) entrypoint in
      let fix get_toplevel_type (function_parameter : tc_context)
        : M? (judgement * Alpha_context.context) :=
        match function_parameter with
        | Lambda =>
          Error_monad.error_value
            (Build_extensible "Self_in_lambda" Alpha_context.Script.location loc)
        | Dip _ prev => get_toplevel_type prev
        |
          Toplevel {|
            tc_context.Toplevel.param_type := param_type;
              tc_context.Toplevel.entrypoints := entrypoints
              |} =>
          let? '(_, Ex_ty param_type) :=
            find_entrypoint param_type entrypoints entrypoint in
          typed_no_lwt ctxt loc (Script_typed_ir.Self param_type entrypoint)
            (Script_typed_ir.Item_t (Script_typed_ir.Contract_t param_type)
              stack_value)
        end in
      get_toplevel_type tc_context_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SELF_ADDRESS [] _annot,
      stack_value) =>
    typed ctxt loc Script_typed_ir.Self_address
      (Script_typed_ir.Item_t Script_typed_ir.Address_t stack_value)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_HASH_KEY [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Key_t rest) =>
    typed ctxt loc Script_typed_ir.Hash_key
      (Script_typed_ir.Item_t Script_typed_ir.Key_hash_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CHECK_SIGNATURE [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Key_t
        (Script_typed_ir.Item_t Script_typed_ir.Signature_t
          (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest))) =>
    typed ctxt loc Script_typed_ir.Check_signature
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_BLAKE2B [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    typed ctxt loc Script_typed_ir.Blake2b
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SHA256 [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    typed ctxt loc Script_typed_ir.Sha256
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SHA512 [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    typed ctxt loc Script_typed_ir.Sha512
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_KECCAK [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    typed ctxt loc Script_typed_ir.Keccak
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SHA3 [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest) =>
    typed ctxt loc Script_typed_ir.Sha3
      (Script_typed_ir.Item_t Script_typed_ir.Bytes_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_bls12_381_g1
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_bls12_381_g2
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_ADD [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)) =>
    typed ctxt loc Script_typed_ir.Add_bls12_381_fr
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_g1
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_g2
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_fr
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Nat_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_fr_z
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Int_t
        (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_fr_z
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t
        (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_z_fr
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_MUL [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    typed ctxt loc Script_typed_ir.Mul_bls12_381_z_fr
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_INT [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest) =>
    typed ctxt loc Script_typed_ir.Int_bls12_381_fr
      (Script_typed_ir.Item_t Script_typed_ir.Int_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEG [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t rest) =>
    typed ctxt loc Script_typed_ir.Neg_bls12_381_g1
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g1_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEG [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t rest) =>
    typed ctxt loc Script_typed_ir.Neg_bls12_381_g2
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_g2_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_NEG [] _annot,
      Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest) =>
    typed ctxt loc Script_typed_ir.Neg_bls12_381_fr
      (Script_typed_ir.Item_t Script_typed_ir.Bls12_381_fr_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_PAIRING_CHECK [] _annot,
      Script_typed_ir.Item_t
        (Script_typed_ir.List_t
          (Script_typed_ir.Pair_t Script_typed_ir.Bls12_381_g1_t
            Script_typed_ir.Bls12_381_g2_t)) rest) =>
    typed ctxt loc Script_typed_ir.Pairing_check_bls12_381
      (Script_typed_ir.Item_t Script_typed_ir.Bool_t rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_TICKET [] _annot,
      Script_typed_ir.Item_t t_value
        (Script_typed_ir.Item_t Script_typed_ir.Nat_t rest)) =>
    let=? '(ty, ctxt) := return= (comparable_ty_of_ty ctxt loc t_value) in
    typed ctxt loc Script_typed_ir.Ticket
      (Script_typed_ir.Item_t (Script_typed_ir.Ticket_t ty) rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_READ_TICKET [] _annot,
      (Script_typed_ir.Item_t (Script_typed_ir.Ticket_t t_value) _) as
        full_stack) =>
    let '_ := check_dupable_comparable_ty t_value in
    let result_value := ty_of_comparable_ty (opened_ticket_type t_value) in
    typed ctxt loc Script_typed_ir.Read_ticket
      (Script_typed_ir.Item_t result_value full_stack)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_SPLIT_TICKET [] _annot,
      Script_typed_ir.Item_t ((Script_typed_ir.Ticket_t t_value) as ticket_t)
        (Script_typed_ir.Item_t
          (Script_typed_ir.Pair_t Script_typed_ir.Nat_t Script_typed_ir.Nat_t)
          rest)) =>
    let '_ := check_dupable_comparable_ty t_value in
    let result_value :=
      Script_typed_ir.Option_t (Script_typed_ir.Pair_t ticket_t ticket_t) in
    typed ctxt loc Script_typed_ir.Split_ticket
      (Script_typed_ir.Item_t result_value rest)
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_JOIN_TICKETS [] _annot,
      Script_typed_ir.Item_t
        (Script_typed_ir.Pair_t ((Script_typed_ir.Ticket_t contents_ty) as ty_a)
          ((Script_typed_ir.Ticket_t _) as ty_b)) rest) =>
    let=? '(Eq, ctxt) := return= (ty_eq ctxt ty_a ty_b) in
    typed ctxt loc (Script_typed_ir.Join_tickets contents_ty)
      (Script_typed_ir.Item_t (Script_typed_ir.Option_t ty_a) rest)
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_DUP | Michelson_v1_primitives.I_SWAP |
      Michelson_v1_primitives.I_SOME | Michelson_v1_primitives.I_UNIT |
      Michelson_v1_primitives.I_PAIR | Michelson_v1_primitives.I_UNPAIR |
      Michelson_v1_primitives.I_CAR | Michelson_v1_primitives.I_CDR |
      Michelson_v1_primitives.I_CONS | Michelson_v1_primitives.I_CONCAT |
      Michelson_v1_primitives.I_SLICE | Michelson_v1_primitives.I_MEM |
      Michelson_v1_primitives.I_UPDATE | Michelson_v1_primitives.I_GET |
      Michelson_v1_primitives.I_EXEC | Michelson_v1_primitives.I_FAILWITH |
      Michelson_v1_primitives.I_SIZE | Michelson_v1_primitives.I_ADD |
      Michelson_v1_primitives.I_SUB | Michelson_v1_primitives.I_MUL |
      Michelson_v1_primitives.I_EDIV | Michelson_v1_primitives.I_OR |
      Michelson_v1_primitives.I_AND | Michelson_v1_primitives.I_XOR |
      Michelson_v1_primitives.I_NOT | Michelson_v1_primitives.I_ABS |
      Michelson_v1_primitives.I_NEG | Michelson_v1_primitives.I_LSL |
      Michelson_v1_primitives.I_LSR | Michelson_v1_primitives.I_COMPARE |
      Michelson_v1_primitives.I_EQ | Michelson_v1_primitives.I_NEQ |
      Michelson_v1_primitives.I_LT | Michelson_v1_primitives.I_GT |
      Michelson_v1_primitives.I_LE | Michelson_v1_primitives.I_GE |
      Michelson_v1_primitives.I_TRANSFER_TOKENS |
      Michelson_v1_primitives.I_SET_DELEGATE | Michelson_v1_primitives.I_NOW |
      Michelson_v1_primitives.I_IMPLICIT_ACCOUNT |
      Michelson_v1_primitives.I_AMOUNT | Michelson_v1_primitives.I_BALANCE |
      Michelson_v1_primitives.I_LEVEL |
      Michelson_v1_primitives.I_CHECK_SIGNATURE |
      Michelson_v1_primitives.I_HASH_KEY | Michelson_v1_primitives.I_SOURCE |
      Michelson_v1_primitives.I_SENDER | Michelson_v1_primitives.I_BLAKE2B |
      Michelson_v1_primitives.I_SHA256 | Michelson_v1_primitives.I_SHA512 |
      Michelson_v1_primitives.I_ADDRESS | Michelson_v1_primitives.I_RENAME |
      Michelson_v1_primitives.I_PACK | Michelson_v1_primitives.I_ISNAT |
      Michelson_v1_primitives.I_INT | Michelson_v1_primitives.I_SELF |
      Michelson_v1_primitives.I_CHAIN_ID | Michelson_v1_primitives.I_NEVER |
      Michelson_v1_primitives.I_VOTING_POWER |
      Michelson_v1_primitives.I_TOTAL_VOTING_POWER |
      Michelson_v1_primitives.I_KECCAK | Michelson_v1_primitives.I_SHA3 |
      Michelson_v1_primitives.I_PAIRING_CHECK | Michelson_v1_primitives.I_TICKET
      | Michelson_v1_primitives.I_READ_TICKET |
      Michelson_v1_primitives.I_SPLIT_TICKET |
      Michelson_v1_primitives.I_JOIN_TICKETS) as name) ((cons _ _) as l_value) _,
      _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, name, 0, (List.length l_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_NONE | Michelson_v1_primitives.I_LEFT |
      Michelson_v1_primitives.I_RIGHT | Michelson_v1_primitives.I_NIL |
      Michelson_v1_primitives.I_MAP | Michelson_v1_primitives.I_ITER |
      Michelson_v1_primitives.I_EMPTY_SET | Michelson_v1_primitives.I_LOOP |
      Michelson_v1_primitives.I_LOOP_LEFT | Michelson_v1_primitives.I_CONTRACT |
      Michelson_v1_primitives.I_CAST | Michelson_v1_primitives.I_UNPACK |
      Michelson_v1_primitives.I_CREATE_CONTRACT) as name)
      (([] | cons _ (cons _ _)) as l_value) _, _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, name, 1, (List.length l_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_PUSH | Michelson_v1_primitives.I_IF_NONE |
      Michelson_v1_primitives.I_IF_LEFT | Michelson_v1_primitives.I_IF_CONS |
      Michelson_v1_primitives.I_EMPTY_MAP |
      Michelson_v1_primitives.I_EMPTY_BIG_MAP | Michelson_v1_primitives.I_IF) as
        name) (([] | cons _ [] | cons _ (cons _ (cons _ _))) as l_value) _, _)
    =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, name, 2, (List.length l_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_LAMBDA
      (([] | cons _ [] | cons _ (cons _ []) |
      cons _ (cons _ (cons _ (cons _ _)))) as l_value) _, _) =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.I_LAMBDA, 3, (List.length l_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_ADD | Michelson_v1_primitives.I_SUB |
      Michelson_v1_primitives.I_MUL | Michelson_v1_primitives.I_EDIV |
      Michelson_v1_primitives.I_AND | Michelson_v1_primitives.I_OR |
      Michelson_v1_primitives.I_XOR | Michelson_v1_primitives.I_LSL |
      Michelson_v1_primitives.I_LSR | Michelson_v1_primitives.I_CONCAT |
      Michelson_v1_primitives.I_PAIRING_CHECK) as name) [] _,
      Script_typed_ir.Item_t ta (Script_typed_ir.Item_t tb _)) =>
    let=? '(ta, ctxt) := return= (serialize_ty_for_error ctxt ta) in
    let=? '(tb, _ctxt) := return= (serialize_ty_for_error ctxt tb) in
    Error_monad.fail
      (Build_extensible "Undefined_binop"
        (Alpha_context.Script.location * Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim) (loc, name, ta, tb))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_NEG | Michelson_v1_primitives.I_ABS |
      Michelson_v1_primitives.I_NOT | Michelson_v1_primitives.I_SIZE |
      Michelson_v1_primitives.I_EQ | Michelson_v1_primitives.I_NEQ |
      Michelson_v1_primitives.I_LT | Michelson_v1_primitives.I_GT |
      Michelson_v1_primitives.I_LE | Michelson_v1_primitives.I_GE |
      Michelson_v1_primitives.I_CONCAT) as name) [] _,
      Script_typed_ir.Item_t t_value _) =>
    let=? '(t_value, _ctxt) := return= (serialize_ty_for_error ctxt t_value) in
    Error_monad.fail
      (Build_extensible "Undefined_unop"
        (Alpha_context.Script.location * Alpha_context.Script.prim *
          Micheline.canonical Alpha_context.Script.prim) (loc, name, t_value))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_UPDATE | Michelson_v1_primitives.I_SLICE) as
        name) [] _, stack_value) =>
    Lwt._return
      (let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
        in
      Error_monad.error_value
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty) (loc, name, 3, stack_value)))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_CREATE_CONTRACT _ _,
      stack_value) =>
    let=? '(stack_value, _ctxt) :=
      return= (serialize_stack_for_error ctxt stack_value) in
    Error_monad.fail
      (Build_extensible "Bad_stack"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int *
          Script_tc_errors.unparsed_stack_ty)
        (loc, Michelson_v1_primitives.I_CREATE_CONTRACT, 7, stack_value))
  |
    (Micheline.Prim loc Michelson_v1_primitives.I_TRANSFER_TOKENS [] _,
      stack_value) =>
    Lwt._return
      (let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
        in
      Error_monad.error_value
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty)
          (loc, Michelson_v1_primitives.I_TRANSFER_TOKENS, 4, stack_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_DROP | Michelson_v1_primitives.I_DUP |
      Michelson_v1_primitives.I_CAR | Michelson_v1_primitives.I_CDR |
      Michelson_v1_primitives.I_UNPAIR | Michelson_v1_primitives.I_SOME |
      Michelson_v1_primitives.I_BLAKE2B | Michelson_v1_primitives.I_SHA256 |
      Michelson_v1_primitives.I_SHA512 | Michelson_v1_primitives.I_DIP |
      Michelson_v1_primitives.I_IF_NONE | Michelson_v1_primitives.I_LEFT |
      Michelson_v1_primitives.I_RIGHT | Michelson_v1_primitives.I_IF_LEFT |
      Michelson_v1_primitives.I_IF | Michelson_v1_primitives.I_LOOP |
      Michelson_v1_primitives.I_IF_CONS |
      Michelson_v1_primitives.I_IMPLICIT_ACCOUNT | Michelson_v1_primitives.I_NEG
      | Michelson_v1_primitives.I_ABS | Michelson_v1_primitives.I_INT |
      Michelson_v1_primitives.I_NOT | Michelson_v1_primitives.I_HASH_KEY |
      Michelson_v1_primitives.I_EQ | Michelson_v1_primitives.I_NEQ |
      Michelson_v1_primitives.I_LT | Michelson_v1_primitives.I_GT |
      Michelson_v1_primitives.I_LE | Michelson_v1_primitives.I_GE |
      Michelson_v1_primitives.I_SIZE | Michelson_v1_primitives.I_FAILWITH |
      Michelson_v1_primitives.I_RENAME | Michelson_v1_primitives.I_PACK |
      Michelson_v1_primitives.I_ISNAT | Michelson_v1_primitives.I_ADDRESS |
      Michelson_v1_primitives.I_SET_DELEGATE | Michelson_v1_primitives.I_CAST |
      Michelson_v1_primitives.I_MAP | Michelson_v1_primitives.I_ITER |
      Michelson_v1_primitives.I_LOOP_LEFT | Michelson_v1_primitives.I_UNPACK |
      Michelson_v1_primitives.I_CONTRACT | Michelson_v1_primitives.I_NEVER |
      Michelson_v1_primitives.I_KECCAK | Michelson_v1_primitives.I_SHA3 |
      Michelson_v1_primitives.I_READ_TICKET |
      Michelson_v1_primitives.I_JOIN_TICKETS) as name) _ _, stack_value) =>
    Lwt._return
      (let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
        in
      Error_monad.error_value
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty) (loc, name, 1, stack_value)))
  |
    (Micheline.Prim loc
      ((Michelson_v1_primitives.I_SWAP | Michelson_v1_primitives.I_PAIR |
      Michelson_v1_primitives.I_CONS | Michelson_v1_primitives.I_GET |
      Michelson_v1_primitives.I_MEM | Michelson_v1_primitives.I_EXEC |
      Michelson_v1_primitives.I_CHECK_SIGNATURE | Michelson_v1_primitives.I_ADD
      | Michelson_v1_primitives.I_SUB | Michelson_v1_primitives.I_MUL |
      Michelson_v1_primitives.I_EDIV | Michelson_v1_primitives.I_AND |
      Michelson_v1_primitives.I_OR | Michelson_v1_primitives.I_XOR |
      Michelson_v1_primitives.I_LSL | Michelson_v1_primitives.I_LSR |
      Michelson_v1_primitives.I_COMPARE |
      Michelson_v1_primitives.I_PAIRING_CHECK | Michelson_v1_primitives.I_TICKET
      | Michelson_v1_primitives.I_SPLIT_TICKET) as name) _ _, stack_value) =>
    Lwt._return
      (let? '(stack_value, _ctxt) := serialize_stack_for_error ctxt stack_value
        in
      Error_monad.error_value
        (Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty) (loc, name, 2, stack_value)))
  | (expr, _) =>
    Error_monad.fail
      (unexpected expr [ Script_tc_errors.Seq_kind ]
        Michelson_v1_primitives.Instr_namespace
        [
          Michelson_v1_primitives.I_DROP;
          Michelson_v1_primitives.I_DUP;
          Michelson_v1_primitives.I_DIG;
          Michelson_v1_primitives.I_DUG;
          Michelson_v1_primitives.I_SWAP;
          Michelson_v1_primitives.I_SOME;
          Michelson_v1_primitives.I_UNIT;
          Michelson_v1_primitives.I_PAIR;
          Michelson_v1_primitives.I_UNPAIR;
          Michelson_v1_primitives.I_CAR;
          Michelson_v1_primitives.I_CDR;
          Michelson_v1_primitives.I_CONS;
          Michelson_v1_primitives.I_MEM;
          Michelson_v1_primitives.I_UPDATE;
          Michelson_v1_primitives.I_MAP;
          Michelson_v1_primitives.I_ITER;
          Michelson_v1_primitives.I_GET;
          Michelson_v1_primitives.I_GET_AND_UPDATE;
          Michelson_v1_primitives.I_EXEC;
          Michelson_v1_primitives.I_FAILWITH;
          Michelson_v1_primitives.I_SIZE;
          Michelson_v1_primitives.I_CONCAT;
          Michelson_v1_primitives.I_ADD;
          Michelson_v1_primitives.I_SUB;
          Michelson_v1_primitives.I_MUL;
          Michelson_v1_primitives.I_EDIV;
          Michelson_v1_primitives.I_OR;
          Michelson_v1_primitives.I_AND;
          Michelson_v1_primitives.I_XOR;
          Michelson_v1_primitives.I_NOT;
          Michelson_v1_primitives.I_ABS;
          Michelson_v1_primitives.I_INT;
          Michelson_v1_primitives.I_NEG;
          Michelson_v1_primitives.I_LSL;
          Michelson_v1_primitives.I_LSR;
          Michelson_v1_primitives.I_COMPARE;
          Michelson_v1_primitives.I_EQ;
          Michelson_v1_primitives.I_NEQ;
          Michelson_v1_primitives.I_LT;
          Michelson_v1_primitives.I_GT;
          Michelson_v1_primitives.I_LE;
          Michelson_v1_primitives.I_GE;
          Michelson_v1_primitives.I_TRANSFER_TOKENS;
          Michelson_v1_primitives.I_CREATE_CONTRACT;
          Michelson_v1_primitives.I_NOW;
          Michelson_v1_primitives.I_AMOUNT;
          Michelson_v1_primitives.I_BALANCE;
          Michelson_v1_primitives.I_LEVEL;
          Michelson_v1_primitives.I_IMPLICIT_ACCOUNT;
          Michelson_v1_primitives.I_CHECK_SIGNATURE;
          Michelson_v1_primitives.I_BLAKE2B;
          Michelson_v1_primitives.I_SHA256;
          Michelson_v1_primitives.I_SHA512;
          Michelson_v1_primitives.I_HASH_KEY;
          Michelson_v1_primitives.I_PUSH;
          Michelson_v1_primitives.I_NONE;
          Michelson_v1_primitives.I_LEFT;
          Michelson_v1_primitives.I_RIGHT;
          Michelson_v1_primitives.I_NIL;
          Michelson_v1_primitives.I_EMPTY_SET;
          Michelson_v1_primitives.I_DIP;
          Michelson_v1_primitives.I_LOOP;
          Michelson_v1_primitives.I_IF_NONE;
          Michelson_v1_primitives.I_IF_LEFT;
          Michelson_v1_primitives.I_IF_CONS;
          Michelson_v1_primitives.I_EMPTY_MAP;
          Michelson_v1_primitives.I_EMPTY_BIG_MAP;
          Michelson_v1_primitives.I_IF;
          Michelson_v1_primitives.I_SOURCE;
          Michelson_v1_primitives.I_SENDER;
          Michelson_v1_primitives.I_SELF;
          Michelson_v1_primitives.I_SELF_ADDRESS;
          Michelson_v1_primitives.I_LAMBDA;
          Michelson_v1_primitives.I_NEVER;
          Michelson_v1_primitives.I_VOTING_POWER;
          Michelson_v1_primitives.I_TOTAL_VOTING_POWER;
          Michelson_v1_primitives.I_KECCAK;
          Michelson_v1_primitives.I_SHA3;
          Michelson_v1_primitives.I_PAIRING_CHECK;
          Michelson_v1_primitives.I_SAPLING_EMPTY_STATE;
          Michelson_v1_primitives.I_SAPLING_VERIFY_UPDATE;
          Michelson_v1_primitives.I_TICKET;
          Michelson_v1_primitives.I_READ_TICKET;
          Michelson_v1_primitives.I_SPLIT_TICKET;
          Michelson_v1_primitives.I_JOIN_TICKETS
        ])
  end

where "'parse_returning" :=
  (fun
    (type_logger_value : option type_logger) (stack_depth : int)
    (tc_context_value : tc_context) (ctxt : Alpha_context.context)
    (legacy : bool) (arg : Script_typed_ir.ty) (ret : Script_typed_ir.ty)
    (script_instr : Alpha_context.Script.node) =>
    let=? function_parameter :=
      parse_instr_with_stack type_logger_value (stack_depth +i 1)
        tc_context_value ctxt legacy script_instr
        (Script_typed_ir.Item_t arg Script_typed_ir.Empty_t) in
    match function_parameter with
    |
      (Typed
        ({|
          Script_typed_ir.descr.loc := loc;
            Script_typed_ir.descr.aft :=
              (Script_typed_ir.Item_t ty Script_typed_ir.Empty_t) as
                stack_ty
            |} as descr_value), ctxt) =>
      Lwt._return
        (Error_monad.record_trace_eval
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let? '(ret, ctxt) := serialize_ty_for_error ctxt ret in
            let? '(stack_ty, _ctxt) := serialize_stack_for_error ctxt stack_ty
              in
            return?
              (Build_extensible "Bad_return"
                (Alpha_context.Script.location *
                  Script_tc_errors.unparsed_stack_ty *
                  Micheline.canonical Alpha_context.Script.prim)
                (loc, stack_ty, ret)))
          (let? '(Eq, ctxt) := ty_eq ctxt ty ret in
          return? ((Script_typed_ir.Lam descr_value script_instr), ctxt)))
    |
      (Typed {|
        Script_typed_ir.descr.loc := loc;
          Script_typed_ir.descr.aft := stack_ty
          |}, ctxt) =>
      Lwt._return
        (let? '(ret, ctxt) := serialize_ty_for_error ctxt ret in
        let? '(stack_ty, _ctxt) := serialize_stack_for_error ctxt stack_ty in
        Error_monad.error_value
          (Build_extensible "Bad_return"
            (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty
              * Micheline.canonical Alpha_context.Script.prim)
            (loc, stack_ty, ret)))
    | (Failed {| judgement.Failed.descr := descr_value |}, ctxt) =>
      Error_monad._return
        ((Script_typed_ir.Lam
          (descr_value (Script_typed_ir.Item_t ret Script_typed_ir.Empty_t))
          script_instr), ctxt)
    end).

Definition parse_returning := 'parse_returning.

Definition parse_code
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (code : Alpha_context.Script.lazy_expr)
  : M=? (ex_code * Alpha_context.context) :=
  let=? '(code, ctxt) :=
    return= (Alpha_context.Script.force_decode_in_context ctxt code) in
  let=? '{|
    toplevel.code_field := code_field;
      toplevel.arg_type := arg_type;
      toplevel.storage_type := storage_type;
      toplevel.root_name := root_name
      |} := return= (parse_toplevel legacy code) in
  let=? '(Ex_parameter_ty_entrypoints arg_type entrypoints, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "parameter"), code, (location arg_type)))
        (parse_parameter_ty_and_entrypoints ctxt legacy root_name arg_type)) in
  let=? '(Ex_ty storage_type, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "storage"), code, (location storage_type)))
        (parse_storage_ty ctxt legacy storage_type)) in
  let arg_type_full := Script_typed_ir.Pair_t arg_type storage_type in
  let ret_type_full :=
    Script_typed_ir.Pair_t (Script_typed_ir.List_t Script_typed_ir.Operation_t)
      storage_type in
  let=? '(code, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map) (code, nil))
      (parse_returning type_logger_value 0
        (Toplevel
          {| tc_context.Toplevel.storage_type := storage_type;
            tc_context.Toplevel.param_type := arg_type;
            tc_context.Toplevel.entrypoints := entrypoints |}) ctxt legacy
        arg_type_full ret_type_full code_field) in
  return=?
    ((Ex_code
      {| code.code := code; code.arg_type := arg_type;
        code.storage_type := storage_type; code.entrypoints := entrypoints |}),
      ctxt).

Definition parse_storage {storage : Set}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (allow_forged : bool) (storage_type : Script_typed_ir.ty)
  (storage_value : Alpha_context.Script.lazy_expr)
  : M=? (storage * Alpha_context.context) :=
  let=? '(storage_value, ctxt) :=
    return= (Alpha_context.Script.force_decode_in_context ctxt storage_value) in
  Error_monad.trace_eval
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Lwt._return
        (let? '(storage_type, _ctxt) := serialize_ty_for_error ctxt storage_type
          in
        return?
          (Build_extensible "Ill_typed_data"
            (option string * Alpha_context.Script.expr *
              Micheline.canonical Alpha_context.Script.prim)
            (None, storage_value, storage_type))))
    (parse_data_with_stack type_logger_value 0 ctxt legacy allow_forged
      storage_type (Micheline.root storage_value)).

Definition parse_script
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (allow_forged_in_storage : bool)
  (function_parameter : Alpha_context.Script.t)
  : M=? (ex_script * Alpha_context.context) :=
  let '{|
    Alpha_context.Script.t.code := code;
      Alpha_context.Script.t.storage := storage_value
      |} := function_parameter in
  let=?
    '(Ex_code {|
      code.code := code;
        code.arg_type := arg_type;
        code.storage_type := storage_type;
        code.entrypoints := entrypoints
        |}, ctxt) := parse_code type_logger_value ctxt legacy code in
  let 'existT _ [__Ex_code_'a, __Ex_code_'c]
    [ctxt, entrypoints, storage_type, arg_type, code] :=
    cast_exists (Es := [Set ** Set])
      (fun '[__Ex_code_'a, __Ex_code_'c] =>
        [Alpha_context.context ** Script_typed_ir.entrypoints **
          Script_typed_ir.ty ** Script_typed_ir.ty ** Script_typed_ir.lambda])
      [ctxt, entrypoints, storage_type, arg_type, code] in
  let=? '(storage_value, ctxt) :=
    parse_storage type_logger_value ctxt legacy allow_forged_in_storage
      storage_type storage_value in
  return=?
    ((Ex_script
      {| Script_typed_ir.script.code := code;
        Script_typed_ir.script.arg_type := arg_type;
        Script_typed_ir.script.storage :=
          (fun (x : __Ex_code_'c) => x) storage_value;
        Script_typed_ir.script.storage_type := storage_type;
        Script_typed_ir.script.entrypoints := entrypoints |}), ctxt).

Definition typecheck_code
  (legacy : bool) (ctxt : Alpha_context.context)
  (code : Alpha_context.Script.expr)
  : M=? (Script_tc_errors.type_map * Alpha_context.context) :=
  let=? '{|
    toplevel.code_field := code_field;
      toplevel.arg_type := arg_type;
      toplevel.storage_type := storage_type;
      toplevel.root_name := root_name
      |} := return= (parse_toplevel legacy code) in
  let type_map := Pervasives.ref_value nil in
  let=? '(Ex_parameter_ty_entrypoints arg_type entrypoints, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "parameter"), code, (location arg_type)))
        (parse_parameter_ty_and_entrypoints ctxt legacy root_name arg_type)) in
  let=? '(Ex_ty storage_type, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "storage"), code, (location storage_type)))
        (parse_storage_ty ctxt legacy storage_type)) in
  let arg_type_full := Script_typed_ir.Pair_t arg_type storage_type in
  let ret_type_full :=
    Script_typed_ir.Pair_t (Script_typed_ir.List_t Script_typed_ir.Operation_t)
      storage_type in
  let result_value :=
    parse_returning
      (Some
        (fun (loc : int) =>
          fun (bef : Script_tc_errors.unparsed_stack_ty) =>
            fun (aft : Script_tc_errors.unparsed_stack_ty) =>
              Pervasives.op_coloneq type_map
                (cons (loc, (bef, aft)) (Pervasives.op_exclamation type_map))))
      0
      (Toplevel
        {| tc_context.Toplevel.storage_type := storage_type;
          tc_context.Toplevel.param_type := arg_type;
          tc_context.Toplevel.entrypoints := entrypoints |}) ctxt legacy
      arg_type_full ret_type_full code_field in
  let=? '(Script_typed_ir.Lam _ _, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map)
        (code, (Pervasives.op_exclamation type_map))) result_value in
  return=? ((Pervasives.op_exclamation type_map), ctxt).

Definition Entrypoints_map :=
  Map.Make
    {|
      Compare.COMPARABLE.compare := String.compare
    |}.

Definition list_entrypoints
  (ctxt : Alpha_context.context) (full : Script_typed_ir.ty)
  (entrypoints : Script_typed_ir.entrypoints)
  : M?
    (list (list Alpha_context.Script.prim) *
      Entrypoints_map.(S.MAP.t)
        (list Alpha_context.Script.prim * Alpha_context.Script.node)) :=
  let merge {A : Set}
    (path : list A) (ty : Script_typed_ir.ty)
    (entrypoints : Script_typed_ir.entrypoints) (reachable : bool)
    (function_parameter :
      list (list A) *
        Entrypoints_map.(S.MAP.t) (list A * Alpha_context.Script.node))
    : M?
      ((list (list A) *
        Entrypoints_map.(S.MAP.t) (list A * Alpha_context.Script.node)) * bool) :=
    let '(unreachables, all) as acc_value := function_parameter in
    match entrypoints with
    | {|
      Script_typed_ir.entrypoints.name :=
        (None | Some (Script_typed_ir.Field_annot ""))
        |} =>
      return?
        ((if reachable then
          acc_value
        else
          match ty with
          | Script_typed_ir.Union_t _ _ => acc_value
          | _ => ((cons (List.rev path) unreachables), all)
          end), reachable)
    | {|
      Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot name)
        |} =>
      if (String.length name) >i 31 then
        return? (((cons (List.rev path) unreachables), all), true)
      else
        if Entrypoints_map.(S.MAP.mem) name all then
          return? (((cons (List.rev path) unreachables), all), true)
        else
          let? '(unparsed_ty, _) := unparse_ty ctxt ty in
          return?
            ((unreachables,
              (Entrypoints_map.(S.MAP.add) name ((List.rev path), unparsed_ty)
                all)), true)
    end in
  let fix fold_tree
    (t_value : Script_typed_ir.ty) (entrypoints : Script_typed_ir.entrypoints)
    (path : list Alpha_context.Script.prim) (reachable : bool)
    (acc_value :
      list (list Alpha_context.Script.prim) *
        Entrypoints_map.(S.MAP.t)
          (list Alpha_context.Script.prim * Alpha_context.Script.node))
    : M?
      (list (list Alpha_context.Script.prim) *
        Entrypoints_map.(S.MAP.t)
          (list Alpha_context.Script.prim * Alpha_context.Script.node)) :=
    match (t_value, entrypoints) with
    |
      (Script_typed_ir.Union_t tl tr, {|
        Script_typed_ir.entrypoints.nested :=
          Script_typed_ir.Entrypoints_Union {|
            Script_typed_ir.nested_entrypoints.Entrypoints_Union._left := _left;
              Script_typed_ir.nested_entrypoints.Entrypoints_Union._right
                := _right
              |}
          |}) =>
      let? '(acc_value, l_reachable) :=
        merge (cons Michelson_v1_primitives.D_Left path) tl _left reachable
          acc_value in
      let? '(acc_value, r_reachable) :=
        merge (cons Michelson_v1_primitives.D_Right path) tr _right reachable
          acc_value in
      let? acc_value :=
        fold_tree tl _left (cons Michelson_v1_primitives.D_Left path)
          l_reachable acc_value in
      fold_tree tr _right (cons Michelson_v1_primitives.D_Right path)
        r_reachable acc_value
    | _ => return? acc_value
    end in
  let? '(unparsed_full, _) := unparse_ty ctxt full in
  let '(init_value, reachable) :=
    match entrypoints with
    | {|
      Script_typed_ir.entrypoints.name :=
        (None | Some (Script_typed_ir.Field_annot ""))
        |} => (Entrypoints_map.(S.MAP.empty), false)
    | {|
      Script_typed_ir.entrypoints.name := Some (Script_typed_ir.Field_annot name)
        |} =>
      ((Entrypoints_map.(S.MAP.singleton) name (nil, unparsed_full)), true)
    end in
  fold_tree full entrypoints nil reachable (nil, init_value).

Definition unparse_unit
  (ctxt : Alpha_context.context) (function_parameter : unit)
  : Alpha_context.Script.node * Alpha_context.context :=
  let '_ := function_parameter in
  ((Micheline.Prim (-1) Michelson_v1_primitives.D_Unit nil nil), ctxt).

Definition unparse_int
  (ctxt : Alpha_context.context) (v : Alpha_context.Script_int.num)
  : Alpha_context.Script.node * Alpha_context.context :=
  ((Micheline.Int (-1) (Alpha_context.Script_int.to_zint v)), ctxt).

Definition unparse_nat
  (ctxt : Alpha_context.context) (v : Alpha_context.Script_int.num)
  : Alpha_context.Script.node * Alpha_context.context :=
  ((Micheline.Int (-1) (Alpha_context.Script_int.to_zint v)), ctxt).

Definition unparse_string (ctxt : Alpha_context.context) (s : string)
  : Alpha_context.Script.node * Alpha_context.context :=
  ((Micheline.String (-1) s), ctxt).

Definition unparse_bytes (ctxt : Alpha_context.context) (s : bytes)
  : Alpha_context.Script.node * Alpha_context.context :=
  ((Micheline.Bytes (-1) s), ctxt).

Definition unparse_bool (ctxt : Alpha_context.context) (b_value : bool)
  : Alpha_context.Script.node * Alpha_context.context :=
  ((Micheline.Prim (-1)
    (if b_value then
      Michelson_v1_primitives.D_True
    else
      Michelson_v1_primitives.D_False) nil nil), ctxt).

Definition unparse_timestamp
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (t_value : Alpha_context.Script_timestamp.t)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    return?
      ((Micheline.Int (-1) (Alpha_context.Script_timestamp.to_zint t_value)),
        ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.timestamp_readable
      in
    match Alpha_context.Script_timestamp.to_notation t_value with
    | None =>
      return?
        ((Micheline.Int (-1) (Alpha_context.Script_timestamp.to_zint t_value)),
          ctxt)
    | Some s => return? ((Micheline.String (-1) s), ctxt)
    end
  end.

Definition unparse_address
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : Alpha_context.Contract.t * string)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let '(c, entrypoint) := function_parameter in
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.contract in
  let? '_ :=
    match entrypoint with
    | "" =>
      Error_monad.error_value
        (Build_extensible "Unparsing_invariant_violated" unit tt)
    | _ => return? tt
    end in
  match mode with
  | (Optimized | Optimized_legacy) =>
    let entrypoint :=
      match entrypoint with
      | "default" => ""
      | name => name
      end in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn
        (Data_encoding.tup2 Alpha_context.Contract.encoding
          Data_encoding._Variable.string_value) (c, entrypoint) in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let notation :=
      match entrypoint with
      | "default" => Alpha_context.Contract.to_b58check c
      | entrypoint =>
        Pervasives.op_caret (Alpha_context.Contract.to_b58check c)
          (Pervasives.op_caret "%" entrypoint)
      end in
    return? ((Micheline.String (-1) notation), ctxt)
  end.

Definition unparse_contract {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : A * (Alpha_context.Contract.t * string))
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let '(_, address) := function_parameter in
  unparse_address ctxt mode address.

Definition unparse_signature
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (s : Signature.t)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.signature_optimized in
    let bytes_value := Data_encoding.Binary.to_bytes_exn Signature.encoding s in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.signature_readable
      in
    return? ((Micheline.String (-1) (Signature.to_b58check s)), ctxt)
  end.

Definition unparse_mutez
  (ctxt : Alpha_context.context) (v : Alpha_context.Tez.tez)
  : Alpha_context.Script.node * Alpha_context.context :=
  ((Micheline.Int (-1) (Z.of_int64 (Alpha_context.Tez.to_mutez v))), ctxt).

Definition unparse_key
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (k : Signature.public_key)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.public_key_optimized in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn
        Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding) k in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.public_key_readable in
    return?
      ((Micheline.String (-1)
        (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.to_b58check) k)), ctxt)
  end.

Definition unparse_key_hash
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (k : Signature.public_key_hash)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.key_hash_optimized
      in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding) k in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.key_hash_readable
      in
    return?
      ((Micheline.String (-1)
        (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_b58check) k)),
        ctxt)
  end.

Definition unparse_operation {A : Set}
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.packed_internal_operation * A)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let '(op, _big_map_diff) := function_parameter in
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn
      Alpha_context.Operation.internal_operation_encoding op in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt (Unparse_costs.operation bytes_value) in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_chain_id
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (chain_id : Chain_id.t)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.chain_id_optimized
      in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn Chain_id.encoding chain_id in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.chain_id_readable
      in
    return? ((Micheline.String (-1) (Chain_id.to_b58check chain_id)), ctxt)
  end.

Definition unparse_bls12_381_g1
  (ctxt : Alpha_context.context) (x : Bls12_381.G1.(S.CURVE.t))
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.bls12_381_g1 in
  let bytes_value := Bls12_381.G1.(S.CURVE.to_bytes) x in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_bls12_381_g2
  (ctxt : Alpha_context.context) (x : Bls12_381.G2.(S.CURVE.t))
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.bls12_381_g2 in
  let bytes_value := Bls12_381.G2.(S.CURVE.to_bytes) x in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_bls12_381_fr
  (ctxt : Alpha_context.context) (x : Bls12_381.Fr.(S.PRIME_FIELD.t))
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.bls12_381_fr in
  let bytes_value := Bls12_381.Fr.(S.PRIME_FIELD.to_bytes) x in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_pair {l r : Set}
  (unparse_l :
    Alpha_context.context -> l ->
    M=? (Alpha_context.Script.node * Alpha_context.context))
  (unparse_r :
    Alpha_context.context -> r ->
    M=? (Alpha_context.Script.node * Alpha_context.context))
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (r_comb_witness : comb_witness) (function_parameter : l * r)
  : M=? (Alpha_context.Script.node * Alpha_context.context) :=
  let '(l_value, r_value) := function_parameter in
  let=? '(l_value, ctxt) := unparse_l ctxt l_value in
  let=? '(r_value, ctxt) := unparse_r ctxt r_value in
  let res :=
    match (mode, r_comb_witness, r_value) with
    | (Optimized, Comb_Pair _, Micheline.Seq _ r_value) =>
      Micheline.Seq (-1) (cons l_value r_value)
    |
      (Readable, Comb_Pair _,
        Micheline.Prim _ Michelson_v1_primitives.D_Pair xs []) =>
      Micheline.Prim (-1) Michelson_v1_primitives.D_Pair (cons l_value xs) nil
    | _ =>
      Micheline.Prim (-1) Michelson_v1_primitives.D_Pair [ l_value; r_value ]
        nil
    end in
  return=? (res, ctxt).

Definition unparse_union {A B C D E : Set}
  (unparse_l :
    A -> B ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D))
  (unparse_r :
    A -> E ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D))
  (ctxt : A) (function_parameter : Script_typed_ir.union B E)
  : M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D) :=
  match function_parameter with
  | Script_typed_ir.L l_value =>
    let=? '(l_value, ctxt) := unparse_l ctxt l_value in
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_Left [ l_value ] nil),
        ctxt)
  | Script_typed_ir.R r_value =>
    let=? '(r_value, ctxt) := unparse_r ctxt r_value in
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_Right [ r_value ] nil),
        ctxt)
  end.

Definition unparse_option {A B C : Set}
  (unparse_v :
    A -> B ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * A) C))
  (ctxt : A) (function_parameter : option B)
  : M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * A) C) :=
  match function_parameter with
  | Some v =>
    let=? '(v, ctxt) := unparse_v ctxt v in
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_Some [ v ] nil), ctxt)
  | None =>
    Error_monad._return
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_None nil nil), ctxt)
  end.

Definition comparable_comb_witness2
  (function_parameter : Script_typed_ir.comparable_ty) : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_key _ (Script_typed_ir.Pair_key _ _) =>
    Comb_Pair (Comb_Pair Comb_Any)
  | Script_typed_ir.Pair_key _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Fixpoint unparse_comparable_data {a : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (ty : Script_typed_ir.comparable_ty) (a_value : a) {struct ty}
  : M=? (Alpha_context.Script.node * Alpha_context.context) :=
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Unparse_costs.unparse_data_cycle) in
  match (ty, a_value) with
  | (Script_typed_ir.Unit_key, v) =>
    let v := cast unit v in
    Error_monad._return (unparse_unit ctxt v)
  
  | (Script_typed_ir.Int_key, v) =>
    let v := cast Alpha_context.Script_int.num v in
    Error_monad._return (unparse_int ctxt v)
  
  | (Script_typed_ir.Nat_key, v) =>
    let v := cast Alpha_context.Script_int.num v in
    Error_monad._return (unparse_nat ctxt v)
  
  | (Script_typed_ir.String_key, s) =>
    let s := cast string s in
    Error_monad._return (unparse_string ctxt s)
  
  | (Script_typed_ir.Bytes_key, s) =>
    let s := cast bytes s in
    Error_monad._return (unparse_bytes ctxt s)
  
  | (Script_typed_ir.Bool_key, b_value) =>
    let b_value := cast bool b_value in
    Error_monad._return (unparse_bool ctxt b_value)
  
  | (Script_typed_ir.Timestamp_key, t_value) =>
    let t_value := cast Alpha_context.Script_timestamp.t t_value in
    Lwt._return (unparse_timestamp ctxt mode t_value)
  
  | (Script_typed_ir.Address_key, address) =>
    let address := cast Script_typed_ir.address address in
    Lwt._return (unparse_address ctxt mode address)
  
  | (Script_typed_ir.Signature_key, s) =>
    let s := cast Alpha_context.signature s in
    Lwt._return (unparse_signature ctxt mode s)
  
  | (Script_typed_ir.Mutez_key, v) =>
    let v := cast Tez_repr.t v in
    Error_monad._return (unparse_mutez ctxt v)
  
  | (Script_typed_ir.Key_key, k) =>
    let k := cast Alpha_context.public_key k in
    Lwt._return (unparse_key ctxt mode k)
  
  | (Script_typed_ir.Key_hash_key, k) =>
    let k := cast Alpha_context.public_key_hash k in
    Lwt._return (unparse_key_hash ctxt mode k)
  
  | (Script_typed_ir.Chain_id_key, chain_id) =>
    let chain_id := cast Chain_id.t chain_id in
    Lwt._return (unparse_chain_id ctxt mode chain_id)
  
  | (Script_typed_ir.Pair_key tl tr, pair_value) =>
    let 'existT _ [__0, __1] [pair_value, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__0, __1] =>
          [__0 * __1 ** Script_typed_ir.comparable_ty **
            Script_typed_ir.comparable_ty]) [pair_value, tr, tl] in
    let r_witness := comparable_comb_witness2 tr in
    let unparse_l (ctxt : Alpha_context.context) (v : __0)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      unparse_comparable_data ctxt mode tl v in
    let unparse_r (ctxt : Alpha_context.context) (v : __1)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      unparse_comparable_data ctxt mode tr v in
    unparse_pair unparse_l unparse_r ctxt mode r_witness pair_value
  
  | (Script_typed_ir.Union_key tl tr, v) =>
    let 'existT _ [__2, __3] [v, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__2, __3] =>
          [Script_typed_ir.union __2 __3 ** Script_typed_ir.comparable_ty **
            Script_typed_ir.comparable_ty]) [v, tr, tl] in
    let unparse_l (ctxt : Alpha_context.context) (v : __2)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      unparse_comparable_data ctxt mode tl v in
    let unparse_r (ctxt : Alpha_context.context) (v : __3)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      unparse_comparable_data ctxt mode tr v in
    unparse_union unparse_l unparse_r ctxt v
  
  | (Script_typed_ir.Option_key t_value, v) =>
    let 'existT _ __4 [v, t_value] :=
      cast_exists (Es := Set)
        (fun __4 => [option __4 ** Script_typed_ir.comparable_ty]) [v, t_value]
      in
    let unparse_v (ctxt : Alpha_context.context) (v : __4)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      unparse_comparable_data ctxt mode t_value v in
    unparse_option unparse_v ctxt v
  | _ => unreachable_gadt_branch
  end.

Definition comb_witness2 (function_parameter : Script_typed_ir.ty)
  : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_t _ (Script_typed_ir.Pair_t _ _) =>
    Comb_Pair (Comb_Pair Comb_Any)
  | Script_typed_ir.Pair_t _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Reserved Notation "'unparse_items".
Reserved Notation "'unparse_code_with_stack".

Fixpoint unparse_data_with_stack {a : Set}
  (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
  (ty : Script_typed_ir.ty) (a_value : a) {struct ty}
  : M=? (Alpha_context.Script.node * Alpha_context.context) :=
  let unparse_items {k v} := 'unparse_items k v in
  let unparse_code_with_stack := 'unparse_code_with_stack in
  let=? ctxt :=
    return= (Alpha_context.Gas.consume ctxt Unparse_costs.unparse_data_cycle) in
  let non_terminal_recursion {B : Set}
    (ctxt : Alpha_context.context) (mode : unparsing_mode)
    (ty : Script_typed_ir.ty) (a_value : B)
    : M=? (Alpha_context.Script.node * Alpha_context.context) :=
    if stack_depth >i 10000 then
      Error_monad.fail
        (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
    else
      unparse_data_with_stack ctxt (stack_depth +i 1) mode ty a_value in
  match (ty, a_value) with
  | (Script_typed_ir.Unit_t, v) =>
    let v := cast unit v in
    Error_monad._return (unparse_unit ctxt v)
  
  | (Script_typed_ir.Int_t, v) =>
    let v := cast Alpha_context.Script_int.num v in
    Error_monad._return (unparse_int ctxt v)
  
  | (Script_typed_ir.Nat_t, v) =>
    let v := cast Alpha_context.Script_int.num v in
    Error_monad._return (unparse_nat ctxt v)
  
  | (Script_typed_ir.String_t, s) =>
    let s := cast string s in
    Error_monad._return (unparse_string ctxt s)
  
  | (Script_typed_ir.Bytes_t, s) =>
    let s := cast bytes s in
    Error_monad._return (unparse_bytes ctxt s)
  
  | (Script_typed_ir.Bool_t, b_value) =>
    let b_value := cast bool b_value in
    Error_monad._return (unparse_bool ctxt b_value)
  
  | (Script_typed_ir.Timestamp_t, t_value) =>
    let t_value := cast Alpha_context.Script_timestamp.t t_value in
    Lwt._return (unparse_timestamp ctxt mode t_value)
  
  | (Script_typed_ir.Address_t, address) =>
    let address := cast Script_typed_ir.address address in
    Lwt._return (unparse_address ctxt mode address)
  
  | (Script_typed_ir.Contract_t _, contract) =>
    let 'existT _ __0 contract :=
      cast_exists (Es := Set) (fun __0 => Script_typed_ir.typed_contract)
        contract in
    Lwt._return (unparse_contract ctxt mode contract)
  
  | (Script_typed_ir.Signature_t, s) =>
    let s := cast Alpha_context.signature s in
    Lwt._return (unparse_signature ctxt mode s)
  
  | (Script_typed_ir.Mutez_t, v) =>
    let v := cast Tez_repr.t v in
    Error_monad._return (unparse_mutez ctxt v)
  
  | (Script_typed_ir.Key_t, k) =>
    let k := cast Alpha_context.public_key k in
    Lwt._return (unparse_key ctxt mode k)
  
  | (Script_typed_ir.Key_hash_t, k) =>
    let k := cast Alpha_context.public_key_hash k in
    Lwt._return (unparse_key_hash ctxt mode k)
  
  | (Script_typed_ir.Operation_t, operation) =>
    let operation := cast Script_typed_ir.operation operation in
    Lwt._return (unparse_operation ctxt operation)
  
  | (Script_typed_ir.Chain_id_t, chain_id) =>
    let chain_id := cast Chain_id.t chain_id in
    Lwt._return (unparse_chain_id ctxt mode chain_id)
  
  | (Script_typed_ir.Bls12_381_g1_t, x) =>
    let x := cast Bls12_381.G1.(S.CURVE.t) x in
    Lwt._return (unparse_bls12_381_g1 ctxt x)
  
  | (Script_typed_ir.Bls12_381_g2_t, x) =>
    let x := cast Bls12_381.G2.(S.CURVE.t) x in
    Lwt._return (unparse_bls12_381_g2 ctxt x)
  
  | (Script_typed_ir.Bls12_381_fr_t, x) =>
    let x := cast Bls12_381.Fr.(S.PRIME_FIELD.t) x in
    Lwt._return (unparse_bls12_381_fr ctxt x)
  
  | (Script_typed_ir.Pair_t tl tr, pair_value) =>
    let 'existT _ [__1, __2] [pair_value, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__1, __2] =>
          [__1 * __2 ** Script_typed_ir.ty ** Script_typed_ir.ty])
        [pair_value, tr, tl] in
    let r_witness := comb_witness2 tr in
    let unparse_l (ctxt : Alpha_context.context) (v : __1)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      non_terminal_recursion ctxt mode tl v in
    let unparse_r (ctxt : Alpha_context.context) (v : __2)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      non_terminal_recursion ctxt mode tr v in
    unparse_pair unparse_l unparse_r ctxt mode r_witness pair_value
  
  | (Script_typed_ir.Union_t tl tr, v) =>
    let 'existT _ [__3, __4] [v, tr, tl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__3, __4] =>
          [Script_typed_ir.union __3 __4 ** Script_typed_ir.ty **
            Script_typed_ir.ty]) [v, tr, tl] in
    let unparse_l (ctxt : Alpha_context.context) (v : __3)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      non_terminal_recursion ctxt mode tl v in
    let unparse_r (ctxt : Alpha_context.context) (v : __4)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      non_terminal_recursion ctxt mode tr v in
    unparse_union unparse_l unparse_r ctxt v
  
  | (Script_typed_ir.Option_t t_value, v) =>
    let 'existT _ __5 [v, t_value] :=
      cast_exists (Es := Set) (fun __5 => [option __5 ** Script_typed_ir.ty])
        [v, t_value] in
    let unparse_v (ctxt : Alpha_context.context) (v : __5)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      non_terminal_recursion ctxt mode t_value v in
    unparse_option unparse_v ctxt v
  
  | (Script_typed_ir.List_t t_value, items) =>
    let 'existT _ __6 [items, t_value] :=
      cast_exists (Es := Set)
        (fun __6 => [Script_typed_ir.boxed_list __6 ** Script_typed_ir.ty])
        [items, t_value] in
    let=? '(items, ctxt) :=
      Error_monad.fold_left_s
        (fun (function_parameter :
          list Alpha_context.Script.node * Alpha_context.context) =>
          let '(l_value, ctxt) := function_parameter in
          fun (element : __6) =>
            let=? '(unparsed, ctxt) :=
              non_terminal_recursion ctxt mode t_value element in
            return=? ((cons unparsed l_value), ctxt)) (nil, ctxt)
        items.(Script_typed_ir.boxed_list.elements) in
    return=? ((Micheline.Seq (-1) (List.rev items)), ctxt)
  
  | (Script_typed_ir.Ticket_t t_value, ticket) =>
    let 'existT _ __7 [ticket, t_value] :=
      cast_exists (Es := Set)
        (fun __7 =>
          [Script_typed_ir.ticket __7 ** Script_typed_ir.comparable_ty])
        [ticket, t_value] in
    let '{|
      Script_typed_ir.ticket.ticketer := ticketer;
        Script_typed_ir.ticket.contents := contents;
        Script_typed_ir.ticket.amount := amount
        |} := ticket in
    let t_value := ty_of_comparable_ty (opened_ticket_type t_value) in
    unparse_data_with_stack ctxt stack_depth mode t_value
      (ticketer, (contents, amount))
  
  | (Script_typed_ir.Set_t t_value, set) =>
    let 'existT _ __8 [set, t_value] :=
      cast_exists (Es := Set)
        (fun __8 => [Script_typed_ir.set __8 ** Script_typed_ir.comparable_ty])
        [set, t_value] in
    let=? '(items, ctxt) :=
      Error_monad.fold_left_s
        (fun (function_parameter :
          list
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim) * Alpha_context.context) =>
          let '(l_value, ctxt) := function_parameter in
          fun (item : __8) =>
            let=? '(item, ctxt) :=
              unparse_comparable_data ctxt mode t_value item in
            return=? ((cons item l_value), ctxt)) (nil, ctxt)
        (set_fold
          (fun (e : __8) => fun (acc_value : list __8) => cons e acc_value) set
          nil) in
    return=? ((Micheline.Seq (-1) items), ctxt)
  
  | (Script_typed_ir.Map_t kt vt, map) =>
    let 'existT _ [__10, __9] [map, vt, kt] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__10, __9] =>
          [Script_typed_ir.map __9 __10 ** Script_typed_ir.ty **
            Script_typed_ir.comparable_ty]) [map, vt, kt] in
    let items :=
      map_fold
        (fun (k : __9) =>
          fun (v : __10) =>
            fun (acc_value : list (__9 * __10)) => cons (k, v) acc_value) map
        nil in
    let=? '(items, ctxt) :=
      unparse_items ctxt (stack_depth +i 1) mode kt vt items in
    return=? ((Micheline.Seq (-1) items), ctxt)
  
  | (Script_typed_ir.Big_map_t kt vt, big_map) =>
    let 'existT _ [__11, __12] [big_map, vt, kt] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__11, __12] =>
          [Script_typed_ir.big_map __11 __12 ** Script_typed_ir.ty **
            Script_typed_ir.comparable_ty]) [big_map, vt, kt] in
    match big_map with
    | {|
      Script_typed_ir.big_map.id := Some id;
        Script_typed_ir.big_map.diff := Diff
        |} =>
      let 'existS _ _ Diff := Diff in
      if
        Diff.(Script_typed_ir.Boxed_map.OPS).(S.MAP.is_empty)
          (Pervasives.fst Diff.(Script_typed_ir.Boxed_map.boxed))
      then
        Error_monad._return
          ((Micheline.Int (-1)
            (Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.unparse_to_z)
              id)), ctxt)
      else
        let items :=
          Diff.(Script_typed_ir.Boxed_map.OPS).(S.MAP.fold)
            (fun (k : __11) =>
              fun (v : Diff.(Script_typed_ir.Boxed_map.value)) =>
                fun (acc_value :
                  list (__11 * Diff.(Script_typed_ir.Boxed_map.value))) =>
                  cons (k, v) acc_value)
            (Pervasives.fst Diff.(Script_typed_ir.Boxed_map.boxed)) nil in
        let vt := Script_typed_ir.Option_t vt in
        let=? '(items, ctxt) :=
          unparse_items ctxt (stack_depth +i 1) mode kt vt items in
        return=?
          ((Micheline.Prim (-1) Michelson_v1_primitives.D_Pair
            [
              Micheline.Int (-1)
                (Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.unparse_to_z)
                  id);
              Micheline.Seq (-1) items
            ] nil), ctxt)
    | {|
      Script_typed_ir.big_map.id := None;
        Script_typed_ir.big_map.diff := Diff
        |} =>
      let 'existS _ _ Diff := Diff in
      let items :=
        Diff.(Script_typed_ir.Boxed_map.OPS).(S.MAP.fold)
          (fun (k : __11) =>
            fun (v : Diff.(Script_typed_ir.Boxed_map.value)) =>
              fun (acc_value : list (__11 * __12)) =>
                match v with
                | None => acc_value
                | Some v => cons (k, v) acc_value
                end) (Pervasives.fst Diff.(Script_typed_ir.Boxed_map.boxed)) nil
        in
      let=? '(items, ctxt) :=
        unparse_items ctxt (stack_depth +i 1) mode kt vt items in
      return=? ((Micheline.Seq (-1) items), ctxt)
    end
  
  | (Script_typed_ir.Lambda_t _ _, l_value) =>
    let 'existT _ [__13, __14] l_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__13, __14] => Script_typed_ir.lambda) l_value in
    let 'Script_typed_ir.Lam _ original_code := l_value in
    unparse_code_with_stack ctxt (stack_depth +i 1) mode original_code
  
  | (Script_typed_ir.Sapling_transaction_t _, s) =>
    let s := cast Alpha_context.Sapling.transaction s in
    Lwt._return
      (let? ctxt :=
        Alpha_context.Gas.consume ctxt (Unparse_costs.sapling_transaction s) in
      let bytes_value :=
        Data_encoding.Binary.to_bytes_exn
          Alpha_context.Sapling.transaction_encoding s in
      return? ((Micheline.Bytes (-1) bytes_value), ctxt))
  
  | (Script_typed_ir.Sapling_state_t _, state) =>
    let state := cast Alpha_context.Sapling.state state in
    let '{|
      Alpha_context.Sapling.state.id := id;
        Alpha_context.Sapling.state.diff := diff_value
        |} := state in
    Lwt._return
      (let? ctxt :=
        Alpha_context.Gas.consume ctxt (Unparse_costs.sapling_diff diff_value)
        in
      return?
        (match diff_value with
        | {|
          Alpha_context.Sapling.diff.commitments_and_ciphertexts := [];
            Alpha_context.Sapling.diff.nullifiers := []
            |} =>
          match id with
          | None => Micheline.Seq (-1) nil
          | Some id =>
            let id :=
              Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.unparse_to_z)
                id in
            Micheline.Int (-1) id
          end
        | diff_value =>
          let diff_bytes :=
            Data_encoding.Binary.to_bytes_exn
              Alpha_context.Sapling.diff_encoding diff_value in
          let unparsed_diff {B : Set} : Micheline.node int B :=
            Micheline.Bytes (-1) diff_bytes in
          match id with
          | None => unparsed_diff
          | Some id =>
            let id :=
              Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.unparse_to_z)
                id in
            Micheline.Prim (-1) Michelson_v1_primitives.D_Pair
              [ Micheline.Int (-1) id; unparsed_diff ] nil
          end
        end, ctxt))
  | _ => unreachable_gadt_branch
  end

where "'unparse_items" :=
  (fun (k v : Set) => fun
    (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
    (kt : Script_typed_ir.comparable_ty) (vt : Script_typed_ir.ty)
    (items : list (k * v)) =>
    Error_monad.fold_left_s
      (fun (function_parameter :
        list Alpha_context.Script.node * Alpha_context.context) =>
        let '(l_value, ctxt) := function_parameter in
        fun (function_parameter : k * v) =>
          let '(k, v) := function_parameter in
          let=? '(key_value, ctxt) := unparse_comparable_data ctxt mode kt k in
          let=? '(value, ctxt) :=
            unparse_data_with_stack ctxt (stack_depth +i 1) mode vt v in
          return=?
            ((cons
              (Micheline.Prim (-1) Michelson_v1_primitives.D_Elt
                [ key_value; value ] nil) l_value), ctxt)) (nil, ctxt) items)

and "'unparse_code_with_stack" :=
  (fix unparse_code_with_stack
    (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
    (code : Alpha_context.Script.node) {struct code}
    : M=? (Alpha_context.Script.node * Alpha_context.context) :=
    let unparse_items {k v} := 'unparse_items k v in
    let legacy := true in
    let=? ctxt :=
      return= (Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle)
      in
    let non_terminal_recursion
      (ctxt : Alpha_context.context) (mode : unparsing_mode)
      (code : Alpha_context.Script.node)
      : M=? (Alpha_context.Script.node * Alpha_context.context) :=
      if stack_depth >i 10000 then
        Error_monad.fail
          (Build_extensible "Unparsing_too_many_recursive_calls" unit tt)
      else
        unparse_code_with_stack ctxt (stack_depth +i 1) mode code in
    match code with
    |
      Micheline.Prim loc Michelson_v1_primitives.I_PUSH (cons ty (cons data []))
        annot =>
      let=? '(Ex_ty t_value, ctxt) := return= (parse_packable_ty ctxt legacy ty)
        in
      let 'existT _ __Ex_ty_'a [ctxt, t_value] :=
        cast_exists (Es := Set)
          (fun __Ex_ty_'a => [Alpha_context.context ** Script_typed_ir.ty])
          [ctxt, t_value] in
      let allow_forged := false in
      let=? '(data, ctxt) :=
        parse_data_with_stack None (stack_depth +i 1) ctxt legacy allow_forged
          t_value data in
      let data := (fun (x : __Ex_ty_'a) => x) data in
      let=? '(data, ctxt) :=
        unparse_data_with_stack ctxt (stack_depth +i 1) mode t_value data in
      Error_monad._return
        ((Micheline.Prim loc Michelson_v1_primitives.I_PUSH [ ty; data ] annot),
          ctxt)
    | Micheline.Seq loc items =>
      let=? '(items, ctxt) :=
        Error_monad.fold_left_s
          (fun (function_parameter :
            list Alpha_context.Script.node * Alpha_context.context) =>
            let '(l_value, ctxt) := function_parameter in
            fun (item : Alpha_context.Script.node) =>
              let=? '(item, ctxt) := non_terminal_recursion ctxt mode item in
              return=? ((cons item l_value), ctxt)) (nil, ctxt) items in
      Error_monad._return ((Micheline.Seq loc (List.rev items)), ctxt)
    | Micheline.Prim loc prim items annot =>
      let=? '(items, ctxt) :=
        Error_monad.fold_left_s
          (fun (function_parameter :
            list Alpha_context.Script.node * Alpha_context.context) =>
            let '(l_value, ctxt) := function_parameter in
            fun (item : Alpha_context.Script.node) =>
              let=? '(item, ctxt) := non_terminal_recursion ctxt mode item in
              return=? ((cons item l_value), ctxt)) (nil, ctxt) items in
      Error_monad._return
        ((Micheline.Prim loc prim (List.rev items) annot), ctxt)
    | (Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _) as atom
      => Error_monad._return (atom, ctxt)
    end).

Definition unparse_items {k v : Set} := 'unparse_items k v.
Definition unparse_code_with_stack := 'unparse_code_with_stack.

Definition unparse_script {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : Script_typed_ir.script A)
  : M=? (Alpha_context.Script.t * Alpha_context.context) :=
  let '{|
    Script_typed_ir.script.code := code;
      Script_typed_ir.script.arg_type := arg_type;
      Script_typed_ir.script.storage := storage_value;
      Script_typed_ir.script.storage_type := storage_type;
      Script_typed_ir.script.entrypoints := entrypoints
      |} := function_parameter in
  let 'Script_typed_ir.Lam _ original_code := code in
  let=? '(code, ctxt) := unparse_code_with_stack ctxt 0 mode original_code in
  let=? '(storage_value, ctxt) :=
    unparse_data_with_stack ctxt 0 mode storage_type storage_value in
  Lwt._return
    (let? '(arg_type, ctxt) := unparse_parameter_ty ctxt arg_type entrypoints in
    let? '(storage_type, ctxt) := unparse_ty ctxt storage_type in
    let code :=
      Micheline.Seq (-1)
        [
          Micheline.Prim (-1) Michelson_v1_primitives.K_parameter [ arg_type ]
            nil;
          Micheline.Prim (-1) Michelson_v1_primitives.K_storage [ storage_type ]
            nil;
          Micheline.Prim (-1) Michelson_v1_primitives.K_code [ code ] nil
        ] in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.strip_locations_cost code) in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.strip_locations_cost storage_value) in
    return?
      ({|
        Alpha_context.Script.t.code :=
          Alpha_context.Script.lazy_expr_value (Micheline.strip_locations code);
        Alpha_context.Script.t.storage :=
          Alpha_context.Script.lazy_expr_value
            (Micheline.strip_locations storage_value) |}, ctxt)).

Definition pack_node
  (unparsed : Alpha_context.Script.node) (ctxt : Alpha_context.context)
  : M? (bytes * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.strip_locations_cost unparsed) in
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn Alpha_context.Script.expr_encoding
      (Micheline.strip_locations unparsed) in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.serialized_cost bytes_value) in
  let bytes_value := Bytes.cat (Bytes.of_string "\005") bytes_value in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.serialized_cost bytes_value) in
  return? (bytes_value, ctxt).

Definition pack_data_with_mode {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.ty) (data : A)
  (mode : unparsing_mode) : M=? (bytes * Alpha_context.context) :=
  let=? '(unparsed, ctxt) := unparse_data_with_stack ctxt 0 mode typ data in
  Lwt._return (pack_node unparsed ctxt).

Definition pack_comparable_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.comparable_ty)
  (data : A) (mode : unparsing_mode) : M=? (bytes * Alpha_context.context) :=
  let=? '(unparsed, ctxt) := unparse_comparable_data ctxt mode typ data in
  Lwt._return (pack_node unparsed ctxt).

Definition hash_bytes (ctxt : Alpha_context.context) (bytes_value : bytes)
  : M? (Script_expr_hash.t * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Michelson_v1_gas.Cost_of.Interpreter.blake2b bytes_value) in
  return? ((Script_expr_hash.hash_bytes None [ bytes_value ]), ctxt).

Definition hash_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.ty) (data : A)
  : M=? (Script_expr_hash.t * Alpha_context.context) :=
  let=? '(bytes_value, ctxt) :=
    pack_data_with_mode ctxt typ data Optimized_legacy in
  Lwt._return (hash_bytes ctxt bytes_value).

Definition hash_comparable_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.comparable_ty)
  (data : A) : M=? (Script_expr_hash.t * Alpha_context.context) :=
  let=? '(bytes_value, ctxt) :=
    pack_comparable_data ctxt typ data Optimized_legacy in
  Lwt._return (hash_bytes ctxt bytes_value).

Definition pack_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.ty) (data : A)
  : M=? (bytes * Alpha_context.context) :=
  pack_data_with_mode ctxt typ data Optimized_legacy.

Definition empty_big_map {A B : Set}
  (key_type : Script_typed_ir.comparable_ty) (value_type : Script_typed_ir.ty)
  : Script_typed_ir.big_map A B :=
  {| Script_typed_ir.big_map.id := None;
    Script_typed_ir.big_map.diff := empty_map key_type;
    Script_typed_ir.big_map.key_type := key_type;
    Script_typed_ir.big_map.value_type := value_type |}.

Definition big_map_mem {A B : Set}
  (ctxt : Alpha_context.context) (key_value : A)
  (function_parameter : Script_typed_ir.big_map A B)
  : M=? (bool * Alpha_context.context) :=
  let '{|
    Script_typed_ir.big_map.id := id;
      Script_typed_ir.big_map.diff := diff_value;
      Script_typed_ir.big_map.key_type := key_type
      |} := function_parameter in
  match ((map_get key_value diff_value), id) with
  | (None, None) => Error_monad._return (false, ctxt)
  | (None, Some id) =>
    let=? '(hash_value, ctxt) := hash_comparable_data ctxt key_type key_value in
    let=? '(ctxt, res) := Alpha_context.Big_map.mem ctxt id hash_value in
    return=? (res, ctxt)
  | (Some None, _) => Error_monad._return (false, ctxt)
  | (Some (Some _), _) => Error_monad._return (true, ctxt)
  end.

Definition big_map_get {A B : Set}
  (ctxt : Alpha_context.context) (key_value : A)
  (function_parameter : Script_typed_ir.big_map A B)
  : M=? (option B * Alpha_context.context) :=
  let '{|
    Script_typed_ir.big_map.id := id;
      Script_typed_ir.big_map.diff := diff_value;
      Script_typed_ir.big_map.key_type := key_type;
      Script_typed_ir.big_map.value_type := value_type
      |} := function_parameter in
  match ((map_get key_value diff_value), id) with
  | (Some x, _) => Error_monad._return (x, ctxt)
  | (None, None) => Error_monad._return (None, ctxt)
  | (None, Some id) =>
    let=? '(hash_value, ctxt) := hash_comparable_data ctxt key_type key_value in
    let=? function_parameter := Alpha_context.Big_map.get_opt ctxt id hash_value
      in
    match function_parameter with
    | (ctxt, None) => Error_monad._return (None, ctxt)
    | (ctxt, Some value) =>
      let=? '(x, ctxt) :=
        parse_data_with_stack None 0 ctxt true true value_type
          (Micheline.root value) in
      return=? ((Some x), ctxt)
    end
  end.

Definition big_map_update {A B : Set}
  (key_value : A) (value : option B)
  (function_parameter : Script_typed_ir.big_map A B)
  : Script_typed_ir.big_map A B :=
  let '{| Script_typed_ir.big_map.diff := diff_value |} as map :=
    function_parameter in
  Script_typed_ir.big_map.with_diff (map_set key_value value diff_value) map.

Definition lazy_storage_ids : Set := Alpha_context.Lazy_storage.IdSet.t.

Definition no_lazy_storage_id : Alpha_context.Lazy_storage.IdSet.t :=
  Alpha_context.Lazy_storage.IdSet.empty.

Definition diff_of_big_map {A B : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (function_parameter : Script_typed_ir.big_map A B)
  : M=?
    (Alpha_context.Lazy_storage.diff
      Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)
      Alpha_context.Big_map.alloc (list Alpha_context.Big_map.update) *
      Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) *
      Alpha_context.context) :=
  let '{|
    Script_typed_ir.big_map.id := id;
      Script_typed_ir.big_map.diff := diff_value;
      Script_typed_ir.big_map.key_type := key_type;
      Script_typed_ir.big_map.value_type := value_type
      |} := function_parameter in
  let=? '(ctxt, init_value, id) :=
    match id with
    | Some id =>
      if
        Alpha_context.Lazy_storage.IdSet.mem
          Alpha_context.Lazy_storage.Kind.Big_map id ids_to_copy
      then
        let=? '(ctxt, duplicate) := Alpha_context.Big_map.fresh temporary ctxt
          in
        return=?
          (ctxt,
            (Alpha_context.Lazy_storage.Copy
              {| Alpha_context.Lazy_storage.init.Copy.src := id |}), duplicate)
      else
        Error_monad._return (ctxt, Alpha_context.Lazy_storage.Existing, id)
    | None =>
      let=? '(ctxt, id) := Alpha_context.Big_map.fresh temporary ctxt in
      Lwt._return
        (let kt := unparse_comparable_ty key_type in
        let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost kt) in
        let? '(kv, ctxt) := unparse_ty ctxt value_type in
        let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost kv) in
        let key_type := Micheline.strip_locations kt in
        let value_type := Micheline.strip_locations kv in
        return?
          (ctxt,
            (Alpha_context.Lazy_storage.Alloc
              {| Alpha_context.Big_map.alloc.key_type := key_type;
                Alpha_context.Big_map.alloc.value_type := value_type |}), id))
    end in
  let pairs :=
    map_fold
      (fun (key_value : A) =>
        fun (value : option B) =>
          fun (acc_value : list (A * option B)) =>
            cons (key_value, value) acc_value) diff_value nil in
  let=? '(updates, ctxt) :=
    Error_monad.fold_left_s
      (fun (function_parameter :
        list Alpha_context.Big_map.update * Alpha_context.context) =>
        let '(acc_value, ctxt) := function_parameter in
        fun (function_parameter : A * option B) =>
          let '(key_value, value) := function_parameter in
          let=? ctxt :=
            return=
              (Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle)
            in
          let=? '(key_hash, ctxt) :=
            hash_comparable_data ctxt key_type key_value in
          let=? '(key_node, ctxt) :=
            unparse_comparable_data ctxt mode key_type key_value in
          let=? ctxt :=
            return=
              (Alpha_context.Gas.consume ctxt
                (Alpha_context.Script.strip_locations_cost key_node)) in
          let key_value := Micheline.strip_locations key_node in
          let=? '(value, ctxt) :=
            match value with
            | None => Error_monad._return (None, ctxt)
            | Some x =>
              let=? '(node, ctxt) :=
                unparse_data_with_stack ctxt 0 mode value_type x in
              Lwt._return
                (let? ctxt :=
                  Alpha_context.Gas.consume ctxt
                    (Alpha_context.Script.strip_locations_cost node) in
                return? ((Some (Micheline.strip_locations node)), ctxt))
            end in
          let diff_item :=
            {| Alpha_context.Big_map.update.key := key_value;
              Alpha_context.Big_map.update.key_hash := key_hash;
              Alpha_context.Big_map.update.value := value |} in
          return=? ((cons diff_item acc_value), ctxt)) (nil, ctxt)
      (List.rev pairs) in
  return=?
    ((Alpha_context.Lazy_storage.Update
      {| Alpha_context.Lazy_storage.diff.Update.init := init_value;
        Alpha_context.Lazy_storage.diff.Update.updates := updates |}), id, ctxt).

Definition diff_of_sapling_state
  (ctxt : Alpha_context.context) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (function_parameter : Alpha_context.Sapling.state)
  : M=?
    (Alpha_context.Lazy_storage.diff
      Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t)
      Alpha_context.Sapling.alloc Alpha_context.Sapling.diff *
      Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t) *
      Alpha_context.context) :=
  let '{|
    Alpha_context.Sapling.state.id := id;
      Alpha_context.Sapling.state.diff := diff_value;
      Alpha_context.Sapling.state.memo_size := memo_size
      |} := function_parameter in
  let=? '(ctxt, init_value, id) :=
    match id with
    | Some id =>
      if
        Alpha_context.Lazy_storage.IdSet.mem
          Alpha_context.Lazy_storage.Kind.Sapling_state id ids_to_copy
      then
        let=? '(ctxt, duplicate) := Alpha_context.Sapling.fresh temporary ctxt
          in
        return=?
          (ctxt,
            (Alpha_context.Lazy_storage.Copy
              {| Alpha_context.Lazy_storage.init.Copy.src := id |}), duplicate)
      else
        Error_monad._return (ctxt, Alpha_context.Lazy_storage.Existing, id)
    | None =>
      let=? '(ctxt, id) := Alpha_context.Sapling.fresh temporary ctxt in
      return=?
        (ctxt,
          (Alpha_context.Lazy_storage.Alloc
            {| Alpha_context.Sapling.alloc.memo_size := memo_size |}), id)
    end in
  return=?
    ((Alpha_context.Lazy_storage.Update
      {| Alpha_context.Lazy_storage.diff.Update.init := init_value;
        Alpha_context.Lazy_storage.diff.Update.updates := diff_value |}), id,
      ctxt).

(** Witness flag for whether a type can be populated by a value containing a
    lazy storage.
    [False_f] must be used only when a value of the type cannot contain a lazy
    storage.

    This flag is built in [has_lazy_storage] and used only in
    [extract_lazy_storage_updates] and [collect_lazy_storage].

    This flag is necessary to avoid these two functions to have a quadratic
    complexity in the size of the type.

    Add new lazy storage kinds here.

    Please keep the usage of this GADT local. *)
Inductive has_lazy_storage : Set :=
| True_f : has_lazy_storage
| False_f : has_lazy_storage
| Pair_f : has_lazy_storage -> has_lazy_storage -> has_lazy_storage
| Union_f : has_lazy_storage -> has_lazy_storage -> has_lazy_storage
| Option_f : has_lazy_storage -> has_lazy_storage
| List_f : has_lazy_storage -> has_lazy_storage
| Map_f : has_lazy_storage -> has_lazy_storage.

(** This function is called only on storage and parameter types of contracts,
    once per typechecked contract. It has a complexity linear in the size of
    the types, which happen to be literally written types, so the gas for them
    has already been paid. *)
Fixpoint has_lazy_storage_value (ty : Script_typed_ir.ty) : has_lazy_storage :=
  let aux1
    (cons_value : has_lazy_storage -> has_lazy_storage)
    (t_value : Script_typed_ir.ty) : has_lazy_storage :=
    match has_lazy_storage_value t_value with
    | False_f => False_f
    | h => cons_value h
    end in
  let aux2
    (cons_value : has_lazy_storage -> has_lazy_storage -> has_lazy_storage)
    (t1 : Script_typed_ir.ty) (t2 : Script_typed_ir.ty) : has_lazy_storage :=
    match ((has_lazy_storage_value t1), (has_lazy_storage_value t2)) with
    | (False_f, False_f) => False_f
    | (h1, h2) => cons_value h1 h2
    end in
  match ty with
  | Script_typed_ir.Big_map_t _ _ => True_f
  | Script_typed_ir.Sapling_state_t _ => True_f
  | Script_typed_ir.Unit_t => False_f
  | Script_typed_ir.Int_t => False_f
  | Script_typed_ir.Nat_t => False_f
  | Script_typed_ir.Signature_t => False_f
  | Script_typed_ir.String_t => False_f
  | Script_typed_ir.Bytes_t => False_f
  | Script_typed_ir.Mutez_t => False_f
  | Script_typed_ir.Key_hash_t => False_f
  | Script_typed_ir.Key_t => False_f
  | Script_typed_ir.Timestamp_t => False_f
  | Script_typed_ir.Address_t => False_f
  | Script_typed_ir.Bool_t => False_f
  | Script_typed_ir.Lambda_t _ _ => False_f
  | Script_typed_ir.Set_t _ => False_f
  | Script_typed_ir.Contract_t _ => False_f
  | Script_typed_ir.Operation_t => False_f
  | Script_typed_ir.Chain_id_t => False_f
  | Script_typed_ir.Never_t => False_f
  | Script_typed_ir.Bls12_381_g1_t => False_f
  | Script_typed_ir.Bls12_381_g2_t => False_f
  | Script_typed_ir.Bls12_381_fr_t => False_f
  | Script_typed_ir.Sapling_transaction_t _ => False_f
  | Script_typed_ir.Ticket_t _ => False_f
  | Script_typed_ir.Pair_t l_value r_value =>
    aux2
      (fun (l_value : has_lazy_storage) =>
        fun (r_value : has_lazy_storage) => Pair_f l_value r_value) l_value
      r_value
  | Script_typed_ir.Union_t l_value r_value =>
    aux2
      (fun (l_value : has_lazy_storage) =>
        fun (r_value : has_lazy_storage) => Union_f l_value r_value) l_value
      r_value
  | Script_typed_ir.Option_t t_value =>
    aux1 (fun (h : has_lazy_storage) => Option_f h) t_value
  | Script_typed_ir.List_t t_value =>
    aux1 (fun (h : has_lazy_storage) => List_f h) t_value
  | Script_typed_ir.Map_t _ t_value =>
    aux1 (fun (h : has_lazy_storage) => Map_f h) t_value
  end.

(** Transforms a value potentially containing lazy storage in an intermediary
  state to a value containing lazy storage only represented by identifiers.

  Returns the updated value, the updated set of ids to copy, and the lazy
  storage diff to show on the receipt and apply on the storage. *)
Definition extract_lazy_storage_updates {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (acc_value : Alpha_context.Lazy_storage.diffs) (ty : Script_typed_ir.ty)
  (x : A)
  : M=?
    (Alpha_context.context * A * Alpha_context.Lazy_storage.IdSet.t *
      Alpha_context.Lazy_storage.diffs) :=
  let fix aux {a : Set}
    (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
    (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
    (acc_value : Alpha_context.Lazy_storage.diffs) (ty : Script_typed_ir.ty)
    (x : a) (has_lazy_storage_value : has_lazy_storage) {struct ty}
    : M=?
      (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
        Alpha_context.Lazy_storage.diffs) :=
    let=? ctxt :=
      return= (Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle)
      in
    match (has_lazy_storage_value, ty, x) with
    | (False_f, _, _) =>
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      ((fun (x :
        M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs)) => x)
        (Error_monad._return (ctxt, x, ids_to_copy, acc_value)))
    
    | (_, Script_typed_ir.Big_map_t _ _, map) =>
      let 'existT _ [__0, __1] map :=
        cast_exists (Es := [Set ** Set])
          (fun '[__0, __1] => Script_typed_ir.big_map __0 __1) map in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      (let=? '(diff_value, id, ctxt) :=
        diff_of_big_map ctxt mode temporary ids_to_copy map in
      let Map := map.(Script_typed_ir.big_map.diff) in
      return=?
        (let 'existS _ _ Map := Map in
        let map :=
          Script_typed_ir.big_map.with_diff
            (empty_map Map.(Script_typed_ir.Boxed_map.key_ty))
            (Script_typed_ir.big_map.with_id (Some id) map) in
        let diff_value :=
          Alpha_context.Lazy_storage.make
            Alpha_context.Lazy_storage.Kind.Big_map id diff_value in
        let ids_to_copy :=
          Alpha_context.Lazy_storage.IdSet.add
            Alpha_context.Lazy_storage.Kind.Big_map id ids_to_copy in
        (ctxt, map, ids_to_copy, (cons diff_value acc_value))))
    
    | (_, Script_typed_ir.Sapling_state_t _, sapling_state) =>
      let sapling_state := cast Alpha_context.Sapling.state sapling_state in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      (let=? '(diff_value, id, ctxt) :=
        diff_of_sapling_state ctxt temporary ids_to_copy sapling_state in
      let sapling_state :=
        Alpha_context.Sapling.empty_state (Some id)
          sapling_state.(Alpha_context.Sapling.state.memo_size) tt in
      let diff_value :=
        Alpha_context.Lazy_storage.make
          Alpha_context.Lazy_storage.Kind.Sapling_state id diff_value in
      let ids_to_copy :=
        Alpha_context.Lazy_storage.IdSet.add
          Alpha_context.Lazy_storage.Kind.Sapling_state id ids_to_copy in
      return=? (ctxt, sapling_state, ids_to_copy, (cons diff_value acc_value)))
    
    | (Pair_f hl hr, Script_typed_ir.Pair_t tyl tyr, x) =>
      let 'existT _ [__2, __3] [x, tyr, tyl, hr, hl] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__2, __3] =>
            [__2 * __3 ** Script_typed_ir.ty ** Script_typed_ir.ty **
              has_lazy_storage ** has_lazy_storage]) [x, tyr, tyl, hr, hl] in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      (let '(xl, xr) := x in
      let=? '(ctxt, xl, ids_to_copy, acc_value) :=
        aux ctxt mode temporary ids_to_copy acc_value tyl xl hl in
      let=? '(ctxt, xr, ids_to_copy, acc_value) :=
        aux ctxt mode temporary ids_to_copy acc_value tyr xr hr in
      return=? (ctxt, (xl, xr), ids_to_copy, acc_value))
    
    |
      (Union_f has_lazy_storage_l has_lazy_storage_r,
        Script_typed_ir.Union_t tyl tyr, x) =>
      let 'existT _ [__4, __5]
        [x, tyr, tyl, has_lazy_storage_r, has_lazy_storage_l] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__4, __5] =>
            [Script_typed_ir.union __4 __5 ** Script_typed_ir.ty **
              Script_typed_ir.ty ** has_lazy_storage ** has_lazy_storage])
          [x, tyr, tyl, has_lazy_storage_r, has_lazy_storage_l] in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      match x with
      | Script_typed_ir.L x =>
        let=? '(ctxt, x, ids_to_copy, acc_value) :=
          aux ctxt mode temporary ids_to_copy acc_value tyl x has_lazy_storage_l
          in
        return=? (ctxt, (Script_typed_ir.L x), ids_to_copy, acc_value)
      | Script_typed_ir.R x =>
        let=? '(ctxt, x, ids_to_copy, acc_value) :=
          aux ctxt mode temporary ids_to_copy acc_value tyr x has_lazy_storage_r
          in
        return=? (ctxt, (Script_typed_ir.R x), ids_to_copy, acc_value)
      end
    
    | (Option_f has_lazy_storage_value, Script_typed_ir.Option_t ty, o) =>
      let 'existT _ __6 [o, ty, has_lazy_storage_value] :=
        cast_exists (Es := Set)
          (fun __6 => [option __6 ** Script_typed_ir.ty ** has_lazy_storage])
          [o, ty, has_lazy_storage_value] in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      match o with
      | Some x =>
        let=? '(ctxt, x, ids_to_copy, acc_value) :=
          aux ctxt mode temporary ids_to_copy acc_value ty x
            has_lazy_storage_value in
        return=? (ctxt, (Some x), ids_to_copy, acc_value)
      | None => Error_monad._return (ctxt, None, ids_to_copy, acc_value)
      end
    
    | (List_f has_lazy_storage_value, Script_typed_ir.List_t ty, l_value) =>
      let 'existT _ __7 [l_value, ty, has_lazy_storage_value] :=
        cast_exists (Es := Set)
          (fun __7 =>
            [Script_typed_ir.boxed_list __7 ** Script_typed_ir.ty **
              has_lazy_storage]) [l_value, ty, has_lazy_storage_value] in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      (let=? '(ctxt, l_value, ids_to_copy, acc_value) :=
        Error_monad.fold_left_s
          (fun (function_parameter :
            Alpha_context.context * Script_typed_ir.boxed_list __7 *
              Alpha_context.Lazy_storage.IdSet.t *
              Alpha_context.Lazy_storage.diffs) =>
            let '(ctxt, l_value, ids_to_copy, acc_value) := function_parameter
              in
            fun (x : __7) =>
              let=? '(ctxt, x, ids_to_copy, acc_value) :=
                aux ctxt mode temporary ids_to_copy acc_value ty x
                  has_lazy_storage_value in
              return=? (ctxt, (list_cons x l_value), ids_to_copy, acc_value))
          (ctxt, list_empty, ids_to_copy, acc_value)
          l_value.(Script_typed_ir.boxed_list.elements) in
      let reversed :=
        {|
          Script_typed_ir.boxed_list.elements :=
            List.rev l_value.(Script_typed_ir.boxed_list.elements);
          Script_typed_ir.boxed_list.length :=
            l_value.(Script_typed_ir.boxed_list.length) |} in
      return=? (ctxt, reversed, ids_to_copy, acc_value))
    
    | (Map_f has_lazy_storage_value, Script_typed_ir.Map_t _ ty, M) =>
      let 'existT _ [__8, __9] [M, ty, has_lazy_storage_value] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__8, __9] =>
            [Script_typed_ir.map __8 __9 ** Script_typed_ir.ty **
              has_lazy_storage]) [M, ty, has_lazy_storage_value] in
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      (let 'existS _ _ M := M in
      let=? '(ctxt, m, ids_to_copy, acc_value) :=
        Error_monad.fold_left_s
          (fun (function_parameter :
            Alpha_context.context *
              M.(Script_typed_ir.Boxed_map.OPS).(S.MAP.t) __9 *
              Alpha_context.Lazy_storage.IdSet.t *
              Alpha_context.Lazy_storage.diffs) =>
            let '(ctxt, m, ids_to_copy, acc_value) := function_parameter in
            fun (function_parameter : __8 * __9) =>
              let '(k, x) := function_parameter in
              let=? '(ctxt, x, ids_to_copy, acc_value) :=
                aux ctxt mode temporary ids_to_copy acc_value ty x
                  has_lazy_storage_value in
              return=?
                (ctxt, (M.(Script_typed_ir.Boxed_map.OPS).(S.MAP.add) k x m),
                  ids_to_copy, acc_value))
          (ctxt, M.(Script_typed_ir.Boxed_map.OPS).(S.MAP.empty), ids_to_copy,
            acc_value)
          (M.(Script_typed_ir.Boxed_map.OPS).(S.MAP.bindings)
            (Pervasives.fst M.(Script_typed_ir.Boxed_map.boxed))) in
      let M :=
        let OPS := M.(Script_typed_ir.Boxed_map.OPS) in
        let key : Set := __8 in
        let value : Set := __9 in
        let key_ty := M.(Script_typed_ir.Boxed_map.key_ty) in
        let boxed := (m, (Pervasives.snd M.(Script_typed_ir.Boxed_map.boxed)))
          in
        {|
          Script_typed_ir.Boxed_map.OPS := OPS;
          Script_typed_ir.Boxed_map.key_ty := key_ty;
          Script_typed_ir.Boxed_map.boxed := boxed
        |} in
      return=? (ctxt, (existS (A := Set -> Set) _ _ M), ids_to_copy, acc_value))
    
    | _ =>
      cast
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs))
      (* ❌ Assert instruction is not handled. *)
      (assert
        (M=?
          (Alpha_context.context * a * Alpha_context.Lazy_storage.IdSet.t *
            Alpha_context.Lazy_storage.diffs)) false)
    end in
  let has_lazy_storage_value := has_lazy_storage_value ty in
  aux ctxt mode temporary ids_to_copy acc_value ty x has_lazy_storage_value.

Fixpoint fold_lazy_storage {acc a : Set}
  (f : Alpha_context.Lazy_storage.IdSet.fold_f acc) (init_value : acc)
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty) (x : a)
  (has_lazy_storage_value : has_lazy_storage) {struct ty}
  : M? (acc * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle
    in
  match (has_lazy_storage_value, ty, x) with
  | (_, Script_typed_ir.Big_map_t _ _, big_map) =>
    let 'existT _ [__0, __1] big_map :=
      cast_exists (Es := [Set ** Set])
        (fun '[__0, __1] => Script_typed_ir.big_map __0 __1) big_map in
    match big_map with
    | {| Script_typed_ir.big_map.id := Some id |} =>
      let? ctxt :=
        Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
      return?
        ((f.(Alpha_context.Lazy_storage.IdSet.fold_f.f)
          Alpha_context.Lazy_storage.Kind.Big_map id init_value), ctxt)
    | {| Script_typed_ir.big_map.id := None |} => return? (init_value, ctxt)
    end
  
  | (_, Script_typed_ir.Sapling_state_t _, state) =>
    let state := cast Alpha_context.Sapling.state state in
    match state with
    | {| Alpha_context.Sapling.state.id := Some id |} =>
      let? ctxt :=
        Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
      return?
        ((f.(Alpha_context.Lazy_storage.IdSet.fold_f.f)
          Alpha_context.Lazy_storage.Kind.Sapling_state id init_value), ctxt)
    | {| Alpha_context.Sapling.state.id := None |} => return? (init_value, ctxt)
    end
  
  | (False_f, _, _) => return? (init_value, ctxt)
  
  | (Pair_f hl hr, Script_typed_ir.Pair_t tyl tyr, x) =>
    let 'existT _ [__2, __3] [x, tyr, tyl, hr, hl] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__2, __3] =>
          [__2 * __3 ** Script_typed_ir.ty ** Script_typed_ir.ty **
            has_lazy_storage ** has_lazy_storage]) [x, tyr, tyl, hr, hl] in
    let '(xl, xr) := x in
    let? '(init_value, ctxt) := fold_lazy_storage f init_value ctxt tyl xl hl in
    fold_lazy_storage f init_value ctxt tyr xr hr
  
  |
    (Union_f has_lazy_storage_l has_lazy_storage_r,
      Script_typed_ir.Union_t ty_l ty_r, x) =>
    let 'existT _ [__4, __5]
      [x, ty_r, ty_l, has_lazy_storage_r, has_lazy_storage_l] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__4, __5] =>
          [Script_typed_ir.union __4 __5 ** Script_typed_ir.ty **
            Script_typed_ir.ty ** has_lazy_storage ** has_lazy_storage])
        [x, ty_r, ty_l, has_lazy_storage_r, has_lazy_storage_l] in
    match x with
    | Script_typed_ir.L x =>
      fold_lazy_storage f init_value ctxt ty_l x has_lazy_storage_l
    | Script_typed_ir.R x =>
      fold_lazy_storage f init_value ctxt ty_r x has_lazy_storage_r
    end
  
  | (Option_f has_lazy_storage_value, Script_typed_ir.Option_t ty, x) =>
    let 'existT _ __6 [x, ty, has_lazy_storage_value] :=
      cast_exists (Es := Set)
        (fun __6 => [option __6 ** Script_typed_ir.ty ** has_lazy_storage])
        [x, ty, has_lazy_storage_value] in
    match x with
    | None => return? (init_value, ctxt)
    | Some x => fold_lazy_storage f init_value ctxt ty x has_lazy_storage_value
    end
  
  | (List_f has_lazy_storage_value, Script_typed_ir.List_t ty, l_value) =>
    let 'existT _ __7 [l_value, ty, has_lazy_storage_value] :=
      cast_exists (Es := Set)
        (fun __7 =>
          [Script_typed_ir.boxed_list __7 ** Script_typed_ir.ty **
            has_lazy_storage]) [l_value, ty, has_lazy_storage_value] in
    List.fold_left
      (fun (acc_value : M? (acc * Alpha_context.context)) =>
        fun (x : __7) =>
          let? '(init_value, ctxt) := acc_value in
          fold_lazy_storage f init_value ctxt ty x has_lazy_storage_value)
      (return? (init_value, ctxt)) l_value.(Script_typed_ir.boxed_list.elements)
  
  | (Map_f has_lazy_storage_value, Script_typed_ir.Map_t _ ty, m) =>
    let 'existT _ [__8, __9] [m, ty, has_lazy_storage_value] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__8, __9] =>
          [Script_typed_ir.map __8 __9 ** Script_typed_ir.ty **
            has_lazy_storage]) [m, ty, has_lazy_storage_value] in
    map_fold
      (fun (function_parameter : __8) =>
        let '_ := function_parameter in
        fun (v : __9) =>
          fun (acc_value : M? (acc * Alpha_context.context)) =>
            let? '(init_value, ctxt) := acc_value in
            fold_lazy_storage f init_value ctxt ty v has_lazy_storage_value) m
      (return? (init_value, ctxt))
  
  | _ =>
    (* ❌ Assert instruction is not handled. *)
    assert (M? (acc * Alpha_context.context)) false
  end.

Definition collect_lazy_storage {A : Set}
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty) (x : A)
  : M? (Alpha_context.Lazy_storage.IdSet.t * Alpha_context.context) :=
  let has_lazy_storage_value := has_lazy_storage_value ty in
  let f {B : Set}
    (kind_value : Alpha_context.Lazy_storage.Kind.t) (id : B)
    (acc_value : Alpha_context.Lazy_storage.IdSet.t)
    : Alpha_context.Lazy_storage.IdSet.t :=
    Alpha_context.Lazy_storage.IdSet.add kind_value id acc_value in
  fold_lazy_storage {| Alpha_context.Lazy_storage.IdSet.fold_f.f _ := f |}
    no_lazy_storage_id ctxt ty x has_lazy_storage_value.

Definition extract_lazy_storage_diff {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (to_duplicate : Alpha_context.Lazy_storage.IdSet.t)
  (to_update : Alpha_context.Lazy_storage.IdSet.t) (ty : Script_typed_ir.ty)
  (v : A)
  : M=? (A * option Alpha_context.Lazy_storage.diffs * Alpha_context.context) :=
  let to_duplicate :=
    Alpha_context.Lazy_storage.IdSet.diff_value to_duplicate to_update in
  let=? '(ctxt, v, alive, diffs) :=
    extract_lazy_storage_updates ctxt mode temporary to_duplicate nil ty v in
  let diffs :=
    if temporary then
      diffs
    else
      let dead := Alpha_context.Lazy_storage.IdSet.diff_value to_update alive in
      let f {B : Set}
        (kind_value : Alpha_context.Lazy_storage.Kind.t) (id : B)
        (acc_value : list Alpha_context.Lazy_storage.diffs_item)
        : list Alpha_context.Lazy_storage.diffs_item :=
        cons
          ((Alpha_context.Lazy_storage.make (a := unit) (u := unit)) kind_value
            id Alpha_context.Lazy_storage.Remove) acc_value in
      Alpha_context.Lazy_storage.IdSet.fold_all
        {| Alpha_context.Lazy_storage.IdSet.fold_f.f _ := f |} dead diffs in
  match diffs with
  | [] => return=? (v, None, ctxt)
  | diffs => return=? (v, (Some diffs), ctxt)
  end.

Definition list_of_big_map_ids (ids : Alpha_context.Lazy_storage.IdSet.t)
  : list Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t) :=
  Alpha_context.Lazy_storage.IdSet.fold Alpha_context.Lazy_storage.Kind.Big_map
    (fun (id : Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) =>
      fun (acc_value :
        list Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) =>
        cons id acc_value) ids nil.

Definition parse_data {A : Set}
  : option type_logger -> Alpha_context.context -> bool -> bool ->
  Script_typed_ir.ty -> Alpha_context.Script.node ->
  M=? (A * Alpha_context.context) := fun x_1 => parse_data_with_stack x_1 0.

Definition parse_instr
  : option type_logger -> tc_context -> Alpha_context.context -> bool ->
  Alpha_context.Script.node -> Script_typed_ir.stack_ty ->
  M=? (judgement * Alpha_context.context) :=
  fun x_1 => parse_instr_with_stack x_1 0.

Definition unparse_data {A : Set}
  : Alpha_context.context -> unparsing_mode -> Script_typed_ir.ty -> A ->
  M=? (Alpha_context.Script.node * Alpha_context.context) :=
  fun x_1 => unparse_data_with_stack x_1 0.

Definition unparse_code
  : Alpha_context.context -> unparsing_mode -> Alpha_context.Script.node ->
  M=? (Alpha_context.Script.node * Alpha_context.context) :=
  fun x_1 => unparse_code_with_stack x_1 0.

Definition get_single_sapling_state {A : Set}
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty) (x : A)
  : M?
    (Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t) *
      Alpha_context.context) :=
  let has_lazy_storage_value := has_lazy_storage_value ty in
  let f {i : Set}
    (kind_value : Alpha_context.Lazy_storage.Kind.t) (id : i)
    (single_id_opt :
      option Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t))
    : option Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t) :=
    match (kind_value, id) with
    | (Alpha_context.Lazy_storage.Kind.Sapling_state, id) =>
      let id :=
        cast Alpha_context.Sapling.Id.(Lazy_storage_kind.IdWithTemp.t) id in
      match single_id_opt with
      | None => Some id
      | Some _ => Pervasives.raise (Build_extensible "Not_found" unit tt)
      end
    | _ => single_id_opt
    end in
  let? function_parameter :=
    fold_lazy_storage {| Alpha_context.Lazy_storage.IdSet.fold_f.f _ := f |}
      None ctxt ty x has_lazy_storage_value in
  match function_parameter with
  | (None, _) => Pervasives.raise (Build_extensible "Not_found" unit tt)
  | (Some id, ctxt) => return? (id, ctxt)
  end.

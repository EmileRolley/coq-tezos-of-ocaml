Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_2021_01.Commitment_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Definition get_opt
  : Raw_context.t -> Blinded_public_key_hash.t -> M=? (option Tez_repr.t) :=
  Storage.Commitments.(Storage_sigs.Indexed_data_storage.get_option).

Definition delete
  : Raw_context.t -> Blinded_public_key_hash.t -> M=? Raw_context.t :=
  Storage.Commitments.(Storage_sigs.Indexed_data_storage.delete).

Definition init_value
  (ctxt : Raw_context.t) (commitments : list Commitment_repr.t)
  : M=? Raw_context.t :=
  let init_commitment
    (ctxt : Raw_context.t) (function_parameter : Commitment_repr.t)
    : M=? Raw_context.t :=
    let '{|
      Commitment_repr.t.blinded_public_key_hash := blinded_public_key_hash;
        Commitment_repr.t.amount := amount
        |} := function_parameter in
    Storage.Commitments.(Storage_sigs.Indexed_data_storage.init_value) ctxt
      blinded_public_key_hash amount in
  Error_monad.fold_left_s init_commitment ctxt commitments.

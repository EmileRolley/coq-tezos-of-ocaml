(** In this file we verify properties related to the comparable data in the
    translator. *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.

Require Import TezosOfOCaml.Proto_2021_01.Proofs.Utils.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Time.
Require TezosOfOCaml.Proto_2021_01.Proofs.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Proofs.Script_typed_ir.

(** A simpler [unparse_comparable_data] function, which does not return the
    context or errors. We only return the node, we return a default node in case
    of error. *)
Definition simple_unparse_comparable_data ctxt mode ty
  (data : Script_typed_ir.Comparable_ty.to_Set ty)
  : Alpha_context.Script.node :=
  match Script_ir_translator.unparse_comparable_data ctxt mode ty data with
  | Lwt.Return (Pervasives.Ok (node, _)) => node
  | _ => Micheline.Prim (-1) Michelson_v1_primitives.T_unit [] []
  end.

(** We show that with an unlimited gas and the right pre-condition, the
    [unparse_comparable_data] function always succeed and is equivalent to
    [simple_unparse_comparable_data]. This lemma should be useful to prove
    properties about [unparse_comparable_data] by simplifying the reasoning in
    the error monad. Indeed, using [simple_unparse_comparable_data] we can give
    a name to the result of [unparse_comparable_data] for when it succeeds. *)
Fixpoint simple_unparse_comparable_data_eq ctxt mode ty
  (data : Script_typed_ir.Comparable_ty.to_Set ty)
  (H_data : Script_typed_ir.Comparable_data.is_valid ty data = true)
  {struct ty}
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    (Script_ir_translator.unparse_comparable_data unlimited_ctxt mode ty data =
    return=? (
      simple_unparse_comparable_data unlimited_ctxt mode ty data,
      unlimited_ctxt
    )).
  destruct ty; unfold simple_unparse_comparable_data; simpl;
    repeat rewrite cast_eval; simpl;
    try reflexivity;
    try now (destruct mode; reflexivity).
  { step Script_typed_ir.Never_key.
    destruct data.
  }
  { step Script_typed_ir.Timestamp_key.
    unfold Script_ir_translator.unparse_timestamp; simpl.
    destruct mode;
      try destruct (Alpha_context.Script_timestamp.to_notation _);
      reflexivity.
  }
  { step Script_typed_ir.Address_key.
    unfold Script_ir_translator.unparse_address; simpl.
    destruct mode; destruct data; destruct s; try reflexivity; inversion H_data.
  }
  { step Script_typed_ir.Pair_key.
    rewrite_cast_exists_eval_eq [
      Script_typed_ir.Comparable_ty.to_Set ty1,
      Script_typed_ir.Comparable_ty.to_Set ty2
    ].
    destruct data.
    destruct (Script_typed_ir.Comparable_data.is_valid_pair H_data).
    unfold Script_ir_translator.unparse_pair.
    do 2 (rewrite simple_unparse_comparable_data_eq; simpl; trivial).
  }
  { step Script_typed_ir.Union_key.
    rewrite_cast_exists_eval_eq [
      Script_typed_ir.Comparable_ty.to_Set ty1,
      Script_typed_ir.Comparable_ty.to_Set ty2
    ].
    unfold Script_ir_translator.unparse_union.
    destruct data; now rewrite simple_unparse_comparable_data_eq.
  }
  { step Script_typed_ir.Option_key.
    rewrite_cast_exists_eval_eq (Script_typed_ir.Comparable_ty.to_Set ty).
    unfold Script_ir_translator.unparse_option.
    destruct data; trivial.
    now rewrite simple_unparse_comparable_data_eq.
  }
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Import Environment.

Require TezosOfOCaml.Proto_2021_01.Proofs.Utils.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Error_monad.

Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Roll_storage.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Import Environment.Notations.

Infix "tz+" := Tez_repr.op_plusquestion (at level 70, no associativity).
Infix "tz-" := Tez_repr.op_minusquestion (at level 70, no associativity).
Infix "tz<" := Tez_repr.op_lt (at level 70, no associativity).
Infix "tz<=" := Tez_repr.op_lteq (at level 70, no associativity).

Module Redefine_functions.

  Definition roll_successor (ctxt : Raw_context.t)
             (head_roll : int64) :=
    Storage.Roll.Successor.(Storage_sigs.Indexed_data_storage.get_option)
                             ctxt head_roll.
  
  Definition traverse_rolls (ctxt : Raw_context.t) (head_roll : int32)
  : M=? (list int32) :=
    Error_monad.fold_left_s
      (fun (acc : list int32) (roll : int32) =>
         let=? function_parameter :=  roll_successor ctxt roll 
         in
         match function_parameter with
         | None => (return=? (List.rev acc))
         | Some next => (return=? (cons next acc))
         end
      ) [head_roll] [head_roll].


  Definition delegate_roll_list (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) :=
    Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
                                      ctxt delegate.
  
  Definition get_rolls
             (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    : M=? (list int32) :=
    let=? function_parameter := delegate_roll_list ctxt delegate
    in
    match function_parameter with
    | None => Error_monad.return_nil
    | Some head_roll => traverse_rolls ctxt head_roll
    end.

  Definition count_rolls
             (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) : M=? int :=
    let=? function_parameter := delegate_roll_list ctxt delegate in
    match function_parameter with
    | None => (return=? 0)
    | Some head_roll =>
      Error_monad.fold_left_s
        (fun (acc: int) roll =>
           let=? function_parameter := roll_successor ctxt roll in
           match function_parameter with
           | None => (return=? acc)
           | Some _next => (return=? (Pervasives.succ acc))
           end             
        ) 1 [head_roll]
    end.

  (*** get_rolls, traverse_rolls *)
  
  
  (* Lemma get_rolls_nil (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) :
    delegate_roll_list ctxt delegate = return=? None ->
    get_rolls ctxt delegate = Error_monad.return_nil.
  
  Proof.
    unfold delegate_roll_list.
    unfold get_rolls.
    destruct (Storage.Roll.Delegate_roll_list.(Storage_sigs.Indexed_data_storage.get_option)
                                                ctxt
                                                delegate) as [t];
      simpl; try Utils.impossible_branch.
  Qed. *)

  Lemma get_rolls_some (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
        (head_roll : int) :
    (delegate_roll_list ctxt delegate = (return=? Some head_roll)) ->
    get_rolls ctxt delegate = traverse_rolls ctxt head_roll.

  Proof.
    unfold get_rolls.
    destruct (delegate_roll_list ctxt delegate); simpl;
      try Utils.impossible_branch.
  Qed.

  (*** count_rolls *)
  
  Lemma count_rolls_empty (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) :
    (delegate_roll_list ctxt delegate = return=? None ->
     count_rolls ctxt delegate = return=? 0).

  Proof.
    unfold count_rolls.
    destruct (delegate_roll_list); simpl; try Utils.impossible_branch.
  Qed.
    
End Redefine_functions.

Module Delegate_redefine.

  (*** add_amount *)

  Definition get_delegate_change (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) : M=? Tez_repr.t :=
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.get)
                                   ctxt delegate.

  Definition set_delegate_change (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash)
             (change : Tez_repr.t) : M=? Raw_context.t :=
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.set)
                                   ctxt delegate change.
  
  Definition get_set_change (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    (amount : Tez_repr.t) : M=? (Tez_repr.t * Raw_context.t) :=
    let=? change := get_delegate_change ctxt delegate in
    let=? change := return= (Tez_repr.op_plusquestion amount change) in
    let=? ctxt := set_delegate_change ctxt delegate change in
    (return=? (change, ctxt)).

  Definition add_amount_aux (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash)
             (amount : Tez_repr.t) : M=? (Tez_repr.t * Raw_context.t) :=
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? '(change, ctxt) := get_set_change ctxt delegate amount in
    let=? delegate_pk := Roll_storage.delegate_pubkey ctxt delegate in
    let=? '(change, ctxt) :=
       Error_monad.fold_left_s
         (fun function_parameter _ =>
            let '(acc, ctxt) := function_parameter in
            if Tez_repr.op_lt acc tokens_per_roll then
              return=? (acc, ctxt)
            else
              (let=? change :=
                 return= (Tez_repr.op_minusquestion acc tokens_per_roll)
              in
              let=? ctxt :=
                 Roll_storage.Delegate.create_roll_in_delegate ctxt delegate delegate_pk
              in
               return=? (change, ctxt))
         ) (change, ctxt) [(change, ctxt)]
    in
    (return=? (change, ctxt)).
  
  Lemma add_amount_aux_sum (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (pub : Signature.public_key)
        (a1 a2 change : Tez_repr.t) :
    (* precondition for return the Ok case when getting change from delegate *)
    get_delegate_change ctxt delegate = Return (Pervasives.Ok change) ->    
    (* precondition of Tez_repr.op_plusquestion when compute new change (amount + change);
       returns false (error in the case of true) *)
    a1 +i64 change tz< a1 = false ->
    a2 +i64 change tz< a2 = false ->
    a1 +i64 a2 +i64 change tz< a1 +i64 a2 = false ->
    (* precondition for set a new change into a delegate, return the success context *)
    set_delegate_change ctxt delegate (a1 +i64 change) = Return (Pervasives.Ok ctxt) ->
    set_delegate_change ctxt delegate (a2 +i64 change) = Return (Pervasives.Ok ctxt) ->
    set_delegate_change ctxt delegate (a1 +i64 a2 +i64 change) = Return (Pervasives.Ok ctxt) ->
    (* precondition for getting the public key from delegate *)
    Roll_storage.delegate_pubkey ctxt delegate = Return (Pervasives.Ok pub) ->
    (* precondition at if condition, choose the case return false *)
    (a1 +i64 change) tz< Constants_storage.tokens_per_roll ctxt = false ->
    (a2 +i64 change) tz< Constants_storage.tokens_per_roll ctxt = false ->
    (a1 +i64 a2 +i64 change) tz< Constants_storage.tokens_per_roll ctxt = false ->
    (* preconditions for op_minusquestion in case the if condition returns false *)
    Constants_storage.tokens_per_roll ctxt tz<= a1 +i64 change = true ->
    Constants_storage.tokens_per_roll ctxt tz<= a2 +i64 change = true ->
    Constants_storage.tokens_per_roll ctxt tz<= a1 +i64 a2 +i64 change = true ->
    (* precondition for create a roll in delegate, returns the success context *)
    Roll_storage.Delegate.create_roll_in_delegate ctxt delegate pub = Return (Pervasives.Ok ctxt) ->
    (* property sum *)
    Error_monad.terminates_lwt (
        let=? '(new_a1, _) := add_amount_aux ctxt delegate a1 in
        let=? '(new_a2, _) := add_amount_aux ctxt delegate a2 in
        let total_a1_a2 := new_a1 +i64 new_a2 in
        let=? '(total, _) := add_amount_aux ctxt delegate (a1 +i64 a2) in
        return=? (Tez_repr.op_eq total_a1_a2 total)).    

  Proof.
    unfold add_amount_aux; simpl.

    (* in a delegate returns its change *)
    
    unfold get_set_change.
    case_eq (get_delegate_change ctxt delegate); simpl.
    destruct t; simpl; try easy.

    (* compute change first time := amount tz+ change *)
    
    unfold Tez_repr.op_plusquestion.
    intros H_get H_eq_get H_plus_a1 H_plus_a2 H_plus_total.
    inversion H_eq_get.
    
    rewrite H_plus_a1.
    rewrite H_plus_a2.
    rewrite H_plus_total.

    (* set a new change to a delegate *)
    
    simpl.

    destruct (set_delegate_change ctxt delegate (a1 +i64 change)); simpl.
    destruct (set_delegate_change ctxt delegate (a2 +i64 change)); simpl.
    destruct (set_delegate_change ctxt delegate (a1 +i64 a2 +i64 change)); simpl.
    
    intros H_eq_set_a1 H_eq_set_a2 H_eq_set_total.
    
    inversion H_eq_set_a1.
    inversion H_eq_set_a2.
    inversion H_eq_set_total.

    simpl.

    (* get the delegate public key from the delegate *)

    destruct (Roll_storage.delegate_pubkey ctxt delegate); simpl; try easy.
    destruct t3; simpl; try Utils.impossible_branch.
    intros H_eq_pub.
    
    (* if condition: change < tokens: choose the case return false *)

    intros H_lt_a1 H_lt_a2 H_lt_total.
    rewrite H_lt_a1.
    rewrite H_lt_a2.
    rewrite H_lt_total.
    
    (* condition for op_minusquestion of total case return false; return true case *)
    
    unfold Tez_repr.op_minusquestion.

    intros H_lteq_a1 H_lteq_a2 H_lteq_total.
    rewrite H_lteq_a1. rewrite H_lteq_a2. rewrite H_lteq_total.
    
    simpl.

    (* create a roll in delegate and sum *)
    
    inversion H_eq_pub.    
    destruct (Roll_storage.Delegate.create_roll_in_delegate ctxt delegate pub); simpl.
    destruct t3; simpl; try Utils.impossible_branch.    
  Qed.
  
  Lemma add_amount_aux_eq_ctxt (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (a1 a2 : Tez_repr.t) :
    (* condition a1 = a2 *)
     Tez_repr.equal a1 a2 = true ->
     (* property *)
     let ctxt1 := add_amount_aux ctxt delegate a1 in
     let ctxt2 := add_amount_aux ctxt delegate a2 in
     ctxt1 = ctxt2.
  
  Proof.

    unfold add_amount_aux; simpl.

    (* a1 = a2 *)
    
    unfold Tez_repr.equal; simpl.
    unfold Int64.compare; simpl.
    rewrite Z.eqb_eq.
    intro H_eq_amount.
    apply Zminus_eq in H_eq_amount.
    rewrite <- H_eq_amount.
    reflexivity.
  Qed.
    
  Definition active_delegate_with_rolls (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) : M=? Raw_context.t :=
    Error_monad.op_gtpipeeq
      (Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.add)
                                             ctxt delegate) Error_monad.ok.
  
  Definition add_and_create_roll (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash)
             (amount : Tez_repr.t) :=
    let=? '(_, ctxt) := add_amount_aux ctxt delegate amount in
    let=? rolls := Redefine_functions.delegate_roll_list ctxt delegate in
    match rolls with
    | None => (return=? ctxt)
    | Some _ => active_delegate_with_rolls ctxt delegate
    end.

  Lemma add_and_create_roll_eq_ctxt (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (amount new_amount : Tez_repr.t) roll :
    (* precondition for add_amount_aux returns a success context *)
    add_amount_aux ctxt delegate amount = Return (Pervasives.Ok (new_amount, ctxt)) ->
    (* precondition for delegate_roll return Some roll *)
    Redefine_functions.delegate_roll_list ctxt delegate = Return (Pervasives.Ok (Some roll)) ->
    (* property *)
    let add_create_ok :=
        add_and_create_roll ctxt delegate amount = Return (Pervasives.Ok ctxt)
    in
    let active_ok :=
        active_delegate_with_rolls ctxt delegate = Return (Pervasives.Ok ctxt)
    in
    add_create_ok = active_ok.

  Proof.
    unfold add_and_create_roll; simpl.
    destruct (add_amount_aux ctxt delegate amount);
      simpl.
    destruct t; simpl; try easy.
    intro H_eq_amount.
    inversion H_eq_amount.
    destruct (Redefine_functions.delegate_roll_list ctxt delegate); simpl.
    destruct t; simpl; try easy.
    destruct o; simpl; try easy.
  Qed.

  Definition add_amount
             (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
             (amount : Tez_repr.t) : M=? Raw_context.t :=
    let=? ctxt := Roll_storage.Delegate.ensure_inited ctxt delegate in
    let=? inactive := Roll_storage.Delegate.is_inactive ctxt delegate in
    if inactive then
      return=? ctxt
    else
      add_and_create_roll ctxt delegate amount.
    
  Lemma add_amount_eq_ctxt (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
        (amount : Tez_repr.t) :
    (* precondition for ensure_inited *)
    Roll_storage.Delegate.ensure_inited ctxt delegate = Return (Pervasives.Ok ctxt) ->   
    (* precondition for is_inactive returns false *)
    Roll_storage.Delegate.is_inactive ctxt delegate = Return (Pervasives.Ok false) ->
    (* property *)
    let add_amount_ok := add_amount ctxt delegate amount = Return (Pervasives.Ok ctxt) in
    let add_and_create_roll_ok := add_and_create_roll ctxt delegate amount = Return (Pervasives.Ok ctxt) in
    add_amount_ok = add_and_create_roll_ok.

  Proof.
    unfold add_amount; simpl.
    (*a. ensure_inited *)
    case_eq (Roll_storage.Delegate.ensure_inited ctxt delegate); simpl.
    destruct t; simpl; try Utils.impossible_branch; try easy.
 
    (*b. is_inactive *)
    intros H_ensure H_eq_ctxt H_inactive_false.
    inversion H_eq_ctxt.
    revert H_inactive_false.
    destruct (Roll_storage.Delegate.is_inactive ctxt delegate);
      simpl.
    destruct t0; simpl; try Utils.impossible_branch.
  Qed.
  
  (*** remove_amount *)

  Definition active_delegate_del (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) :=
    Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.del)
                                          ctxt delegate.

  Definition fold_aux (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
             (amount tokens_per_roll change : Tez_repr.t) :=
    let=? '(ctxt, change) :=
       Error_monad.fold_left_s
         (fun function_parameter _ =>
            let '(ctxt, acc) := function_parameter in
            if Tez_repr.op_lteq amount acc then
              return=? (ctxt, acc)
            else
              (let=? '(_, ctxt) := Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate in
               let=? change :=
                  return= (Tez_repr.op_plusquestion acc tokens_per_roll)
               in
               return=? (ctxt, change))
         ) (ctxt, change) [(ctxt, change)]
    in (return=? (ctxt, change)).
    
  Definition roll_none (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    (change : Tez_repr.t) : M=? (Raw_context.t * Tez_repr.t) :=
    let= ctxt := active_delegate_del ctxt delegate in
    (return=? (ctxt, change)).

  Definition remove_amount_aux 
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    (amount : Tez_repr.t) : M=? (Raw_context.t * Tez_repr.t)  :=
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? change := get_delegate_change ctxt delegate in
    let=? inactive := Roll_storage.Delegate.is_inactive ctxt delegate in
    
    let=? '(ctxt, change) :=
       if inactive then
          return=? (ctxt, change)
       else
         let=? '(ctxt, change) := fold_aux ctxt delegate amount tokens_per_roll change in
         let=? rolls := Redefine_functions.delegate_roll_list ctxt delegate in
         match rolls with
         | None => roll_none ctxt delegate change 
         | Some _ => (return=? (ctxt, change))
         end         
    in
    (return=? (ctxt, change)).

  Lemma remove_amount_aux_sub (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (a1 a2 change : Tez_repr.t) roll :
    (* precondition for get_delegate *)
    get_delegate_change ctxt delegate = Return (Pervasives.Ok change) ->
    (* choose the case inactive returns false *)
    Roll_storage.Delegate.is_inactive ctxt delegate = Return (Pervasives.Ok false) ->
    (* if condition: amount <= change: choose the case false *)
    a1 tz<= change = false ->
    a2 tz<= change = false ->
    a1 -i64 a2 tz<= change = false ->
    (* precondition for pop roll from delegate *)
    Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate = Return (Pervasives.Ok (roll, ctxt)) ->
    (* condition for op_plusquestion *)
    change +i64 Constants_storage.tokens_per_roll ctxt tz< change = false ->
    (* choose the delegate roll list returns none *)
    Redefine_functions.delegate_roll_list ctxt delegate = return=? None ->
    (* property *)
    Error_monad.terminates_lwt
      (let=? '(_, new_a1) := remove_amount_aux ctxt delegate a1 in
       let=? '(_, new_a2) := remove_amount_aux ctxt delegate a2 in
       let sub_a1_a2 := new_a1 -i64 new_a2 in
       let=? '(_, new_sub) := remove_amount_aux ctxt delegate (a1 -i64 a2) in
       return=? (Tez_repr.op_eq sub_a1_a2 new_sub)).

  Proof.
    unfold remove_amount_aux; simpl.

    (* from a delegate get its change *)
    
    case_eq (get_delegate_change ctxt delegate); simpl.
    destruct t; simpl; try easy.

    (* is_inactive, choose the false case *)

    case_eq (Roll_storage.Delegate.is_inactive ctxt delegate); simpl.
    intro inactive.
    destruct inactive; simpl; try easy.
    destruct b; simpl; try easy.

    intros H_inactive H_get H_eq_get H_eq_inactive.
    inversion H_eq_get.
        
    (* fold_aux *)
    unfold fold_aux; simpl.

    (* if condition: amount <= change: choose the case false *)
    intros H_lteq_a1 H_lteq_a2 H_lteq_sub.
    rewrite H_lteq_a1.
    rewrite H_lteq_a2.
    rewrite H_lteq_sub.

    (* pop roll from delegate *)
    case_eq (Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate); simpl.
    destruct t0; simpl; try easy.
    destruct p; simpl; try easy.

    intros H_pop_roll H_eq_pop.

    (* compute new_change: change tz+ tokens *)
    unfold Tez_repr.op_plusquestion.
    intros H_plus.
    rewrite H_plus.
    simpl.

    (* delegate roll list return none case *)
    inversion H_eq_pop.
    destruct (Redefine_functions.delegate_roll_list ctxt delegate); simpl.
    destruct t1; simpl; try easy.
    destruct o; simpl; try easy.
  Qed.

  Definition remove_amount_tez (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
             (amount : Tez_repr.t) : M=? (Raw_context.t * Tez_repr.t) :=
    let=? '(ctxt, change) := remove_amount_aux ctxt delegate amount in
    let=? change := return= (Tez_repr.op_minusquestion change amount) in
  (return=? (ctxt, change)).
  
  Lemma remove_amount_tez_sub (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
        (a1 a2 change : Tez_repr.t) roll :
    (* precondition for get_change *)
    get_delegate_change ctxt delegate = Return (Pervasives.Ok change) ->
    (* choose the case inactive returns false *)
    Roll_storage.Delegate.is_inactive ctxt delegate = Return (Pervasives.Ok false) ->
    (* condition for lteq return false *)
    a1 tz<= change = false ->
    a2 tz<= change = false ->
    a1 -i64 a2 tz<= change = false ->
    (* precondition for pop roll from delegate *)
    Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate = Return (Pervasives.Ok (roll, ctxt)) ->
    (* precondition for op_plusquestion *)
    change +i64 Constants_storage.tokens_per_roll ctxt tz< change = false ->
    (* precondition for delegate roll list, choose the none case *)
    Redefine_functions.delegate_roll_list ctxt delegate = Return (Pervasives.Ok None) ->
    (* precondition for op_minusquestion *)
    a1 tz<= change +i64 Constants_storage.tokens_per_roll ctxt = true ->
    a2 tz<= change +i64 Constants_storage.tokens_per_roll ctxt = true ->
    a1 -i64 a2 tz<= change +i64 Constants_storage.tokens_per_roll ctxt = true ->
    (* property *)
    Error_monad.terminates_lwt (
        let=? sub_a1_a2 :=
           let=? '(_, new_a1) := remove_amount_tez ctxt delegate a1 in
           let=? '(_, new_a2) := remove_amount_tez ctxt delegate a2 in
           return=? (new_a1 -i64 new_a2) 
        in
        let=? '(_, sub) := remove_amount_tez ctxt delegate (a1 -i64 a2) in
        return=? (Tez_repr.op_eq sub_a1_a2 sub)).

  Proof.

    unfold remove_amount_tez; simpl.
    unfold remove_amount_aux; simpl.
        
    (* get delegate change *)
    case_eq (get_delegate_change ctxt delegate); simpl.
    destruct t; simpl; try easy.
    intros H_get H_eq_get.
    inversion H_eq_get.
    
    (* if condition at inactive, choose the case return false *)
    destruct (Roll_storage.Delegate.is_inactive ctxt delegate); simpl;
      try easy.
    intro H_eq_inactive.
    inversion H_eq_inactive.

    (* fold_aux *)
    unfold fold_aux; simpl.

    (* if condition: amount tz<= change, choose the case returns false *)

    intros H_lteq_a1 H_lteq_a2 H_lteq_sub.
    rewrite H_lteq_a1.
    rewrite H_lteq_a2.
    rewrite H_lteq_sub.
   
    (* pop roll from delegate *)
    case_eq (Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate); simpl.
    destruct t1; simpl; try easy.
    intros H_pop_roll H_eq_pop.
    inversion H_eq_pop.
    
    (* compute the new_change: change tz+ tokens *)
    unfold Tez_repr.op_plusquestion.
    intros H_plus.
    rewrite H_plus.
    simpl.

    (* delegate roll list *)
    case_eq (Redefine_functions.delegate_roll_list ctxt delegate); simpl.
    destruct t1; simpl; try easy.
    destruct o; simpl; try easy.
    intros H_roll H_eq_roll.
        
    (* compute change again with new_change: new_change tz- amount *)
    unfold Tez_repr.op_minusquestion.
    
    intros H_minus_a1 H_minus_a2 H_minus_sub.
    rewrite H_minus_a1.
    rewrite H_minus_a2.
    rewrite H_minus_sub.
    simpl; try easy.
  Qed.

  Definition remove_amount (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
             (amount : Tez_repr.t) : M=? Raw_context.t :=
    let=? '(ctxt, change) := remove_amount_tez ctxt delegate amount in    
    set_delegate_change ctxt delegate change.
      
  Lemma remove_amount_eq_ctxt (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
        (amount : Tez_repr.t) :
    Error_monad.terminates_lwt (
        let=? '(ctxt, change) := remove_amount_tez ctxt delegate amount in
        set_delegate_change ctxt delegate change) =
    Error_monad.terminates_lwt (remove_amount ctxt delegate amount).    

  Proof.
    unfold remove_amount.
    destruct (remove_amount_tez ctxt delegate amount);
      simpl; try Utils.impossible_branch.
    destruct t; simpl; try easy.
  Qed.
      
  Lemma remove_amount_eq_tez_ctxt (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (a1 a2 : Tez_repr.t) :
    (* condition a1 = a2 *)
    Tez_repr.equal a1 a2 = true ->
    let ctxt1 := remove_amount ctxt delegate a1 in
    let ctxt2 := remove_amount ctxt delegate a2 in
    ctxt1 = ctxt2.

  Proof.
    unfold remove_amount; simpl.
    (* a1 = a2 *)
    unfold Tez_repr.equal; simpl.
    unfold Int64.compare; simpl.
    rewrite Z.eqb_eq.
    intro H_eq_amount.
    apply Zminus_eq in H_eq_amount.
    rewrite <- H_eq_amount.
    reflexivity.
  Qed.


  (*** set_inactive *)
  
  Definition set_inactive_aux (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash)
             (tokens_per_roll change : Tez_repr.t) :=
    Error_monad.fold_left_s
      (fun function_parameter _ =>
         let '(ctxt, acc) := function_parameter in
         let=? function_parameter := Redefine_functions.delegate_roll_list ctxt delegate in
         match function_parameter with
         | None => (return=? (ctxt, acc))
         | Some _roll =>
           let=? '(_, ctxt) := Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate in
           let=? change :=
              return= (Tez_repr.op_plusquestion acc tokens_per_roll) in
         return=? (ctxt, change)
         end
      ) (ctxt, change) [(ctxt, change)].

  Lemma set_inactive_aux_eq_ctxt (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (tokens_per_roll change : Tez_repr.t) roll :
    (* precondition for op_plusquestion *)
    change +i64 tokens_per_roll tz< change = false ->
    (* delegate roll list choose Some case *)
    Redefine_functions.delegate_roll_list ctxt delegate = Return (Pervasives.Ok (Some roll)) ->
    (* precondition of pop roll from delegate *)
    Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate = Return (Pervasives.Ok (change, ctxt)) ->
    (* property *)
    Error_monad.terminates_lwt (set_inactive_aux ctxt delegate tokens_per_roll change) =
    Error_monad.terminates_lwt (
        let=? '(_, ctxt) := Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate in
        let=? change1 := return= (Tez_repr.op_plusquestion change tokens_per_roll) in
        return=? (ctxt, change1)).

  Proof.
    unfold set_inactive_aux.
    simpl.
    destruct (Redefine_functions.delegate_roll_list ctxt delegate); simpl.
    destruct t; simpl; try easy.
    destruct o; simpl; try easy.
    destruct (Roll_storage.Delegate.pop_roll_from_delegate ctxt delegate);
      simpl; try easy; try Utils.impossible_branch.
    destruct t0; simpl; try easy.
    destruct p; simpl; try easy.
    intros H_eq_1 H_eq_2.
    inversion H_eq_1.
    inversion H_eq_2.
    unfold Tez_repr.op_plusquestion.
    rewrite H0.
    intros H_eq.
    inversion H_eq.
    simpl; reflexivity.
  Qed.
    
  Definition inactive_delegate_add (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) :=
    Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.add)
                                          ctxt (Contract_repr.implicit_contract delegate).
  
  Definition set_inactive
    (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    let=? ctxt := Roll_storage.Delegate.ensure_inited ctxt delegate in
    let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
    let=? change := get_delegate_change ctxt delegate in
    let= ctxt := inactive_delegate_add ctxt delegate in
    let= ctxt := active_delegate_del ctxt delegate in
    let=? '(ctxt, change) := set_inactive_aux ctxt delegate tokens_per_roll change in
    set_delegate_change ctxt delegate change.    
    
  (*** set_active *)

  Definition set_active_aux (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash)
             (delegate_pk : Signature.public_key)
             (tokens_per_roll change : Tez_repr.t) :=
    Error_monad.fold_left_s
      (fun function_parameter _ =>
         let '(ctxt, acc) := function_parameter in
          if Tez_repr.op_lt acc tokens_per_roll then
            (return=? (ctxt, acc))
          else
            let=? change :=
               return= (Tez_repr.op_minusquestion acc tokens_per_roll) in
            let=? ctxt := Roll_storage.Delegate.create_roll_in_delegate ctxt delegate delegate_pk in
            return=? (ctxt, change)
      ) (ctxt, change) [(ctxt, change)].
 
  Definition delegate_desactivation (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) :=
    (Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.get_option)
                                               ctxt
                                               (Contract_repr.implicit_contract delegate)).
  
  Definition expiration_aux (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) : M=? Cycle_repr.t :=
    let=? inactive := Roll_storage.Delegate.is_inactive ctxt delegate in
    let current_cycle := (Raw_context.current_level ctxt).(Level_repr.t.cycle) in
    let preserved_cycles := Constants_storage.preserved_cycles ctxt in
    let=? current_expiration := delegate_desactivation ctxt delegate in
    let expiration :=
        match current_expiration with
        | None => Cycle_repr.add current_cycle (1 +i (2 *i preserved_cycles))
        | Some current_expiration =>
          let delay :=
              if inactive then
                1 +i (2 *i preserved_cycles)
              else
                1 +i preserved_cycles in
          let updated := Cycle_repr.add current_cycle delay in
          Cycle_repr.max current_expiration updated
        end in
    (return=? expiration).
  
  Lemma expiration_aux_none (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) :
    Roll_storage.Delegate.is_inactive ctxt delegate = Return (Pervasives.Ok true) ->
    delegate_desactivation ctxt delegate = Return (Pervasives.Ok None) ->
    let current_cycle := (Raw_context.current_level ctxt).(Level_repr.t.cycle) in
    let preserved_cycles := Constants_storage.preserved_cycles ctxt in
    let c := Cycle_repr.add current_cycle (1 +i (2 *i preserved_cycles)) in
    expiration_aux ctxt delegate = (return=? c).
  
  Proof.
    unfold expiration_aux.
    destruct (Roll_storage.Delegate.is_inactive ctxt delegate); simpl.
    destruct (delegate_desactivation ctxt delegate); simpl;
      try easy.
    destruct t; simpl; try easy.
    destruct t0; simpl; try easy.
    destruct o; simpl; try easy.
  Qed.
  
  Lemma expiration_aux_some (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (current_exp : Cycle_repr.t) :
    delegate_desactivation ctxt delegate = Return (Pervasives.Ok (Some current_exp)) ->
    Roll_storage.Delegate.is_inactive ctxt delegate = Return (Pervasives.Ok true) ->
    let current_cycle := (Raw_context.current_level ctxt).(Level_repr.t.cycle) in
    let preserved_cycles := Constants_storage.preserved_cycles ctxt in
    let delay := 1 +i (2 *i preserved_cycles) in
    let updated := Cycle_repr.add current_cycle delay in
    let c := Cycle_repr.max current_exp updated in
    expiration_aux ctxt delegate = (return=? c).

  Proof.
    unfold expiration_aux.
    destruct (delegate_desactivation ctxt delegate); try easy; simpl.
    destruct t; simpl; try easy.
    destruct o; simpl; try easy.
    destruct (Roll_storage.Delegate.is_inactive ctxt delegate);
      simpl; try easy.
    destruct t0; simpl; try easy.
    destruct b; simpl; try easy.
    intros H_eq_1 H_eq_2.
    inversion H_eq_1.
    try easy.
  Qed.

  Definition delegate_desactivation_init_set (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash)
             value :=
    Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.init_set)
                                              ctxt (Contract_repr.implicit_contract delegate)
                                              value.
  
  Definition inactive_delegate_del (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) :=
    Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.del)
                                             ctxt (Contract_repr.implicit_contract delegate).
  
  Definition set_active
             (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
    : M=? Raw_context.t :=
    
    let=? inactive := Roll_storage.Delegate.is_inactive ctxt delegate in
    let=? expiration := expiration_aux ctxt delegate in
    let= ctxt := delegate_desactivation_init_set ctxt delegate expiration in
    if Pervasives.not inactive then
      return=? ctxt
    else
      let=? ctxt := Roll_storage.Delegate.ensure_inited ctxt delegate in
      let tokens_per_roll := Constants_storage.tokens_per_roll ctxt in
      let=? change := get_delegate_change ctxt delegate in
      let= ctxt := inactive_delegate_del ctxt delegate in
      let=? delegate_pk := Roll_storage.delegate_pubkey ctxt delegate
      in
      let=? '(ctxt, _) := set_active_aux ctxt delegate delegate_pk tokens_per_roll change in
      let=? rolls := Redefine_functions.delegate_roll_list ctxt delegate in
      match rolls with
      | None => (return=? ctxt)
      | Some _ => active_delegate_with_rolls ctxt delegate
      end.
  
End Delegate_redefine.

(*** Module Delegate  **)

Module Delegate.

  (*** fresh_roll and get_limbo_roll *)
 
  Definition limbo_none (ctxt : Raw_context.t) :=
    (Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.get_option) ctxt = return=? None).
  
  Definition fresh_roll_limbo (ctxt : Raw_context.t) :=
    let=? '(roll, ctxt) := Roll_storage.Delegate.fresh_roll ctxt in
    let=? ctxt :=
       Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.init_value)
                            ctxt roll
    in
  return=? (roll, ctxt).
             
  Lemma fresh_limbo_roll (ctxt : Raw_context.t) :
    limbo_none ctxt ->
    Error_monad.terminates_lwt (fresh_roll_limbo ctxt) =
    Error_monad.terminates_lwt (Roll_storage.Delegate.get_limbo_roll ctxt).

  Proof.
    unfold limbo_none.
    unfold fresh_roll_limbo.
    unfold Roll_storage.Delegate.fresh_roll.
    unfold Roll_storage.Delegate.get_limbo_roll.
    unfold Roll_storage.Delegate.fresh_roll.
    destruct (Storage.Roll.Limbo.(Storage_sigs.Single_data_storage.get_option) ctxt);
    try now (intro H; inversion H).
  Qed.

  (*** ensure_inited *)
  
  Definition is_delegate_change (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) :=
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.mem)
                                   ctxt delegate.

  Lemma ensure_inited_not_mem (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) :
    is_delegate_change ctxt delegate = (return= false) ->
    Roll_storage.Delegate.ensure_inited ctxt delegate =
    Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.init_value)
                                   ctxt delegate Tez_repr.zero.

  Proof.
    unfold is_delegate_change.
    unfold Roll_storage.Delegate.ensure_inited.
    destruct (Storage.Roll.Delegate_change.(Storage_sigs.Indexed_data_storage.mem)
                                             ctxt delegate); try Utils.impossible_branch.
  Qed.
  
  (*** is_inactive *)
    
  Definition inactive_delegate (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) :=
    Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.mem)
                                         ctxt (Contract_repr.implicit_contract delegate).
  
  Lemma is_inactive_none (ctxt : Raw_context.t) (delegate : Signature.public_key_hash) :
    inactive_delegate ctxt delegate = (return= false) ->
    Delegate_redefine.delegate_desactivation ctxt delegate = (return=? None) ->
    (Roll_storage.Delegate.is_inactive ctxt delegate = return=? false).
  
  Proof.
    unfold inactive_delegate.
    unfold Delegate_redefine.delegate_desactivation.
    unfold Roll_storage.Delegate.is_inactive.
    
    destruct (Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.mem)
                                                   ctxt
                                                   (Contract_repr.implicit_contract delegate))
      as [inactive]; try Utils.impossible_branch.
    destruct (Storage.Contract.Delegate_desactivation
              .(Storage_sigs.Indexed_data_storage.get_option)
                 ctxt
                 (Contract_repr.implicit_contract delegate)) as [t];
      try Utils.impossible_branch.
    
    destruct inactive; simpl; try Utils.impossible_branch.
    destruct t; simpl; try easy. 
    destruct o; simpl; try easy.
  Qed.
  
  Definition some_delegate_desactivation (ctxt : Raw_context.t)
             (delegate : Signature.public_key_hash) (last_active_cycle : Cycle_repr.t) : M=? bool :=
    let _ :=   Delegate_redefine.delegate_desactivation ctxt delegate  = return=? (Some last_active_cycle) in
    let '{| Level_repr.t.cycle := current_cycle |} :=
        Raw_context.current_level ctxt
    in
  (return=? Cycle_repr.op_lt last_active_cycle current_cycle).
  
  Lemma is_inactive_some (ctxt : Raw_context.t)
        (delegate : Signature.public_key_hash)
        (last_active_cycle : Cycle_repr.t) :
    Delegate_redefine.delegate_desactivation ctxt delegate  =
    Return (Pervasives.Ok (Some last_active_cycle)) ->
    some_delegate_desactivation ctxt delegate last_active_cycle = Return (Pervasives.Ok false) ->
    inactive_delegate ctxt delegate = (return= false) ->
    (Roll_storage.Delegate.is_inactive ctxt delegate = return=? false).
  
  Proof.
    unfold inactive_delegate. 
    unfold some_delegate_desactivation.
    intros.

    unfold Roll_storage.Delegate.is_inactive.
    revert H0.
    destruct (Storage.Contract.Inactive_delegate.(Storage_sigs.Data_set_storage.mem)
                                                   ctxt
                                                   (Contract_repr.implicit_contract delegate));
      try easy.

    revert H.
    unfold Delegate_redefine.delegate_desactivation.   

    destruct (Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.get_option)
                                                        ctxt
                                                        (Contract_repr.implicit_contract delegate));
      simpl.
    
    destruct b; simpl; try easy; try Utils.impossible_branch. 
  Qed.


End Delegate.


(*** snapshot_rolls, 
     snapshot_rolls_for_cycle **)

Lemma snapshot_rolls_snapshot_rolls_for_cycle (ctxt : Raw_context.t) (cycle : Cycle_repr.t) :
  let ctxt' := Roll_storage.snapshot_rolls_for_cycle ctxt cycle in
  let ctxt'' := Roll_storage.snapshot_rolls ctxt in
  cycle =
  (Cycle_repr.add
     (Raw_context.current_level ctxt).(Level_repr.t.cycle)
                                        (Constants_storage.preserved_cycles ctxt +i 2)) ->
  Error_monad.terminates_lwt ctxt' -> Error_monad.terminates_lwt ctxt'' ->
  ctxt' = ctxt''.

Proof.
  unfold Roll_storage.snapshot_rolls; simpl.
  intros. rewrite <- H. auto. 
Qed.


(*** cycle_end, clear_cycle *)

Definition cycle_end_simpl (ctxt : Raw_context.t) (last_cycle : Cycle_repr.cycle) :=
  let preserved := Constants_storage.preserved_cycles ctxt in
  let=? ctxt :=
    match Cycle_repr.sub last_cycle preserved with
    | None => return=? ctxt
    | Some cleared_cycle => Roll_storage.clear_cycle ctxt cleared_cycle
    end
  in
  return=? ctxt.
          
Lemma cycle_end_clear_cycle (ctxt : Raw_context.t) (last_cycle : Cycle_repr.cycle) :
  let ctxt' := cycle_end_simpl ctxt last_cycle in
  let preserved := Constants_storage.preserved_cycles ctxt in
  forall cleared_cycle, Cycle_repr.sub last_cycle preserved = Some cleared_cycle ->
                   let ctxt'' := Roll_storage.clear_cycle ctxt cleared_cycle in
  Error_monad.terminates_lwt ctxt' -> Error_monad.terminates_lwt ctxt'' ->
  ctxt' = ctxt''.
Proof.
  unfold cycle_end_simpl; simpl.
  intros c H. rewrite H.
  destruct (Cycle_repr.sub last_cycle (Constants_storage.preserved_cycles ctxt)) in H;
    try (inversion H).
  rewrite Error_monad.bind_return_lwt. auto.
Qed.

Lemma cycle_end (ctxt : Raw_context.t)
      (cleared_cycle last_cycle : Cycle_repr.cycle) :
  let preserved := Constants_storage.preserved_cycles ctxt in
  (Cycle_repr.sub last_cycle preserved = Some cleared_cycle) ->
  Error_monad.terminates_lwt (
      let=? ' ctxt0 := Roll_storage.clear_cycle ctxt cleared_cycle in
      let=? ' ctxt1 := Roll_storage.freeze_rolls_for_cycle
                         ctxt0
                         (Cycle_repr.add last_cycle (preserved +i 1))
      in
      Storage.Roll.Snapshot_for_cycle.(Storage_sigs.Indexed_data_storage.init_value)
                                        ctxt1
                                        (Cycle_repr.succ
                                           (Cycle_repr.succ
                                              (Cycle_repr.add last_cycle
                                                              (preserved +i 1))))
                                        0)
  = Error_monad.terminates_lwt (Roll_storage.cycle_end ctxt last_cycle).

Proof.
  unfold Roll_storage.cycle_end; simpl; try easy.
  intro H.
  rewrite H; try easy.
Qed.

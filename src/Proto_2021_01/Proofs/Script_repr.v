Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Script_repr.

Fixpoint with_minus_one (n : Script_repr.node) : bool :=
  match n with
  | Micheline.Int l _
  | Micheline.String l _
  | Micheline.Bytes l _ =>
    Z.eqb l (-1)
  | Micheline.Prim l _ ns _
  | Micheline.Seq l ns =>
    Z.eqb l (-1) &&
    List.for_all with_minus_one ns
  end.

Fixpoint strip_locations_on_minus_one n
  : with_minus_one n = true ->
    Micheline.strip_locations n = n.
  assert (H_aux : forall ns,
    List.for_all with_minus_one ns = true ->
    Micheline.map_strip_locations ns = ns
  ).
  { intros ns Hs.
    induction ns as [|n' ns' H_aux]; simpl in *; trivial.
    rewrite Bool.andb_true_iff in Hs; destruct Hs.
    rewrite strip_locations_on_minus_one; trivial.
    now rewrite H_aux.
  }
  destruct n; simpl; intro H;
    try (now rewrite (proj1 (Z.eqb_eq _ _) H));
    rewrite Bool.andb_true_iff in H; destruct H as [H Hs];
    rewrite H_aux; trivial;
    now rewrite (proj1 (Z.eqb_eq _ _) H).
Qed.

Fixpoint canonical_expand_pair (ns : list Script_repr.node)
  : list Script_repr.node :=
  match ns with
  | [] => ns
  | n1 :: ns' =>
    match ns' with
    | _ :: _ :: _ =>
      let ns' := canonical_expand_pair ns' in
      [n1; Micheline.Prim (-1) Michelson_v1_primitives.T_pair ns' []]
    | _ => ns
    end
  end.

Notation "'canonicals" :=
  (fun canonical =>
  fix canonicals (ns : list Script_repr.node) : list Script_repr.node :=
  match ns with
  | [] => []
  | n :: ns => canonical n :: canonicals ns
  end).

Fixpoint canonical (n : Script_repr.node) : Script_repr.node :=
  match n with
  | Micheline.Int _ z => Micheline.Int (-1) z
  | Micheline.String _ s => Micheline.String (-1) s
  | Micheline.Bytes _ b => Micheline.Bytes (-1) b
  | Micheline.Prim _ p ns _ =>
    let ns := 'canonicals canonical ns in
    let ns_with_pairs :=
      match p with
      | Michelson_v1_primitives.T_pair => canonical_expand_pair ns
      | _ => ns
      end in
    Micheline.Prim (-1) p ns_with_pairs []
  | Micheline.Seq _ ns => Micheline.Seq (-1) ('canonicals canonical ns)
  end.

Definition canonicals : list Script_repr.node -> list Script_repr.node :=
  'canonicals canonical.

Definition canonical_eq (n_a n_b : Script_repr.node) : Prop :=
  canonical n_a = canonical n_b.

Definition default_location : Alpha_context.Script.location := -1.

Reserved Notation "'remove_annots_list".

Fixpoint remove_annots (n : Script_repr.node) : Script_repr.node :=
  match n with
  | Micheline.Int _ z => Micheline.Int default_location z
  | Micheline.String _ s => Micheline.String default_location s
  | Micheline.Bytes _ b => Micheline.Bytes default_location b
  | Micheline.Prim _ p ns _ =>
    Micheline.Prim default_location p ('remove_annots_list ns) []
  | Micheline.Seq _ ns => Micheline.Seq default_location ('remove_annots_list ns)
  end

where "'remove_annots_list" := (
  fix remove_annots_list (ns : list Script_repr.node) : list Script_repr.node :=
    match ns with
    | [] => ns
    | n :: ns => remove_annots n :: remove_annots_list ns
    end
).

Definition remove_annots_list := 'remove_annots_list.

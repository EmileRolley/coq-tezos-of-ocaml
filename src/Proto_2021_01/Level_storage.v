Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Constants_storage.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Raw_level_repr.

Definition from_raw
  (c : Raw_context.context) (offset : option int32)
  (l_value : Raw_level_repr.raw_level) : Level_repr.level :=
  let l_value :=
    match offset with
    | None => l_value
    | Some o =>
      Raw_level_repr.of_int32_exn ((Raw_level_repr.to_int32 l_value) +i32 o)
    end in
  let constants := Raw_context.constants c in
  let first_level := Raw_context.first_level c in
  Level_repr.level_from_raw first_level
    constants.(Constants_repr.parametric.blocks_per_cycle)
    constants.(Constants_repr.parametric.blocks_per_commitment) l_value.

Definition root (c : Raw_context.context) : Level_repr.level :=
  Level_repr.root_level (Raw_context.first_level c).

Definition succ (c : Raw_context.context) (l_value : Level_repr.t)
  : Level_repr.level :=
  from_raw c None (Raw_level_repr.succ l_value.(Level_repr.t.level)).

Definition pred (c : Raw_context.context) (l_value : Level_repr.t)
  : option Level_repr.level :=
  match Raw_level_repr.pred l_value.(Level_repr.t.level) with
  | None => None
  | Some l_value => Some (from_raw c None l_value)
  end.

Definition current (ctxt : Raw_context.context) : Level_repr.t :=
  Raw_context.current_level ctxt.

Definition previous (ctxt : Raw_context.context) : Level_repr.level :=
  let l_value := current ctxt in
  match pred ctxt l_value with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert Level_repr.level false
  | Some p_value => p_value
  end.

Definition first_level_in_cycle
  (ctxt : Raw_context.context) (c : Cycle_repr.cycle) : Level_repr.level :=
  let constants := Raw_context.constants ctxt in
  let first_level := Raw_context.first_level ctxt in
  from_raw ctxt None
    (Raw_level_repr.of_int32_exn
      ((Raw_level_repr.to_int32 first_level) +i32
      (constants.(Constants_repr.parametric.blocks_per_cycle) *i32
      (Cycle_repr.to_int32 c)))).

Definition last_level_in_cycle
  (ctxt : Raw_context.context) (c : Cycle_repr.cycle) : Level_repr.level :=
  match pred ctxt (first_level_in_cycle ctxt (Cycle_repr.succ c)) with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert Level_repr.level false
  | Some x => x
  end.

Fixpoint iterate {A : Set}
  (ctxt : Raw_context.context) (first : Level_repr.t) (n : Level_repr.t)
  (acc_value : A) (f : Level_repr.t -> A -> A) {struct n} : A :=
  if Cycle_repr.op_eq n.(Level_repr.t.cycle) first.(Level_repr.t.cycle) then
    iterate ctxt first (succ ctxt n) (f n acc_value) f
  else
    acc_value.

Definition iterate_in_first {A : Set}
  (ctxt : Raw_context.context) (cycle : Cycle_repr.cycle) (acc_value : A)
  (f : Level_repr.t -> A -> A) : A :=
  let first := first_level_in_cycle ctxt cycle in
  iterate ctxt first first acc_value f.

Definition levels_in_cycle
  (ctxt : Raw_context.context) (cycle : Cycle_repr.cycle) : list Level_repr.t :=
  iterate_in_first ctxt cycle nil
    (fun (n : Level_repr.t) =>
      fun (acc_value : list Level_repr.t) => cons n acc_value).

Definition levels_in_current_cycle
  (ctxt : Raw_context.context) (op_staroptstar : option int32)
  : unit -> list Level_repr.t :=
  let offset :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => 0
    end in
  fun (function_parameter : unit) =>
    let '_ := function_parameter in
    let current_cycle := Cycle_repr.to_int32 (current ctxt).(Level_repr.t.cycle)
      in
    let cycle := current_cycle +i32 offset in
    if cycle <i32 0 then
      nil
    else
      let cycle := Cycle_repr.of_int32_exn cycle in
      levels_in_cycle ctxt cycle.

Definition levels_with_commitments_in_cycle
  (ctxt : Raw_context.context) (c : Cycle_repr.cycle) : list Level_repr.t :=
  iterate_in_first ctxt c nil
    (fun (n : Level_repr.t) =>
      fun (acc_value : list Level_repr.t) =>
        if n.(Level_repr.t.expected_commitment) then
          cons n acc_value
        else
          acc_value).

Definition last_allowed_fork_level (c : Raw_context.context)
  : Raw_level_repr.raw_level :=
  let level := Raw_context.current_level c in
  let preserved_cycles := Constants_storage.preserved_cycles c in
  match Cycle_repr.sub level.(Level_repr.t.cycle) preserved_cycles with
  | None => Raw_level_repr.root
  | Some cycle => (first_level_in_cycle c cycle).(Level_repr.t.level)
  end.

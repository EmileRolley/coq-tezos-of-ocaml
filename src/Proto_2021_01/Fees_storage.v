Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Constants_storage.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Contract_storage.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "contract.cannot_pay_storage_fee" "Cannot pay storage fee"
      "The storage fee is higher than the contract balance"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Cannot pay storage storage fee"
                  CamlinternalFormatBasics.End_of_format)
                "Cannot pay storage storage fee"))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Cannot_pay_storage_fee" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Cannot_pay_storage_fee" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "storage_exhausted.operation" "Storage quota exceeded for the operation"
      "A script or one of its callee wrote more bytes than the operation said it would"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Operation_quota_exceeded" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Operation_quota_exceeded" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent "storage_limit_too_high"
    "Storage limit out of protocol hard bounds"
    "A transaction tried to exceed the hard limit on storage" None
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Storage_limit_too_high" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Storage_limit_too_high" unit tt).

Definition origination_burn (c : Raw_context.context)
  : M? (Raw_context.t * Tez_repr.t) :=
  let origination_size := Constants_storage.origination_size c in
  let cost_per_byte := Constants_storage.cost_per_byte c in
  let? to_be_paid :=
    Tez_repr.op_starquestion cost_per_byte (Int64.of_int origination_size) in
  return? ((Raw_context.update_allocated_contracts_count c), to_be_paid).

Definition record_paid_storage_space
  (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? (Raw_context.t * Z.t * Z.t * Tez_repr.t) :=
  let=? size := Contract_storage.used_storage_space c contract in
  let=? '(to_be_paid, c) :=
    Contract_storage.set_paid_storage_space_and_return_fees_to_pay c contract
      size in
  let c := Raw_context.update_storage_space_to_pay c to_be_paid in
  let cost_per_byte := Constants_storage.cost_per_byte c in
  Lwt._return
    (let? to_burn :=
      Tez_repr.op_starquestion cost_per_byte (Z.to_int64 to_be_paid) in
    return? (c, size, to_be_paid, to_burn)).

Definition burn_storage_fees
  (c : Raw_context.context) (storage_limit : Z.t) (payer : Contract_repr.t)
  : M=? Raw_context.t :=
  let origination_size := Constants_storage.origination_size c in
  let '(c, storage_space_to_pay, allocated_contracts) :=
    Raw_context.clear_storage_space_to_pay c in
  let storage_space_for_allocated_contracts :=
    (Z.of_int allocated_contracts) *Z (Z.of_int origination_size) in
  let consumed := storage_space_to_pay +Z storage_space_for_allocated_contracts
    in
  let remaining := storage_limit -Z consumed in
  if remaining <Z Z.zero then
    Error_monad.fail (Build_extensible "Operation_quota_exceeded" unit tt)
  else
    let cost_per_byte := Constants_storage.cost_per_byte c in
    let=? to_burn :=
      return= (Tez_repr.op_starquestion cost_per_byte (Z.to_int64 consumed)) in
    if Tez_repr.op_eq to_burn Tez_repr.zero then
      Error_monad._return c
    else
      Error_monad.trace_value
        (Build_extensible "Cannot_pay_storage_fee" unit tt)
        (let=? '_ := Contract_storage.must_exist c payer in
        Contract_storage.spend c payer to_burn).

Definition check_storage_limit (c : Raw_context.context) (storage_limit : Z.t)
  : M? unit :=
  if
    (storage_limit >Z
    (Raw_context.constants c).(Constants_repr.parametric.hard_storage_limit_per_operation))
    || (storage_limit <Z Z.zero)
  then
    Error_monad.error_value (Build_extensible "Storage_limit_too_high" unit tt)
  else
    Error_monad.ok_unit.

Definition start_counting_storage_fees (c : Raw_context.t) : Raw_context.t :=
  Raw_context.init_storage_space_to_pay c.

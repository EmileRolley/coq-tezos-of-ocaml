Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Block_header_repr.
Require TezosOfOCaml.Proto_2021_01.Bootstrap_storage.
Require TezosOfOCaml.Proto_2021_01.Commitment_repr.
Require TezosOfOCaml.Proto_2021_01.Commitment_storage.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Constants_storage.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Contract_storage.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Delegate_storage.
Require TezosOfOCaml.Proto_2021_01.Fees_storage.
Require TezosOfOCaml.Proto_2021_01.Fitness_repr.
Require TezosOfOCaml.Proto_2021_01.Fitness_storage.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Init_storage.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_diff.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Level_storage.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Nonce_storage.
Require TezosOfOCaml.Proto_2021_01.Operation_repr.
Require TezosOfOCaml.Proto_2021_01.Parameters_repr.
Require TezosOfOCaml.Proto_2021_01.Period_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Raw_level_repr.
Require TezosOfOCaml.Proto_2021_01.Receipt_repr.
Require TezosOfOCaml.Proto_2021_01.Roll_repr.
Require TezosOfOCaml.Proto_2021_01.Roll_storage.
Require TezosOfOCaml.Proto_2021_01.Sapling_repr.
Require TezosOfOCaml.Proto_2021_01.Sapling_storage.
Require TezosOfOCaml.Proto_2021_01.Sapling_validator.
Require TezosOfOCaml.Proto_2021_01.Script_expr_hash.
Require TezosOfOCaml.Proto_2021_01.Script_int_repr.
Require TezosOfOCaml.Proto_2021_01.Script_repr.
Require TezosOfOCaml.Proto_2021_01.Script_timestamp_repr.
Require TezosOfOCaml.Proto_2021_01.Seed_repr.
Require TezosOfOCaml.Proto_2021_01.Seed_storage.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_description.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.
Require TezosOfOCaml.Proto_2021_01.Time_repr.
Require TezosOfOCaml.Proto_2021_01.Vote_repr.
Require TezosOfOCaml.Proto_2021_01.Vote_storage.
Require TezosOfOCaml.Proto_2021_01.Voting_period_repr.
Require TezosOfOCaml.Proto_2021_01.Voting_period_storage.

Definition t : Set := Raw_context.t.

Definition context : Set := t.

Module BASIC_DATA.
  Record signature {t : Set} : Set := {
    t := t;
    op_eq : t -> t -> bool;
    op_ltgt : t -> t -> bool;
    op_lt : t -> t -> bool;
    op_lteq : t -> t -> bool;
    op_gteq : t -> t -> bool;
    op_gt : t -> t -> bool;
    compare : t -> t -> int;
    equal : t -> t -> bool;
    max : t -> t -> t;
    min : t -> t -> t;
    encoding : Data_encoding.t t;
    pp : Format.formatter -> t -> unit;
  }.
End BASIC_DATA.
Definition BASIC_DATA := @BASIC_DATA.signature.
Arguments BASIC_DATA {_}.

Module Tez := Tez_repr.

Module Period := Period_repr.

Module Timestamp.
  Include Time_repr.
  
  Definition current : Raw_context.context -> Time.t :=
    Raw_context.current_timestamp.
End Timestamp.

Include Operation_repr.

Module Operation.
  Definition t : Set := operation.
  
  Definition packed : Set := packed_operation.
  
  Definition unsigned_encoding
    : Data_encoding.t (Operation.shell_header * packed_contents_list) :=
    unsigned_operation_encoding.
  
  Include Operation_repr.
End Operation.

Module Block_header := Block_header_repr.

Module Vote.
  Include Vote_repr.
  
  Include Vote_storage.
End Vote.

Module Raw_level := Raw_level_repr.

Module Cycle := Cycle_repr.

Module Script_int := Script_int_repr.

Module Script_timestamp.
  Include Script_timestamp_repr.
  
  Definition now (ctxt : Raw_context.context) : t :=
    let '{|
      Constants_repr.parametric.time_between_blocks := time_between_blocks
        |} := Raw_context.constants ctxt in
    match time_between_blocks with
    | [] =>
      Pervasives.failwith
        "Internal error: 'time_between_block' constants is an empty list."
    | cons first_delay _ =>
      let current_timestamp := Raw_context.predecessor_timestamp ctxt in
      of_int64
        (Timestamp.to_seconds
          (Time.add current_timestamp (Period_repr.to_seconds first_delay)))
    end.
End Script_timestamp.

Module Script.
  Include Michelson_v1_primitives.
  
  Include Script_repr.
  
  Definition force_decode_in_context
    (ctxt : Raw_context.context) (lexpr : Script_repr.lazy_expr)
    : M? (Script_repr.expr * Raw_context.context) :=
    let? '(v, cost) := Script_repr.force_decode lexpr in
    let? ctxt := Raw_context.consume_gas ctxt cost in
    return? (v, ctxt).
  
  Definition force_bytes_in_context
    (ctxt : Raw_context.context) (lexpr : Script_repr.lazy_expr)
    : M? (bytes * Raw_context.context) :=
    let? '(b_value, cost) := Script_repr.force_bytes lexpr in
    let? ctxt := Raw_context.consume_gas ctxt cost in
    return? (b_value, ctxt).
End Script.

Module Fees := Fees_storage.

Definition public_key : Set := Signature.public_key.

Definition public_key_hash : Set := Signature.public_key_hash.

Definition signature : Set := Signature.t.

Module Constants.
  Include Constants_repr.
  
  Include Constants_storage.
End Constants.

Module Voting_period.
  Include Voting_period_repr.
  
  Include Voting_period_storage.
End Voting_period.

Module Gas.
  Include Gas_limit_repr.
  
  Definition check_limit : Raw_context.t -> Gas_limit_repr.Arith.t -> M? unit :=
    Raw_context.check_gas_limit.
  
  Definition set_limit
    : Raw_context.t -> Gas_limit_repr.Arith.t -> Raw_context.t :=
    Raw_context.set_gas_limit.
  
  Definition set_unlimited : Raw_context.t -> Raw_context.t :=
    Raw_context.set_gas_unlimited.
  
  Definition consume
    : Raw_context.context -> Gas_limit_repr.cost -> M? Raw_context.context :=
    Raw_context.consume_gas.
  
  Definition check_enough
    : Raw_context.context -> Gas_limit_repr.cost -> M? unit :=
    Raw_context.check_enough_gas.
  
  Definition level : Raw_context.t -> Gas_limit_repr.t := Raw_context.gas_level.
  
  Definition consumed
    : Raw_context.t -> Raw_context.t -> Gas_limit_repr.Arith.fp :=
    Raw_context.gas_consumed.
  
  Definition block_level : Raw_context.t -> Gas_limit_repr.Arith.fp :=
    Raw_context.block_gas_level.
  
  Definition cost_of_repr {A : Set} (cost : A) : A := cost.
End Gas.

Module Level.
  Include Level_repr.
  
  Include Level_storage.
End Level.

Module Lazy_storage.
  Module Kind := Lazy_storage_kind.
  
  Module IdSet := Kind.IdSet.
  
  Include Lazy_storage_diff.
  
  Definition legacy_big_map_diff_encoding
    : Data_encoding.encoding Lazy_storage_diff.diffs :=
    Data_encoding.conv Contract_storage.Legacy_big_map_diff.of_lazy_storage_diff
      Contract_storage.Legacy_big_map_diff.to_lazy_storage_diff None
      Contract_storage.Legacy_big_map_diff.encoding.
End Lazy_storage.

Module Contract.
  Include Contract_repr.
  
  Include Contract_storage.
  
  Definition originate
    (c : Raw_context.t) (contract : Contract_repr.t) (balance : Tez_repr.t)
    (script : Script_repr.t * option Lazy_storage_diff.diffs)
    (delegate : option Signature.public_key_hash) : M=? Raw_context.t :=
    raw_originate c None contract balance script delegate.
  
  Definition init_origination_nonce
    : Raw_context.t -> Operation_hash.t -> Raw_context.t :=
    Raw_context.init_origination_nonce.
  
  Definition unset_origination_nonce : Raw_context.t -> Raw_context.t :=
    Raw_context.unset_origination_nonce.
End Contract.

Module Big_map.
  Include Lazy_storage_kind.Big_map.
  
  Definition fresh (temporary : bool) (c : Raw_context.t)
    : M=?
      (Raw_context.t *
        Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
    Lazy_storage.fresh Lazy_storage_kind.Big_map temporary c.
  
  Definition mem
    (c : Raw_context.t) (m : Storage.Big_map.id) (k : Script_expr_hash.t)
    : M=? (Raw_context.t * bool) :=
    Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem)
      (c, m) k.
  
  Definition get_opt
    (c : Raw_context.t) (m : Storage.Big_map.id) (k : Script_expr_hash.t)
    : M=? (Raw_context.t * option Script_repr.expr) :=
    Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option)
      (c, m) k.
  
  Definition _exists
    (c : Raw_context.context)
    (id : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    : M=? (Raw_context.context * option (Script_repr.expr * Script_repr.expr)) :=
    let=? c :=
      return=
        (Raw_context.consume_gas c (Gas_limit_repr.read_bytes_cost Z.zero)) in
    let=? kt :=
      Storage.Big_map.Key_type.(Storage_sigs.Indexed_data_storage.get_option) c
        id in
    match kt with
    | None => Error_monad._return (c, None)
    | Some kt =>
      let=? kv :=
        Storage.Big_map.Value_type.(Storage_sigs.Indexed_data_storage.get) c id
        in
      return=? (c, (Some (kt, kv)))
    end.
End Big_map.

Module Sapling.
  Include Lazy_storage_kind.Sapling_state.
  
  Include Sapling_repr.
  
  Include Sapling_storage.
  
  Include Sapling_validator.
  
  Definition fresh (temporary : bool) (c : Raw_context.t)
    : M=?
      (Raw_context.t *
        Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.IdWithTemp.t)) :=
    Lazy_storage.fresh Lazy_storage_kind.Sapling_state temporary c.
End Sapling.

Module Receipt := Receipt_repr.

Module Delegate := Delegate_storage.

Module Roll.
  Include Roll_repr.
  
  Include Roll_storage.
End Roll.

Module Nonce := Nonce_storage.

Module Seed.
  Include Seed_repr.
  
  Include Seed_storage.
End Seed.

Module Fitness.
  Include Fitness_repr.
  
  Include Environment.Fitness.
  
  Definition fitness : Set := t.
  
  Include Fitness_storage.
End Fitness.

Module Bootstrap := Bootstrap_storage.

Module Commitment.
  Include Commitment_repr.
  
  Include Commitment_storage.
End Commitment.

Module Global.
  Definition get_block_priority : Raw_context.t -> M=? int :=
    Storage.Block_priority.(Storage.GET_SET_INIT.get).
  
  Definition set_block_priority : Raw_context.t -> int -> M=? Raw_context.t :=
    Storage.Block_priority.(Storage.GET_SET_INIT.set).
End Global.

Definition prepare_first_block
  : Context.t ->
  (Raw_context.t -> Script_repr.t ->
  M=? ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t)) ->
  int32 -> Time.t -> Fitness.t -> M=? Raw_context.t :=
  Init_storage.prepare_first_block.

Definition prepare
  : Context.t -> Int32.t -> Time.t -> Time.t -> Fitness.t ->
  M=? (Raw_context.context * Receipt_repr.balance_updates) :=
  Init_storage.prepare.

Definition finalize (message : option string) (c : Raw_context.context)
  : Updater.validation_result :=
  let fitness := Fitness.from_int64 (Fitness.current c) in
  let context_value := Raw_context.recover c in
  {| Updater.validation_result.context := context_value;
    Updater.validation_result.fitness := fitness;
    Updater.validation_result.message := message;
    Updater.validation_result.max_operations_ttl := 60;
    Updater.validation_result.last_allowed_fork_level :=
      Raw_level.to_int32 (Level.last_allowed_fork_level c) |}.

Definition activate
  : Raw_context.context -> Protocol_hash.t -> M= Raw_context.t :=
  Raw_context.activate.

Definition fork_test_chain
  : Raw_context.context -> Protocol_hash.t -> Time.t -> M= Raw_context.t :=
  Raw_context.fork_test_chain.

Definition record_endorsement
  : Raw_context.context -> Signature.public_key_hash -> Raw_context.context :=
  Raw_context.record_endorsement.

Definition allowed_endorsements
  : Raw_context.context ->
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    (Signature.public_key * list int * bool) :=
  Raw_context.allowed_endorsements.

Definition init_endorsements
  : Raw_context.context ->
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    (Signature.public_key * list int * bool) -> Raw_context.context :=
  Raw_context.init_endorsements.

Definition included_endorsements : Raw_context.context -> int :=
  Raw_context.included_endorsements.

Definition reset_internal_nonce : Raw_context.context -> Raw_context.context :=
  Raw_context.reset_internal_nonce.

Definition fresh_internal_nonce
  : Raw_context.context -> M? (Raw_context.context * int) :=
  Raw_context.fresh_internal_nonce.

Definition record_internal_nonce
  : Raw_context.context -> int -> Raw_context.context :=
  Raw_context.record_internal_nonce.

Definition internal_nonce_already_recorded
  : Raw_context.context -> int -> bool :=
  Raw_context.internal_nonce_already_recorded.

Definition add_deposit
  : Raw_context.context -> Signature.public_key_hash -> Tez_repr.t ->
  M? Raw_context.context := Raw_context.add_deposit.

Definition add_fees
  : Raw_context.context -> Tez_repr.t -> M? Raw_context.context :=
  Raw_context.add_fees.

Definition add_rewards
  : Raw_context.context -> Tez_repr.t -> M? Raw_context.context :=
  Raw_context.add_rewards.

Definition get_deposits
  : Raw_context.context ->
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    Tez_repr.t := Raw_context.get_deposits.

Definition get_fees : Raw_context.context -> Tez_repr.t := Raw_context.get_fees.

Definition get_rewards : Raw_context.context -> Tez_repr.t :=
  Raw_context.get_rewards.

Definition description : Storage_description.t Raw_context.context :=
  Raw_context.description.

Module Parameters := Parameters_repr.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

(** The Lwt monad *)
Inductive M (a : Set) : Set :=
| Return : a -> M a.
Arguments Return {_}.

Definition t : Set -> Set := M.

Definition _return {a : Set} (x : a) : t a :=
  Return x.

Definition bind {a b : Set} (x : t a) (f : a -> t b) : t b :=
  match x with
  | Return v => f v
  end.

Module Notations.
  Notation "M= X" := (t X) (at level 20).

  Notation "return= X" := (_return X) (at level 20).

  Notation "'let=' x ':=' X 'in' Y" :=
    (bind X (fun x => Y))
    (at level 200, x name, X at level 100, Y at level 200).

  Notation "'let=' ' x ':=' X 'in' Y" :=
    (bind X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).
End Notations.
Import Notations.

Definition map {a b : Set} (f : a -> b) (x : t a) : t b :=
  let= x := x in
  return= (f x).

Definition return_unit : t unit := return= tt.

Definition return_none {A : Set} : t (option A) :=
  return= None.

Definition return_nil {A : Set} : t (list A) :=
  return= [].

Definition return_true : t bool := return= true.

Definition return_false : t bool := return= false.

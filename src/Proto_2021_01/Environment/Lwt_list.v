Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Lwt.
Import Lwt.Notations.

Fixpoint iter_s {a : Set}
  (f : a -> Lwt.t unit) (l : list a) : Lwt.t unit :=
  match l with
  | [] => return= tt
  | x :: l =>
    let= _ := f x in
    iter_s f l
  end.

Parameter iteri_s : forall {a : Set},
  (int -> a -> Lwt.t unit) -> list a -> Lwt.t unit.

Fixpoint map_s {a b : Set}
  (f : a -> Lwt.t b) (l : list a) : Lwt.t (list b) :=
  match l with
  | [] => return= []
  | x :: l =>
    let= x := f x in
    let= l := map_s f l in
    return= (x :: l)
  end.

Parameter mapi_s : forall {a b : Set},
  (int -> a -> Lwt.t b) -> list a -> Lwt.t (list b).

Parameter rev_map_s : forall {a b : Set},
  (a -> Lwt.t b) -> list a -> Lwt.t (list b).

Fixpoint fold_left_s {a b : Set}
  (f : a -> b -> Lwt.t a) (accumulator : a) (l : list b) : Lwt.t a :=
  match l with
  | [] => return= accumulator
  | x :: l =>
    let= accumulator := f accumulator x in
    fold_left_s f accumulator l
  end.

Fixpoint fold_right_s {a b : Set}
  (f : a -> b -> Lwt.t b) (l : list a) (accumulator : b) : Lwt.t b :=
  match l with
  | [] => return= accumulator
  | x :: l =>
    let= accumulator := fold_right_s f l accumulator in
    f x accumulator
  end.

Fixpoint for_all_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t bool :=
  match l with
  | [] => return= true
  | x :: l =>
    let= b := f x in
    if b then
      for_all_s f l
    else
      return= false
  end.

Fixpoint exists_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t bool :=
  match l with
  | [] => return= false
  | x :: l =>
    let= b := f x in
    if b then
      return= true
    else
      exists_s f l
  end.

Parameter find_s : forall {a : Set}, (a -> Lwt.t bool) -> list a -> Lwt.t a.

Fixpoint filter_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t (list a) :=
  match l with
  | [] => return= []
  | x :: l =>
    let= b := f x in
    let= l := filter_s f l in
    if b then
      return= (x :: l)
    else
      return= l
  end.

Fixpoint filter_map_s {a b : Set}
  (f : a -> Lwt.t (option b)) (l : list a) : Lwt.t (list b) :=
  match l with
  | [] => return= []
  | x :: l =>
    let= y := f x in
    let= l' := filter_map_s f l in
    match y with
    | Some y => return= (y :: l')
    | None => return= l'
    end
  end.

Fixpoint partition_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t (list a * list a) :=
  match l with
  | [] => return= ([], [])
  | x :: l =>
    let= b := f x in
    let= '(yes, no) := partition_s f l in
    if b then
      return= (x :: yes, no)
    else
    return= (yes, x :: no)
  end.  

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Definition code : ascii -> int :=
  fun c =>
  Z.of_N (Ascii.N_of_ascii c).

Definition chr : int -> ascii :=
  fun z =>
  Ascii.ascii_of_N (Z.to_N z).

Parameter escaped : ascii -> string.

Parameter lowercase_ascii : ascii -> ascii.

Parameter uppercase_ascii : ascii -> ascii.

Definition t : Set := ascii.

Definition compare : t -> t -> int :=
  fun x y =>
  Z.sub (code x) (code y).

Definition equal : t -> t -> bool :=
  Ascii.eqb.

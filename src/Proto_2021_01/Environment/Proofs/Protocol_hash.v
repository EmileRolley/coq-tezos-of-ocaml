Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Import Environment.Notations.
Require TezosOfOCaml.Proto_2021_01.Environment.Protocol_hash.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Data_encoding.

Parameter encoding_is_complete :
  Data_encoding.Binary.is_complete Protocol_hash.encoding.

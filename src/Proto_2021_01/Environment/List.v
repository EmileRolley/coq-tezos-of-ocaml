Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Pervasives.

Definition t (a : Set) : Set := list a.

Definition length : forall {a : Set}, list a -> int :=
  fun _ =>
  List.length.

Definition compare_lengths : forall {a b : Set}, list a -> list b -> int :=
  fun _ _ l1 l2 =>
  Z.sub (length l1) (length l2).

Definition compare_length_with : forall {a : Set}, list a -> int -> int :=
  fun _ l n =>
  Z.sub (length l) n.

Definition cons_value : forall {a : Set}, a -> list a -> list a :=
  fun _ x l =>
  x :: l.

Definition hd : forall {a : Set}, list a -> a :=
  fun _ l =>
    match l with
    | [] => Pervasives.invalid_arg "empty list"
    | x :: _ => x
    end.

Definition tl : forall {a : Set}, list a -> list a :=
  fun _ l =>
    match l with
    | [] => Pervasives.invalid_arg "empty list"
    | _x :: m => m
    end. 

Fixpoint nth_nat {a : Set} (l : list a) (n : nat) : option a :=
  match l, n with
  | [], _ => None
  | x :: _, O => Some x
  | _ :: l', S n' => nth_nat l' n'
  end.

Definition nth_opt : forall {a : Set}, list a -> int -> option a :=
  fun _ l n =>
  nth_nat l (Z.to_nat n).

Definition rev : forall {a : Set}, list a -> list a :=
  fun _ =>
  List.rev.

Parameter init_value : forall {a : Set}, int -> (int -> a) -> list a.

Definition append : forall {a : Set}, list a -> list a -> list a :=
  fun _ =>
  List.append.

Definition rev_append : forall {a : Set}, list a -> list a -> list a :=
  fun _ =>
  List.rev_append.

Definition concat : forall {a : Set}, list (list a) -> list a :=
  fun _ =>
  List.concat.

Definition flatten : forall {a : Set}, list (list a) -> list a :=
  fun _ =>
  List.concat.

Parameter iter : forall {a : Set}, (a -> unit) -> list a -> unit.

Parameter iteri : forall {a : Set}, (int -> a -> unit) -> list a -> unit.

Definition map : forall {a b : Set}, (a -> b) -> list a -> list b :=
  fun _ _ =>
  List.map.

Definition mapi : forall {a b : Set}, (int -> a -> b) -> list a -> list b :=
  fun _ _ =>
  List.mapi.

Definition rev_map : forall {a b : Set}, (a -> b) -> list a -> list b :=
  fun _ _ =>
  List.rev_map.

Fixpoint filter_map {a b : Set} (f : a -> option b) (l : list a) : list b :=
  match l with
  | [] => []
  | x :: l' =>
    match f x with
    | None => filter_map f l'
    | Some y => y :: filter_map f l'
    end
  end.

Definition fold_left : forall {a b : Set}, (a -> b -> a) -> a -> list b -> a :=
  fun _ _ =>
  List.fold_left.

Definition fold_right : forall {a b : Set}, (a -> b -> b) -> list a -> b -> b :=
  fun _ _ =>
  List.fold_right.

Parameter iter2 : forall {a b : Set},
  (a -> b -> unit) -> list a -> list b -> unit.

Parameter map2 : forall {a b c : Set},
  (a -> b -> c) -> list a -> list b -> list c.

Parameter rev_map2 : forall {a b c : Set},
  (a -> b -> c) -> list a -> list b -> list c.

Parameter fold_left2 : forall {a b c : Set},
  (a -> b -> c -> a) -> a -> list b -> list c -> a.

Parameter fold_right2 : forall {a b c : Set},
  (a -> b -> c -> c) -> list a -> list b -> c -> c.

Definition for_all : forall {a : Set}, (a -> bool) -> list a -> bool :=
  fun a =>
  List.forallb (A := a).

Definition _exists : forall {a : Set}, (a -> bool) -> list a -> bool :=
  fun a =>
  List.existsb (A := a).

Parameter for_all2 : forall {a b : Set},
  (a -> b -> bool) -> list a -> list b -> bool.

Parameter _exists2 : forall {a b : Set},
  (a -> b -> bool) -> list a -> list b -> bool.

Definition find_opt : forall {a : Set}, (a -> bool) -> list a -> option a :=
  fun a =>
  List.find (A := a).

Definition filter : forall {a : Set}, (a -> bool) -> list a -> list a :=
  fun _ =>
  List.filter.

Definition find_all : forall {a : Set}, (a -> bool) -> list a -> list a :=
  fun _ =>
  filter.

Definition partition : forall {a : Set},
  (a -> bool) -> list a -> list a * list a :=
  fun _ =>
  List.partition.

Definition split : forall {a b : Set}, list (a * b) -> list a * list b :=
  fun _ _ =>
  List.split.

Parameter combine : forall {a b : Set}, list a -> list b -> list (a * b).

Parameter sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter stable_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter fast_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter sort_uniq : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter merge : forall {a : Set},
  (a -> a -> int) -> list a -> list a -> list a.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Base58.
Require Proto_2021_01.Environment.Lwt.
Require Proto_2021_01.Environment.Map.
Import Lwt.Notations.

Definition key : Set := list string.

Definition value : Set := bytes.

Definition StringMap : S.MAP (key := string) (t := _) :=
  Map.Make
    {|
      Compare.COMPARABLE.compare := String.compare
    |}.

Module Memory.
  Inductive t : Set :=
  | Dir : StringMap.(S.MAP.t) t -> t
  | Key : value -> t.
End Memory.

Definition t : Set := Memory.t.

Definition empty : t := Memory.Dir StringMap.(S.MAP.empty).

Fixpoint get_sub_context (m : t) (k : list string) : option t :=
  match (k, m) with
  | ([], m) => Some m
  | (n :: k, Memory.Dir m) =>
    match StringMap.(S.MAP.find_opt) n m with
    | Some res => get_sub_context res k
    | None => None
    end
  | (_ :: _, Memory.Key _) => None
  end.

Definition raw_mem (m : t) (k : list string) : bool :=
  match get_sub_context m k with
  | Some (Memory.Key _) => true
  | Some (Memory.Dir _) | None => false
  end.

Definition mem (m : t) (k : list string) : Lwt.t bool :=
  return= raw_mem m k.

Definition raw_dir_mem (m : t) (k : list string) : bool :=
  match get_sub_context m k with
  | Some (Memory.Dir _) => true
  | (Some (Memory.Key _) | None) => false
  end.

Definition dir_mem (m : t) (k : list string) : Lwt.t bool :=
  return= raw_dir_mem m k.

Definition raw_get (m : t) (k : list string) : option value :=
  match get_sub_context m k with
  | Some (Memory.Key v) => Some v
  | (Some (Memory.Dir _) | None) => None
  end.

Definition get (m : t) (k : list string) : Lwt.t (option value) :=
  return= raw_get m k.

Fixpoint raw_set (m : t) (k : key) (v : value) : t :=
  match (k, m) with
  | ([], Memory.Key _) => Memory.Key v
  | ([], Memory.Dir _) => m
  | (_ :: _, Memory.Key _) => m
  | (n :: k, Memory.Dir dir) =>
    match StringMap.(S.MAP.find_opt) n dir with
    | None => m
    | Some sub_m =>
      let sub_m := raw_set sub_m k v in
      Memory.Dir (StringMap.(S.MAP.add) n sub_m dir)
    end
  end.

Definition set (m : t) (k : key) (v : value) : Lwt.t t :=
  return= raw_set m k v.

Parameter raw_copy : t -> key -> key -> option t.

Definition copy (m : t) (k1 k2 : key) : Lwt.t (option t) :=
  return= raw_copy m k1 k2.

Parameter raw_remove_rec : t -> key -> t.

Definition remove_rec (m : t) (k :key) : Lwt.t t :=
  return= raw_remove_rec m k.

Inductive key_or_dir : Set :=
| Dir : key -> key_or_dir
| Key : key -> key_or_dir.

Definition fold {A : Set}
  (m : t) (k : list string) (_init_value : A)
  (f : Context.key_or_dir -> A -> Lwt.t A) : Lwt.t A :=
  match get_sub_context m k with
  | None | Some (Memory.Key _) => return= _init_value
  | Some (Memory.Dir m) =>
    StringMap.(S.MAP.fold)
      (fun n m acc =>
        let= acc := acc in
        match m with
        | Memory.Key _ => f (Context.Key (Pervasives.op_at k [ n ])) acc
        | Memory.Dir _ => f (Context.Dir (Pervasives.op_at k [ n ])) acc
        end)
      m
      (return= _init_value)
  end.

Parameter keys : t -> key -> Lwt.t (list key).

Parameter fold_keys : forall {a : Set},
  t -> key -> a -> (key -> a -> Lwt.t a) -> Lwt.t a.

Parameter register_resolver : forall {a : Set},
  Base58.encoding a -> (t -> string -> Lwt.t (list a)) -> unit.

Parameter complete : t -> string -> Lwt.t (list string).

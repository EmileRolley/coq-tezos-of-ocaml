Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Services_registration.

Definition custom_root : RPC_path.context RPC_context.t :=
  RPC_path.op_div (RPC_path.op_div RPC_path.open_root "context") "constants".

Module S.
  Definition errors
    : RPC_service.service RPC_context.t RPC_context.t unit unit
      Data_encoding.json_schema :=
    RPC_service.get_service
      (Some "Schema for all the RPC errors from this protocol version")
      RPC_query.empty Data_encoding.json_schema_value
      (RPC_path.op_div custom_root "errors").
  
  Definition all
    : RPC_service.service RPC_context.t RPC_context.t unit unit
      Alpha_context.Constants.t :=
    RPC_service.get_service (Some "All constants") RPC_query.empty
      Alpha_context.Constants.encoding custom_root.
End S.

Definition register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ :=
    Services_registration.register0_noctxt S.errors
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Error_monad._return
            (Data_encoding.Json.schema None Error_monad.error_encoding)) in
  Services_registration.register0 S.all
    (fun (ctxt : Alpha_context.t) =>
      fun (function_parameter : unit) =>
        let '_ := function_parameter in
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Error_monad._return
            {|
              Alpha_context.Constants.t.fixed :=
                Alpha_context.Constants.fixed_value;
              Alpha_context.Constants.t.parametric :=
                Alpha_context.Constants.parametric_value ctxt |}).

Definition errors {A : Set} (ctxt : RPC_context.simple A) (block : A)
  : M= (Error_monad.shell_tzresult Data_encoding.json_schema) :=
  RPC_context.make_call0 S.errors ctxt block tt tt.

Definition all {A : Set} (ctxt : RPC_context.simple A) (block : A)
  : M= (Error_monad.shell_tzresult Alpha_context.Constants.t) :=
  RPC_context.make_call0 S.all ctxt block tt tt.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Cycle_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Int32.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage_description.

Module Valid.
  Definition t (cycle : Cycle_repr.t) : Prop :=
    cycle >=i32 0 = true.
End Valid.

Lemma encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Cycle_repr.encoding.
  auto with Data_encoding_db.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma rpc_arg_is_valid : RPC_arg.Valid.t Valid.t Cycle_repr.rpc_arg.
  apply RPC_arg.Valid.like.
  apply RPC_arg.Valid.uint31.
Qed.

Lemma compare_is_valid : Compare.Valid.t id Cycle_repr.compare.
  apply Compare.Valid.int32.
Qed.

Lemma Index_is_valid
  : Storage_description.INDEX.Valid.t Valid.t Cycle_repr.Index.
  constructor.
  { constructor; try reflexivity.
    { cbv.
      apply Int32.of_string_opt_to_string.
    }
    { cbv - [length].
      intro path.
      destruct path as [|s[]]; trivial.
      assert (H_s := Int32.to_string_of_string_opt s).
      destruct (Int32.of_string_opt s); congruence.
    }
  }
  { apply rpc_arg_is_valid. }
  { Data_encoding.Valid.data_encoding_auto. }
  { apply compare_is_valid. }
Qed.

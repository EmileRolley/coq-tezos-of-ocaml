Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.

Lemma Blake2B_Make_include_is_valid :
  S.HASH.Valid.t (fun _ => True) Block_payload_hash.Blake2B_Make_include.
  apply Blake2B.Make_is_valid.
Qed.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Block_payload_hash.encoding.
  apply Blake2B_Make_include_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

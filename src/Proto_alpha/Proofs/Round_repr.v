Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Round_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Period_repr.

Module Valid.
  Definition t (n : Round_repr.t) : Prop :=
    0 <= n <= Int32.max_int.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t Round_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
  unfold Valid.t, Round_repr.of_int32, Round_repr.op_gteq.
  hauto lq: on solve: lia.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Durations.
  Module Valid.
    Import Proto_alpha.Round_repr.Durations.t.

    Record t (durations : Round_repr.Durations.t) : Prop := {
      at_least_one_second :
        1 <= durations.(first_round_duration);
      increasing_rounds :
        1 <= durations.(delay_increment_per_round);
      first_round_duration :
        Period_repr.Valid.t durations.(first_round_duration);
      delay_increment_per_round :
        Period_repr.Valid.t durations.(delay_increment_per_round);
    }.
  End Valid.

  Lemma encoding_is_valid
    : Data_encoding.Valid.t Valid.t Round_repr.Durations.encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros ? [].
    unfold Round_repr.Durations.create_opt,
      Round_repr.Durations.create,
      Period_repr.to_seconds.
    repeat match goal with
    | |- context [error_when ?e] => destruct e eqn:?; simpl
    end; sauto lq: on rew: off solve: lia.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Durations.

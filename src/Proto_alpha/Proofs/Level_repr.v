Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Z.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.

Require TezosOfOCaml.Proto_alpha.Level_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Contract_repr.

Require TezosOfOCaml.Proto_alpha.Proofs.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Cycle_repr.

Lemma compare_is_valid : Compare.Valid.t
    (fun '{| Level_repr.t.level := level |} => level)
    Level_repr.compare.
  apply
    (Compare.Valid.projection
       (fun pat : Level_repr.t.record => pat.(Level_repr.t.level))
       id
       Int32.compare).
  apply Compare.Valid.int32.
Qed.

Module Valid.
  Record t (l : Level_repr.t) : Prop := {
    level : Raw_level_repr.Valid.t l.(Level_repr.t.level);
    cycle : Cycle_repr.Valid.t l.(Level_repr.t.cycle);
  }.
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Level_repr.encoding.
  Data_encoding.Valid.data_encoding_auto;
  repeat constructor;
  destruct x; destruct level; try constructor;
  destruct H; simpl in *; inversion level.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
  
Lemma diff_value_eq_zero : forall l, Level_repr.diff_value l l = 0.
  intro l; unfold Level_repr.diff_value, Raw_level_repr.to_int32.
  autounfold with tezos_z.
  now rewrite Z.sub_diag.
Qed.

Module Cycle_era.
  Module Valid.
    Record t (cycle : Level_repr.cycle_era) : Prop := {
      first_level : Raw_level_repr.Valid.t cycle.(Level_repr.cycle_era.first_level);
      first_cycle : Cycle_repr.Valid.t cycle.(Level_repr.cycle_era.first_cycle);
    }.
  End Valid.
End Cycle_era.

Lemma cycle_era_encoding_is_valid :
  Data_encoding.Valid.t Cycle_era.Valid.t Level_repr.cycle_era_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.

Module Cycle_eras.
  Module Valid.
    Record t (cycle_eras : list Level_repr.cycle_era) : Prop := {
      can_create :
        match Level_repr.create_cycle_eras cycle_eras with
        | Pervasives.Ok cycle_eras' => cycle_eras' = cycle_eras
        | Pervasives.Error _ => False
        end;
      cycle_era : List.Forall Cycle_era.Valid.t cycle_eras;
    }.
  End Valid.
End Cycle_eras.

Lemma cycle_eras_encoding_is_valid :
  Data_encoding.Valid.t Cycle_eras.Valid.t Level_repr.cycle_eras_encoding.
  Data_encoding.Valid.data_encoding_auto.
  apply cycle_era_encoding_is_valid.
  intros x H; split.
  - apply H.
  - destruct H.
    destruct (Level_repr.create_cycle_eras _); [
      congruence | tauto
    ].
Qed.

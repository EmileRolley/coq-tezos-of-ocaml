Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Storage_functors.
Require Import TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs._Set.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Context.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Map.
Require TezosOfOCaml.Proto_alpha.Proofs.Misc.
Require TezosOfOCaml.Proto_alpha.Proofs.Path_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_sigs.

Module ENCODER.
  Module Valid.
    Record property {t : Set} {E : ENCODER (t := t)} : Prop := {
      of_bytes_to_bytes path value :
        E.(ENCODER.of_bytes) path (E.(ENCODER.to_bytes) value) = return? value;
    }.
  End Valid.
End ENCODER.

Module Make_single_data_storage.
  Lemma of_nil_sub_context_eq {value : Set}
    (R : REGISTER)
    (N : NAME)
    (V : VALUE (t := value))
    : let N_nil := {| Storage_sigs.NAME.name := [] |} in
      let C := Storage_functors.Make_subcontext R Raw_context.M N_nil in
      Make_single_data_storage R C N V =
      Make_single_data_storage R Raw_context.M N V.
    reflexivity.
  Qed.

  Lemma eq {value : Set}
    (R : REGISTER)
    (N1 N2 : NAME)
    (V : VALUE (t := value))
    : let path := op_at N1.(NAME.name) N2.(NAME.name) in
      Single_data_storage.Eq.t
        (let C := Storage_functors.Make_subcontext R Raw_context.M N1 in
        Make_single_data_storage R C N2 V)
        (Single_data_storage.Make
          (fun ctxt =>
            let value := Context.find (Raw_context.context_value ctxt) path in
            Single_data_storage.State.parse V.(VALUE.encoding) value
          )
          (fun ctxt change =>
            Single_data_storage.Change.apply ctxt path V.(VALUE.encoding) change
          )
          path
        ).
    constructor; intros; simpl;
      unfold
        Make_single_data_storage.mem, Single_data_storage.Op.mem,
        Make_single_data_storage.get, Single_data_storage.Op.get,
        Make_single_data_storage.find, Single_data_storage.Op.find,
        Make_single_data_storage.init_value, Single_data_storage.Op.init_value,
        Make_single_data_storage.update,
        Make_single_data_storage.add_or_remove,
        Make_single_data_storage.remove_existing, Single_data_storage.Op.remove_existing,
        Make_single_data_storage.remove;
      simpl;
      unfold
        Make_subcontext.mem,
        Make_subcontext.get,
        Make_subcontext.find,
        Make_subcontext.init_value,
        Make_subcontext.to_key,
        Make_subcontext.update,
        Make_subcontext.project,
        Make_subcontext.add_or_remove,
        Make_subcontext.remove_existing,
        Make_subcontext.remove;
      simpl.
    { unfold Raw_context.M.mem, Raw_context.mem.
      rewrite Context.mem_find; simpl.
      destruct (Context.find _ _); trivial; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { unfold Raw_context.M.get, Raw_context.get.
      destruct (Context.find _ _); trivial; simpl.
      unfold Make_single_data_storage.of_bytes; simpl.
      unfold Make_encoder.of_bytes; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { unfold Raw_context.M.find, Raw_context.find.
      destruct (Context.find _ _); trivial; simpl.
      unfold Make_single_data_storage.of_bytes; simpl.
      unfold Make_encoder.of_bytes; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { unfold Raw_context.M.init_value, Raw_context.init_value.
      rewrite Context.mem_find; simpl.
      destruct (Context.find _ _); trivial; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { unfold Raw_context.M.update, Raw_context.update.
      rewrite Context.mem_find; simpl.
      destruct (Context.find _ _); trivial; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { reflexivity.
    }
    { now destruct value0.
    }
    { unfold Raw_context.M.remove_existing, Raw_context.remove_existing.
      rewrite Context.mem_find; simpl.
      destruct (Context.find _ _); trivial; simpl.
      now destruct (Binary.of_bytes_opt).
    }
    { reflexivity.
    }
  Qed.
End Make_single_data_storage.

Module INDEX.
  Definition to_Path_encoding_S {t ipath}
    (index : INDEX (t := t) (ipath := ipath)) : Path_encoding.S (t := t) := {|
      Path_encoding.S.to_path := index.(Storage_functors.INDEX.to_path);
      Path_encoding.S.of_path := index.(Storage_functors.INDEX.of_path);
      Path_encoding.S.path_length := index.(Storage_functors.INDEX.path_length);
    |}.

  Module Valid.
    Definition t {t ipath} (index : INDEX (t := t) (ipath := ipath)) : Prop :=
      Path_encoding.S.Valid.t (to_Path_encoding_S index).
  End Valid.
End INDEX.

Module Make_data_set_storage.
  Lemma of_nil_sub_context_eq {ipath : Set -> Set} {value : Set}
    (I : INDEX (ipath := ipath) (t := value))
    (R : REGISTER)
    (N : NAME)
    : let N_nil := {| Storage_sigs.NAME.name := [] |} in
      let C := Storage_functors.Make_subcontext R Raw_context.M N_nil in
      Make_data_set_storage C I =
      Make_data_set_storage Raw_context.M I.
    reflexivity.
  Qed.

  Parameter flatten_view : option depth -> View.t -> key -> list (key * Tree.t).

  Axiom flatten_view_fold : forall (view : View.t) k d {a : Set} (acc : a) f,
    fold d view k Sorted acc f = List.fold_right
      (fun p acc' => f (fst p) (snd p) acc') (flatten_view d view k) acc.

  Axiom flatten_view_find_In : forall (view : View.t) (root path : key) (d : int),
    length path = d -> (exists v, find view (op_at root path) = Some v) <->
    exists t, In (path, t) (flatten_view (Some (Eq d)) view root).

  (* Should flatten_view have an INDEX as an argument? *)
  Axiom flatten_view_sorted : forall {ipath : Set -> Set} {value : Set}
    `{@Make.FArgs value}
    (I : INDEX (ipath := ipath) (t := value))
    (d : option depth) (v : View.t) (k : key),
    Map.sorted (List.opt_map (fun p => I.(INDEX.of_path) (fst p))
    (flatten_view d v k)).

  Lemma eq {ipath : Set -> Set}{value : Set}
    (compare : value -> value -> int)
    (lawful : Map.lawful_compare compare)
    (I : INDEX (ipath := ipath) (t := value))
    (R : REGISTER)
    (N : NAME)
    : let C := Storage_functors.Make_subcontext R Raw_context.M N in
      (* this incantation is needed to ensure that
         `args` is used as an implicit argument *)
      let args := Make_data_set_storage.Build_FArgs C I in
      INDEX.Valid.t I ->
      Data_set_storage.Eq.t
        (Make_data_set_storage C I)
        (Data_set_storage.Make (compare := compare)
        (fun ctxt =>
            let view := Raw_context.context_value ctxt in
            Context.fold (Some (Context.Eq I.(INDEX.path_length)))
              view N.(NAME.name) Sorted _Set.Make.empty (fun k _ s =>
                match I.(INDEX.of_path) k with
                | Some element => _Set.Make.add element s
                | None => s
                end
            ))
          (fun ctxt change =>
             match change with
             | Data_set_storage.Change.Add val =>
                 Make_data_set_storage.add ctxt val
             | Data_set_storage.Change.Clear =>
                 Make_data_set_storage.clear ctxt
             | Data_set_storage.Change.Remove val =>
                 Make_data_set_storage.remove ctxt val
             end)
        ).
      constructor; intros; simpl.
    { unfold Make_data_set_storage.mem; simpl.
      unfold Make_subcontext.mem; simpl.
      unfold Data_set_storage.Op.mem; simpl.
      unfold Make_subcontext.to_key; simpl.
      unfold Raw_context.M.mem; simpl.
      unfold Raw_context.mem.
      rewrite Context.mem_find; simpl.
      unfold _Set.Make.mem.
      simpl.
      destruct (find _ _) eqn:G; symmetry.
      - rewrite flatten_view_fold.
        rewrite (@Map.mem_In _
          (@Make.Build_FArgs value
        (COMPARABLE.Build_signature value
           compare)) lawful).
        destruct (flatten_view_find_In
          (Raw_context.context_value ctxt)
          (N.(NAME.name))
          (I.(INDEX.to_path) value0 nil)
          (I.(INDEX.path_length))) as [G' _].
        + apply H.
        + destruct G'; [exists v; auto|idtac]. 
          eapply (List.In_to_In_fold_add
            (fun p : key * Tree.t => I.(INDEX.of_path) (fst p))
            (fun v => (v,tt))
            (@_Set.Make.add _ (@_Set.Make.Build_FArgs value
               (COMPARABLE.Build_signature value
                  compare)))).
          * apply _Set.added_In; assumption.
          * intros. apply _Set.In_add_still_In; assumption.
          * exact H0.
          * simpl.
            apply H.
        + rewrite List.fold_right_opt_map.
          rewrite (_Set.sorted_fold_right_add compare);
          apply flatten_view_sorted.
      - rewrite flatten_view_fold.
        rewrite (@Map.mem_not_In _
          (@Make.Build_FArgs value
        (COMPARABLE.Build_signature value
           compare)) lawful). 
        + intro.
          pose proof (List.In_fold_add_to_In
            (fun p : key * Tree.t => I.(INDEX.of_path) (fst p))
            (fun v => (v,tt))
            (@_Set.Make.add _ (@_Set.Make.Build_FArgs value
              (COMPARABLE.Build_signature value compare)))
            (_Set.In_add_destr compare)
            axiom (* inj of to_path, a bit trickier
              because I think we need some local data about
              the list we are inside *)
            (flatten_view
              (Some (Eq I.(INDEX.path_length)))
              (Raw_context.context_value ctxt)
              N.(NAME.name))
            ((I.(INDEX.to_path) value0 nil), Tree.Value "")
            value0
            ((Path_encoding.S.Valid.of_path_to_path H _))
            H0).
          destruct (flatten_view_find_In
            (Raw_context.context_value ctxt)
            N.(NAME.name)
            (I.(INDEX.to_path) value0 nil)
            I.(INDEX.path_length)
            ) as [_ G1]; [apply H|idtac].
          destruct G1 as [v Hv].
          * eexists; exact H1.
          * rewrite Hv in G; discriminate.
      + rewrite List.fold_right_opt_map.
        unfold _Set.Make.empty; simpl.
        rewrite (_Set.sorted_fold_right_add compare);
        apply flatten_view_sorted.
    }
    { reflexivity. }
    { reflexivity. }
    { unfold Make_data_set_storage.elements, Data_set_storage.Op.elements,
      Make.elements, Make_data_set_storage.elements, Make.map, Make.bindings; simpl.
      unfold Make.elements.
      unfold _Set.Make.empty, Make.empty; simpl.
      unfold Make.bindings.
      unfold Make_data_set_storage.fold; simpl.
      unfold Make_subcontext.fold; simpl.
      unfold Raw_context.fold; simpl.
      unfold Make_subcontext.to_key; simpl.
      unfold op_at; rewrite app_nil_r.
      symmetry.
      unfold Raw_context.M.fold.
      unfold Raw_context.fold.
      repeat rewrite flatten_view_fold.
      simpl.
      rewrite List.fold_right_opt_map.
      rewrite Context.remove_kind_match.
      rewrite List.fold_right_opt_no_None.
      - rewrite Context.opt_map_filter_tree.
        + unfold nil. rewrite List.fold_right_cons_nil.
          apply (_Set.sorted_fold_right_add compare).
          apply flatten_view_sorted.
        + apply axiom. (*axiomatize this*)
      - apply axiom. (* kind_value vs of_path*)
      }
      { reflexivity. }
Qed.

End Make_data_set_storage.

Module Pair.
  (** We always produce a valid index from a pair of two valid indexes. *)
  Lemma is_valid {t1 ipath1 t2 ipath2}
    {I1 : INDEX (t := t1) (ipath := ipath1)}
    {I2 : INDEX (t := t2) (ipath := ipath2)}
    : INDEX.Valid.t I1 ->
      INDEX.Valid.t I2 ->
      INDEX.Valid.t (Pair I1 I2).
    intros H1 H2; constructor; simpl; intros; try destruct v; simpl.
    { rewrite H1.(Path_encoding.S.Valid.to_path_postfix).
      simpl.
      rewrite H2.(Path_encoding.S.Valid.to_path_postfix).
      simpl.
      rewrite H1.(Path_encoding.S.Valid.to_path_postfix)
        with (postfix := I2.(INDEX.to_path) _ []).
      simpl.
      now rewrite List.app_assoc.
    }
    { unfold Pair.of_path; simpl.
      destruct I1, H1; simpl in *.
      erewrite <- to_path_path_length.
      rewrite (to_path_postfix _ (I2.(INDEX.to_path) _ [])).
      rewrite Misc.take_of_length_app.
      rewrite of_path_to_path.
      now rewrite H2.(Path_encoding.S.Valid.of_path_to_path).
    }
    { unfold Pair.of_path.
      match goal with
      | [|- context[Misc.take ?n ?l]] => assert (H_take := Misc.take_cases n l)
      end.
      destruct (Misc.take _ _) as [[p1 p2]|]; trivial.
      assert (H_p1 := H1.(Path_encoding.S.Valid.to_path_of_path) p1).
      assert (H_p2 := H2.(Path_encoding.S.Valid.to_path_of_path) p2).
      simpl in *.
      repeat destruct (_.(INDEX.of_path) _); trivial.
      simpl.
      rewrite H1.(Path_encoding.S.Valid.to_path_postfix).
      rewrite H2.(Path_encoding.S.Valid.to_path_postfix).
      simpl.
      rewrite H_p1, H_p2.
      now rewrite List.app_nil_r.
    }
    { rewrite H1.(Path_encoding.S.Valid.to_path_postfix).
      rewrite List.length_app_sum; simpl.
      rewrite H1.(Path_encoding.S.Valid.to_path_path_length).
      rewrite H2.(Path_encoding.S.Valid.to_path_path_length).
      reflexivity.
    }
  Qed.
End Pair.

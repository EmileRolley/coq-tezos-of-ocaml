Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Round_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Module Ratio.
  Module Valid.
    Import Constants_repr.ratio.

    Record t (ratio : Constants_repr.ratio) : Prop := {
      numerator :
        Pervasives.UInt16.Valid.t ratio.(Constants_repr.ratio.numerator);
      denominator :
        Pervasives.UInt16.Valid.t ratio.(Constants_repr.ratio.denominator) /\
        ratio.(Constants_repr.ratio.denominator) > 0;
    }.
  End Valid.
End Ratio.

Lemma ratio_encoding_is_valid
  : Data_encoding.Valid.t Ratio.Valid.t Constants_repr.ratio_encoding.
  Data_encoding.Valid.data_encoding_auto.
  scrush.
Qed.
#[global] Hint Resolve ratio_encoding_is_valid : Data_encoding_db.

Lemma fixed_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Constants_repr.fixed_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros []; hfcrush.
Qed.
#[global] Hint Resolve fixed_encoding_is_valid : Data_encoding_db.

Module Parametric.
  Module Valid.
    Import Constants_repr.parametric.

    Record t (c : Constants_repr.parametric) : Prop := {
      preserved_cycles :
        Pervasives.UInt8.Valid.t c.(preserved_cycles);
      tokens_per_roll :
        Tez_repr.Valid.t c.(tokens_per_roll);
      seed_nonce_revelation_tip :
        Tez_repr.Valid.t c.(seed_nonce_revelation_tip);
      origination_size :
        Pervasives.Int31.Valid.t c.(origination_size);
      baking_reward_fixed_portion :
        Tez_repr.Valid.t c.(baking_reward_fixed_portion);
      baking_reward_bonus_per_slot :
        Tez_repr.Valid.t c.(baking_reward_bonus_per_slot);
      endorsing_reward_per_slot :
        Tez_repr.Valid.t c.(endorsing_reward_per_slot);
      cost_per_byte :
        Tez_repr.Valid.t c.(cost_per_byte);
      liquidity_baking_subsidy :
        Tez_repr.Valid.t c.(liquidity_baking_subsidy);
      max_operations_time_to_live :
        Pervasives.Int16.Valid.t c.(max_operations_time_to_live);
      minimal_block_delay :
        Period_repr.Valid.t c.(minimal_block_delay);
      delay_increment_per_round :
        Period_repr.Valid.t c.(delay_increment_per_round);
      minimal_participation_ratio :
        Ratio.Valid.t c.(minimal_participation_ratio);
      consensus_committee_size :
        Pervasives.Int31.Valid.t c.(consensus_committee_size);
      consensus_threshold :
        Pervasives.Int31.Valid.t c.(consensus_threshold);
      max_slashing_period :
        Pervasives.Int31.Valid.t c.(max_slashing_period);
      frozen_deposits_percentage :
        Pervasives.Int31.Valid.t c.(frozen_deposits_percentage);
      double_baking_punishment :
        Tez_repr.Valid.t c.(double_baking_punishment);
      ratio_of_frozen_deposits_slashed_per_double_endorsement :
        Ratio.Valid.t c.(ratio_of_frozen_deposits_slashed_per_double_endorsement);
      tx_rollup_origination_size :
        Pervasives.Int31.Valid.t c.(tx_rollup_origination_size);
      sc_rollup_origination_size :
        Pervasives.Int31.Valid.t c.(sc_rollup_origination_size);
    }.
  End Valid.
End Parametric.

Lemma parametric_encoding_is_valid
  : Data_encoding.Valid.t
      Parametric.Valid.t Constants_repr.parametric_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros [] []; simpl in *; destruct initial_seed; tauto.
Qed.
#[global] Hint Resolve parametric_encoding_is_valid : Data_encoding_db.

Module Valid.
  Definition t (c : Constants_repr.t) : Prop :=
    Parametric.Valid.t c.(Constants_repr.t.parametric).
End Valid.

Lemma encoding_is_valid
  : Data_encoding.Valid.t Valid.t Constants_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Proto_previous.
  Module Valid.
    Import Constants_repr.Proto_previous.parametric.

    Record t (c : Constants_repr.Proto_previous.parametric) : Prop := {
      preserved_cycles :
        Pervasives.UInt8.Valid.t c.(preserved_cycles);
      time_between_blocks :
        List.Forall Period_repr.Valid.t c.(time_between_blocks);
      minimal_block_delay :
        Period_repr.Valid.t c.(minimal_block_delay);
      endorsers_per_block :
        Pervasives.UInt16.Valid.t c.(endorsers_per_block);
      tokens_per_roll :
        Tez_repr.Valid.t c.(tokens_per_roll);
      seed_nonce_revelation_tip :
        Tez_repr.Valid.t c.(seed_nonce_revelation_tip);
      origination_size :
        Pervasives.Int31.Valid.t c.(origination_size);
      block_security_deposit :
        Tez_repr.Valid.t c.(block_security_deposit);
      endorsement_security_deposit :
        Tez_repr.Valid.t c.(endorsement_security_deposit);
      baking_reward_per_endorsement :
        List.Forall Tez_repr.Valid.t c.(baking_reward_per_endorsement);
      endorsement_reward :
        List.Forall Tez_repr.Valid.t c.(endorsement_reward);
      cost_per_byte :
        Tez_repr.Valid.t c.(cost_per_byte);
      initial_endorsers :
        Pervasives.UInt16.Valid.t c.(initial_endorsers);
      delay_per_missing_endorsement :
        Period_repr.Valid.t c.(delay_per_missing_endorsement);
      liquidity_baking_subsidy :
        Tez_repr.Valid.t c.(liquidity_baking_subsidy);
    }.
  End Valid.

  Lemma parametric_encoding_is_valid 
    : Data_encoding.Valid.t Valid.t Constants_repr.Proto_previous.parametric_encoding.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve parametric_encoding_is_valid : Data_encoding_db.
End Proto_previous.

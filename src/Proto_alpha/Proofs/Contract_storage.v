Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_storage.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_repr.

Module Legacy_big_map_diff.
  Lemma item_encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Contract_storage.Legacy_big_map_diff.item_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros []; simpl; try tauto.
    destruct u, diff_value; tauto.
  Qed.
  #[global] Hint Resolve item_encoding_is_valid : Data_encoding_db.

  Lemma encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Contract_storage.Legacy_big_map_diff.encoding.
    Data_encoding.Valid.data_encoding_auto.
    now intros; apply List.Forall_True.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Legacy_big_map_diff.

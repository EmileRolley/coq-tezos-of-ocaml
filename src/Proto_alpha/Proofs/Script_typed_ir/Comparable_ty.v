Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require TezosOfOCaml.Proto_alpha.Michocoq.syntax_type.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.Ty_metadata.

(** A type family for the [comparable_ty] GADT. *)
Module Family.
  Inductive t : Set :=
  | Unit_key : t
  | Never_key : t
  | Int_key : t
  | Nat_key : t
  | Signature_key : t
  | String_key : t
  | Bytes_key : t
  | Mutez_key : t
  | Bool_key : t
  | Key_hash_key : t
  | Key_key : t
  | Timestamp_key : t
  | Chain_id_key : t
  | Address_key : t
  | Pair_key : t -> t -> t
  | Union_key : t -> t -> t
  | Option_key : t -> t.

  Fixpoint to_Set (v : t) : Set :=
    match v with
    | Unit_key => unit
    | Never_key => Script_typed_ir.never
    | Int_key => Script_int_repr.num
    | Nat_key => Script_int_repr.num
    | Signature_key => Alpha_context.signature
    | String_key => Script_string_repr.t
    | Bytes_key => Bytes.t
    | Mutez_key => Tez_repr.t
    | Bool_key => bool
    | Key_hash_key => public_key_hash
    | Key_key => public_key
    | Timestamp_key => Script_timestamp_repr.t
    | Chain_id_key => Chain_id.t
    | Address_key => Script_typed_ir.address
    | Pair_key v_a v_b => to_Set v_a * to_Set v_b
    | Union_key v_a v_b => Script_typed_ir.union (to_Set v_a) (to_Set v_b)
    | Option_key v => option (to_Set v)
    end.
End Family.

Module With_family.
  Inductive t : Script_typed_ir.comparable_ty -> Family.t -> Set :=
  | Unit_key : forall metadata,
    t (Script_typed_ir.Unit_key metadata) Family.Unit_key
  | Never_key : forall metadata,
    t (Script_typed_ir.Never_key metadata) Family.Never_key
  | Int_key : forall metadata,
    t (Script_typed_ir.Int_key metadata) Family.Int_key
  | Nat_key : forall metadata,
    t (Script_typed_ir.Nat_key metadata) Family.Nat_key
  | Signature_key : forall metadata,
    t (Script_typed_ir.Signature_key metadata) Family.Signature_key
  | String_key : forall metadata,
    t (Script_typed_ir.String_key metadata) Family.String_key
  | Bytes_key : forall metadata,
    t (Script_typed_ir.Bytes_key metadata) Family.Bytes_key
  | Mutez_key : forall metadata,
    t (Script_typed_ir.Mutez_key metadata) Family.Mutez_key
  | Bool_key : forall metadata,
    t (Script_typed_ir.Bool_key metadata) Family.Bool_key
  | Key_hash_key : forall metadata,
    t (Script_typed_ir.Key_hash_key metadata) Family.Key_hash_key
  | Key_key : forall metadata,
    t (Script_typed_ir.Key_key metadata) Family.Key_key
  | Timestamp_key : forall metadata,
    t (Script_typed_ir.Timestamp_key metadata) Family.Timestamp_key
  | Chain_id_key : forall metadata,
    t (Script_typed_ir.Chain_id_key metadata) Family.Chain_id_key
  | Address_key : forall metadata,
    t (Script_typed_ir.Address_key metadata) Family.Address_key
  | Pair_key : forall v1 m1 annot1 v2 m2 annot2 metadata,
    t v1 m1 -> t v2 m2 ->
    t (Script_typed_ir.Pair_key (v1, annot1) (v2, annot2) metadata)
      (Family.Pair_key m1 m2)
  | Union_key : forall v1 m1 annot1 v2 m2 annot2 metadata,
    t v1 m1 -> t v2 m2 ->
    t (Script_typed_ir.Union_key (v1, annot1) (v2, annot2) metadata)
      (Family.Union_key m1 m2)
  | Option_key : forall v m metadata,
    t v m ->
    t (Script_typed_ir.Option_key v metadata) (Family.Option_key m).
End With_family.

(** Compute the [Set] representing a [Script_typed_ir.comparable_ty]. *)
Fixpoint to_Set (ty : Script_typed_ir.comparable_ty) : Set :=
  match ty with
  | Script_typed_ir.Unit_key _ => unit
  | Script_typed_ir.Never_key _ => Script_typed_ir.never
  | Script_typed_ir.Int_key _ => Alpha_context.Script_int.num
  | Script_typed_ir.Nat_key _ => Alpha_context.Script_int.num
  | Script_typed_ir.Signature_key _ => Alpha_context.signature
  | Script_typed_ir.String_key _ => Alpha_context.Script_string.t
  | Script_typed_ir.Bytes_key _ => Bytes.t
  | Script_typed_ir.Mutez_key _ => Alpha_context.Tez.t
  | Script_typed_ir.Bool_key _ => bool
  | Script_typed_ir.Key_hash_key _ => public_key_hash
  | Script_typed_ir.Key_key _ => public_key
  | Script_typed_ir.Timestamp_key _ => Alpha_context.Script_timestamp.t
  | Script_typed_ir.Chain_id_key _ => Chain_id.t
  | Script_typed_ir.Address_key _ => Script_typed_ir.address
  | Script_typed_ir.Pair_key (ty1, _) (ty2, _) _ => to_Set ty1 * to_Set ty2
  | Script_typed_ir.Union_key (ty1, _) (ty2, _) _ =>
    Script_typed_ir.union (to_Set ty1) (to_Set ty2)
  | Script_typed_ir.Option_key ty _ => option (to_Set ty)
  end.

Fixpoint to_Set_eq_Family_to_Set {v m} (ty : With_family.t v m)
  : to_Set v = Family.to_Set m.
  destruct ty; simpl; repeat erewrite to_Set_eq_Family_to_Set; trivial.
Qed.

(** Get the canonical form of a comparable type, erasing the metadata. *)
Fixpoint get_canonical (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.comparable_ty :=
  match ty with
  | Script_typed_ir.Unit_key _ =>
    Script_typed_ir.Unit_key Ty_metadata.default
  | Script_typed_ir.Never_key _ =>
    Script_typed_ir.Never_key Ty_metadata.default
  | Script_typed_ir.Int_key _ =>
    Script_typed_ir.Int_key Ty_metadata.default
  | Script_typed_ir.Nat_key _ =>
    Script_typed_ir.Nat_key Ty_metadata.default
  | Script_typed_ir.Signature_key _ =>
    Script_typed_ir.Signature_key Ty_metadata.default
  | Script_typed_ir.String_key _ =>
    Script_typed_ir.String_key Ty_metadata.default
  | Script_typed_ir.Bytes_key _ =>
    Script_typed_ir.Bytes_key Ty_metadata.default
  | Script_typed_ir.Mutez_key _ =>
    Script_typed_ir.Mutez_key Ty_metadata.default
  | Script_typed_ir.Bool_key _ =>
    Script_typed_ir.Bool_key Ty_metadata.default
  | Script_typed_ir.Key_hash_key _ =>
    Script_typed_ir.Key_hash_key Ty_metadata.default
  | Script_typed_ir.Key_key _ =>
    Script_typed_ir.Key_key Ty_metadata.default
  | Script_typed_ir.Timestamp_key _ =>
    Script_typed_ir.Timestamp_key Ty_metadata.default
  | Script_typed_ir.Chain_id_key _ =>
    Script_typed_ir.Chain_id_key Ty_metadata.default
  | Script_typed_ir.Address_key _ =>
    Script_typed_ir.Address_key Ty_metadata.default
  | Script_typed_ir.Pair_key (ty1, _) (ty2, _) _ =>
    let ty1 := get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Pair_key (ty1, None) (ty2, None) Ty_metadata.default
  | Script_typed_ir.Union_key (ty1, _) (ty2, _) _ =>
    let ty1 := get_canonical ty1 in
    let ty2 := get_canonical ty2 in
    Script_typed_ir.Union_key (ty1, None) (ty2, None) Ty_metadata.default
  | Script_typed_ir.Option_key ty _ =>
    let ty := get_canonical ty in
    Script_typed_ir.Option_key ty Ty_metadata.default
  end.

(** Getting a canonical term is involutive. *)
Fixpoint get_canonical_is_involutive ty
  : get_canonical (get_canonical ty) = get_canonical ty.
  destruct ty; try reflexivity; simpl;
    repeat match goal with
    | [x : Script_typed_ir.comparable_ty * _ |- _] => destruct x
    end;
    simpl; now repeat rewrite get_canonical_is_involutive.
Qed.

(** Convert the comparable types to Mi-Cho-Coq for the case of simple
    comparable types. This does not cover all the cases so we use an option
    type. *)
Definition to_Michocoq_simple (ty : Script_typed_ir.comparable_ty)
  : option syntax_type.simple_comparable_type :=
  match ty with
  | Script_typed_ir.Unit_key _ => None
  | Script_typed_ir.Never_key _ => None
  | Script_typed_ir.Int_key _ => Some syntax_type.int
  | Script_typed_ir.Nat_key _ => Some syntax_type.nat
  | Script_typed_ir.Signature_key _ => None
  | Script_typed_ir.String_key _ => Some syntax_type.string
  | Script_typed_ir.Bytes_key _ => Some syntax_type.bytes
  | Script_typed_ir.Mutez_key _ => Some syntax_type.mutez
  | Script_typed_ir.Bool_key _ => Some syntax_type.bool
  | Script_typed_ir.Key_hash_key _ => Some syntax_type.key_hash
  | Script_typed_ir.Key_key _ => None
  | Script_typed_ir.Timestamp_key _ => Some syntax_type.timestamp
  | Script_typed_ir.Chain_id_key _ => None
  | Script_typed_ir.Address_key _ => Some syntax_type.address
  | Script_typed_ir.Pair_key _ _ _ => None
  | Script_typed_ir.Union_key _ _ _ => None
  | Script_typed_ir.Option_key _ _ => None
  end.

(** The [to_Michocoq_simple] function does not depend on the metadata. *)
Lemma to_Michocoq_simple_canonical_eq ty
  : to_Michocoq_simple (get_canonical ty) = to_Michocoq_simple ty.
  destruct ty; try reflexivity; simpl;
    now repeat match goal with
    | [x : _ * _ |- _] => destruct x
    end.
Qed.

(** Import a simple comparable type from Mi-Cho-Coq, using the default
    metadata. *)
Definition of_Michocoq_simple (ty : syntax_type.simple_comparable_type)
  : Script_typed_ir.comparable_ty :=
  match ty with
  | syntax_type.string => Script_typed_ir.String_key Ty_metadata.default
  | syntax_type.nat => Script_typed_ir.Nat_key Ty_metadata.default
  | syntax_type.int => Script_typed_ir.Int_key Ty_metadata.default
  | syntax_type.bytes => Script_typed_ir.Bytes_key Ty_metadata.default
  | syntax_type.bool => Script_typed_ir.Bool_key Ty_metadata.default
  | syntax_type.mutez => Script_typed_ir.Mutez_key Ty_metadata.default
  | syntax_type.address => Script_typed_ir.Address_key Ty_metadata.default
  | syntax_type.key_hash => Script_typed_ir.Key_hash_key Ty_metadata.default
  | syntax_type.timestamp => Script_typed_ir.Timestamp_key Ty_metadata.default
  end.

(** The function [of_Michocoq_simple] produces terms which are already in a
    canonical form. *)
Lemma of_Michocoq_simple_is_canonical ty
  : of_Michocoq_simple ty = get_canonical (of_Michocoq_simple ty).
  destruct ty; reflexivity.
Qed.

(** [to_Michocoq_simple] is the inverse of [of_Michocoq_simple]. *)
Lemma to_Michocoq_simple_of_Michocoq_simple ty
  : to_Michocoq_simple (of_Michocoq_simple ty) = Some ty.
  destruct ty; reflexivity.
Qed.

(** [of_Michocoq_simple] is the inverse of [to_Michocoq_simple]. *)
Lemma of_Michocoq_simple_to_Michocoq_simple ty
  : match to_Michocoq_simple ty with
    | Some ty' => of_Michocoq_simple ty' = get_canonical ty
    | None => True
    end.
  destruct ty; try exact I; reflexivity.
Qed.

(** Convert comparable types to Mi-Cho-Coq. We use an option type for the
    cases which are not covered. *)
Fixpoint to_Michocoq (ty : Script_typed_ir.comparable_ty)
  : option syntax_type.comparable_type :=
  match to_Michocoq_simple ty with
  | Some ty => Some (syntax_type.Comparable_type_simple ty)
  | None =>
    match ty with
    | Script_typed_ir.Pair_key (ty1, _) (ty2, _) _ =>
      let* ty1 := to_Michocoq_simple ty1 in
      let* ty2 := to_Michocoq ty2 in
      Some (syntax_type.Cpair ty1 ty2)
    | _ => None
    end
  end.

(** The [to_Michocoq] function does not depend on the metadata. *)
Fixpoint to_Michocoq_canonical_eq ty
  : to_Michocoq (get_canonical ty) = to_Michocoq ty.
  destruct ty; try reflexivity; simpl;
    repeat match goal with
    | [x : _ * _ |- _] => destruct x
    end; simpl;
    repeat rewrite to_Michocoq_simple_canonical_eq;
    repeat rewrite to_Michocoq_canonical_eq;
    reflexivity.
Qed.

(** Import a comparable type from Mi-Cho-Coq, using the default metadata. *)
Fixpoint of_Michocoq (ty : syntax_type.comparable_type)
  : Script_typed_ir.comparable_ty :=
  match ty with
  | syntax_type.Comparable_type_simple ty => of_Michocoq_simple ty
  | syntax_type.Cpair ty1 ty2 =>
    let ty1 := of_Michocoq_simple ty1 in
    let ty2 := of_Michocoq ty2 in
    Script_typed_ir.Pair_key (ty1, None) (ty2, None) Ty_metadata.default
  end.

(** The function [of_Michocoq] produces terms which are already in a
    canonical form. *)
Fixpoint of_Michocoq_is_canonical ty
  : of_Michocoq ty = get_canonical (of_Michocoq ty).
  destruct ty; simpl;
    rewrite <- of_Michocoq_simple_is_canonical;
    try rewrite <- of_Michocoq_is_canonical;
    reflexivity.
Qed.

(** [to_Michocoq] is the inverse of [of_Michocoq]. *)
Fixpoint to_Michocoq_of_Michocoq ty
  : to_Michocoq (of_Michocoq ty) = Some ty.
  destruct ty; simpl.
  { unfold to_Michocoq.
    destruct (of_Michocoq_simple _) eqn:H; rewrite <- H; clear H;
    now rewrite to_Michocoq_simple_of_Michocoq_simple.
  }
  { rewrite to_Michocoq_simple_of_Michocoq_simple.
    now rewrite to_Michocoq_of_Michocoq.
  }
Qed.

(** [of_Michocoq] is the inverse of [to_Michocoq]. *)
Fixpoint of_Michocoq_to_Michocoq ty
  : match to_Michocoq ty with
    | Some ty' => of_Michocoq ty' = get_canonical ty
    | None => True
    end.
  destruct ty; simpl; trivial.
  repeat match goal with
  | [x : Script_typed_ir.comparable_ty * _ |- _] => destruct x
  end; simpl.
  destruct (to_Michocoq_simple _) eqn:H_simple; simpl; try exact I.
  destruct (to_Michocoq _) eqn:H; simpl; try exact I.
  eassert (H_of_to_simple := of_Michocoq_simple_to_Michocoq_simple _).
  eassert (H_of_to := of_Michocoq_to_Michocoq _).
  rewrite H_simple in H_of_to_simple.
  rewrite H in H_of_to.
  rewrite H_of_to_simple.
  rewrite H_of_to.
  reflexivity.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Storage.

Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_description.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_functors.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_sigs.

(** In this file we give a specification of the storage system. For now, we
    axiomatize many properties. We give a simulation between the storage and
    standard OCaml data structures like records, sets and maps.

    For each sub-store, we give a state and a change type. We compose the state
    types in records, according to the sub-store names. The change types denode
    which changes can be applied to the state, in a way which can be computed
    both in the simulated state and the concrete storage. We give a [reduce]
    function to explicit the way to apply a change on a simulated state.

    Then we assume two functions [parse] and [apply] to go to and from the
    simulated state, and axiomatize that they are compatible.

    Finally, we state that the operations on each sub-store are equivalent to
    direct operations on the simulated state. We also set the sub-stores as
    opaque since from now on we should only use the simulation to reason about
    them. The opacity prevents evaluating the sub-stores when running a [simpl]
    tactic for example.
*)

Lemma Make_index_is_valid {t : Set}
  (domain : t -> Prop) (I : Storage_description.INDEX (t := t)) :
  Storage_description.INDEX.Valid.t domain I ->
  Storage_functors.INDEX.Valid.t (Storage.Make_index I).
  intro H; apply H.
Qed.

Module Big_maps.
  Record State : Set := {
    next : Single_data_storage.State.t Storage.Big_map.id;
  }.
  Definition with_next next (r : State) := {| next := next |}.

  Inductive Change : Set :=
  | Next : Single_data_storage.Change.t Storage.Big_map.id -> Change.

  Definition reduce (state : State) (change : Change) : State :=
    match change with
    | Next change =>
      with_next (Single_data_storage.reduce state.(next) change) state
    end.
End Big_maps.

Module Votes.
  Record State : Set := {
    proposals :
      Data_set_storage.State.t
        (Compare.lexicographic_compare
          Protocol_hash.compare
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare)
        );
    proposals_count :
      Indexed_data_storage.State.t
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) int;
  }.
  Definition with_proposals proposals (r : State) :=
    {| proposals := proposals; proposals_count := r.(proposals_count) |}.
  Definition with_proposals_count proposals_count (r : State) :=
    {| proposals := r.(proposals); proposals_count := proposals_count |}.

  Inductive Change : Set :=
  | Proposals :
    Data_set_storage.Change.t (Protocol_hash.t * Signature.public_key_hash) ->
    Change
  | Proposals_count :
    Indexed_data_storage.Change.t Signature.public_key_hash int -> Change.

  Definition reduce (state : State) (change : Change) : State :=
    match change with
    | Proposals change =>
      with_proposals (Data_set_storage.reduce state.(proposals) change) state
    | Proposals_count change =>
      with_proposals_count
        (Indexed_data_storage.reduce state.(proposals_count) change) state
    end.
End Votes.

Record State : Set := {
  big_maps : Big_maps.State;
  votes : Votes.State;
}.
Definition with_big_maps big_maps (r : State) :=
  {| big_maps := big_maps; votes := r.(votes) |}.
Definition with_votes votes (r : State) :=
  {| big_maps := r.(big_maps); votes := votes |}.

Inductive Change : Set :=
| Big_maps : Big_maps.Change -> Change
| Votes : Votes.Change -> Change.

Definition reduce (state : State) (change : Change) : State :=
  match change with
  | Big_maps change =>
    with_big_maps (Big_maps.reduce state.(big_maps) change) state
  | Votes change =>
    with_votes (Votes.reduce state.(votes) change) state
  end.

Parameter parse : Raw_context.t -> State.

Parameter apply : Raw_context.t -> Change -> Raw_context.t.

Axiom parse_apply : forall ctxt change,
  parse (apply ctxt change) = reduce (parse ctxt) change.

Module Eq.
  Module Big_maps.
    Module Next.
      Definition parse ctxt :=
        (parse ctxt).(big_maps).(Big_maps.next).

      Definition apply ctxt change :=
        apply ctxt (Big_maps (Big_maps.Next change)).

      Axiom eq : Single_data_storage.Eq.t
        Storage.Big_map.Next.Storage
        (Single_data_storage.Make parse apply ["big_maps"; "next"]).

      Global Opaque Storage.Big_map.Next.Storage.
    End Next.
  End Big_maps.

  Module Votes.
    Module Proposals.
      Definition parse ctxt :=
        (parse ctxt).(votes).(Votes.proposals).

      Definition apply ctxt change :=
        apply ctxt (Votes (Votes.Proposals change)).

      Axiom eq : Data_set_storage.Eq.t
        Storage.Vote.Proposals
        (Data_set_storage.Make parse apply).

      Global Opaque Storage.Vote.Proposals.
    End Proposals.

    Module Proposals_count.
      Definition parse ctxt :=
        (parse ctxt).(votes).(Votes.proposals_count).

      Definition apply ctxt change :=
        apply ctxt (Votes (Votes.Proposals_count change)).

      Axiom eq : Indexed_data_storage.Eq.t
        Storage.Vote.Proposals_count
        (Indexed_data_storage.Make parse apply).

      Global Opaque Storage.Vote.Proposals_count.
    End Proposals_count.
  End Votes.
End Eq.

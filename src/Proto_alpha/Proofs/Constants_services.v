Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Constants_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_service.
Require TezosOfOCaml.Proto_alpha.Proofs.Constants_repr.

Module S.
  Lemma errors_is_valid
    : RPC_service.Valid.t (fun _ => True) (fun _ => True)
        Constants_services.S.errors.
        RPC_service.rpc_auto.
  Qed.

  Lemma all_is_valid
    : RPC_service.Valid.t (fun _ => True) Constants_repr.Valid.t
        Constants_services.S.all.
        RPC_service.rpc_auto.
  Qed.
End S.
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Delegate_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_service.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Delegate_storage.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Module info.
  Module Valid.
    Import Proto_alpha.Delegate_services.info.

    Record t (x : Delegate_services.info) : Prop := {
      full_balance : Tez_repr.Valid.t x.(full_balance);
      current_frozen_deposits : Tez_repr.Valid.t x.(current_frozen_deposits);
      frozen_deposits : Tez_repr.Valid.t x.(frozen_deposits);
      staking_balance : Tez_repr.Valid.t x.(staking_balance);
      frozen_deposits_limit :
        match x.(frozen_deposits_limit) with
        | Some v => Tez_repr.Valid.t v
        | None => True
        end;
      delegated_balance : Tez_repr.Valid.t x.(delegated_balance);
    }.
  End Valid.
End info.

Lemma info_encoding_is_valid
  : Data_encoding.Valid.t info.Valid.t Delegate_services.info_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros [] []; simpl in *.
  sauto lq: on.
Qed.
#[global] Hint Resolve info_encoding_is_valid : Data_encoding_db.

Lemma participation_info_encoding_is_valid
  : Data_encoding.Valid.t
    Delegate_storage.participation_info.Valid.t
    Delegate_services.participation_info_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve participation_info_encoding_is_valid : Data_encoding_db.

Module S.
  Lemma list_delegate_is_valid
    : RPC_service.Valid.t (fun _ => True) (fun _ => True)
        Delegate_services.S.list_delegate.
    constructor; trivial with Data_encoding_db.
    Data_encoding.Valid.data_encoding_auto.
    intros; now apply List.Forall_True.
  Qed.

  Lemma info_value_is_valid
    : RPC_service.Valid.t (fun _ => True) info.Valid.t
        Delegate_services.S.info_value.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma full_balance_is_valid
    : RPC_service.Valid.t (fun _ => True) Tez_repr.Valid.t
        Delegate_services.S.full_balance.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma frozen_deposits_is_valid
    : RPC_service.Valid.t (fun _ => True) Tez_repr.Valid.t
        Delegate_services.S.frozen_deposits.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma staking_balance_is_valid
    : RPC_service.Valid.t (fun _ => True) Tez_repr.Valid.t
        Delegate_services.S.staking_balance.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma frozen_deposits_limit_is_valid
    : RPC_service.Valid.t
        (fun _ => True)
        (fun x =>
          match x with
          | Some v => Tez_repr.Valid.t v
          | None => True
          end
        )
        Delegate_services.S.frozen_deposits_limit.
    constructor; Data_encoding.Valid.data_encoding_auto.
  Qed.

  Lemma delegated_contracts_is_valid
    : RPC_service.Valid.t (fun _ => True) (fun _ => True)
        Delegate_services.S.delegated_contracts.
    constructor; trivial with Data_encoding_db.
    Data_encoding.Valid.data_encoding_auto.
    intros; now apply List.Forall_True.
  Qed.

  Lemma delegated_balance_is_valid
    : RPC_service.Valid.t (fun _ => True) Tez_repr.Valid.t
        Delegate_services.S.delegated_balance.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma deactivated_is_valid
    : RPC_service.Valid.t (fun _ => True) (fun _ => True)
        Delegate_services.S.deactivated.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma grace_period_is_valid
    : RPC_service.Valid.t (fun _ => True) (fun _ => True)
        Delegate_services.S.grace_period.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma voting_power_is_valid
    : RPC_service.Valid.t (fun _ => True) (fun _ => True)
        Delegate_services.S.voting_power.
    constructor; trivial with Data_encoding_db.
  Qed.

  Lemma participation_is_valid
    : RPC_service.Valid.t
        (fun _ => True) Delegate_storage.participation_info.Valid.t
        Delegate_services.S.participation.
    constructor; trivial with Data_encoding_db.
  Qed.
End S.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_alpha.Voting_period_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Z.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Lemma string_of_kind_inj : forall x y,
  Voting_period_repr.string_of_kind x = Voting_period_repr.string_of_kind y ->
  x = y.
  intros x y; destruct x, y; easy.
Qed.

Lemma kind_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Voting_period_repr.kind_encoding.
  Data_encoding.Valid.data_encoding_auto;
    intros []; tauto.
Qed.
#[global] Hint Resolve kind_encoding_is_valid : Data_encoding_db.

Lemma succ_kind_inj : forall x y,
  Voting_period_repr.succ_kind x = Voting_period_repr.succ_kind y ->
  x = y.
  intros x y; destruct x, y; easy.
Qed.

Lemma succ_kind_diff : forall x, Voting_period_repr.succ_kind x <> x.
  intro x; destruct x; easy.
Qed.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Voting_period_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma info_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Voting_period_repr.info_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve info_encoding_is_valid : Data_encoding_db.

Lemma compare_is_valid :
    Compare.Valid.t
      (fun period => period.(Voting_period_repr.voting_period.index))
      Voting_period_repr.compare.
  apply
    (Compare.Valid.projection
       (fun period =>
          period.(Voting_period_repr.voting_period.index))
       id
       Z.compare).
  apply Z.compare_is_valid.  
Qed.  

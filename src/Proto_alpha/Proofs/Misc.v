Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Misc.

Lemma take_of_length_app {a : Set} (l1 l2 : list a)
  : Misc.take (List.length l1) (l1 ++ l2) =
    Some (l1, l2).
Admitted.

Lemma take_cases {a : Set} (n : int) (l : list a)
  : match Misc.take n l with
    | Some (l1, l2) => List.length l1 = n /\ l = (l1 ++ l2)%list
    | None => True
    end.
Admitted.

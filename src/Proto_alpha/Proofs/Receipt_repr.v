Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Receipt_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Proofs.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Lemma balance_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Receipt_repr.balance_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve balance_encoding_is_valid : Data_encoding_db.

Lemma is_not_zero_false : Receipt_repr.is_not_zero 0 = false.
  reflexivity.
Qed.

Lemma is_not_zero_true : forall x, x <> 0 -> Receipt_repr.is_not_zero x = true.
  intro x; destruct x; easy.
Qed.

Module Balance.
  Module Valid.
    Definition t :=
      (fun b =>
         match b with
         | Receipt_repr.Debited tz => 0 < tz <= Int64.max_int
         | Receipt_repr.Credited tz => 0 <= tz <= Int64.max_int
         end).
  End Valid.
End Balance.

Lemma balance_update_encoding_is_valid :
  Data_encoding.Valid.t Balance.Valid.t Receipt_repr.balance_update_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros x H; simpl in *; split; trivial; unfold Json.wrap_error.
  destruct x; destruct H; autounfold with tezos_z in *;
    unfold normalize_int64, two_pow_63, two_pow_64; simpl;
    repeat match goal with
    | |- context[if ?e then _ else _] => destruct e eqn:?
    end;
    hauto lq: on rew: off solve: lia.
Qed.
#[global] Hint Resolve balance_update_encoding_is_valid : Data_encoding_db.

Definition cuo_index (o_value : Receipt_repr.update_origin) : int :=
    match o_value with
    | Receipt_repr.Block_application => 0
    | Receipt_repr.Protocol_migration => 1
    | Receipt_repr.Subsidy => 2
    | Receipt_repr.Simulation => 3
    end.

Lemma compare_update_origin_is_valid :
  Compare.Valid.t cuo_index Receipt_repr.compare_update_origin.
  apply (Compare.Valid.projection cuo_index id Z.compare).
  exact Compare.Valid.z.
Qed.
 
Lemma update_origin_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Receipt_repr.update_origin_encoding.
  Data_encoding.Valid.data_encoding_auto;
  intro x; destruct x; trivial; tauto.
Qed.
#[global] Hint Resolve update_origin_encoding_is_valid : Data_encoding_db.

Module Balance_updates.
  Module Valid.
    Definition t (balance_updates : list
                                      (Receipt_repr.balance *
                                       Receipt_repr.balance_update *
                                       Receipt_repr.update_origin)) : Prop :=
      List.Forall (fun '(_, bu, _) => Balance.Valid.t bu) balance_updates.
  End Valid.
End Balance_updates.

Lemma balance_updates_encoding_is_valid :
  Data_encoding.Valid.t
    Balance_updates.Valid.t
    Receipt_repr.balance_updates_encoding.
  Data_encoding.Valid.data_encoding_auto;
  intros xs Hv; apply Forall_forall;
  assert(H : (forall x0, In x0 xs ->
                    (fun '(_, bu, _) => Balance.Valid.t bu) x0))
    by (apply Forall_forall; trivial);
  now intros x Hin; apply H in Hin; destruct x, p.
Qed.
#[global] Hint Resolve balance_updates_encoding_is_valid : Data_encoding_db.

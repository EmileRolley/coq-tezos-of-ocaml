Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Option.

(** We put the saturation operations which can be trivially converted to their
    Z equivalent into the [tezos_z] hint database. *)
Module Valid.
  Definition t (x : Saturation_repr.t) : Prop :=
    0 <= x <= Saturation_repr.saturated.

  Lemma decide {x} :
    (0 <=? x) && (x <=? Saturation_repr.saturated) = true ->
    t x.
    rewrite Bool.andb_true_iff.
    unfold t; lia.
  Qed.
End Valid.

Module Strictly_valid.
  Definition t (x : Saturation_repr.t) : Prop :=
    0 <= x < Saturation_repr.saturated.

  Lemma implies_valid (x : Saturation_repr.t) : t x -> Valid.t x.
    unfold t, Valid.t, Saturation_repr.saturated.
    autounfold with tezos_z; lia.
  Qed.
End Strictly_valid.

Module Unfold.
  Import Saturation_repr.

  Global Hint Unfold
    Valid.t
    Strictly_valid.t
    add
    compare
    equal
    erem
    may_saturate_value
    mul
    mul_fast
    mul_safe_value
    of_int_opt
    one
    op_eq
    op_gt
    op_gteq
    op_lt
    op_lteq
    op_ltgt
    saturated
    sub
    to_int
    to_z
    zero
    : tezos_z.
End Unfold.

Lemma saturation_valid_implies_int_valid x :
  Valid.t x ->
  Pervasives.Int.Valid.t x.
  Utils.tezos_z_autounfold.
  simpl. unfold Z.compare, "-Z". intros. lia.
Qed.

Lemma max_is_valid (x y : Saturation_repr.t)
  : Valid.t x -> Valid.t y -> Valid.t (Saturation_repr.max x y).
  intros.
  unfold Saturation_repr.max.
  now destruct (Saturation_repr.op_gteq _ _).
Qed.

Lemma max_eq x y : Saturation_repr.max x y = Z.max x y.
  unfold Saturation_repr.max.
  autounfold with tezos_z; cbn; autounfold with tezos_z.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H
  end; lia.
Qed.

Lemma min_is_valid (x y : Saturation_repr.t)
  : Valid.t x -> Valid.t y -> Valid.t (Saturation_repr.min x y).
  intros.
  unfold Saturation_repr.min.
  now destruct (Saturation_repr.op_gteq _ _).
Qed.

Lemma min_eq x y : Saturation_repr.min x y = Z.min x y.
  unfold Saturation_repr.min.
  autounfold with tezos_z; cbn; autounfold with tezos_z.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H
  end; lia.
Qed.

Lemma saturated_is_valid : Valid.t Saturation_repr.saturated.
  autounfold with tezos_z; lia.
Qed.

Lemma of_int_opt_is_strictly_valid (i : int) :
  match Saturation_repr.of_int_opt i with
  | Some i => Strictly_valid.t i
  | None => True
  end.
  unfold Saturation_repr.of_int_opt, "&&".
  autounfold with tezos_z.
  destruct (_ && _)%bool eqn:H_eq; simpl; trivial.
  cbn in *; autounfold with tezos_z in *.
  lia.
Qed.

Lemma of_int_opt_eq (i : int) :
  Strictly_valid.t i ->
  Saturation_repr.of_int_opt i = Some i.
  autounfold with tezos_z; simpl.
  destruct (op_andand _ _) eqn:?; try reflexivity.
  rewrite Bool.andb_false_iff in *.
  lia.
Qed.

Lemma of_z_opt_is_strictly_valid (z : Z.t) :
  match Saturation_repr.of_z_opt z with
  | Some z => Strictly_valid.t z
  | None => True
  end.
  apply of_int_opt_is_strictly_valid.
Qed.

Lemma of_z_opt_eq (z : Z.t) :
  Saturation_repr.Strictly_valid.t z ->
  Saturation_repr.of_z_opt z = Some z.
  intro H.
  unfold Saturation_repr.of_z_opt, to_int.
  rewrite Pervasives.normalize_identity.
  { now apply of_int_opt_eq. }
  { now apply saturation_valid_implies_int_valid,
      Strictly_valid.implies_valid.
  }
Qed.

Lemma safe_int_is_valid x : Valid.t (Saturation_repr.safe_int x).
  unfold Saturation_repr.safe_int, Saturation_repr.saturate_if_undef.
  assert (H := of_int_opt_is_strictly_valid x).
  destruct (Saturation_repr.of_int_opt _).
  { now apply Strictly_valid.implies_valid. }
  { exact saturated_is_valid. }
Qed.

Axiom small_enough_eq :
  forall x, Saturation_repr.small_enough x = (x <=i 2147483647).
Hint Rewrite small_enough_eq : tezos_z.

(* @TODO prove this *)
Axiom mul_safe_exn : 
  forall (x : Saturation_repr.t),  Saturation_repr.Valid.t x -> Saturation_repr.mul_safe_exn x = x.
Hint Rewrite mul_safe_exn : tezos_z.

(* @TODO prove this *)
Axiom mul_safe_of_int_exn_eq : 
  forall (x : Saturation_repr.t), Saturation_repr.Valid.t x -> Saturation_repr.mul_safe_of_int_exn x = x.
Hint Rewrite mul_safe_of_int_exn_eq : tezos_z.

Lemma t_to_z_exn_to_z x :
  Saturation_repr.Strictly_valid.t x ->
  Saturation_repr.t_to_z_exn (Saturation_repr.to_z x) = x.
  intro.
  unfold Saturation_repr.to_z, of_int.
  unfold Saturation_repr.t_to_z_exn.
  now rewrite of_z_opt_eq.
Qed.

Lemma add_valid : forall (x y : int),
    Saturation_repr.Valid.t (Saturation_repr.add x y).
  Utils.tezos_z_autounfold. simpl. intros.
  match goal with 
    [|- context[if ?e then _ else _]] => destruct e eqn:Eq_x
  end; try lia.
Qed.

(* @TODO Prove this using 
 * Pervasives.Int.Valid.t x -> 
 * Pervasives.Int.Valid.t y -> *)
Lemma sub_valid : forall (x y : int),
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t y ->
    Saturation_repr.Valid.t (Saturation_repr.sub x y).
  repeat (Utils.tezos_z_autounfold; simpl); lia.
Qed.

(** A simple check that small_enough bounds do not saturate *)
Fact mul_small_enough_bounds_do_not_saturate: 
  0 * 0 <= Saturation_repr.saturated /\ 2147483647 * 2147483647 <= Saturation_repr.saturated.
  autounfold with tezos_z; lia.
Qed.

(* @TODO  prove this *)
Axiom mul_safe_do_not_saturate : forall {x : Saturation_repr.t} {y : Saturation_repr.t},
  (y <=? 2147483647)%bool = true \/
  (y >? Saturation_repr.saturated /i x)%bool = false ->
  ((0 <=? x *i y) && (x *i y <=? 4611686018427387903))%bool = true.

Lemma saturation_repr_valid_eq : forall (x: int),
  Saturation_repr.Valid.t x <->
  (((0 <=? x) && (x <=? 4611686018427387903))%bool = true).
  Utils.tezos_z_autounfold; lia.
Qed.
Hint Rewrite saturation_repr_valid_eq : tezos_z.

(* @TODO Prove this using 
 * Pervasives.Int.Valid.t x -> 
 * Pervasives.Int.Valid.t y -> *)
Lemma mul_is_valid : forall (x y : int),
  Saturation_repr.Valid.t x ->
  Saturation_repr.Valid.t y ->
  Saturation_repr.Valid.t (Saturation_repr.mul x y).
  Utils.tezos_z_autounfold; simpl.
  intros x y Hx Hy.
  repeat rewrite small_enough_eq; simpl.
  (* Even if [x] appears first, we destruct it last to avoid generating two
     distinct similar cases for positive and negative integers. *)
  destruct (_ && _) eqn:?.
  { rewrite Bool.andb_true_iff in *.
    assert (0 <= x * y) by lia.
    assert (x * y <= 2147483647 * 2147483647) by (apply Zmult_le_compat; lia).
    destruct x; lia.
  }
  { rewrite Bool.andb_false_iff in *.
    destruct (_ >? _) eqn:?; [destruct x; lia|].
    assert (0 <= Pervasives.max_int / x) by (autounfold with tezos_z; lia).
    assert (Pervasives.max_int / x <= Pervasives.max_int)
      by (autounfold with tezos_z; apply Z.div_le_upper_bound; lia).
    assert (Pervasives.Int.Valid.t (Pervasives.max_int / x))
      by (autounfold with tezos_z in *; lia).
    assert (H_yx : y <= Pervasives.normalize_int (Pervasives.max_int / x))
      by (Utils.tezos_z_autounfold; lia).
    rewrite Pervasives.normalize_identity in H_yx; trivial.
    assert (Pervasives.Int.Valid.t (x * y)).
    Utils.tezos_z_autounfold.
    { split; [lia|].
      assert (x * y <= x * (Pervasives.max_int / x))
        by (apply Zmult_le_compat; lia).
      assert (x * (Pervasives.max_int / x) <= Pervasives.max_int) by lia.
      unfold Pervasives.max_int in *; lia.
    }
    { assert (H_xy : 0 <= x * y <= Pervasives.max_int)
        by (autounfold with tezos_z in *; lia).
      destruct x; trivial;
        now rewrite <- (Pervasives.normalize_identity (x := (_ * y)%Z)) in H_xy.
    }
  }
Qed.

Lemma scale_fast_valid : forall (x : Saturation_repr.t) (y : Saturation_repr.t),
  Saturation_repr.Valid.t x ->
  Saturation_repr.Valid.t y ->
  Saturation_repr.Valid.t (Saturation_repr.scale_fast x y).

  intros x y.
  autorewrite with tezos_z.
  intros Hx Hy.

  Utils.tezos_z_autounfold.
  unfold Saturation_repr.scale_fast.
  autorewrite with tezos_z; try (autounfold with tezos_z; lia).

  match goal with 
    [|- context[if ?e then _ else _]] => destruct e eqn:Eq_x
  end; try lia.

  match goal with 
    [|- context[if ?e then _ else _]] => destruct e eqn:Eq_y
  end; try Utils.tezos_z_auto.
  apply mul_safe_do_not_saturate. left; assumption.

  match goal with 
    [|- context[if ?e then _ else _]] => destruct e eqn:Eq_y_lt
  end; try Utils.tezos_z_auto.
  apply mul_safe_do_not_saturate; right; assumption.
Qed.

Lemma z_encoding_is_valid :
  Data_encoding.Valid.t Strictly_valid.t Saturation_repr.z_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros; simpl; repeat split.
  now apply Saturation_repr.t_to_z_exn_to_z.
Qed.
#[global] Hint Resolve z_encoding_is_valid : Data_encoding_db.

Axiom shift_right_is_valid : forall a b, Valid.t (Saturation_repr.shift_right a b).

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Vote_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Lemma ballot_encoding_is_valid
  : Data_encoding.Valid.t (fun _ => True) Vote_repr.ballot_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros [] ?; cbv - ["<="]; intuition lia.
Qed.
#[global] Hint Resolve ballot_encoding_is_valid : Data_encoding_db.

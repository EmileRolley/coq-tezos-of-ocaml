Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require TezosOfOCaml.Proto_alpha.Proofs.Utils.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.

#[global] Hint Unfold
  Gas_limit_repr.Arith.add
  Gas_limit_repr.Arith.op_eq
  Gas_limit_repr.Arith.scaling_factor
  Gas_limit_repr.Arith.sub
  Gas_limit_repr.Arith.zero
  Gas_limit_repr.S.add
  Gas_limit_repr.S.erem
  Gas_limit_repr.S.sub
  Gas_limit_repr.S.mul
  Gas_limit_repr.scaling_factor
  _mod
  : tezos_z.

Lemma scaling_factor_eq : Gas_limit_repr.scaling_factor = 1000.
  unfold Gas_limit_repr.scaling_factor.
  autorewrite with tezos_z; autounfold with tezos_z; simpl; lia.
Qed.
Hint Rewrite scaling_factor_eq : tezos_z.

Module Arith.
  (** TODO This should be proved but some of the functions (land for ex) are
      still opaque. *)
  Axiom integral_exn_integral_to_z : forall {v_a},
    Gas_limit_repr.Arith.integral_exn (Gas_limit_repr.Arith.integral_to_z v_a) =
    v_a.

  (* @TODO *)
  Axiom integral_of_int_exn_eq : forall {i : int},
    Gas_limit_repr.Arith.integral_of_int_exn i = i.

  (* @TODO *)
  Axiom integral_exn_eq : forall {z : Z.t},
    Gas_limit_repr.Arith.integral_exn z = z.

  (* @TODO *)
  Axiom integral_to_z_eq : forall {x},
    Gas_limit_repr.Arith.integral_to_z x = x.

  (* @TODO *)
  Axiom usafe_fp_eq : forall {x},
    Gas_limit_repr.Arith.unsafe_fp x = Z.to_int x.

  Lemma z_integral_encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Gas_limit_repr.Arith.z_integral_encoding.
    eapply Data_encoding.Valid.implies.
    eapply
      Data_encoding.Valid.conv,
      Data_encoding.Valid.z_value.
    intuition; now rewrite integral_exn_integral_to_z.
  Qed.
  #[global] Hint Resolve z_integral_encoding_is_valid : Data_encoding_db.

  Lemma n_integral_encoding_is_valid : 
    Data_encoding.Valid.t (fun _ => True) Gas_limit_repr.Arith.n_integral_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros x HTrue. simpl.  split.
    { constructor. }
    apply integral_exn_integral_to_z.
  Qed.
  #[global] Hint Resolve n_integral_encoding_is_valid : Data_encoding_db.

  Lemma ceil_is_valid : forall {x},
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t (Gas_limit_repr.Arith.ceil x).
    unfold Gas_limit_repr.Arith.ceil.
    intros.
    match goal with 
    | [|- context [ if ?e then _ else _] ] => destruct e eqn:Eq_1
    end; try assumption.

    unfold Gas_limit_repr.Arith.add.
    apply Saturation_repr.add_valid; try assumption.
  Qed.

  Lemma floor_is_valid : forall {x},
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t (Gas_limit_repr.Arith.floor x).

    unfold Gas_limit_repr.Arith.floor.
    intros.
    apply Saturation_repr.sub_valid; try assumption.

    revert H.
    Utils.tezos_z_autounfold. simpl. autounfold with tezos_z.
    autorewrite with tezos_z; autounfold with tezos_z; lia.
  Qed.
End Arith.

Module Cost.
  Module Valid.
    Definition t (x : Gas_limit_repr.cost) : Prop :=
      Saturation_repr.Valid.t x.
  End Valid.
End Cost.

Lemma S_z_encoding_is_valid :
  Data_encoding.Valid.t Saturation_repr.Strictly_valid.t
    Gas_limit_repr.S.z_encoding.
  apply Saturation_repr.z_encoding_is_valid.
Qed.
#[global] Hint Resolve S_z_encoding_is_valid : Data_encoding_db.

Lemma Arith_z_fp_encoding_is_valid :
  Data_encoding.Valid.t Saturation_repr.Strictly_valid.t
    Gas_limit_repr.Arith.z_fp_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve Arith_z_fp_encoding_is_valid : Data_encoding_db.

Module Valid.
  Definition t (x : Gas_limit_repr.t) : Prop :=
    match x with
    | Gas_limit_repr.Limited {|
        Gas_limit_repr.t.Limited.remaining := remaining
      |} => Saturation_repr.Strictly_valid.t remaining
    | Gas_limit_repr.Unaccounted => True
    end.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t Gas_limit_repr.encoding.
  unfold Gas_limit_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma cost_encoding_is_valid : 
  Data_encoding.Valid.t Saturation_repr.Strictly_valid.t Gas_limit_repr.cost_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve cost_encoding_is_valid : Data_encoding_db.

Ltac gas_limit_repr_auto :=
  repeat (match goal with 
    |[|- Saturation_repr.Valid.t ?x ] => unfold x
    |[|- Saturation_repr.Valid.t (?f _) ] => unfold f
  end);
  Utils.tezos_z_autounfold; simpl;
    autounfold with tezos_z; autorewrite with tezos_z;
    first [reflexivity | Utils.tezos_z_auto].

Lemma allocation_weight_is_valid : Saturation_repr.Valid.t Gas_limit_repr.allocation_weight.
  gas_limit_repr_auto.
Qed.

Lemma step_weight_is_valid : Saturation_repr.Valid.t Gas_limit_repr.step_weight.
  gas_limit_repr_auto.
Qed.

Lemma read_base_weight_valid : Saturation_repr.Valid.t Gas_limit_repr.read_base_weight. 
  gas_limit_repr_auto.
Qed.

Lemma write_base_weight_valid : Saturation_repr.Valid.t Gas_limit_repr.write_base_weight.
  gas_limit_repr_auto.
Qed.

Lemma byte_read_weight_valid : Saturation_repr.Valid.t Gas_limit_repr.byte_read_weight.
  gas_limit_repr_auto.
Qed.

Lemma byte_written_weight_valid : Saturation_repr.Valid.t Gas_limit_repr.byte_written_weight.
  gas_limit_repr_auto.
Qed.

Lemma cost_to_milligas_valid : forall {x}, Saturation_repr.Valid.t x -> Saturation_repr.Valid.t (Gas_limit_repr.cost_to_milligas x).
  unfold Gas_limit_repr.cost_to_milligas; intros;assumption.
Qed.

Lemma raw_consume_is_valid gas_counter cost
    : cost <= gas_counter ->
    Saturation_repr.Valid.t cost ->
    Saturation_repr.Valid.t gas_counter ->
    Option.post_when_success (Gas_limit_repr.raw_consume gas_counter cost)
      (fun gas_counter' =>
        Gas_limit_repr.Arith.op_lteq gas_counter' gas_counter = true).
  unfold Option.post_when_success. 
  unfold Gas_limit_repr.raw_consume.
  unfold Gas_limit_repr.Arith.sub_opt.
  unfold Gas_limit_repr.cost_to_milligas.
  unfold Gas_limit_repr.S.sub_opt.
  unfold Cost.Valid.t.
  unfold Saturation_repr.Valid.t.
  unfold Saturation_repr.saturated, Pervasives.max_int.
  cbn; autounfold with tezos_z.
  unfold Pervasives.normalize_int, two_pow_62, two_pow_63, two_pow_62.
  repeat rewrite Zminus_0_r.
  simpl. unfold Z.compare, Z.sub.
  intros.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H_eq
  end; try constructor.
  lia.
Qed.

Lemma alloc_cost_valid : forall {x},
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t (Gas_limit_repr.alloc_cost x).

  intros. 
  unfold Gas_limit_repr.alloc_cost, 
    Gas_limit_repr.allocation_weight, 
    Gas_limit_repr.scaling_factor.

  apply Saturation_repr.scale_fast_valid; try Utils.tezos_z_auto.
  Utils.tezos_z_autounfold.
  autorewrite with tezos_z; try Utils.tezos_z_auto; try reflexivity;
  try autorewrite with tezos_z; try reflexivity;
  try Utils.tezos_z_auto.

  apply Saturation_repr.add_valid.
Qed.

Lemma alloc_bytes_valid : forall {x},
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t (Gas_limit_repr.alloc_bytes_cost x).
  intros. 
  unfold Gas_limit_repr.alloc_bytes_cost.
  apply alloc_cost_valid.
  apply Saturation_repr.safe_int_is_valid.
Qed.

Lemma atomic_step_cost_valid : forall {x},
  Saturation_repr.Valid.t x -> 
  Saturation_repr.Valid.t (Gas_limit_repr.atomic_step_cost x).
  intro x. 
  unfold Gas_limit_repr.atomic_step_cost, Gas_limit_repr.S.may_saturate_value.
  autounfold with tezos_z; trivial.
Qed.

Lemma step_cost_valid : forall {x},
  Saturation_repr.Valid.t x ->
  Saturation_repr.Valid.t (Gas_limit_repr.step_cost x).
  unfold Gas_limit_repr.step_cost, Gas_limit_repr.step_cost,
    Gas_limit_repr.step_weight, Gas_limit_repr.scaling_factor.
  autounfold with tezos_z.
  intros.
  apply Saturation_repr.scale_fast_valid; try (autounfold with tezos_z; lia).
  autorewrite with tezos_z; try (autounfold with tezos_z; lia);
  reflexivity.
Qed.

Lemma free_valid : Saturation_repr.Valid.t Gas_limit_repr.free.
  unfold Gas_limit_repr.free.
  Utils.tezos_z_auto.
Qed.

Lemma read_bytes_cost_valid : forall {x},
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t (Gas_limit_repr.read_bytes_cost x).

  unfold Gas_limit_repr.read_bytes_cost, 
    Gas_limit_repr.byte_read_weight,
    Gas_limit_repr.read_base_weight,
    Gas_limit_repr.scaling_factor.

  Utils.tezos_z_autounfold; autorewrite with tezos_z; try reflexivity; try Utils.tezos_z_auto.
  intros.
  apply Saturation_repr.add_valid.
Qed.

Lemma write_bytes_cost_valid : forall {x : int},
    Saturation_repr.Valid.t x ->
    Saturation_repr.Valid.t (Gas_limit_repr.write_bytes_cost x).

  unfold Gas_limit_repr.write_bytes_cost,
    Gas_limit_repr.write_base_weight,
    Gas_limit_repr.byte_written_weight, 
    Gas_limit_repr.scaling_factor.

  autorewrite with tezos_z; try Utils.tezos_z_auto; try reflexivity.
  autounfold with tezos_z.
  intros.
  apply Saturation_repr.add_valid.
Qed.

Lemma op_plusat_valid : forall {x y},
  Saturation_repr.Valid.t x ->
  Saturation_repr.Valid.t y ->
  Saturation_repr.Valid.t (Gas_limit_repr.op_plusat x y).
  intros.
  apply Saturation_repr.add_valid.
Qed.

Lemma op_starat_valid : forall {x y},
  Saturation_repr.Valid.t x ->
  Saturation_repr.Valid.t y ->
  Saturation_repr.Valid.t (Gas_limit_repr.op_starat x y).

  intros x y. unfold Gas_limit_repr.op_starat.
  apply Saturation_repr.mul_is_valid.
Qed.
  
Lemma alloc_mbytes_cost_is_valid : forall {x},
  Saturation_repr.Valid.t x ->
  Saturation_repr.Valid.t (Gas_limit_repr.alloc_mbytes_cost x).
  intros.
  unfold Gas_limit_repr.alloc_mbytes_cost.
  apply op_plusat_valid.
  { apply alloc_cost_valid. autorewrite with tezos_z; try reflexivity; try Utils.tezos_z_auto. }
  { unfold Gas_limit_repr.alloc_bytes_cost.
    apply alloc_cost_valid. 
    apply Saturation_repr.safe_int_is_valid.
  }
Qed.

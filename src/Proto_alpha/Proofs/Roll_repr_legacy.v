Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Proofs.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_level_repr.

Lemma compare_is_valid : Compare.Valid.t id Roll_repr_legacy.compare.
  apply Compare.Valid.int32.
Qed.

Module Valid.
  Definition t (v : int32) : Prop := v >=i32 0 = true.
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Roll_repr_legacy.encoding.
  Data_encoding.Valid.data_encoding_auto.
  intro x; destruct x; simpl; auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma rpc_arg_valid : RPC_arg.Valid.t Valid.t Roll_repr_legacy.rpc_arg.
  eapply RPC_arg.Valid.implies.
  apply RPC_arg.Valid.like.
  apply RPC_arg.Valid.uint31.
  intro x; destruct x; try reflexivity.
  intro H; inversion H.
Qed.

Lemma index_path_encoding_is_valid :
  Path_encoding.S.Valid.t
    (Storage_description.INDEX.to_Path Roll_repr_legacy.Index).
  constructor; try reflexivity.
  - intro v; apply Int32.of_string_opt_to_string.
  - intro path; simpl; unfold Roll_repr_legacy.Index.of_path;
    destruct path; auto; destruct path; auto;
    destruct (Int32.of_string_opt s) eqn:E; auto;
    unfold Roll_repr_legacy.Index.to_path;
    specialize (Int32.to_string_of_string_opt s);
    rewrite E; intro H; rewrite H; reflexivity.
Qed.

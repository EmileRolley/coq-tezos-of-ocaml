Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Int64.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Utils.

Module Valid.
  Definition t (tz : Tez_repr.t) :=
    0 <= tz <= Int64.max_int.
End Valid.

#[global] Hint Unfold
     Tez_repr.op_minusquestion
     Tez_repr.op_plusquestion
     Tez_repr.op_starquestion
     Tez_repr.op_divquestion     
     Tez_repr.sub_opt
     Tez_repr.of_mutez
     Tez_repr.to_mutez
     Tez_repr.op_lteq
     Tez_repr.op_lt
     Tez_repr.op_eq
     Tez_repr.op_gt
     Tez_repr.of_string
     Valid.t
     Z.gtb
     max_int
     min_int
     normalize_int64     
     div
     Int64.Valid.t
  : tezos_z.

Lemma op_minusquestion_is_valid : forall {t1 t2 : Tez_repr.tez},
    Valid.t t1 ->
    Valid.t t2 ->
    match Tez_repr.op_minusquestion t1 t2 with
    | Pervasives.Ok t => t1 >= t2 /\ Valid.t t /\ t = t1 - t2
    | Pervasives.Error _ => t1 < t2
    end.
  autounfold with tezos_z; simpl; intros t1 t2 H H0;
  destruct (Z.leb_spec0 t2 t1); simpl; repeat split; lia.
Qed.

Lemma sub_opt_is_valid : forall {t1 t2 : Tez_repr.tez},
    Valid.t t1 ->
    Valid.t t2 ->
    match Tez_repr.sub_opt t1 t2 with
    | Some t => t1 >= t2 /\ Valid.t t /\ t = t1 - t2
    | None => t1 < t2
    end.
  autounfold with tezos_z; simpl; intros t1 t2 [Ht11 Ht12] [Ht21 Ht22];
  destruct (Z.leb_spec0 t2 t1); simpl; repeat split; lia.
Qed.

Lemma op_plusquestion_is_valid : forall {t1 t2 : Tez_repr.tez},
    Valid.t t1 ->
    Valid.t t2 ->
    match Tez_repr.op_plusquestion t1 t2 with
    | Pervasives.Ok t => Valid.t t
    | Pervasives.Error _ => t1 + t2 > Int64.max_int
    end.
  autounfold with tezos_z; simpl; intros t1 t2 H H0; destruct H, H0;
  remember
    ((t1 + t2 + 9223372036854775808) mod 18446744073709551616
      - 9223372036854775808) as t3;
  destruct (Z.ltb_spec0 t3 t1); simpl; repeat split; lia.
Qed.

Lemma mul_pos_is_valid : forall {t1 t2 : BinNums.Z},
    0 <= t1 <= Int64.max_int ->
    0 < t2 <= Int64.max_int ->
    t1 <= Int64.max_int / t2 -> Int64.Valid.t (t1 * t2)%Z.
  intros t1 t2 [Ht11 Ht12] [Ht21 Ht22] H__t1t3;
    unfold Int64.Valid.t, min_int; split; [lia|];
    replace (t1 * t2)%Z with (t2 * t1)%Z by lia;
    replace max_int with (max_int * 1)%Z by lia;
    replace 1 with (t2 / t2) by (apply Z_div_same; lia).
    assert(t2 * t1 <= t2 * (max_int / t2))
      by (apply Zmult_le_compat_l; [assumption|lia]).
    assert(t2 * (max_int / t2) <= max_int * (t2 / t2))
      by (
          replace (max_int * (t2 / t2))%Z
            with (max_int * t2 / t2)%Z
            by (rewrite Z_div_mult, Z_div_same; lia);
          replace (max_int * t2)%Z
            with (t2 * max_int)%Z by lia;
          apply Zdiv_mult_le; lia
        ); lia.
Qed.

Lemma op_starquestion_is_valid : forall {t1 t2 : Tez_repr.tez},
    Valid.t t1 ->
    Valid.t t2 ->
    (t2 = 0 /\
     Tez_repr.op_starquestion t1 t2 = Pervasives.Ok 0 /\ Valid.t 0) \/
    (t2 <> 0 /\
     match Tez_repr.op_starquestion t1 t2 with
     | Pervasives.Ok t => Valid.t t /\ t1 <= (Int64.max_int / t2)
     | Pervasives.Error _ => t1 > Int64.max_int / t2
     end).
  autounfold with tezos_z; simpl; intros t1 t2 [Ht11 Ht12] [Ht21 Ht22];
    destruct (Z.eq_decidable t2 0).
  - left; split; [assumption|]; split; [|lia];
    destruct (Z.ltb_spec0 t2 0); [lia|];
    destruct (Z.eqb_spec t2 0); [reflexivity|lia].
  - right; split; [assumption|];
    destruct (Z.ltb_spec0 t2 0); simpl; repeat split; simpl in *; [lia|];
    destruct (Z.compare_spec t2 0); [subst;rewrite Z.eqb_refl; split|..];
    [lia..|]; destruct (Z.eqb_spec t2 0); simpl; [split; lia|].
    autounfold with tezos_z; remember
       ((9223372036854775807 / t2 + 9223372036854775808) mod
         18446744073709551616 - 9223372036854775808) as t3.

    (* hypothesis name includes t3 due to equality below *)
    assert(Hv_t3 : Int64.Valid.t (9223372036854775807 / t2)).
    { autounfold with tezos_z; split; [lia|];
      apply Z.div_le_upper_bound; [lia|];
      replace 9223372036854775807
        with ((1 * 9223372036854775807)%Z) at 1 by lia;
      apply Zmult_le_compat_r; lia.
    }
    assert(Heq_t3 : t3 = 9223372036854775807 / t2).
    { specialize (Int64.normalize_identity (9223372036854775807 / t2)) as Hni;
        subst t3; apply Hni; assumption.
    }
    clear Heqt3.

    destruct (Z.le_decidable t1 t3).
    + assert(Ht1 : 0 <= t1 <= max_int) by (autounfold with tezos_z; lia);
      assert(Ht2 : 0 < t2 <= max_int) by (autounfold with tezos_z; lia);  
      rewrite Heq_t3 in H1; specialize (mul_pos_is_valid Ht1 Ht2 H1) as Hv__t1t2;
      specialize (Int64.normalize_identity (t1 * t2)%Z Hv__t1t2) as Hni;
      autounfold with tezos_z in *; rewrite Hni; clear Hni;
      destruct (Z.compare_spec t1 t3); simpl; [split .. |]; lia.
    + destruct (Z.compare_spec t1 t3); simpl; lia.
Qed.

Lemma op_divquestion_is_valid : forall {t1 t2 : Tez_repr.tez},
    Valid.t t1 ->
    Valid.t t2 ->
    match Tez_repr.op_divquestion t1 t2 with
    | Pervasives.Ok t => t2 <> 0 /\ Valid.t t
    | Pervasives.Error _ => t2 = 0
    end.
  autounfold with tezos_z; simpl; intros t1 t2 [Ht11 Ht12] [Ht21 Ht22].
  destruct (_ <=? _) eqn:?; simpl; [lia|].
  split; [lia|].
  assert (0 <= t1 / t2 <= t1).
  { split. 
    { apply Z.div_pos; lia. }
    { apply Z.div_le_upper_bound; [lia|].
      replace t1 with ((1 * t1)%Z) at 1 by lia.
      apply Zmult_le_compat_r; lia.
    }
  }
  {
    assert (Int64.Valid.t (t1 / t2))
      by (autounfold with tezos_z; lia).
    assert (H_t1t2 := Int64.normalize_identity (t1 / t2)).
    autounfold with tezos_z in *; lia.
  }
Qed.

Lemma of_mutez_is_valid : forall {t1 : Tez_repr.tez},
    Valid.t t1 ->
    Tez_repr.of_mutez t1 = Some t1.
  autounfold with tezos_z; intros t1 [H1 H2]; simpl.
  destruct (Z.ltb_spec t1 0); auto; lia.
Qed.

Lemma of_string_is_valid : forall (s_value : string),
    match Tez_repr.of_string s_value with
    | Some t => Int64.Valid.t t
    | None => True
    end.
  intros.
  unfold Tez_repr.of_string.
  destruct (String.split_on_char _ _); [easy|].
  now repeat (
    try match goal with
    | l : list string |- context[match l with _ => _ end] => destruct l
    end;
    try match goal with
    | |- context[if ?e then _ else _] => destruct e
    end;
    try apply Int64.of_string_opt_is_valid
  ).
Qed.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t Tez_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
  simpl.
  unfold Valid.t, Json.wrap_error, to_int64, of_int64.
  Utils.tezos_z_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

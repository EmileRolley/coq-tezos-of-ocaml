Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Slot_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Valid.
  Definition t (s : Slot_repr.t) : Prop :=
    0 <= s <= Pervasives.UInt16.max.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t Slot_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Axiom zero_is_valid : Valid.t Slot_repr.zero.

Axiom succ_is_valid : forall s,
  Valid.t s ->
  s <> Pervasives.UInt16.max ->
  Valid.t (Slot_repr.succ s).

Axiom max_value_is_valid : Valid.t Slot_repr.max_value.

Axiom max_value_eq : Slot_repr.max_value = Pervasives.UInt16.max.

Axiom of_int_eq : forall i,
  Valid.t i ->
  Slot_repr.of_int_exn i = i.

Module List.
  Module Valid.
    Fixpoint at_least_n (n : int) (l : list int) : Prop :=
      match l with
      | [] => True
      | m :: l' => n < m /\ at_least_n m l'
      end.

    Record t (l : list int) : Prop := {
      strictly_increasing :
        match l with
        | [] => True
        | n :: l' => at_least_n n l'
        end;
      slots : List.Forall Slot_repr.Valid.t l;
    }.
  End Valid.

  Module Compressed.
    Module Elt.
      Module Valid.
        Import Slot_repr.List.Compressed.elt.

        Record t (e : Slot_repr.List.Compressed.elt) : Prop := {
          skip : Slot_repr.Valid.t e.(skip);
          take : Slot_repr.Valid.t e.(take);
        }.
      End Valid.
    End Elt.

    Axiom elt_encoding_is_valid :
      Data_encoding.Valid.t Elt.Valid.t Slot_repr.List.Compressed.elt_encoding.
  End Compressed.

  (** TODO: make preliminary axioms about Compressed.encode and
      Compressed.decode being inverse functions. *)
  Axiom encoding_is_valid :
    Data_encoding.Valid.t List.Valid.t Slot_repr.List.encoding.

  Axiom slot_range_eq : forall min count,
    Slot_repr.Valid.t min ->
    Slot_repr.Valid.t count ->
    count <> 0 ->
    min + count - 1 <= Pervasives.UInt16.max ->
    Slot_repr.List.slot_range min count =
    return? Misc.op_minusminusgt min count.
End List.

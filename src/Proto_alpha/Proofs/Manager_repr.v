Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Manager_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.

Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) Manager_repr.encoding.
  Data_encoding.Valid.data_encoding_auto; destruct x; try easy; try tauto.
Qed.

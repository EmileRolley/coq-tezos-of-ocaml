Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Voting_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.RPC_service.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Protocol_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Vote_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Voting_period_repr.

Module S.
  Lemma ballots_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.ballots.
      RPC_service.rpc_auto.
  Qed.

  Lemma ballot_list_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.ballot_list.
      RPC_service.rpc_auto.
      intros x [].
      hauto l: on use: List.Forall_forall.
  Qed.

  Lemma current_period_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.current_period.
      RPC_service.rpc_auto.
  Qed.

  Lemma successor_period_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.successor_period.
      RPC_service.rpc_auto.
  Qed.

  Lemma current_quorum_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.current_quorum.
      RPC_service.rpc_auto.
  Qed.

  Lemma listings_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.listings.
      RPC_service.rpc_auto.
      intros x [].
      hauto l: on use: List.Forall_forall.
  Qed.

  Lemma proposals_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.proposals.
      RPC_service.rpc_auto.
      apply Protocol_hash.Map_encoding_is_valid.
      Data_encoding.Valid.data_encoding_auto.
      intros x [].
      tauto.
  Qed.

  Lemma current_proposal_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.current_proposal.
      RPC_service.rpc_auto.
  Qed.

  Lemma total_voting_power_is_valid :
    RPC_service.Valid.t (fun _ => True) (fun _ => True)
      Voting_services.S.total_voting_power.
      RPC_service.rpc_auto.
  Qed.
End S.
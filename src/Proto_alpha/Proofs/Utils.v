Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

(** Document the proof state by checking that a certain expression is there,
    either in the goal or the hypothesis. *)
Ltac step e :=
  match goal with
  | |- context [e] => idtac
  | _ : context [e] |- _ => idtac
  | _ => fail "expression not found in the current goal"
  end.

Ltac tezos_z_autounfold :=
  autounfold with tezos_z;
  unfold
    Pervasives.normalize_int,
    Pervasives.normalize_int32,
    Pervasives.normalize_int64;
  unfold
    Pervasives.two_pow_31,
    Pervasives.two_pow_32,
    Pervasives.two_pow_62,
    Pervasives.two_pow_63,
    Pervasives.two_pow_64.

Ltac tezos_z_auto :=
  tezos_z_autounfold; intros; lia.

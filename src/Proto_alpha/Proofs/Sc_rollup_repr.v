Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Sc_rollup_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Kind.
  Lemma encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.Kind.encoding.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Kind.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Format.
Require Proto_alpha.Environment.Pervasives.
Require Proto_alpha.Environment.Seq.

Definition t (a : Set) : Set := option a.

Definition none {a : Set} : option a :=
  None.

Parameter none_e : forall {a trace : Set}, Pervasives.result (option a) trace.

Parameter none_s : forall {a : Set}, option a.

Parameter none_es : forall {a trace : Set},
  Pervasives.result (option a) trace.

Definition some {a : Set} (x : a) : option a :=
  Some x.

Parameter some_unit : option unit.

Parameter some_nil : forall {a : Set}, option (list a).

Parameter some_e : forall {a trace : Set},
  a -> Pervasives.result (option a) trace.

Parameter some_s : forall {a : Set}, a -> option a.

Parameter some_es : forall {a trace : Set},
  a -> Pervasives.result (option a) trace.

Definition value_value {a : Set} (x : option a) (v : a) : a :=
  match x with
  | None => v
  | Some v => v
  end.

Parameter value_e : forall {a trace : Set},
  option a -> trace -> Pervasives.result a trace.

Parameter value_f : forall {a : Set}, option a -> (unit -> a) -> a.

Parameter value_fe : forall {a trace : Set},
  option a -> (unit -> trace) -> Pervasives.result a trace.

Definition bind {a b : Set} (x : option a) (f : a -> option b) : option b :=
  match x with
  | None => None
  | Some v => f v
  end.

Definition join {a : Set} (x : option (option a)) : option a :=
  match x with
  | None | Some None => None
  | Some (Some v) => Some v
  end.

Definition either {a : Set} (x1 x2 : option a) : option a :=
  match x1, x2 with
  | Some _, _ => x1
  | None, Some _ => x2
  | None, None => None
  end.

Definition map {a b : Set} (f : a -> b) (x : option a) : option b :=
  match x with
  | None => None
  | Some v => Some (f v)
  end.

Parameter map_s : forall {a b : Set},
  (a -> b) -> option a -> option b.

Parameter map_e : forall {a b trace : Set},
  (a -> Pervasives.result b trace) -> option a ->
  Pervasives.result (option b) trace.

Parameter map_es : forall {a b trace : Set},
  (a -> Pervasives.result b trace) -> option a ->
  Pervasives.result (option b) trace.

Definition fold {a b : Set} (default : a) (f : b -> a) (x : option b) : a :=
  match x with
  | None => default
  | Some v => f v
  end.

Parameter fold_s : forall {a b : Set},
  a -> (b -> a) -> option b -> a.

Parameter fold_f : forall {a b : Set},
  (unit -> a) -> (b -> a) -> option b -> a.

Definition iter {a : Set} (f : a -> unit) (x : option a) : unit :=
  tt.

Parameter iter_s : forall {a : Set},
  (a -> unit) -> option a -> unit.

Parameter iter_e : forall {a trace : Set},
  (a -> Pervasives.result unit trace) -> option a ->
  Pervasives.result unit trace.

Parameter iter_es : forall {a trace : Set},
  (a -> Pervasives.result unit trace) -> option a ->
  Pervasives.result unit trace.

Definition is_none {a : Set} (x : option a) : bool :=
  match x with
  | None => true
  | Some _ => false
  end.

Definition is_some {a : Set} (x : option a) : bool :=
  match x with
  | None => false
  | Some _ => true
  end.

Definition equal {a : Set} (f : a -> a -> bool) (x1 x2 : option a) : bool :=
  match x1, x2 with
  | None, None => true
  | Some v1, Some v2 => f v1 v2
  | None, Some _ | Some _, None => false
  end.

Definition compare {a : Set} (f : a -> a -> int) (x1 x2 : option a) : int :=
  match x1, x2 with
  | None, None => 0
  | Some v1, Some v2 => f v1 v2
  | None, Some _ => -1
  | Some _, None => 1
  end.

Definition to_result {a trace : Set} (error : trace) (v : option a)
  : Pervasives.result a trace :=
  match v with
  | None => Pervasives.Error error
  | Some v => Pervasives.Ok v
  end.

Parameter of_result : forall {a e : Set}, Pervasives.result a e -> option a.

Definition to_list {a : Set} (x : option a) : list a :=
  match x with
  | None => []
  | Some v => [v]
  end.

Parameter to_seq : forall {a : Set}, option a -> Seq.t a.

Parameter catch : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) -> option a.

Parameter catch_s : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) -> option a.

Module Notations.
  Notation "M* X" := (option X) (at level 20).

  Notation "return* X" := (some X) (at level 20).

  Notation "'let*' x ':=' X 'in' Y" :=
    (bind X (fun x => Y))
    (at level 200, x name, X at level 100, Y at level 200).

  Notation "'let*' ' x ':=' X 'in' Y" :=
    (bind X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).
End Notations.
Import Notations.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Error_monad.
Require Proto_alpha.Environment.Pervasives.
Import Error_monad.Notations.

Reserved Notation "'t".

Inductive node (a : Set) : Set :=
| Nil : node a
| Cons : a -> 't a -> node a

where "'t" := (fun (t_a : Set) => unit -> node t_a).

Definition t := 't.

Arguments Nil {_}.
Arguments Cons {_}.

Parameter empty : forall {a : Set}, t a.

Parameter _return : forall {a : Set}, a -> t a.

Parameter cons_value : forall {a : Set}, a -> t a -> t a.

Parameter append : forall {a : Set}, t a -> t a -> t a.

Parameter map : forall {a b : Set}, (a -> b) -> t a -> t b.

Parameter filter : forall {a : Set}, (a -> bool) -> t a -> t a.

Parameter filter_map : forall {a b : Set}, (a -> option b) -> t a -> t b.

Parameter flat_map : forall {a b : Set}, (a -> t b) -> t a -> t b.

Parameter fold_left : forall {a b : Set}, (a -> b -> a) -> a -> t b -> a.

Parameter iter : forall {a : Set}, (a -> unit) -> t a -> unit.

Parameter unfold : forall {a b : Set}, (b -> option (a * b)) -> b -> t a.

Parameter first : forall {a : Set}, t a -> option a.

Parameter fold_left_e : forall {a b trace : Set},
  (a -> b -> Pervasives.result a trace) -> a -> t b ->
  Pervasives.result a trace.

Parameter fold_left_s : forall {a b : Set},
  (a -> b -> a) -> a -> t b -> a.

Parameter fold_left_es : forall {a b trace : Set},
  (a -> b -> Pervasives.result a trace) -> a -> t b ->
  Pervasives.result a trace.

Parameter iter_e : forall {a trace : Set},
  (a -> Pervasives.result unit trace) -> t a -> Pervasives.result unit trace.

Parameter iter_s : forall {a : Set}, (a -> unit) -> t a -> unit.

Parameter iter_es : forall {a trace : Set},
  (a -> Pervasives.result unit trace) -> t a ->
  Pervasives.result unit trace.

Parameter iter_ep : forall {a : Set},
  (a -> M? unit) -> t a -> M? unit.

Parameter iter_p : forall {a : Set}, (a -> unit) -> t a -> unit.

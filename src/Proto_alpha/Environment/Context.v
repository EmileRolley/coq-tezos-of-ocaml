Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Base58.
Require Proto_alpha.Environment.Context_hash.
Require Proto_alpha.Environment.Map.

Inductive depth : Set :=
| Ge : int -> depth
| Lt : int -> depth
| Eq : int -> depth
| Le : int -> depth
| Gt : int -> depth.

Inductive order : Set :=
| Sorted : order
| Undefined : order.

Module VIEW.
  Record signature {t key value tree : Set} : Set := {
    t := t;
    key := key;
    value := value;
    tree := tree;
    mem : t -> key -> bool;
    mem_tree : t -> key -> bool;
    find : t -> key -> option value;
    find_tree : t -> key -> option tree;
    list_value :
      t -> option int -> option int -> key -> list (string * tree);
    add : t -> key -> value -> t;
    add_tree : t -> key -> tree -> t;
    remove : t -> key -> t;
    fold :
      forall {a : Set},
      option depth -> t -> key -> order -> a -> (key -> tree -> a -> a) -> a;
  }.
End VIEW.
Definition VIEW := @VIEW.signature.
Arguments VIEW {_ _ _ _}.

Module Kind.
  Inductive t : Set :=
  | Tree : t
  | Value : t.
End Kind.

Module TREE.
  Record signature {t tree key value : Set} : Set := {
    t := t;
    tree := tree;
    key := key;
    value := value;
    mem : tree -> key -> bool;
    mem_tree : tree -> key -> bool;
    find : tree -> key -> option value;
    find_tree : tree -> key -> option tree;
    list_value :
      tree -> option int -> option int -> key -> list (string * tree);
    add : tree -> key -> value -> tree;
    add_tree : tree -> key -> tree -> tree;
    remove : tree -> key -> tree;
    fold :
      forall {a : Set},
      option depth -> tree -> key -> order -> a -> (key -> tree -> a -> a) -> a;
    empty : t -> tree;
    is_empty : tree -> bool;
    kind_value : tree -> Kind.t;
    to_value : tree -> option value;
    of_value : t -> value -> tree;
    hash_value : tree -> Context_hash.t;
    equal : tree -> tree -> bool;
    clear : option int -> tree -> unit;
  }.
End TREE.
Definition TREE := @TREE.signature.
Arguments TREE {_ _ _ _}.

Definition key : Set := list string.

Definition value : Set := bytes.

Definition StringMap : Map.S (key := string) (t := _) :=
  Map.Make
    {|
      Compare.COMPARABLE.compare := String.compare
    |}.

Module Tree.
  Inductive t : Set :=
  | Tree : StringMap.(Map.S.t) t -> t
  | Value : value -> t.

  (** Auxiliary functions to define tree operations. *)
  Module Aux.
    Fixpoint get (tree : t) (path : key) : option t :=
      match tree, path with
      | tree, [] => Some tree
      | Tree map, name :: path =>
        match StringMap.(Map.S.find) name map with
        | Some tree => get tree path
        | None => None
        end
      | Value _, _ :: _ => None
      end.

    Fixpoint add (tree : option t) (path : key) (inserted_tree : t)
      {struct path} : t :=
      match path with
      | [] => inserted_tree
      | name :: path =>
        match tree with
        | None | Some (Value _) =>
          let map :=
            StringMap.(Map.S.singleton)
              name (add None path inserted_tree) in
          Tree map
        | Some (Tree map) =>
          let map :=
            StringMap.(Map.S.update)
              name
              (fun sub_tree => Some (add sub_tree path inserted_tree))
              map in
          Tree map
        end
      end.

    Fixpoint remove (tree : t) (path : key) : t :=
      match tree, path with
      | Tree map, name :: path =>
        let map :=
          StringMap.(Map.S.update)
            name
            (fun sub_tree =>
              match sub_tree with
              | Some sub_tree =>
                match path with
                | [] => None
                | _ :: _ => Some (remove sub_tree path)
                end
              | None => None
              end
            )
            map in
        Tree map
      | Value _, _ | _, [] => tree
      end.

    #[bypass_check(guard=yes)]
    Fixpoint fold {a : Set} (tree : t) (acc : a) (rev_prefix_path : key)
      (f : key -> t -> a -> a) {struct tree} : a :=
      let acc := f (List.rev rev_prefix_path) tree acc in
      match tree with
      | Value _ => acc
      | Tree map =>
        StringMap.(Map.S.fold_s)
          (fun name tree acc => fold tree acc (name :: rev_prefix_path) f)
          map
          acc
      end.
  End Aux.

  Definition mem (tree : t) (path : key) : bool :=
    match Aux.get tree path with
    | Some (Value _) => true
    | Some (Tree _) | None => false
    end.

  Definition mem_tree (tree : t) (path : key) : bool :=
    match Aux.get tree path with
    | Some (Tree _) => true
    | Some (Value _) | None => false
    end.

  Definition find (tree : t) (path : key) : option value :=
    match Aux.get tree path with
    | Some (Value value) => Some value
    | Some (Tree _) | None => None
    end.

  Definition find_tree (tree : t) (path : key) : option t :=
    match Aux.get tree path with
    | Some (Tree _ as tree) => Some tree
    | Some (Value _) | None => None
    end.

  Parameter list_value :
    t -> option int -> option int -> key -> list (string * t).

  Definition add (tree : t) (path : key) (value : value) : t :=
    Aux.add (Some tree) path (Value value).

  Definition add_tree (tree : t) (path : key) (inserted_tree : t) : t :=
    Aux.add (Some tree) path inserted_tree.

  Definition remove (tree : t) (path : key) : t :=
    Aux.remove tree path.

  Parameter fold : forall {a : Set},
    option depth -> t -> key -> order -> a -> (key -> t -> a -> a) -> a.

  Definition is_empty (tree : t) : bool :=
    match tree with
    | Tree map => StringMap.(Map.S.is_empty) map
    | Value _ => false
    end.
  
  Definition kind_value (tree : t) : Kind.t :=
    match tree with
    | Tree _ => Kind.Tree
    | Value _ => Kind.Value
    end.

  Definition to_value (tree : t) : option value :=
    match tree with
    | Value value => Some value
    | Tree _ => None
    end.
  
  Parameter hash_value : t -> Context_hash.t.
  
  Definition equal (tree1 tree2 : t) : bool :=
    Context_hash.equal (hash_value tree1) (hash_value tree2).
  
  Definition clear (_ : option int) (_ : t) : unit :=
    tt.
End Tree.

Module View.
  (** We model a view as a type containing a tree (and potentially other
      information). This corresponds to the implementation. *)
  Record t : Set := {
    tree : Tree.t;
  }.

  Definition with_tree (tree : Tree.t) (view : t) : t :=
    {| tree := tree |}.
End View.

Definition mem (view : View.t) (path : key) : bool :=
  Tree.mem view.(View.tree) path.

Definition mem_tree (view : View.t) (path : key) : bool :=
  Tree.mem_tree view.(View.tree) path.

Definition find (view : View.t) (path : key) : option value :=
  Tree.find view.(View.tree) path.

Definition find_tree (view : View.t) (path : key) : option Tree.t :=
  Tree.find_tree view.(View.tree) path.

Definition list_value (view : View.t) (offset length : option int) (path : key)
  : list (string * Tree.t) :=
  Tree.list_value view.(View.tree) offset length path.

Definition add (view : View.t) (path : key) (value : value) : View.t :=
  let tree := Tree.add view.(View.tree) path value in
  View.with_tree tree view.

Definition add_tree (view : View.t) (path : key) (tree : Tree.t) : View.t :=
  let tree := Tree.add_tree view.(View.tree) path tree in
  View.with_tree tree view.

Definition remove (view : View.t) (path : key) : View.t :=
  let tree := Tree.remove view.(View.tree) path in
  View.with_tree tree view.

Parameter fold : forall {a : Set},
  option depth -> View.t -> key -> order -> a -> (key -> Tree.t -> a -> a) -> a.

Definition Included_VIEW_t : Set := View.t.

Definition Included_VIEW_tree : Set := Tree.t.

Definition Included_VIEW :
  VIEW (t := Included_VIEW_t) (key := list string) (value := bytes)
    (tree := Included_VIEW_tree) := {|
    VIEW.mem := mem;
    VIEW.mem_tree := mem_tree;
    VIEW.find := find;
    VIEW.find_tree := find_tree;
    VIEW.list_value := list_value;
    VIEW.add := add;
    VIEW.add_tree := add_tree;
    VIEW.remove := remove;
    VIEW.fold _ := fold;
  |}.

Definition empty (_ : View.t) : Tree.t :=
  Tree.Tree StringMap.(Map.S.empty).

Definition of_value (_ : View.t) (value : value) : Tree.t :=
  (Tree.Value value).

Definition Tree
  : TREE (t := View.t) (tree := Tree.t) (key := key) (value := value) :=
  {|
    TREE.mem := Tree.mem;
    TREE.mem_tree := Tree.mem_tree;
    TREE.find := Tree.find;
    TREE.find_tree := Tree.find_tree;
    TREE.list_value := Tree.list_value;
    TREE.add := Tree.add;
    TREE.add_tree := Tree.add_tree;
    TREE.remove := Tree.remove;
    TREE.fold _ := Tree.fold;
    TREE.empty := empty;
    TREE.is_empty := Tree.is_empty;
    TREE.kind_value := Tree.kind_value;
    TREE.to_value := Tree.to_value;
    TREE.of_value := of_value;
    TREE.hash_value := Tree.hash_value;
    TREE.equal := Tree.equal;
    TREE.clear := Tree.clear;
  |}.

Parameter register_resolver : forall {a : Set},
  Base58.encoding a -> (View.t -> string -> list a) -> unit.

Parameter complete : View.t -> string -> list string.

Parameter get_hash_version : View.t -> Context_hash.Version.t.

Parameter set_hash_version :
  View.t -> Context_hash.Version.t -> Error_monad.shell_tzresult View.t.

(* We define the types [t] and [tree] at the end to avoid name collisions with
   the values of these types (especially true for [tree]). *)
Definition t : Set := View.t.

Definition tree : Set := Tree.t.

Parameter cache_key : Set.
  
Definition cache_value := extensible_type.

Module CACHE.
  Record signature {t size index identifier key value : Set} : Set := {
    t := t;
    size := size;
    index := index;
    identifier := identifier;
    key := key;
    value := value;
    key_of_identifier : index -> identifier -> key;
    identifier_of_key : key -> identifier;
    pp : Format.formatter -> t -> unit;
    find : t -> key -> option value;
    set_cache_layout : t -> list size -> t;
    update : t -> key -> option (value * size) -> t;
    sync : t -> Bytes.t -> t;
    clear : t -> t;
    list_keys : t -> index -> option (list (key * size));
    key_rank : t -> key -> option int;
    future_cache_expectation : t -> int -> t;
    cache_size : t -> index -> option size;
    cache_size_limit : t -> index -> option size;
  }.
End CACHE.
Definition CACHE := @CACHE.signature.
Arguments CACHE {_ _ _ _ _ _}.

Parameter Cache :
  CACHE (t := t) (size := int) (index := int) (identifier := string)
    (key := cache_key) (value := cache_value).

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Bytes.

Parameter pk : Set.
  
Definition signature : Set := Bytes.t.

Parameter unsafe_pk_of_bytes : Bytes.t -> pk.

Parameter pk_of_bytes_opt : Bytes.t -> option pk.

Parameter pk_to_bytes : pk -> Bytes.t.

Parameter aggregate_signature_opt : list Bytes.t -> option Bytes.t.

Parameter verify : pk -> Bytes.t -> signature -> bool.

Parameter aggregate_verify : list (pk * Bytes.t) -> signature -> bool.

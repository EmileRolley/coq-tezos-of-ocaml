Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Parameter length : bytes -> int.

Parameter get : bytes -> int -> ascii.

Parameter set : bytes -> int -> ascii -> unit.

Parameter make : int -> ascii -> bytes.

Parameter init_value : int -> (int -> ascii) -> bytes.

Parameter empty : bytes.

Parameter copy : bytes -> bytes.

Parameter of_string : string -> bytes.

Parameter to_string : bytes -> string.

Parameter sub : bytes -> int -> int -> bytes.

Parameter sub_string : bytes -> int -> int -> string.

Parameter extend : bytes -> int -> int -> bytes.

Parameter fill : bytes -> int -> int -> ascii -> unit.

Parameter blit : bytes -> int -> bytes -> int -> int -> unit.

Parameter blit_string : string -> int -> bytes -> int -> int -> unit.

Parameter concat : bytes -> list bytes -> bytes.

Parameter cat : bytes -> bytes -> bytes.

Parameter iter : (ascii -> unit) -> bytes -> unit.

Parameter iteri : (int -> ascii -> unit) -> bytes -> unit.

Parameter map : (ascii -> ascii) -> bytes -> bytes.

Parameter mapi : (int -> ascii -> ascii) -> bytes -> bytes.

Parameter trim : bytes -> bytes.

Parameter escaped : bytes -> bytes.

Parameter index_opt : bytes -> ascii -> option int.

Parameter rindex_opt : bytes -> ascii -> option int.

Parameter index_from_opt : bytes -> int -> ascii -> option int.

Parameter rindex_from_opt : bytes -> int -> ascii -> option int.

Parameter contains : bytes -> ascii -> bool.

Parameter contains_from : bytes -> int -> ascii -> bool.

Parameter rcontains_from : bytes -> int -> ascii -> bool.

Parameter uppercase_ascii : bytes -> bytes.

Parameter lowercase_ascii : bytes -> bytes.

Parameter capitalize_ascii : bytes -> bytes.

Parameter uncapitalize_ascii : bytes -> bytes.

Definition t : Set := bytes.

Parameter compare : t -> t -> int.

Parameter equal : t -> t -> bool.

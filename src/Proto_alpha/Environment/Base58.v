Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Parameter encoding : forall (a : Set), Set.

Parameter simple_decode : forall {a : Set}, encoding a -> string -> option a.

Parameter simple_encode : forall {a : Set}, encoding a -> a -> string.

Definition data : Set := extensible_type.

Parameter register_encoding : forall {a : Set},
  string -> int -> (a -> string) -> (string -> option a) -> (a -> data) ->
  encoding a.

Parameter check_encoded_prefix : forall {a : Set},
  encoding a -> string -> int -> unit.

Parameter decode : string -> option data.

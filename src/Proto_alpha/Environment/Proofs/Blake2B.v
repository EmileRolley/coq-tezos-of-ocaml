Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Block_payload_hash.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.S.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Axiom Make_is_valid
  : forall (R : Blake2B.Register) (N : Blake2B.PrefixedName),
    S.HASH.Valid.t (fun _ => True) (Blake2B.Make R N).

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.S.

Axiom Included_HASH_is_valid
  : S.HASH.Valid.t (fun _ => True) Operation_hash.Included_HASH.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Operation_hash.encoding.
  apply Included_HASH_is_valid.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

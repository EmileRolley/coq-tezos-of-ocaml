Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.

Lemma compare_is_valid
  : Proofs.Compare.Valid.t id Z.compare.
  exact Compare.Valid.z.
Qed.

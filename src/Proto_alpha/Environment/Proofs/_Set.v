Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment._Set.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Map.

Lemma added_In : forall {a : Set} `{@Make.FArgs a} (x : a) xs,
  In (x,tt) (_Set.Make.add x xs).
  intros.
  induction xs.
  - left; reflexivity.
  - unfold _Set.Make.add; simpl.
    destruct a0 as [k' []].
    destruct Make.compare_keys.
    + left; reflexivity.
    + left; reflexivity.
    + right; assumption.
Qed.

Lemma In_add_still_In : forall {a : Set} (compare : a -> a -> int)
  (x x' : a) xs, Map.lawful_compare compare ->
  let args := @Make.Build_FArgs a (COMPARABLE.Build_signature a compare) in
  let setargs := @Map.Make.Build_FArgs a
        (COMPARABLE.Build_signature a compare) in
  In (x, tt) xs -> In (x, tt) (_Set.Make.add x' xs).
  intros a compare x x' xs lawful arg maparg Hin.
  induction xs.
  - destruct Hin.
  - destruct Hin as [Heq|Hin].
    + rewrite Heq.
      unfold _Set.Make.add; simpl.
      destruct (Make.compare_keys x' x) eqn:C.
      * left; f_equal.
        eapply Map.lawful_compare_keys_Eq_equals; assumption.
      * right; left; reflexivity.
      * left; reflexivity.
    + unfold _Set.Make.add; simpl.
      destruct a0 as [k' []].
      destruct Make.compare_keys.
      * right; assumption.
      * right; right; assumption.
      * right; tauto.
Qed.

Lemma In_add_destr : forall {a : Set} (compare : a -> a -> int)
  (x x' : a) xs, let args := @Make.Build_FArgs a (COMPARABLE.Build_signature a compare) in
  In (x,tt) (_Set.Make.add x' xs) -> x = x' \/ In (x,tt) xs.
  intros a compare x x' xs args Hin.
  induction xs.
  - destruct Hin as [Heq|[]].
    left; inversion Heq; reflexivity.
  - unfold _Set.Make.add in Hin; simpl in Hin.
    destruct a0 as [k' []].
    destruct (Make.compare_keys);
    destruct Hin as [Heq|Hin].
    + left; inversion Heq; reflexivity.
    + right; right; assumption.
    + inversion Heq; tauto.
    + simpl in *; tauto.
    + right; left; inversion Heq; reflexivity.
    + simpl in *; tauto.
Qed.

Lemma hd_fold_add : forall {a : Set} (compare : a -> a -> int) xs,
  let args := @Make.Build_FArgs a (COMPARABLE.Build_signature a compare) in
  let setargs := @Map.Make.Build_FArgs a
        (COMPARABLE.Build_signature a compare) in
  Map.sorted xs ->
  hd xs = option_map fst (hd (fold_right _Set.Make.add xs Make.empty)).
  induction xs; intros.
  - reflexivity.
  - pose proof (IHxs (Map.sorted_tail _ _ H)).
    destruct xs.
    + reflexivity.
    + simpl in H0; simpl.
      destruct (List.hd_Some_to_cons (k,tt)
            ((_Set.Make.add k
             (fold_right _Set.Make.add xs Make.empty)))).
      * destruct (Make.add _ _) eqn:G.
        ** discriminate.
        ** destruct p as [k' []].
           inversion H0; reflexivity.
      * simpl in *.
        rewrite H1.
        unfold _Set.Make.add.
        simpl.
        destruct H as [_ [H|H]];
        destruct (Make.compare_keys _ _); try discriminate.
        ** reflexivity.
        ** reflexivity.
Qed.

Lemma sorted_fold_right_add : forall {a : Set} (compare : a -> a -> int),
  let args := @Make.Build_FArgs a (COMPARABLE.Build_signature a compare) in
  let setargs := @Map.Make.Build_FArgs a
        (COMPARABLE.Build_signature a compare) in
  forall xs, Map.sorted xs ->
  List.map fst (fold_right _Set.Make.add xs Make.empty) = xs.
  intros.
  rewrite <- (List.fold_right_cons_nil xs) at 2.
  induction xs; intros.
  - reflexivity.
  - simpl.
    pose proof (hd_fold_add compare _ (Map.sorted_tail _ _ H)).
    destruct xs.
    + reflexivity.
    + simpl hd at 1 in H0.
      destruct (hd _) eqn:G; try discriminate.
      simpl in H0.
      destruct (List.hd_Some_to_cons _ _ G) as [ys Hys].
      unfold args, setargs in *.
      rewrite Hys.
      unfold _Set.Make.add; simpl.
      destruct H as [Hs [Hkeys|Hkeys]];
      destruct (Make.compare_keys _) eqn:G'; try discriminate.
      * simpl.
        pose (IHxs Hs) as IH.
        simpl in *; rewrite <- IH.
        rewrite Hys.
        destruct p as [k' []].
        inversion H0; simpl.
        rewrite H1 in G'.
        destruct (Make.compare_keys _); (reflexivity||discriminate).
      * apply axiom. (*NoDup stuff*)
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.S.

Axiom Included_SIGNATURE_is_valid :
  S.SIGNATURE.Valid.t (fun _ => True) Ed25519.Included_SIGNATURE.

Module Public_key_hash.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Ed25519.Public_key_hash.(SIGNATURE_PUBLIC_KEY_HASH.encoding).
    apply Included_SIGNATURE_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Public_key_hash.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Definition wrap_compare {a : Set} (compare : a -> a -> int) (x y : a) : int :=
  let res := compare x y in
  match res with
  | 0 => 0
  | _ =>
    if res >i 0 then
      1
    else
      -1
  end.

Lemma wrap_compare_zero {a : Set} {compare : a -> a -> int} {x y}
  : wrap_compare compare x y = 0 ->
    compare x y = 0.
  unfold wrap_compare.
  set (res := compare x y).
  destruct res; simpl; congruence.
Qed.

Lemma wrap_lexicographic_compare {a b : Set} {compare_a : a -> a -> int}
  {compare_b : b -> b -> int} {x y : a * b}
  : wrap_compare (Compare.lexicographic_compare compare_a compare_b) x y =
    Compare.lexicographic_compare
      (wrap_compare compare_a) (wrap_compare compare_b) x y.
  unfold wrap_compare, Compare.lexicographic_compare.
  destruct x, y.
  now destruct (compare_a _ _); destruct (compare_b _ _).
Qed.

Lemma lexicographic_one {a b : Set}
  {compare_a : a -> a -> _} {compare_b : b -> b -> _}
  {x_a y_a : a} {x_b y_b : b}
  : Compare.lexicographic_compare compare_a compare_b (x_a, x_b) (y_a, y_b) = 1 ->
    compare_a x_a y_a = 1 \/ (compare_a x_a y_a = 0 /\ compare_b x_b y_b = 1).
  simpl.
  destruct (compare_a _ _); tauto.
Qed.

(** Definition of the comparison as a pre-order on an equivalence relation. *)
Module Valid.
  (** We consider two elements to be equivalent if their images using [f] are
      the same. *)
  Record t {a a' : Set} {f : a -> a'} {compare : a -> a -> int} : Prop := {
    congruence_left x1 x2 y :
      f x1 = f x2 -> compare x1 y = compare x2 y;
    domain x y :
      match compare x y with
      | -1 | 0 | 1 => True
      | _ => False
      end;
    zero x y : compare x y = 0 -> f x = f y;
    sym x y : compare x y = - compare y x;
    trans x y z :
      compare x y = 1 ->
      compare y z = 1 ->
      compare x z = 1;
  }.
  Arguments t {_ _}.

  Lemma refl {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    (H : t f compare) (x : a)
    : compare x x = 0.
    set (c := compare x x).
    assert (c = -c) by apply H.(sym).
    lia.
  Qed.

  Lemma refl_wrap {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    (H : t f (wrap_compare compare)) (x : a)
    : compare x x = 0.
    apply wrap_compare_zero.
    set (c := wrap_compare compare x x).
    assert (c = -c) by apply H.(sym).
    lia.
  Qed.

  Lemma congruence_right {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    {x y1 y2 : a}
    : t f compare -> f y1 = f y2 -> compare x y1 = compare x y2.
    intros H_t H_eq.
    rewrite H_t.(sym).
    rewrite (H_t.(congruence_left) _ _ _ H_eq).
    now rewrite (H_t.(sym) x).
  Qed.

  Lemma option {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    : t f compare ->
      t (Option.map f) (Compare.Option.compare compare).
    intro H.
    constructor; intros; destruct_all (option a); simpl in *; try easy;
      try apply H; try congruence;
      try (f_equal; now apply H.(zero)).
    match goal with
    | [v : a |- _] => now apply H with (y := v)
    end.
  Qed.

  Lemma lexicographic_compare {a b a' b' : Set}
    {f_a : a -> a'} {f_b : b -> b'}
    {compare_a : a -> a -> int} {compare_b : b -> b -> int}
    : t f_a compare_a -> t f_b compare_b ->
      let f '(x_a, x_b) := (f_a x_a, f_b x_b) in
      t f (lexicographic_compare compare_a compare_b).
    intros H_a H_b.
    constructor; unfold lexicographic_compare; intros; destruct_all (a * b).
    erewrite H_a.(congruence_left);
      try erewrite H_b.(congruence_left);
      try reflexivity;
      try congruence.
    match goal with
    | [|- context[compare_a ?v1 ?v2]] =>
      let H := fresh "H" in
      assert (H := H_a.(domain) v1 v2);
      destruct (compare_a v1 v2); trivial;
      apply H_b
    end.
    destruct (compare_a _ _) eqn:H_ca; try congruence.
    f_equal.
    now erewrite H_a.(zero); try reflexivity.
    now erewrite H_b.(zero); try reflexivity.
    rewrite H_a.(sym); rewrite H_b.(sym).
    now destruct (compare_a _ _).

    repeat match goal with
    | [H : match _ with _ => _ end = 1 |- _] =>
      let H_zero := fresh "H_zero" in
      let H_one := fresh "H_one" in
      destruct (lexicographic_one H) as [H_one|[H_zero H_one]];
      clear H
    end;
    repeat match goal with
    | [H : compare_a ?v1 ?v2 = 0 |- _] =>
      let H_eq := fresh "H_eq" in
      assert (H_eq := H_a.(zero) _ _ H);
      clear H;
      try (rewrite (H_a.(congruence_left) _ _ _ H_eq); clear H_eq);
      try (rewrite <- (congruence_right H_a H_eq); clear H_eq)
    end.
    match goal with
    | [v : a |- _] => now rewrite H_a.(trans) with (y := v)
    end.
    match goal with
    | [|- context[compare_a ?v1 ?v2]] =>
      now replace (compare_a v1 v2) with 1
    end.
    match goal with
    | [|- context[compare_a ?v1 ?v2]] =>
      now replace (compare_a v1 v2) with 1
    end.
    match goal with
    | [v3 : b |- context[compare_b ?v1 ?v2]] =>
      rewrite (refl H_a);
      now apply H_b.(trans) with (y := v3)
    end.
  Qed.

  Lemma equality {a a' : Set} {f : a -> a'} {compare1 compare2 : a -> a -> int}
    : (forall x y, compare1 x y = compare2 x y) -> t f compare1 -> t f compare2.
    intros H_eq H_compare1.
    constructor; intros; repeat rewrite <- H_eq in *; try now apply H_compare1.
    now apply H_compare1 with (y := y).
  Qed.

  Lemma equality_f {a a' : Set} {f1 f2 : a -> a'} {compare : a -> a -> int}
    : t f1 compare -> (forall x, f1 x = f2 x) -> t f2 compare.
    constructor; try sfirstorder; sauto lq: on rew: off.
  Qed.

  Lemma projection {a b c : Set}
        (f_a : a -> b) (f_b : b -> c) (compare_b : b -> b -> int) :
    t f_b compare_b ->
    let compare_a x y :=
        compare_b (f_a x) (f_a y) in
    t (fun x => f_b (f_a x)) compare_a.
    intro H; inversion H; simpl; constructor; auto.
    intros x y; apply domain0.
    intros x y z; apply trans0.
  Qed.

  Lemma unit : t id Compare.Unit.(Compare.S.compare).
    constructor; intros; try easy.
    now destruct_all unit.
  Qed.

  Lemma bool : t id Compare.Bool.(Compare.S.compare).
    constructor; intros;
      now repeat match goal with
      | [b : bool |- _] => destruct b
      end.
  Qed.

  Lemma z : t id Compare.Z.(Compare.S.compare).
    constructor; unfold id;
      repeat match goal with
      | [|- forall (_ : int), _] => intro
      end;
      simpl; unfold Z.compare;
      sauto.
  Qed.

  Lemma int : t id Compare.Int.(Compare.S.compare).
    exact z.
  Qed.

  Lemma int32 : t id Compare.Int32.(Compare.S.compare).
    exact z.
  Qed.

  Lemma int64 : t id Compare.Int64.(Compare.S.compare).
    exact z.
  Qed.

  Axiom string : t id Compare.String.(Compare.S.compare).

  Lemma wrap_compare_eq {a : Set} {f : a -> a} {compare : a -> a -> _}
    : t f compare -> forall x y, (wrap_compare compare) x y = compare x y.
    unfold id, wrap_compare.
    intros H x y.
    assert (H_domain := H.(domain) x y).
    destruct (compare x y);
      now try match goal with
      | [p : positive |- _] => destruct p
      end.
  Qed.

  Lemma wrap_compare {a : Set} {f : a -> a} {compare : a -> a -> _}
    : t f compare -> t f (wrap_compare compare).
    hauto l: on use: equality, wrap_compare_eq.
  Qed.
End Valid.

Module COMPARABLE.
  Module Valid.
    Definition t {t} (C : Compare.COMPARABLE (t := t)) : Prop :=
      Valid.t id C.(Compare.COMPARABLE.compare).
  End Valid.
End COMPARABLE.

Module S.
  Definition to_COMPARABLE {t} (C : Compare.S (t := t))
    : Compare.COMPARABLE (t := t) := {|
      Compare.COMPARABLE.compare := C.(Compare.S.compare);
    |}.

  Module Eq.
    Record t {t} {C1 C2 : Compare.S (t := t)} : Prop := {
      op_eq x y : C1.(Compare.S.op_eq) x y = C2.(Compare.S.op_eq) x y;
      op_ltgt x y : C1.(Compare.S.op_ltgt) x y = C2.(Compare.S.op_ltgt) x y;
      op_lt x y : C1.(Compare.S.op_lt) x y = C2.(Compare.S.op_lt) x y;
      op_lteq x y : C1.(Compare.S.op_lteq) x y = C2.(Compare.S.op_lteq) x y;
      op_gteq x y : C1.(Compare.S.op_gteq) x y = C2.(Compare.S.op_gteq) x y;
      op_gt x y : C1.(Compare.S.op_gt) x y = C2.(Compare.S.op_gt) x y;
      equal x y : C1.(Compare.S.equal) x y = C2.(Compare.S.equal) x y;
      max x y : C1.(Compare.S.max) x y = C2.(Compare.S.max) x y;
      min x y : C1.(Compare.S.min) x y = C2.(Compare.S.min) x y;
    }.
    Arguments t {_}.
  End Eq.

  Module Valid.
    Record t {t} {C : Compare.S (t := t)} : Prop := {
      compare : COMPARABLE.Valid.t (to_COMPARABLE C);
      eq : Eq.t C (Compare.Make (to_COMPARABLE C));
    }.
    Arguments t {_}.
  End Valid.
End S.

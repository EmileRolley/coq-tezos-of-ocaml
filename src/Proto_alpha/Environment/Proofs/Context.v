Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Map.

Module Tree.
  Lemma mem_find (tree : Context.Tree.t) (path : Context.key)
    : Context.Tree.mem tree path =
      let value := Tree.find tree path in
      match value with
      | Some _ => true
      | None => false
      end.
    unfold Tree.mem, Tree.find.
    destruct (Tree.Aux.get _ _) as [tree'|]; simpl; trivial.
    now destruct tree'.
  Qed.
End Tree.

Lemma mem_find (view : Context.View.t) (path : Context.key)
  : Context.mem view path =
    let value := Context.find view path in
    match value with
    | Some _ => true
    | None => false
    end.
  unfold Context.mem, Context.find.
  now rewrite Tree.mem_find.
Qed.

Module Key.
  Parameter compare : key -> key -> int.
End Key.

Fixpoint filter_trees {a} (f : a -> Kind.t) (xs : list a) : list a :=
  match xs with
  | [] => []
  | x::ys =>
    match f x with
    | Kind.Tree => filter_trees f ys
    | Kind.Value => x :: filter_trees f ys
    end
  end.

Lemma remove_kind_match : forall {a b : Set}
  (f : a -> Kind.t) (g : a -> b -> b) (xs : list a) y,
  fold_right
    (fun x acc =>
      match f x with
      | Kind.Tree => acc
      | Kind.Value => g x acc
      end) xs y =
      fold_right g (filter_trees f xs) y.
  induction xs; intros.
  - reflexivity.
  - simpl; destruct (f a0).
    + apply IHxs.
    + simpl; rewrite IHxs; reflexivity.
Qed.

Lemma opt_map_filter_tree : forall {a b : Set}
  (f : a -> Kind.t) (h : a -> option b),
  (forall x, f x = Kind.Tree -> h x = None) -> forall xs,
  List.opt_map h (filter_trees f xs) = List.opt_map h xs.
  induction xs; intros.
  - reflexivity.
  - simpl.
    + destruct (f a0) eqn:F.
      * rewrite (H a0 F); assumption.
      * simpl.
        destruct (h a0).
        ** congruence.
        ** exact IHxs.
Qed.

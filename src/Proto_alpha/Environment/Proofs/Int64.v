Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Proofs.Utils.

Module Valid.
  Definition t (n : int64) : Prop :=
    Int64.min_int <= n <= Int64.max_int.
  #[global] Hint Unfold t : tezos_z.
End Valid.

Lemma normalize_is_valid (n : Z.t) : Valid.t (Pervasives.normalize_int64 n).
  Utils.tezos_z_auto.
Qed.

Lemma normalize_identity (n : Z.t) :
  Valid.t n -> Pervasives.normalize_int64 n = n.
  Utils.tezos_z_auto.
Qed.

Lemma int64_add_valid a b : Valid.t (a +i64 b). Utils.tezos_z_auto. Qed.
Lemma int64_mul_valid a b : Valid.t (a *i64 b). Utils.tezos_z_auto. Qed.
Lemma int64_add_assoc a b c : a +i64 b +i64 c = a +i64 (b +i64 c).  Utils.tezos_z_auto.  Qed.
Lemma normalize_int64_0_r a : (a + Pervasives.normalize_int64 0)%Z = a.  Utils.tezos_z_auto.  Qed.
Lemma normalize_int64_0_l a : (Pervasives.normalize_int64 0 + a)%Z = a.  Utils.tezos_z_auto.  Qed.
Lemma normalize_int64_lte_max_int a : Pervasives.normalize_int64 a <= Int64.max_int. Utils.tezos_z_auto. Qed.

(** normalize is not distrubutive *)
Lemma normalize_add_not_distr : 
  exists a b, Valid.t a -> Valid.t b ->
  Pervasives.normalize_int64 (a + b)%Z <> (Pervasives.normalize_int64 a + Pervasives.normalize_int64 b)%Z.
  exists 1, max_int.
  Utils.tezos_z_auto.
Qed.

Lemma normalize_mul_not_distr : 
  exists a b, Valid.t a -> Valid.t b ->
  Pervasives.normalize_int64 (a * b)%Z <> (Pervasives.normalize_int64 a * Pervasives.normalize_int64 b)%Z.
  exists 2, max_int.
  Utils.tezos_z_auto.
Qed.

Lemma normalize_div_mul_false : exists i p,
    Int64.Valid.t i ->
    Int64.Valid.t p ->
    p <> 0 ->
    (i *i64 p) /i64 p <> i.
  exists Int64.max_int, 2.
  unfold "<>".
  Utils.tezos_z_autounfold.
  intros. inversion H2.
Qed.

Axiom of_string_opt_to_string : forall i,
  Int64.of_string_opt (Int64.to_string i) = Some i.

Axiom to_string_of_string_opt : forall s,
  match Int64.of_string_opt s with
  | Some i => Int64.to_string i = s
  | None => True
  end.

Axiom of_string_opt_is_valid : forall s,
  match Int64.of_string_opt s with
  | Some i => Valid.t i
  | None => True
  end.

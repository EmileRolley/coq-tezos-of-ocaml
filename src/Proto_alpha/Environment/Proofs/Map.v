Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Import Map.

Axiom find_add : forall `{Make.FArgs} {a : Set} k v (m : Make.t a),
  Make.find k (Make.add k v m) = Some v.

Axiom find_update : forall `{Make.FArgs} {a : Set} k f (m : Make.t a),
  Make.find k (Make.update k f m) = f (Make.find k m).

Axiom find_singleton : forall `{Make.FArgs} {a : Set} k (v : a),
  Make.find k (Make.singleton k v) = Some v.

Record lawful_compare { a : Set} (compare : a -> a -> int) : Prop := {
  compare_refl_0 : forall x, compare x x = 0;
  compare_0_equal : forall x x', compare x x' = 0 -> x = x';
  compare_trans_lt : forall x y z, compare x y < 0 -> compare y z < 0
    -> compare x z < 0
  }.

Arguments compare_refl_0 {_} {_}.
Arguments compare_0_equal {_} {_}.
Arguments compare_trans_lt {_} {_}.

Lemma lawful_compare_keys_refl_Eq : forall `{Make.FArgs},
  lawful_compare (Make.Ord.(COMPARABLE.compare)) ->
  forall k, Make.compare_keys k k = Datatypes.Eq.
Proof.
  intros t args Hlawful k.
  unfold Make.compare_keys.
  rewrite (compare_refl_0 Hlawful); simpl; reflexivity.
Qed.

Lemma lawful_compare_keys_Eq_equals : forall `{Make.FArgs},
  lawful_compare (Make.Ord.(COMPARABLE.compare)) ->
  forall k k', Make.compare_keys k k' = Datatypes.Eq -> k = k'.
Proof.
  intros t args Hlawful k k'.
  unfold Make.compare_keys.
  destruct (Z.eqb _ 0) eqn:G.
  - rewrite Z.eqb_eq in G.
    intro; apply (compare_0_equal Hlawful); auto.
  - destruct (Z.leb _ _); intro; discriminate.
Qed.

Lemma lawful_compare_trans_lt : forall `{Make.FArgs},
  lawful_compare (Make.Ord.(COMPARABLE.compare)) -> forall k l m,
  Make.compare_keys k l = Datatypes.Lt ->
  Make.compare_keys l m = Datatypes.Lt ->
  Make.compare_keys k m = Datatypes.Lt.
Proof.
  apply axiom.
  (* leaving for now in case it's already somewhere *)
Qed.

(* 
    Context `{Make.FArgs}.

    Hypothesis lawful : Map.lawful_compare (Make.Ord.(COMPARABLE.compare)).
 *)
    

Definition key_le `{Make.FArgs} (x y : Make.key) : Prop :=
  Make.compare_keys x y = Datatypes.Lt \/
  Make.compare_keys x y = Datatypes.Eq.

Definition key_le_trans `{Make.FArgs} : forall x y z,
  key_le x y -> key_le y z -> key_le x z.
  apply axiom. (* trivial but tedious atm*)
Qed.

Fixpoint sorted `{Make.FArgs} (xs : list Make.key) : Prop :=
  match xs with
  | [] => True
  | x::ys =>
    match ys with
    | [] => True
    | y::zs => sorted ys /\ key_le x y
    end
  end.

Lemma sorted_cons2 `{Make.FArgs} : forall x y xs,
  sorted (x::y::xs) = (sorted (y::xs) /\ key_le x y).
  reflexivity.
Qed.

Lemma sorted_tail `{Make.FArgs} : forall x xs,
  sorted (x::xs) -> sorted xs.
  intros; destruct xs.
  - auto.
  - simpl in *.
    destruct xs; tauto.
Qed.

Lemma sorted_remove `{Make.FArgs} : forall x y xs,
  sorted (x::y::xs) -> sorted (x::xs).
  intros.
  destruct xs.
  - simpl; tauto.
  - repeat rewrite sorted_cons2 in *.
    split; simpl in *; try tauto.
    destruct H0 as [[_ H1] H2].
    eapply key_le_trans; eassumption.
Qed.

Lemma sorted_tail_In `{Make.FArgs} : forall xs x y,
  sorted (x :: xs) -> In y xs -> key_le x y.
  induction xs; intros x y Hs Hin.
  - destruct Hin.
  - destruct Hin as [Heq|Hin].
    + rewrite Heq in Hs.
      simpl in Hs.
      destruct Make.compare_keys; tauto.
    + apply IHxs; auto.
      eapply sorted_remove; exact Hs.
Qed.

Lemma mem_In `{Make.FArgs} (lawful : Map.lawful_compare(Make.Ord.(COMPARABLE.compare))) :
  forall k (s : Make.t unit), sorted (List.map fst s) ->
    Make.mem k s = true <-> In (k,tt) s.
  intros.
  induction s.
  - simpl; split; intro; (discriminate || tauto).
  - simpl; split; intro; destruct a.
    + destruct (Make.compare_keys k k0) eqn:G.
      * left; simpl.
        rewrite (Map.lawful_compare_keys_Eq_equals lawful _ _ G).
        destruct u; reflexivity.
      * discriminate.
      * right.
        apply IHs; try assumption.
        eapply sorted_tail; eassumption.
    + destruct H1.
      * simpl in H1.
        destruct u; inversion H1.
        assert (Make.compare_keys k k = Datatypes.Eq) by apply axiom.
        rewrite (Map.lawful_compare_keys_refl_Eq); auto.
      * destruct Make.compare_keys eqn:G; auto.
        ** assert (key_le k0 k).
           *** apply (sorted_tail_In _ _ _ H0);
               exact (in_map fst _ _ H1).
           *** destruct H2; apply axiom. (*trivial*)
        ** apply IHs; auto.
           eapply sorted_tail; eauto.
Qed.

Lemma mem_not_In `{Make.FArgs} (lawful : Map.lawful_compare(Make.Ord.(COMPARABLE.compare))) :
  forall k (s : Make.t unit), sorted (List.map fst s) ->
    Make.mem k s = false <-> ~ In (k,tt) s.
  intros.
  split; intros.
  - intro.
    rewrite <- mem_In in H2; auto.
    congruence.
  - destruct Make.mem eqn:G.
    + rewrite mem_In in G; auto.
      contradiction.
    + reflexivity.
Qed.




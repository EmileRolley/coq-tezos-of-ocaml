Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Utils.

Lemma length_destruct {a : Set} (x : a) (l : list a) :
  List.length (x :: l) = List.length l +i 1.
  Utils.tezos_z_autounfold.
  induction l; simpl; try Utils.tezos_z_auto.
Qed.

Lemma length_is_valid {a : Set} (l : list a) :
    Pervasives.Int.Valid.t (List.length l).
  Utils.tezos_z_autounfold.
  induction l; simpl; try lia.
  Utils.tezos_z_auto.
Qed.

Fixpoint length_app_sum {a : Set} (l1 l2 : list a)
  : List.length (l1 ++ l2) =
    List.length l1 +i List.length l2.
  assert (H_l1l2 := length_is_valid (l1 ++ l2)).
  destruct l1; simpl.
  { now rewrite Pervasives.normalize_identity. }
  { rewrite length_app_sum.
    Utils.tezos_z_auto.
  }
Qed.

Fixpoint mem_eq_dec {a : Set} {eq_dec : a -> a -> bool}
  (H_eq_dec : forall x y, eq_dec x y = true -> x = y) (v : a) (l : list a)
  : List.mem eq_dec v l = true ->
    List.In v l.
  destruct l; simpl; [congruence|].
  destruct (eq_dec _ _) eqn:H; simpl; [left | right].
  { symmetry.
    now apply H_eq_dec.
  }
  { now apply (mem_eq_dec _ eq_dec). }
Qed.

Definition in_list_string (s : string) (list : list string) : bool :=
  List.mem String.eqb s list.

Fixpoint all_distinct_strings (l : list string) : bool :=
  match l with
  | [] => true
  | h :: tl =>
    if in_list_string h tl then
      false
    else
      all_distinct_strings tl
  end.

Fixpoint Forall_True {a : Set} {P : a -> Prop} {l : list a}
  : (forall x, P x) -> List.Forall P l.
  intro; destruct l; constructor; trivial; now apply Forall_True.
Qed.
#[global] Hint Resolve Forall_True : core.

Lemma nil_has_no_last : forall (a : Set), last_opt (nil : List.t a) = None.
  reflexivity.
Qed.

Lemma tz_rev_append_rev {a : Set} : forall (l l' : list a),
  CoqOfOCaml.List.rev_append l l' = (Lists.List.rev l ++ l')%list.
  intro l; induction l; simpl; auto; intros.
  rewrite <- app_assoc; firstorder.
Qed.

Lemma lst_lst : forall (a : Set) (l : list a) (x : a),
  last_opt (l ++ [x])%list = Some x.
  intros.
  induction l; auto.
  simpl; rewrite IHl.
  destruct l; reflexivity.
Qed.

Lemma head_is_last_of_rev : forall (a : Set) (l : list a) (x : a),
  last_opt (rev l) = hd l.
  unfold rev; unfold CoqOfOCaml.List.rev.
  intros.
  induction l; auto.
  simpl; rewrite tz_rev_append_rev; apply lst_lst.
Qed.

Lemma head_of_init : forall {a trace : Set} (f : int -> a) (x : trace),
  init_value x 1 f = Pervasives.Ok [f(0)].
  intros.
  unfold init_value.
  reflexivity.
Qed.

Lemma in_map : forall {a b : Set} (f : a -> b) (xs : list a) (x : a),
  In x xs -> In (f x) (List.map f xs).
  induction xs; intros x Hin; destruct Hin.
  - left; congruence.
  - right; apply IHxs; auto.
Qed.

(** remove Nones, otherwise maps *)
Fixpoint opt_map {a b : Set} (f : a -> option b) (xs : list a) : list b :=
  match xs with
  | [] => []
  | x :: xs' =>
    match f x with
    | Some y => y :: opt_map f xs'
    | None => opt_map f xs'
    end
  end.

Lemma fold_right_opt_map : forall {a b c : Set} (f : a -> option b)
  (g : b -> c -> c) (xs : list a) (z : c),
  fold_right
    (fun x acc =>
      match f x with
      | Some y => g y acc
      | None => acc
      end) xs z =
  fold_right g (opt_map f xs) z.
  induction xs;intros.
  - reflexivity.
  - simpl.
    destruct (f a0).
    + simpl; congruence.
    + apply IHxs.
Qed.

Lemma fold_right_opt_no_None : forall {a b c : Set} (f : a -> option b)
  (g : b -> c -> c) (xs : list a) (z z_t : c),
  (forall x, In x xs -> exists y, f x = Some y) ->
  fold_right
    (fun x acc =>
      match f x with
      | Some y => g y acc
      | None => z_t
      end) xs z =
  fold_right g (opt_map f xs) z.
  induction xs; intros.
  - reflexivity.
  - simpl.
    destruct (H a0) as [y Hy]; try (left; reflexivity).
    rewrite Hy.
    simpl; rewrite IHxs; auto.
    intros; apply H.
    right; assumption.
Qed.

Lemma fold_right_cons_nil : forall {a : Set} (xs : list a),
  fold_right cons xs [] = xs.
  induction xs; intros.
  - reflexivity.
  - simpl; congruence.
Qed.

Lemma In_to_In_fold_add : forall {a b c : Set}
  (f : a -> option b) (g : b -> c) (add : b -> list c -> list c)
  (xs : list a) (x : a) (y : b),
  (forall x xs, In (g x) (add x xs)) ->
  (forall x y xs, In (g x) xs -> In (g x) (add y xs)) ->
  In x xs -> f x = Some y ->
    In (g y) (fold_right
      (fun x' acc =>
        match f x' with
        | Some y' => add y' acc
        | None => acc
        end) xs []).
  induction xs; intros x y add_In1 add_In2 Hxxs Hfx;
  destruct Hxxs; simpl.
  - rewrite H, Hfx.
    apply add_In1.
  - destruct (f a0); [apply add_In2|idtac]; 
    apply (IHxs x); assumption.
Qed.

Lemma In_fold_add_to_In : forall {a b c : Set}
  (f : a -> option b) (g : b -> c) (add : b -> list c -> list c),
  (forall x x' xs, In (g x) (add x' xs) -> x = x' \/ In (g x) xs) ->
  (forall x x' y, f x = Some y -> f x' = Some y -> x = x') ->
  forall (xs : list a) (x : a) (y : b),
  f x = Some y ->
  In (g y) (fold_right
    (fun x' acc =>
          match f x' with
          | Some y' => add y' acc
          | None => acc
          end) xs []) -> In x xs.
  induction xs; intros.
  - exact H2.
  - destruct (f a0) eqn:G.
    + simpl in H2.
      rewrite G in H2.
      destruct (H _ _ _ H2).
      * rewrite H3 in H1; left.
        eapply H0; eassumption.
      * right; eapply IHxs; eassumption.
    + right; eapply IHxs; auto.
      * exact H1.
      * simpl in H2.
        rewrite G in H2.
        exact H2.
Qed.

Lemma hd_Some_to_cons : forall {a : Set} (x : a) xs, hd xs = Some x ->
  exists ys, xs = x :: ys.
  intros X x [|y ys] Hxs.
  - discriminate.
  - exists ys; auto.
    inversion Hxs; reflexivity.
Qed.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Option.

Module Valid.
  (** We should be more precise by also talking about the JSON encoding, but
      this is enough for now. *)
  Record t {a : Set} {domain : a -> Prop} {encoding : Data_encoding.t a}
    : Prop := {
    of_bytes_opt_to_bytes_opt value :
      domain value ->
      match Data_encoding.Binary.to_bytes_opt None encoding value with
      | Some bytes =>
        Data_encoding.Binary.of_bytes_opt encoding bytes = Some value
      | None => False
      end;
    to_bytes_opt_of_bytes_opt bytes :
      match Data_encoding.Binary.of_bytes_opt encoding bytes with
      | Some value =>
        Data_encoding.Binary.to_bytes_opt None encoding value = Some bytes
      | None => True
      end;
  }.
  Arguments t {_}.

  Lemma implies {a : Set} {domain domain' : a -> Prop}
    {encoding : Data_encoding.t a}
    : t domain encoding ->
      (forall x, domain' x -> domain x) ->
      t domain' encoding.
    intros H_encoding H_implies; constructor; intros;
    now apply H_encoding; apply H_implies.
  Qed.

  (** Since we do not specify the JSON encoding, we require both of the
      arguments of [splitted] to be valid. We believe this is a good enough
      approximation for now. *)
  Axiom splitted : forall {a : Set} {domain_json domain_binary}
    {json binary : Data_encoding.t a},
    t domain_json json -> t domain_binary binary ->
    let domain x :=
      domain_json x /\ domain_binary x in
    t domain (Data_encoding.splitted json binary).

  Axiom option_value : forall {a : Set} {domain : a -> Prop}
    {encoding : Data_encoding.t a},
    t domain encoding ->
    let option_domain x :=
      match x with
      | None => True
      | Some x => domain x
      end in
    t option_domain (Data_encoding.option_value encoding).

  Axiom string_enum : forall {a : Set} (enum : list (string * a)),
    List.all_distinct_strings (List.map fst enum) = true ->
    let domain x :=
      List.In x (List.map snd enum) in
    t domain (Data_encoding.string_enum enum).

  Lemma string_enum_dec {a : Set} (enum : list (string * a))
    {eq_dec : a -> a -> bool}
    (H_eq_dec : forall x y, eq_dec x y = true -> x = y)
    : List.all_distinct_strings (List.map fst enum) = true ->
      let domain x :=
        List.mem eq_dec x (List.map snd enum) = true in
      t domain (Data_encoding.string_enum enum).
    intro H.
    eapply implies.
    now apply string_enum.
    intros; simpl.
    now apply (List.mem_eq_dec H_eq_dec).
  Qed.

  Module Fixed.
    Axiom string_value : forall size,
      let domain s := String.length s = size in
      t domain (Data_encoding.Fixed.string_value size).

    Axiom bytes_value : forall size,
      let domain s := Bytes.length s = size in
      t domain (Data_encoding.Fixed.bytes_value size).

    Axiom add_padding : forall {a : Set} {domain} {encoding : Data_encoding.t a} {size : int},
      t domain encoding ->
      t domain (Data_encoding.Fixed.add_padding encoding size).
  End Fixed.

  Module _Variable.
    Axiom string_value :
      t (fun _ => True) (Data_encoding._Variable.string_value).

    Axiom bytes_value :
      t (fun _ => True) (Data_encoding._Variable.bytes_value).

    Axiom array : forall {a : Set} {domain} {encoding : Data_encoding.t a},
      t domain encoding ->
      t (List.Forall domain) (Data_encoding._Variable.array None encoding).

    Axiom list_value : forall {a : Set} {domain} {encoding : Data_encoding.t a},
      t domain encoding ->
      t (List.Forall domain) (Data_encoding._Variable.list_value None encoding).
  End _Variable.

  Module Bounded.
    Axiom string_value : forall size,
      let domain s := String.length s <= size in
      t domain (Data_encoding.Bounded.string_value size).

    Axiom bytes_value : forall size,
      let domain s := Bytes.length s <= size in
      t domain (Data_encoding.Bounded.bytes_value size).
  End Bounded.

  Axiom dynamic_size : forall {a : Set} {domain : a -> Prop} {encoding},
    t domain encoding ->
    t domain (Data_encoding.dynamic_size None encoding).
  
  Axiom json_value : t (fun _ => True) Data_encoding.json_value.

  Axiom json_schema_value : t (fun _ => True) Data_encoding.json_schema_value.

  Axiom merge_objs : forall {o1 o2 : Set} 
    {domain1 domain2}
    {encoding1 : Data_encoding.t o1}
    {encoding2 : Data_encoding.t o2},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    let domain '(x1, x2) := domain1 x1 /\ domain2 x2 in
    t domain (Data_encoding.merge_objs encoding1 encoding2).

  Module Field.
    (** The validity of a [Data_encoding.field] on a certain domain. *)
    Inductive t (name : string) :
      forall {a : Set}, (a -> Prop) -> Data_encoding.field a -> Prop :=
    | Req :
      forall {a : Set} title description (domain_a : a -> Prop) encoding_a,
      Valid.t domain_a encoding_a ->
      t _ domain_a (Data_encoding.req title description name encoding_a)
    | Opt :
      forall {a : Set} title description (domain_a : a -> Prop) encoding_a,
      Valid.t domain_a encoding_a ->
      let domain x :=
        match x with
        | Some v => domain_a v
        | None => True
        end in
      t _ domain (Data_encoding.opt title description name encoding_a)
    | Varopt :
      forall {a : Set} title description (domain_a : a -> Prop) encoding_a,
      Valid.t domain_a encoding_a ->
      let domain x :=
        match x with
        | Some v => domain_a v
        | None => True
        end in
      t _ domain (Data_encoding.varopt title description name encoding_a)
    | Dft :
      forall {a : Set} title description (domain_a : a -> Prop) encoding_a
        default,
      Valid.t domain_a encoding_a ->
      t _ domain_a (Data_encoding.dft title description name encoding_a default).
  End Field.

  (* @TODO proper verify the domain for lazy_encoding. This can be done
   * once we verify data_encoding library. *)
  Axiom lazy_encoding : forall {a : Set} {domain : a -> Prop} {encoding : Data_encoding.t a},
    t domain encoding ->
    t (fun _ => True) (Data_encoding.lazy_encoding encoding).

  Axiom obj1 : forall {a1 : Set}
    {name1}
    {domain1}
    {field1 : Data_encoding.field a1},
    Field.t name1 domain1 field1 ->
    t domain1 (Data_encoding.obj1 field1).

  Axiom obj2 : forall {a1 a2 : Set}
    {name1 name2}
    {domain1 domain2}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    List.all_distinct_strings
      [name1; name2] =
      true ->
    let domain '(x1, x2) :=
      domain1 x1 /\ domain2 x2 in
    t domain (Data_encoding.obj2
      field1 field2).

  Axiom obj3 : forall {a1 a2 a3 : Set}
    {name1 name2 name3}
    {domain1 domain2 domain3}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    List.all_distinct_strings
      [name1; name2; name3] =
      true ->
    let domain '(x1, x2, x3) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 in
    t domain (Data_encoding.obj3
      field1 field2 field3).

  Axiom obj4 : forall {a1 a2 a3 a4 : Set}
    {name1 name2 name3 name4}
    {domain1 domain2 domain3 domain4}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    List.all_distinct_strings
      [name1; name2; name3; name4] =
      true ->
    let domain '(x1, x2, x3, x4) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 in
    t domain (Data_encoding.obj4
      field1 field2 field3 field4).

  Axiom obj5 : forall {a1 a2 a3 a4 a5 : Set}
    {name1 name2 name3 name4 name5}
    {domain1 domain2 domain3 domain4 domain5}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4}
    {field5 : Data_encoding.field a5},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    Field.t name5 domain5 field5 ->
    List.all_distinct_strings
      [name1; name2; name3; name4; name5] =
      true ->
    let domain '(x1, x2, x3, x4, x5) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 in
    t domain (Data_encoding.obj5
      field1 field2 field3 field4 field5).

  Axiom obj6 : forall {a1 a2 a3 a4 a5 a6 : Set}
    {name1 name2 name3 name4 name5 name6}
    {domain1 domain2 domain3 domain4 domain5 domain6}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4}
    {field5 : Data_encoding.field a5}
    {field6 : Data_encoding.field a6},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    Field.t name5 domain5 field5 ->
    Field.t name6 domain6 field6 ->
    List.all_distinct_strings
      [name1; name2; name3; name4; name5; name6] =
      true ->
    let domain '(x1, x2, x3, x4, x5, x6) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 in
    t domain (Data_encoding.obj6
      field1 field2 field3 field4 field5 field6).

  Axiom obj7 : forall {a1 a2 a3 a4 a5 a6 a7 : Set}
    {name1 name2 name3 name4 name5 name6 name7}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4}
    {field5 : Data_encoding.field a5}
    {field6 : Data_encoding.field a6}
    {field7 : Data_encoding.field a7},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    Field.t name5 domain5 field5 ->
    Field.t name6 domain6 field6 ->
    Field.t name7 domain7 field7 ->
    List.all_distinct_strings
      [name1; name2; name3; name4; name5; name6; name7] =
      true ->
    let domain '(x1, x2, x3, x4, x5, x6, x7) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 in
    t domain (Data_encoding.obj7
      field1 field2 field3 field4 field5 field6 field7).

  Axiom obj8 : forall {a1 a2 a3 a4 a5 a6 a7 a8 : Set}
    {name1 name2 name3 name4 name5 name6 name7 name8}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7 domain8}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4}
    {field5 : Data_encoding.field a5}
    {field6 : Data_encoding.field a6}
    {field7 : Data_encoding.field a7}
    {field8 : Data_encoding.field a8},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    Field.t name5 domain5 field5 ->
    Field.t name6 domain6 field6 ->
    Field.t name7 domain7 field7 ->
    Field.t name8 domain8 field8 ->
    List.all_distinct_strings
      [name1; name2; name3; name4; name5; name6; name7; name8] =
      true ->
    let domain '(x1, x2, x3, x4, x5, x6, x7, x8) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 /\ domain8 x8 in
    t domain (Data_encoding.obj8
      field1 field2 field3 field4 field5 field6 field7 field8).

  Axiom obj9 : forall {a1 a2 a3 a4 a5 a6 a7 a8 a9 : Set}
    {name1 name2 name3 name4 name5 name6 name7 name8 name9}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7 domain8 domain9}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4}
    {field5 : Data_encoding.field a5}
    {field6 : Data_encoding.field a6}
    {field7 : Data_encoding.field a7}
    {field8 : Data_encoding.field a8}
    {field9 : Data_encoding.field a9},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    Field.t name5 domain5 field5 ->
    Field.t name6 domain6 field6 ->
    Field.t name7 domain7 field7 ->
    Field.t name8 domain8 field8 ->
    Field.t name9 domain9 field9 ->
    List.all_distinct_strings
      [name1; name2; name3; name4; name5; name6; name7; name8; name9] =
      true ->
    let domain '(x1, x2, x3, x4, x5, x6, x7, x8, x9) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 /\ domain8 x8 /\ domain9 x9 in
    t domain (Data_encoding.obj9
      field1 field2 field3 field4 field5 field6 field7 field8 field9).

  Axiom obj10 : forall {a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 : Set}
    {name1 name2 name3 name4 name5 name6 name7 name8 name9 name10}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7 domain8 domain9
      domain10}
    {field1 : Data_encoding.field a1}
    {field2 : Data_encoding.field a2}
    {field3 : Data_encoding.field a3}
    {field4 : Data_encoding.field a4}
    {field5 : Data_encoding.field a5}
    {field6 : Data_encoding.field a6}
    {field7 : Data_encoding.field a7}
    {field8 : Data_encoding.field a8}
    {field9 : Data_encoding.field a9}
    {field10 : Data_encoding.field a10},
    Field.t name1 domain1 field1 ->
    Field.t name2 domain2 field2 ->
    Field.t name3 domain3 field3 ->
    Field.t name4 domain4 field4 ->
    Field.t name5 domain5 field5 ->
    Field.t name6 domain6 field6 ->
    Field.t name7 domain7 field7 ->
    Field.t name8 domain8 field8 ->
    Field.t name9 domain9 field9 ->
    Field.t name10 domain10 field10 ->
    List.all_distinct_strings
      [name1; name2; name3; name4; name5; name6; name7; name8; name9; name10] =
      true ->
    let domain '(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 /\ domain8 x8 /\ domain9 x9 /\ domain10 x10 in
    t domain (Data_encoding.obj10
      field1 field2 field3 field4 field5 field6 field7 field8 field9 field10).

  Axiom tup1 : forall {a : Set}
    {domain}
    {encoding : Data_encoding.t a},
    t domain encoding ->
    t domain (Data_encoding.tup1 encoding).

  Axiom tup2 : forall {a1 a2 : Set}
    {domain1 domain2}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    let domain '(x1, x2) :=
      domain1 x1 /\ domain2 x2 in
    t domain (Data_encoding.tup2
      encoding1 encoding2).

  Axiom tup3 : forall {a1 a2 a3 : Set}
    {domain1 domain2 domain3}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    let domain '(x1, x2, x3) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 in
    t domain (Data_encoding.tup3
      encoding1 encoding2 encoding3).

  Axiom tup4 : forall {a1 a2 a3 a4 : Set}
    {domain1 domain2 domain3 domain4}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    let domain '(x1, x2, x3, x4) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 in
    t domain (Data_encoding.tup4
      encoding1 encoding2 encoding3 encoding4).

  Axiom tup5 : forall {a1 a2 a3 a4 a5 : Set}
    {domain1 domain2 domain3 domain4 domain5}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4}
    {encoding5 : Data_encoding.t a5},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    t domain5 encoding5 ->
    let domain '(x1, x2, x3, x4, x5) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 in
    t domain (Data_encoding.tup5
      encoding1 encoding2 encoding3 encoding4 encoding5).

  Axiom tup6 : forall {a1 a2 a3 a4 a5 a6 : Set}
    {domain1 domain2 domain3 domain4 domain5 domain6}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4}
    {encoding5 : Data_encoding.t a5}
    {encoding6 : Data_encoding.t a6},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    t domain5 encoding5 ->
    t domain6 encoding6 ->
    let domain '(x1, x2, x3, x4, x5, x6) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 in
    t domain (Data_encoding.tup6
      encoding1 encoding2 encoding3 encoding4 encoding5 encoding6).

  Axiom tup7 : forall {a1 a2 a3 a4 a5 a6 a7 : Set}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4}
    {encoding5 : Data_encoding.t a5}
    {encoding6 : Data_encoding.t a6}
    {encoding7 : Data_encoding.t a7},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    t domain5 encoding5 ->
    t domain6 encoding6 ->
    t domain7 encoding7 ->
    let domain '(x1, x2, x3, x4, x5, x6, x7) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 in
    t domain (Data_encoding.tup7
      encoding1 encoding2 encoding3 encoding4 encoding5 encoding6 encoding7).

  Axiom tup8 : forall {a1 a2 a3 a4 a5 a6 a7 a8 : Set}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7 domain8}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4}
    {encoding5 : Data_encoding.t a5}
    {encoding6 : Data_encoding.t a6}
    {encoding7 : Data_encoding.t a7}
    {encoding8 : Data_encoding.t a8},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    t domain5 encoding5 ->
    t domain6 encoding6 ->
    t domain7 encoding7 ->
    t domain8 encoding8 ->
    let domain '(x1, x2, x3, x4, x5, x6, x7, x8) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 /\ domain8 x8 in
    t domain (Data_encoding.tup8
      encoding1 encoding2 encoding3 encoding4 encoding5 encoding6 encoding7
      encoding8).

  Axiom tup9 : forall {a1 a2 a3 a4 a5 a6 a7 a8 a9 : Set}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7 domain8 domain9}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4}
    {encoding5 : Data_encoding.t a5}
    {encoding6 : Data_encoding.t a6}
    {encoding7 : Data_encoding.t a7}
    {encoding8 : Data_encoding.t a8}
    {encoding9 : Data_encoding.t a9},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    t domain5 encoding5 ->
    t domain6 encoding6 ->
    t domain7 encoding7 ->
    t domain8 encoding8 ->
    t domain9 encoding9 ->
    let domain '(x1, x2, x3, x4, x5, x6, x7, x8, x9) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 /\ domain8 x8 /\ domain9 x9 in
    t domain (Data_encoding.tup9
      encoding1 encoding2 encoding3 encoding4 encoding5 encoding6 encoding7
      encoding8 encoding9).

  Axiom tup10 : forall {a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 : Set}
    {domain1 domain2 domain3 domain4 domain5 domain6 domain7 domain8 domain9
      domain10}
    {encoding1 : Data_encoding.t a1}
    {encoding2 : Data_encoding.t a2}
    {encoding3 : Data_encoding.t a3}
    {encoding4 : Data_encoding.t a4}
    {encoding5 : Data_encoding.t a5}
    {encoding6 : Data_encoding.t a6}
    {encoding7 : Data_encoding.t a7}
    {encoding8 : Data_encoding.t a8}
    {encoding9 : Data_encoding.t a9}
    {encoding10 : Data_encoding.t a10},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    t domain4 encoding4 ->
    t domain5 encoding5 ->
    t domain6 encoding6 ->
    t domain7 encoding7 ->
    t domain8 encoding8 ->
    t domain9 encoding9 ->
    t domain10 encoding10 ->
    let domain '(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) :=
      domain1 x1 /\ domain2 x2 /\ domain3 x3 /\ domain4 x4 /\ domain5 x5 /\
      domain6 x6 /\ domain7 x7 /\ domain8 x8 /\ domain9 x9 /\ domain10 x10 in
    t domain (Data_encoding.tup10
      encoding1 encoding2 encoding3 encoding4 encoding5 encoding6 encoding7
      encoding8 encoding9 encoding10).

  Axiom list_value : forall {a : Set} {domain_a}
    {encoding_a : Data_encoding.t a},
    t domain_a encoding_a ->
    t (List.Forall domain_a) (Data_encoding.list_value None encoding_a).

  Axiom assoc : forall {a : Set} {domain : a -> Prop} {encoding : Data_encoding.t a},
    t domain encoding ->
    t (List.Forall (fun p => domain (snd p))) (Data_encoding.assoc encoding).

  (** We define the domain and validity on cases as an inductive rather than a
      function. This is because a case contains an existential set, and we
      cannot match on existential sets in [Prop] with impredicative sets. *)
  Module Cases.
    Inductive t {u : Set}
      (excluding_titles : list string) (excluding_tags : list int)
      : (u -> Prop) -> list (Data_encoding.case u) -> Prop :=
    | Nil : t _ _ (fun '_ => False) []
    | Cons :
      forall {a : Set} {domain_a : a -> Prop},
      forall (title : string) (description : option string) (tag : int)
        (encoding : Data_encoding.t a) (proj : u -> option a) (inj : a -> u)
        domain cases,
      ~ List.In title excluding_titles ->
      ~ List.In tag excluding_tags ->
      t (title :: excluding_titles) (tag :: excluding_tags) domain cases ->
      Valid.t domain_a encoding ->
      let domain x :=
        match proj x with
        | None => domain x
        | Some y => domain_a y /\ inj y = x
        end in
      let case :=
        Data_encoding.case_value
          title description (Data_encoding.Tag tag) encoding proj inj in
      t _ _ domain (case :: cases).
  End Cases.

  Axiom union : forall {u : Set} {o : option tag_size} {domain} {cases : list (case u)},
    Cases.t [] [] domain cases ->
    t domain (Data_encoding.union o cases).

  Axiom def : forall {a : Set} {domain_a : a -> Prop}
    (id : string) (title description : option string)
    {encoding_a : Data_encoding.t a},
    t domain_a encoding_a ->
    t domain_a (Data_encoding.def id title description encoding_a).

  Axiom check_size : forall {a : Set} {domain} {encoding : Data_encoding.t a}
    {size : int},
    t domain encoding ->
    t domain (Data_encoding.check_size size encoding).

  Axiom conv : forall {a b : Set} {domain_b : b -> Prop}
    {a_to_b : a -> b} {b_to_a} {encoding_b : Data_encoding.t b},
    t domain_b encoding_b ->
    t
      (fun v_a => domain_b (a_to_b v_a) /\ b_to_a (a_to_b v_a) = v_a)
      (Data_encoding.conv a_to_b b_to_a None encoding_b).

  Axiom conv_with_guard : forall {a b : Set} {domain_b : b -> Prop}
    {a_to_b : a -> b} {b_to_a} {encoding_b : Data_encoding.t b},
    t domain_b encoding_b ->
    t
      (fun v_a =>
        domain_b (a_to_b v_a) /\ b_to_a (a_to_b v_a) = Pervasives.Ok v_a)
      (Data_encoding.conv_with_guard a_to_b b_to_a None encoding_b).

  Axiom with_decoding_guard : forall {a : Set} {domain}
    {guard : a -> Pervasives.result unit string} {encoding : Data_encoding.t a},
    t domain encoding ->
    let domain v :=
      domain v /\
      match guard v with
      | Pervasives.Ok _ => True
      | Pervasives.Error _ => False
      end in
    t domain (Data_encoding.with_decoding_guard guard encoding).

  Axiom null : t (fun _ => True) Data_encoding.null.
  Axiom empty : t (fun _ => True) Data_encoding.empty.
  Axiom unit_value : t (fun _ => True) Data_encoding.unit_value.
  Axiom constant : forall s, t (fun _ => True) (Data_encoding.constant s).
  Axiom int8 : t Pervasives.Int8.Valid.t Data_encoding.int8.
  Axiom uint8 : t Pervasives.UInt8.Valid.t Data_encoding.uint8.
  Axiom int16 : t Pervasives.Int16.Valid.t Data_encoding.int16.
  Axiom uint16 : t Pervasives.UInt16.Valid.t Data_encoding.uint16.
  Axiom int31 : t Pervasives.Int31.Valid.t Data_encoding.int31.
  Axiom int32_value : t (fun _ => True) Data_encoding.int32_value.
  Axiom int64_value : t (fun _ => True) Data_encoding.int64_value.
  Axiom n_value : t (fun _ => True) Data_encoding.n_value.
  Axiom z_value : t (fun _ => True) Data_encoding.z_value.
  Axiom bool_value : t (fun _ => True) Data_encoding.bool_value.
  Axiom string_value : t (fun _ => True) Data_encoding.string_value.
  Axiom bytes_value : t (fun _ => True) Data_encoding.bytes_value.

  Create HintDb Data_encoding_db.

  (* Hints for primitives *)
  #[global] Hint Resolve null
    null
    empty
    unit_value
    constant
    int8
    uint8
    int16
    uint16
    int31
    int32_value
    int64_value
    n_value
    z_value
    bool_value
    string_value
    bool_value
    bytes_value
    json_value
    json_schema_value
    _Variable.string_value
    _Variable.bytes_value
    : Data_encoding_db.

  Ltac data_encoding_auto :=
    (* We try to solve the goal using the hints first *)
    (try now eauto 1 with Data_encoding_db);

    (* If this doens't help we start by unfolding the 
     * definition of the encoding we verify *)
    match goal with
    | [|- t _ ?encoding] => try unfold encoding
    end;
    eapply implies;
    repeat (
      match goal with
      (* Encodings as combinators, with at least one function application *)
      | [|- t _ (_ _)] =>
        first [
          eapply splitted |
          eapply union |
          eapply option_value |
          eapply string_enum |
          eapply string_value |
          eapply bytes_value |
          eapply obj1 |
          eapply obj2 |
          eapply obj3 |
          eapply obj4 |
          eapply obj5 |
          eapply obj6 |
          eapply obj7 |
          eapply obj8 |
          eapply obj9 |
          eapply obj10 |
          eapply tup1 |
          eapply tup2 |
          eapply tup3 |
          eapply tup4 |
          eapply tup5 |
          eapply tup6 |
          eapply tup7 |
          eapply tup8 |
          eapply tup9 |
          eapply tup10 |
          eapply list_value |
          eapply def |
          eapply conv |
          eapply conv_with_guard |
          eapply with_decoding_guard |
          eapply check_size |
          eapply Fixed.string_value |
          eapply Fixed.bytes_value |
          eapply Fixed.add_padding |
          eapply _Variable.array |
          eapply _Variable.list_value |
          eapply Bounded.string_value |
          eapply Bounded.bytes_value |
          eapply dynamic_size |
          eapply merge_objs |
          eapply lazy_encoding |
          eapply assoc
        ]
      (* Special cases defined as inductive *)
      | [|- Field.t _ _ _] => constructor
      | [|- Cases.t _ _ _ _] =>
        constructor;
        (* Solve the [In] conditions *)
        try match goal with
        | |- ~ In _ _ =>
          (* The evaluation should be almost instantaneous here given our
             practical cases but we still protect it with a timeout. *)
          (timeout 1 cbv);
          intuition congruence
        end
      (* Finally try the hints *)
      | _ => solve [eauto 1 with Data_encoding_db]
      end
    ); simpl; trivial; try tauto; try dtauto;
      try (intros []; tauto); try (intros []; dtauto).
End Valid.

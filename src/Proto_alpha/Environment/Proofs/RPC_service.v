Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Valid.
  Record t {prefix params query input output : Set}
    (domain_input : input -> Prop) (domain_output : output -> Prop)
    (service : RPC_service.t prefix params query input output) : Prop := {
    input_encoding :
      Data_encoding.Valid.t domain_input service.(RPC_service.t.input_encoding);
    output_encoding :
      Data_encoding.Valid.t domain_output service.(RPC_service.t.output_encoding);
  }.
End Valid.

Ltac rpc_auto :=
  match goal with
    [|- RPC_service.Valid.t _ _ ?x] => try unfold x
  end;
  constructor; Data_encoding.Valid.data_encoding_auto.

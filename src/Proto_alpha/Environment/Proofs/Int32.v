Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Proofs.Utils.

Module Valid.
  Definition t (n : int32) : Prop :=
    Int32.min_int <= n <= Int32.max_int.
End Valid.

Lemma normalize_is_valid (n : Z.t) : Valid.t (Pervasives.normalize_int32 n).
  unfold Valid.t; Utils.tezos_z_auto.
Qed.

Lemma normalize_identity (n : Z.t) :
  Valid.t n -> Pervasives.normalize_int32 n = n.
  unfold Valid.t; Utils.tezos_z_auto.
Qed.

Axiom of_string_opt_to_string : forall i,
  Int32.of_string_opt (Int32.to_string i) = Some i.

Axiom to_string_of_string_opt : forall s,
  match Int32.of_string_opt s with
  | Some i => Int32.to_string i = s
  | None => True
  end.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Module Valid.
  Record t {a : Set} {domain : a -> Prop} {arg : RPC_arg.t a} : Prop := {
    destruct_construct v :
      domain v ->
      arg.(RPC_arg.t.destruct) (arg.(RPC_arg.t.construct) v) =
      Pervasives.Ok v;
    construct_destruct s :
      match arg.(RPC_arg.t.destruct) s with
      | Pervasives.Ok v => arg.(RPC_arg.t.construct) v = s
      | Pervasives.Error _ => True
      end;
  }.
  Arguments t {_}.

  Lemma implies {a : Set} {domain domain' : a -> Prop} {arg : RPC_arg.t a}
    : t domain arg ->
      (forall x, domain' x -> domain x) ->
      t domain' arg.
    intros H H_implies; constructor; intros; apply H; now try apply H_implies.
  Qed.

  Axiom bool_value : t (fun _ => True) RPC_arg.bool_value.
  Axiom int_value : t (fun _ => True) RPC_arg.int_value.
  Axiom uint : t (fun i => i >=i 0 = true) RPC_arg.uint.
  Axiom int32_value : t (fun _ => True) RPC_arg.int32_value.
  Axiom uint31 : t (fun i => i >=i32 0 = true) RPC_arg.uint31.
  Axiom int64_value : t (fun _ => True) RPC_arg.int64_value.
  Axiom uint63 : t (fun i => i >=i64 0 = true) RPC_arg.uint63.
  Axiom string_value : t (fun _ => True) RPC_arg.string_value.

  Axiom like : forall {a : Set} {domain} {arg : RPC_arg.t a} {description name},
    t domain arg -> t domain (RPC_arg.like arg description name).
End Valid.

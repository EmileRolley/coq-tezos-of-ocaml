# Import the protocol using `coq-of-ocaml`
require 'parallel'

if ARGV.size < 3 then
  puts "Usage:"
  puts "  ruby import.rb tezos_path protocol_path target_path"
  exit(1)
end

tezos_path, protocol_path, target_path = ARGV
full_protocol_path = File.join(tezos_path, protocol_path)

# Environment
ml_path = File.join(tezos_path, "_build", "default", "src", "lib_protocol_environment", "sigs", "v4.ml")
ml_content = File.read(ml_path, encoding: "UTF-8")
mli_content = ml_content.split("\n")[1..-2].select {|line| line[0] != "#"}.join("\n")
File.open("environment.mli", "w") do |file|
  file << mli_content
end
command = "coq-of-ocaml -config config-environment.json environment.mli"
system(command)

# Protocol
mli_files = [
  "script_ir_translator.mli"
]
protocol_files =
  Dir.glob(File.join(full_protocol_path, "*.ml")) +
  mli_files.map {|path| File.join(full_protocol_path, path)}
Parallel.each(protocol_files.sort) do |ocaml_file_name|
  command = "cd #{full_protocol_path} && coq-of-ocaml -config #{File.expand_path("config.json")} #{File.basename(ocaml_file_name)} "
  system(command) if command.include?("")
end

# Patching
system("ruby", "patch.rb", target_path, full_protocol_path)

# Copying
system("rsync --checksum #{full_protocol_path}/*.v #{target_path}")
system("rm #{full_protocol_path}/*.v")

# Renaming the environment
system("rsync --checksum Environment_mli.v #{File.join(target_path, "Environment.v")}")
system("rm Environment_mli.v")

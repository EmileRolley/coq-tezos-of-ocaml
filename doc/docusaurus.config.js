const baseUrl = '/coq-tezos-of-ocaml/';

module.exports = {
  title: 'Coq Tezos of OCaml 💫',
  tagline: 'A translation & verification in Coq of the code of the economic protocol of Tezos',
  url: 'https://nomadic-labs.gitlab.io',
  baseUrl,
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/rooster-tezos.png',
  organizationName: 'nomadiclabs', // Usually your GitHub org/user name.
  projectName: 'coq-tezos-of-ocaml', // Usually your repo name.
  themeConfig: {
    announcementBar: {
      id: 'support_us',
      content:
        'To add proofs on the protocol, see <a href="/coq-tezos-of-ocaml/docs/contribute">Contribute</a> 🏇',
      backgroundColor: '#fafbfc',
      textColor: '#091E42',
      isCloseable: false,
    },
    colorMode: {
      // Hides the switch in the navbar
      // Useful if you want to support a single color mode
      disableSwitch: true,
    },
    prism: {
      additionalLanguages: ['ocaml'],
    },
    algolia: {
      apiKey: '3fe0c50bf60c12ee19ed2c883f61f526',
      indexName: 'coq-tezos',
      contextualSearch: false,
    },
    navbar: {
      hideOnScroll: true,
      title: 'Coq Tezos of OCaml 💫',
      logo: {
        alt: 'Logo',
        src: 'img/rooster-tezos.png',
      },
      items: [
        {
          to: 'docs/readme/',
          activeBasePath: 'docs/readme',
          label: 'Protocol',
          position: 'left',
        },
        {
          to: 'docs/environment/readme/',
          activeBasePath: 'docs/environment/readme',
          label: 'Environment',
          position: 'left',
        },
        {
          to: 'docs/proofs/readme/',
          activeBasePath: 'docs/proofs/readme',
          label: 'Proofs',
          position: 'left',
        },
        {
          to: 'blog',
          label: 'Blog',
          position: 'left',
        },
        // {
        //   label: 'Securing money',
        //   className: 'slogan_src-pages-',
        //   position: 'right',
        // },
        {
          href: 'https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Formalization',
          items: [
            {
              label: 'Protocol',
              to: 'docs/readme/',
            },
            {
              label: 'Environment',
              to: 'docs/environment/readme/',
            },
            {
              label: 'Proofs',
              to: 'docs/proofs/readme/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              html: `
              <a
                class="footer__link-item"
                href="https://tezos.com/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img
                  alt="logo"
                  height="24px"
                  src="${baseUrl}/img/white_logo/tezos.png"
                  style="vertical-align: middle;"
                />
                Tezos
              </a>
              `,
            },
            {
              html: `
              <a
                class="footer__link-item"
                href="https://www.nomadic-labs.com/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img
                  alt="logo"
                  height="24px"
                  src="${baseUrl}/img/white_logo/nomadic_labs.png"
                  style="vertical-align: middle;"
                />
                Nomadic Labs
              </a>
              `,
            },
            {
              html: `
              <a
                class="footer__link-item"
                href="https://foobar.land/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <img
                  alt="logo"
                  height="24px"
                  src="${baseUrl}/img/white_logo/foobar_land_new.png"
                  style="vertical-align: middle;"
                />
                Foobar.land
              </a>
              `,
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} <a class="footer__link-item" href="https://www.nomadic-labs.com/" target="_blank">Nomadic Labs</a>`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
